import React from 'react';
import { View, Text, Image } from 'react-native';

import { DefaultButton } from '../../components';

let getStatusTitle = text => {
  switch (text) {
    case 'CONFIRMADO':
      return 'Realizar pagamento';
    case 'PAGO':
      return 'Confirmada';
    case 'Aguardando pagamento':
      return 'Aguardando pagamento';
    case 'MAOS':
      return 'Pagamento em mãos';
    case 'CANCELADO':
      return 'Cancelada';
    case 'PENDENTE':
      return 'Aguardando confirmação';
    case 'ESTORNADA':
      return 'Estornada';
    case 'CONCLUIDO':
      return 'Concluída';
    case 'DISPONIVEL':
      return 'Iniciar aula';

    default:
      return 'Responder';
  }
};

let getStatusPayment = text => {
  switch (text) {
    case 'CARTAO':
      return 'Cartão de crédito';
    case 'PENDENTE':
      return 'Pendente';
    case 'MAOS':
      return 'Em mãos';
    case 'BOLETO':
      return 'Boleto';
    default:
      return 'Não identificado';
  }
};

let getStatusBG = text => {
  switch (text) {
    case 'DISPONIVEL':
      return '#F76E1E';
    case 'disponivel':
      return '#fff';
    default:
      return '#E9E9E9';
  }
};

let getStatusColor = text => {
  switch (text) {
    case 'CONFIRMADO':
      return '#049526';
    case 'PAGO':
      return '#049526';
    case 'MAOS':
      return '#049526';
    case 'Aguardando pagamento':
      return '#F76E1E';
    case 'CANCELADO':
      return '#D22020';
    case 'PENDENTE':
      return '#707070';
    case 'ESTORNADA':
      return '#F76E1E';
    case 'CONCLUIDO':
      return '#37153D';
    default:
      return '';
  }
};

const ClassItem = ({
  date,
  gym,
  personal,
  onPress,
  status,
  plan,
  avatar,
  methodPayment,
}) => {
  return (
    <>
      <View
        style={{
          flexDirection: 'column',
          alignItems: 'center',
          paddingTop: 20,
          paddingBottom: 10,
          margin: 10,
          borderRadius: 10,
          elevation: 1,
          backgroundColor: '#fff',
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            width: 320,
          }}>
          <Image
            style={{ height: 120, width: 120, borderRadius: 135 }}
            source={avatar}
          />
          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'space-evenly',
              height: 100,
            }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Data/hora:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {date}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Local:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {gym}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Personal:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {personal}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Pacote:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {plan}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Pagamento:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {getStatusPayment(methodPayment ?? 'PENDENTE')}
              </Text>
            </View>
          </View>
        </View>

        <View
          style={{
            borderTopWidth: 1,
            borderTopColor: 'rgba(112, 112, 112, 0.1)',
            padding: 5,
            width: '100%',
            alignItems: 'center',
            marginTop: 15,
          }}>
          <DefaultButton
            title={getStatusTitle(status)}
            onPress={onPress}
            backgroundColor={getStatusBG(status)}
            fontColor={getStatusColor(status)}
            width={250}
            height={35}
          />
        </View>
      </View>
    </>
  );
};

export default ClassItem;
