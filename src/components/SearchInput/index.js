import React from 'react';
import { View, TouchableWithoutFeedback } from 'react-native';

import { TextInput, InputWrapper } from './styles';
import { Touchable } from '../../components';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';

const SearchInput = ({
  iconName,
  iconColor,
  iconSize,
  onChangeText,
  value,
  filterAction,
  placeholder,
  placeholderTextColor,
  secureTextEntry,
  onFocus,
  onBlur,
  editable,
  pointerEvents,
  onTouchStart,
  onFilterButtonClick,
  hideFilter,
}) => {
  return (
    <InputWrapper>
      <Ionicon
        style={{
          fontWeight: '900',
        }}
        name={'ios-search'}
        size={23}
        color={'#2e2e2e'}
      />

      <TextInput
        width={200}
        height={40}
        textAlign={'center'}
        onFocus={onFocus}
        onBlur={onBlur}
        placeholder={placeholder}
        placeholderTextColor={'#636363'}
        onChangeText={onChangeText}
        value={value}
        secureTextEntry={secureTextEntry}
        editable={editable}
        pointerEvents={pointerEvents}
        onTouchStart={onTouchStart}
      />

      <Touchable onPress={filterAction}>
        <Ionicon
          style={{
            fontWeight: '900',
          }}
          name={'md-funnel'}
          size={21}
          color={'#2e2e2e'}
        />
      </Touchable>
    </InputWrapper>
  );
};

SearchInput.defaultProps = {
  iconName: 'edit-2',
  // iconColor: '#636363',
  iconSize: 14,
  placeholder: 'Filtrar por nome',
  // placeholderTextColor: '#636363',
  onChangeText: () => {},
  onFocus: () => {},
  onBlur: () => {},
  value: '',
};

export default SearchInput;
