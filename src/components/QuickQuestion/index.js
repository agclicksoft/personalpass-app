import React from 'react';
import { View, Text, TextInput, TouchableWithoutFeedback } from 'react-native';

const QuickQuestion = ({ question, width, answer, onPress }) => {
  return (
    <View style={{ flexDirection: 'column', marginLeft: 20, marginRight: 20 }}>
      <Text
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaPro-Medium',
          textAlign: 'left',
          margin: 10,
          fontSize: 13,
        }}>
        {question}
      </Text>

      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <TouchableWithoutFeedback onPress={answer != 'Sim' ? onPress : null}>
          <View
            style={{
              backgroundColor: answer == 'Sim' ? '#37153D' : '#FFFFFF',
              height: 24,
              width: 24,
              borderRadius: 20,
            }}
          />
        </TouchableWithoutFeedback>

        <Text
          style={{
            color: '#2E2E2E',
            fontFamily: 'SofiaPro-Medium',
            margin: 10,
            fontSize: 13,
            borderWidth: 0.1,
          }}>
          Sim
        </Text>

        <View style={{ width: 30 }} />

        <TouchableWithoutFeedback onPress={answer != 'Não' ? onPress : null}>
          <View
            style={{
              backgroundColor: answer == 'Não' ? '#37153D' : '#FFFFFF',
              height: 24,
              width: 24,
              borderRadius: 20,
            }}
          />
        </TouchableWithoutFeedback>

        <Text
          style={{
            color: '#2E2E2E',
            fontFamily: 'SofiaPro-Medium',
            margin: 10,
            fontSize: 13,
            borderWidth: 0.1,
          }}>
          Não
        </Text>
      </View>
    </View>
  );
};

QuickQuestion.defaultProps = {
  width: 150,
  answer: 'Sim',
};

export default QuickQuestion;
