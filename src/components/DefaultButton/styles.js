import styled from 'styled-components/native'

export const CustomTouchableOpacity = styled.TouchableOpacity`
  border-radius: 20px;
  width: ${props => (props.width ? props.width : '100%')};
  height: ${props => (props.height ? props.height : '40')};
  background: ${props =>
    props.backgroundColor ? props.backgroundColor : '#37153D'};
  opacity: ${props => props.opacity ? props.opacity : '1'};
  justify-content: center;
  padding: 8px;
  margin: ${props =>
    props.margin ? props.margin : '10px'};
  align-items: center;
`
export const ButtonText = styled.Text`
  text-align: center;
  font-family: SofiaPro-Medium;
  letter-spacing: 0;
  color: ${props => (props.fontColor ? props.fontColor : '#fff')};
  opacity: 1;
  font-size: ${props => (props.fontSize ? props.fontSize : 15)};
`
