import React, { useState, useEffect } from 'react';
import { ButtonText, CustomTouchableOpacity } from './styles';
import PropTypes from 'prop-types';

export default function DefaultButton({
  title,
  backgroundColor,
  textColor,
  shape,
  margin,
  width,
  height,
  onPress,
  fontColor,
  fontSize,
  opacity,
}) {
  return (
    <CustomTouchableOpacity
      backgroundColor={backgroundColor}
      width={width}
      height={height}
      opacity={opacity}
      margin={margin}
      onPress={onPress}>
      <ButtonText fontColor={fontColor} fontSize={fontSize}>
        {title}
      </ButtonText>
    </CustomTouchableOpacity>
  );
}
