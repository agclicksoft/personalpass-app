import React from 'react';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

navigator.geolocation = require('react-native-geolocation-service');

const GoogleSearch = () => {
  return (
    <GooglePlacesAutocomplete
      placeholder="Pesquisar endereço"
      fetchDetails={true}
      onPress={(data, details = null) => {
        // 'details' is provided when fetchDetails = true
        console.log(data);
        console.log(details);
      }}
      query={{
        key: 'AIzaSyAj-X5gMkLO-x2UvebX5Dp_iR90MK0JjSI',
        language: 'pt-BR',
        components: 'country:br',
      }}
      currentLocation={true}
      currentLocationLabel="Localização atual"
      filterReverseGeocodingByTypes={[
        'locality',
        'administrative_area_level_3',
      ]} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
      styles={{
        textInputContainer: {
          backgroundColor: 'rgba(0,0,0,0)',
          borderTopWidth: 0,
          borderBottomWidth: 0,
          maxWidth: 300,
          minWidth: 300,
        },
        textInput: {
          marginLeft: 0,
          marginRight: 0,
          color: '#636363',
          fontFamily: 'SofiaProLight',
          fontSize: 13,
          backgroundColor: '#fff',
          height: 40,
          borderRadius: 50,
          borderWidth: 0.1,
          paddingLeft: 30,
          paddingRight: 30,
        },
        listView: {
          maxWidth: 300,
          marginTop: 10,
        },
        powered: {
          height: 0,
        },
        poweredContainer: {
          backgroundColor: 'rgba(0,0,0,0)',
        },
      }}
    />
  );
};

export default GoogleSearch;
