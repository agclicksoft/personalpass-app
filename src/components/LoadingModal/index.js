import React, {Component} from 'react'
import {Text, View, Modal, StyleSheet} from 'react-native'
import {MaterialIndicator} from 'react-native-indicators'

class LoadingModal extends Component {
  static defaultProps = {
    show: false,
  }

  render() {
    return (
      <Modal
        animationType="none"
        transparent={true}
        onRequestClose={() => {}}
        visible={this.props.show}>
        <View style={styles.container}>
          <MaterialIndicator color={'#37153D'} size={48} />
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    height: 60,
    width: 60,
    backgroundColor: 'white',
    justifyContent: 'center',
    borderRadius: 50,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.7,
    elevation: 10,
  },
})

export default LoadingModal
