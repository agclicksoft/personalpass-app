import React from 'react';
import { View, Text, TextInput } from 'react-native';

const PARQQuestion = ({
  question,
  answer,
  width,
  onPress,
  keyboardType,
  maxLength,
  onChangeText,
}) => {
  return (
    <View style={{ flexDirection: 'column', marginLeft: 20, marginRight: 20 }}>
      <Text
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaPro-Medium',
          margin: 10,
          fontSize: 13,
        }}>
        {question}
      </Text>
      <TextInput
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaProLight',
          fontSize: 13,
          backgroundColor: '#fff',
          height: 40,
          borderRadius: 50,
          borderWidth: 0.1,
          paddingLeft: 20,
          flex: 1,
        }}
        value={answer}
        maxLength={maxLength}
        keyboardType={keyboardType}
        onChangeText={onChangeText}
      />
    </View>
  );
};

PARQQuestion.defaultProps = {
  width: 150,
  keyboardType: 'default',
  maxLength: 300,
};

export default PARQQuestion;
