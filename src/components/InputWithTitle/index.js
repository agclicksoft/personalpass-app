import React, { Component } from 'react';
import { View, Button, Text, ScrollView, Modal, TextInput, Image, TouchableWithoutFeedback } from 'react-native';

const InputWithTitle = ({ question, answer, width, onPress, keyboardType, maxLength, onChangeText }) => {

    return (

        <View style={{ flexDirection: 'column', marginLeft: 10, marginRight: 10 }}>
            <Text style={{ color: '#2E2E2E', fontFamily: 'SofiaPro-Medium', margin: 10, fontSize: 13 }}>{question}</Text>
            <TextInput style={{
                color: '#2E2E2E',
                fontFamily: 'SofiaProLight',
                fontSize: 13,
                backgroundColor: '#fff',
                height: 40,
                borderRadius: 50,
                borderWidth: 0.1,
                paddingLeft: 30,
                paddingRight: 30,
                width: width ? width : 100

            }}
                value={answer}
                maxLength={maxLength}
                keyboardType={keyboardType}
                onChangeText={onChangeText}

            >
            </TextInput>
        </View>


    )

}

export default InputWithTitle