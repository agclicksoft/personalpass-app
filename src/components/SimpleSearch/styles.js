import styled from 'styled-components/native'

export const InputWrapper = styled.View`
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  padding-left: 19px;
  padding-right: 12px;
  border-width: 0.1;
  width: 88%;
  margin: 10px;
  border-color: #707070;
  border-radius: 50;
  background-color: #fff;
`
export const TextInput = styled.TextInput`
  text-align: left;
`
