import React from 'react'
import {TouchableOpacity} from 'react-native'

const Touchable = ({children, onPress, activeOpacity, style}) => {
  return (
    <TouchableOpacity
      activeOpacity={activeOpacity}
      style={style}
      onPress={onPress}>
      {children}
    </TouchableOpacity>
  )
}

export default Touchable
