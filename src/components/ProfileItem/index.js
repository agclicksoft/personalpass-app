import React, { Component } from 'react';
import {
  View,
  Button,
  Text,
  ScrollView,
  Modal,
  TextInput,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';

import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import publicProfile from '../../assets/images/perfil-publico.png';

const ProfileItem = ({ name, description, icon, onPress }) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          backgroundColor: '#fafafa',
          borderBottomWidth: 0.3,
          borderBottomColor: '#00000030',
          padding: 30,
        }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Image
            source={icon}
            style={{ width: 30, height: 30 }}
            resizeMode="contain"
          />
          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              paddingLeft: 15,
              flexShrink: 1,
            }}>
            <Text
              style={{
                fontFamily: 'SofiaPro-Medium',
                fontSize: 16,
                flexWrap: 'wrap',
                color: '#37153D',
              }}>
              {name}
            </Text>
            <Text
              style={{
                fontFamily: 'SofiaProLight',
                fontSize: 11,
                color: '#707070',
              }}>
              {description}
            </Text>
          </View>
        </View>
        <Ionicon
          style={{
            fontWeight: '900',
            color: 'rgba(112,112,112,0.5)',
            transform: [{ rotate: '180deg' }],
          }}
          name={'ios-arrow-back'}
          size={21}
          color={'red'}
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

export default ProfileItem;
