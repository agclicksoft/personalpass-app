import React, { Component } from 'react';
import {
  View,
  Button,
  Text,
  ScrollView,
  Modal,
  TextInput,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';

import { Picker } from '@react-native-community/picker';

const Dropdown = ({
  title,
  selectedValue,
  onValueChange,
  options,
  colorTitle,
}) => {
  return (
    <>
      <View
        style={{
          flexDirection: 'column',
          alignItems: 'center',
          width: '100%',
          marginBottom: 10,
          padding: 0,
        }}>
        <View
          style={{
            flexDirection: 'column',
            alignItems: 'flex-start',
            marginTop: 0,
          }}>
          <Text
            style={{
              fontFamily: 'SofiaPro-Medium',
              fontSize: 14,
              color: colorTitle,
              textAlign: 'left',
              marginLeft: 10,
              padding: 5,
            }}>{`${title}:`}</Text>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              borderColor: '#707070',
              backgroundColor: '#fff',
              borderWidth: 0.1,
              borderRadius: 30,
              padding: 5,
              width: '100%',
              height: 30,
            }}>
            <Picker
              mode={'dropdown'}
              style={{ width: 220, height: 40, marginLeft: 10 }}
              onValueChange={onValueChange}
              selectedValue={selectedValue}>
              <Picker.Item
                style={{}}
                label={`${selectedValue}`}
                value={selectedValue}
              />

              {options.map(item => {
                return (
                  <Picker.Item style={{}} label={item.name} value={item.id} />
                );
              })}
            </Picker>
          </View>
        </View>
      </View>
    </>
  );
};

Dropdown.defaultProps = {
  colorTitle: '#2E2E2E',
};

export default Dropdown;
