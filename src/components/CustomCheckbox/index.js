import React from 'react'
import { View, Text, TextInput, TouchableWithoutFeedback } from 'react-native'

const CustomCheckbox = ({ question, width, answer, onPress, options, symptoms }) => {

    return (

        <View style={{ flexDirection: 'column', marginLeft: 20, marginRight: 20, alignItems: 'flex-start' }}>
            <Text style={{ color: '#2E2E2E', fontFamily: 'SofiaPro-Medium', textAlign: 'left', margin: 10, fontSize: 13 }}>{question}</Text>

            <View style={{ flexDirection: 'column', marginLeft: 20, alignItems: 'flex-start', justifyContent: 'center' }}>


                {
                    options.map((result) => {
                        return (<View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>



                            <TouchableWithoutFeedback onPress={answer != "Sim" ? () => onPress : null}>
                                <View style={{ backgroundColor: result.value ? '#37153D' : '#FFFFFF', height: 24, width: 24, borderRadius: 20 }}></View>
                            </TouchableWithoutFeedback>

                            <Text style={{ color: '#2E2E2E', fontFamily: 'SofiaPro-Medium', margin: 10, fontSize: 13, borderWidth: 0.1, }}>{result.text}</Text>
                        </View>



                        )
                    })
                }



            </View>


        </View>


    )
}

CustomCheckbox.defaultProps = {
    width: 150,
    answer: "Sim"
}



export default CustomCheckbox