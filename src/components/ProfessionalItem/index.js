import React from 'react';
import {
  View,
  Button,
  Text,
  ScrollView,
  Modal,
  TextInput,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';

import avaliationSrc from '../../assets/images/avaliation.png';

const ProfessionalItem = ({
  name,
  avatar,
  specialties,
  description,
  onPress,
}) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: '#fff',
          width: '100%',
          padding: 15,
          marginTop: 20,
          alignItems: 'center',
          borderRadius: 10,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.18,
          shadowRadius: 1.0,
          elevation: 1,
        }}>
        <View style={{ flex: 6 }}>
          <Image
            style={{ height: 120, width: 120, borderRadius: 135 }}
            source={avatar}
          />
        </View>

        <View style={{ flex: 8, padding: 10, paddingLeft: 20 }}>
          <Text
            style={{
              color: '#37153D',
              fontSize: 18,
              fontFamily: 'SofiaPro-Medium',
            }}>
            {name}
          </Text>
          <Text
            numberOfLines={2}
            style={{
              color: '#2E2E2E',
              marginTop: 2,
              fontSize: 12,
              fontFamily: 'SofiaProLight',
              textTransform: 'capitalize',
            }}>
            {specialties}
          </Text>
          <Text
            numberOfLines={2}
            style={{
              color: '#707070',
              marginTop: 5,
              fontSize: 10,
              fontFamily: 'SofiaProLight',
            }}>
            {description}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'flex-start',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: '#F2F2F2',
              marginTop: 5,
              borderRadius: 5,
              padding: 6,
            }}>
            <Image
              style={{ height: 20, width: 20 }}
              resizeMode="contain"
              source={avaliationSrc}
            />
            <Text
              style={{
                marginLeft: 6,
                fontFamily: 'SofiaProLight',
                fontSize: 11,
                color: '#F76E1E',
              }}>{`Não avaliado`}</Text>
            {/* <Text
              style={{
                marginLeft: 4,
                fontFamily: 'SofiaProLight',
                fontSize: 10,
                color: '#707070',
              }}>{`(29 avaliações)`}</Text> */}
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default ProfessionalItem;
