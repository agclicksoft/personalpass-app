import styled from 'styled-components/native';

export const TextInput = styled.TextInput`
  flex: 1;
  font-family: 'SofiaPro-Regular';
  font-size: 14px;
  color: #636363;
  border-width: 0.1px;
  border-radius: 50px;
  border-color: #707070;
  background-color: #fff;
  margin: 10px;
  width: 100%;
  height: 40px;
  text-align: center;
`;
