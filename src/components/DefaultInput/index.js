import React from 'react';
import { TextInput } from './styles';

const DefaultInput = ({
  onChangeText,
  value,
  placeholder,
  placeholderTextColor,
  secureTextEntry,
  keyboardType,
  onFocus,
  onBlur,
  editable,
  pointerEvents,
  onTouchStart,
}) => {
  return (
    <TextInput
      onFocus={onFocus}
      onBlur={onBlur}
      placeholder={placeholder}
      placeholderTextColor={placeholderTextColor}
      onChangeText={onChangeText}
      keyboardType={keyboardType}
      value={value}
      secureTextEntry={secureTextEntry}
      editable={editable}
      pointerEvents={pointerEvents}
      onTouchStart={onTouchStart}
      autoCorrect={false}
      autoCapitalize="none"
    />
  );
};

DefaultInput.defaultProps = {
  placeholder: 'Digite aqui...',
  placeholderTextColor: '#999999',
  onChangeText: () => {},
  onFocus: () => {},
  onBlur: () => {},
  value: '',
};

export default DefaultInput;
