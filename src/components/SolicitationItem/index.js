import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';

import { DefaultButton } from '../../components';

let getStatusTitle = text => {
  switch (text) {
    case 'em andamento':
      return 'Em andamento';
    case 'concluido':
      return 'Concluída';
    case 'pendente':
      return 'Responder';
    case 'confirmado':
      return 'Confirmado';
    case 'PAGO':
      return 'Pagamento confirmado';
    case 'cancelado':
      return 'Cancelada';
    case 'disponivel':
      return 'Iniciar aula';
    default:
      return 'Responder';
  }
};

let getStatusPayment = text => {
  switch (text) {
    case 'CARTAO':
      return 'Cartão de crédito';
    case 'PENDENTE':
      return 'Pendente';
    case 'MAOS':
      return 'Em mãos';
    case 'BOLETO':
      return 'Em mãos';
    default:
      return 'Não identificado';
  }
};

let getStatusBG = text => {
  switch (text) {
    case 'concluido':
      return '#E9E9E9';
    case 'em andamento':
      return '#E9E9E9';
    case 'pendente':
      return '#37153D';
    case 'confirmado':
      return '#E9E9E9';
    case 'PAGO':
      return '#E9E9E9';
    case 'cancelado':
      return '#E9E9E9';
    case 'disponivel':
      return '#F76E1E';
    default:
      return '';
  }
};

let getStatusColor = text => {
  switch (text) {
    case 'em andamento':
      return '#F76E1E';
    case 'pendente':
      return '#E9E9E9';
    case 'concluido':
      return '#37153D';
    case 'confirmado':
      return '#049526';
    case 'PAGO':
      return '#049526';
    case 'cancelado':
      return '#D22020';
    case 'disponivel':
      return '#fff';
    default:
      return '';
  }
};

let button = text => {
  if (text == 'pendente' || 'disponivel') return true;

  return false;
};

const SolicitationItem = ({
  onPress,
  status,
  avatar,
  student,
  date,
  gym,
  plan,
  methodPayment,
}) => {
  return (
    <>
      <View
        style={{
          flexDirection: 'column',
          alignItems: 'center',
          paddingTop: 20,
          paddingBottom: 10,
          margin: 10,
          borderRadius: 10,
          elevation: 1,
          backgroundColor: '#fff',
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            width: 320,
          }}>
          <Image
            style={{ height: 120, width: 120, borderRadius: 135 }}
            source={avatar}
          />
          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'space-evenly',
              height: 100,
            }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Data/hora:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {date}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Local:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {gym}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Aluno:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {student}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Pacote:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {plan}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Pagamento:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {getStatusPayment(methodPayment ?? 'PENDENTE')}
              </Text>
            </View>
          </View>
        </View>

        <View
          style={{
            borderTopWidth: 1,
            borderTopColor: 'rgba(112, 112, 112, 0.1)',
            padding: 5,
            width: '100%',
            alignItems: 'center',
            marginTop: 15,
          }}>
          <DefaultButton
            title={getStatusTitle(status)}
            onPress={button(status) ? onPress : null}
            backgroundColor={getStatusBG(status)}
            fontColor={getStatusColor(status)}
            width={220}
            height={35}
          />
        </View>
      </View>
    </>
  );
};

export default SolicitationItem;
