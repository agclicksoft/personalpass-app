import * as React from 'react';
import { View, Text, Icon, Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import * as views from '../screens';
import homeFocused from '../assets/images/home-focused.png';
import homeUnfocused from '../assets/images/home-unfocused.png';
import dumbbellFocused from '../assets/images/dumbbell-focused.png';
import dumbbellUnfocused from '../assets/images/dumbbell-unfocused.png';
import userFocused from '../assets/images/user-focused.png';
import userUnfocused from '../assets/images/user-unfocused.png';
import barChartUnfocused from '../assets/images/bar-chart.png';
import barChartFocused from '../assets/images/bar-chart-focused.png';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function LogoTitle() {
  return (
    <View style={{ backgroundColor: 'red' }}>
      <Image style={{ alignSelf: 'center' }} source={barLogo} />
    </View>
  );
}

TabRoutes = () => {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          switch (route.name) {
            case 'ProfessionalsScreen':
              return (
                <Image
                  style={{ height: 25, resizeMode: 'contain' }}
                  source={focused ? homeFocused : homeUnfocused}
                />
              );
            case 'MyClasses':
              return (
                <Image
                  style={{ height: 25, resizeMode: 'contain' }}
                  source={focused ? dumbbellFocused : dumbbellUnfocused}
                />
              );
            case 'MyProfile':
              return (
                <Image
                  style={{ height: 25, resizeMode: 'contain' }}
                  source={focused ? userFocused : userUnfocused}
                />
              );
          }
        },
      })}
      tabBarOptions={{
        keyboardHidesTabBar: true,
        activeTintColor: '#37153D',
        inactiveTintColor: '#37153D',
        labelStyle: {
          fontFamily: 'SofiaPro-Regular',
          fontSize: 9,
          margin: 0,
        },
        style: {
          elevation: 2,
          height: '9%',
        },
      }}>
      <Tab.Screen
        name="ProfessionalsScreen"
        component={Professional}
        options={{
          tabBarLabel: 'Início',
          tabBarColor: '#37153D',
        }}
      />
      <Tab.Screen
        name="MyClasses"
        component={views.MyClassesScreen}
        options={{ tabBarLabel: 'Minhas Aulas' }}
      />
      <Tab.Screen
        name="MyProfile"
        component={MyProfile}
        options={{ tabBarLabel: 'Meu Perfil' }}
      />
    </Tab.Navigator>
  );
};

TabProfessionalRoutes = () => {
  return (
    <Tab.Navigator
      initialRouteName={'MyProfessionalProfile'}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          switch (route.name) {
            case 'StatisticsScreen':
              return (
                <Image
                  style={{ height: 25, resizeMode: 'contain' }}
                  source={focused ? barChartFocused : barChartUnfocused}
                />
              );
            case 'MyClasses':
              return (
                <Image
                  style={{ height: 25, resizeMode: 'contain' }}
                  source={focused ? dumbbellFocused : dumbbellUnfocused}
                />
              );
            case 'MyProfessionalProfile':
              return (
                <Image
                  style={{ height: 25, resizeMode: 'contain' }}
                  source={focused ? userFocused : userUnfocused}
                />
              );
          }
        },
      })}
      tabBarOptions={{
        keyboardHidesTabBar: true,
        activeTintColor: '#37153D',
        inactiveTintColor: '#37153D',

        labelStyle: {
          fontFamily: 'SofiaPro-Regular',
          fontSize: 10,
          margin: 0,
        },
        style: {
          elevation: 2,
          height: '9%',
        },
      }}>
      <Tab.Screen
        name="StatisticsScreen"
        component={Statistics}
        options={{
          tabBarLabel: 'Estatísticas',
          tabBarColor: '#37153D',
        }}
      />
      <Tab.Screen
        name="MyClasses"
        component={views.SolicitationsScreen}
        options={{ tabBarLabel: 'Solicitações' }}
      />
      <Tab.Screen
        name="MyProfessionalProfile"
        component={MyProfessionalProfile}
        options={{ tabBarLabel: 'Meu Perfil' }}
      />
    </Tab.Navigator>
  );
};

function Professional() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="ProfessionalsList"
        component={views.ProfessionalsScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ProfessionalProfileScreen"
        component={views.ProfessionalProfileScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="FilterScreen"
        component={views.FilterScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}

function MyProfile() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="MyProfileScreen"
        component={views.MyProfileScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="PARQScreen"
        component={views.PARQScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="StudentProfileScreen"
        component={views.StudentProfileScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="PaymentListScreen"
        component={views.PaymentListScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="AttachmentsScreen"
        component={views.AttachmentsScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="EditStudentScreen"
        component={views.EditStudentScreen}
        options={{ headerShown: false, tabBarVisible: false }}
      />
    </Stack.Navigator>
  );
}

function Statistics() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="StatisticsScreen"
        component={views.StatisticsScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ReportsScreen"
        component={views.ReportsScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ChartsScreen"
        component={views.ChartsScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}

function MyProfessionalProfile() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="ProfileProfessionalScreen"
        component={views.ProfileProfessionalScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="MyPlansScreen"
        component={views.MyPlansScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="PlansListScreen"
        component={views.PlansListScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ProfessionalScheduleScreen"
        component={views.ProfessionalScheduleScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ProfessionalProfileScreen"
        component={views.ProfessionalProfileScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="LocationsScreen"
        component={views.LocationsScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="GymsScreen"
        component={views.GymsScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="NewGymScreen"
        component={views.NewGymScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ConditionsScreen"
        component={views.ConditionsScreen}
        options={{ headerShown: false, tabBarVisible: false }}
      />
      <Stack.Screen
        name="SpecialtiesScreen"
        component={views.SpecialtiesScreen}
        options={{ headerShown: false, tabBarVisible: false }}
      />
      <Stack.Screen
        name="BankScreen"
        component={views.BankScreen}
        options={{ headerShown: false, tabBarVisible: false }}
      />
      <Stack.Screen
        name="MyAssessmentsScreen"
        component={views.MyAssessmentsScreen}
        options={{ headerShown: false, tabBarVisible: false }}
      />
      <Stack.Screen
        name="EditProfessionalScreen"
        component={views.EditProfessionalScreen}
        options={{ headerShown: false, tabBarVisible: false }}
      />
    </Stack.Navigator>
  );
}

function Routes() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          gestureEnabled: false,
        }}>
        <Stack.Screen
          name="AccessScreen"
          component={views.AccessScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="RegisterScreen"
          component={views.RegisterScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="RecoverPassScreen"
          component={views.RecoverPassScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ProfessionalRegisterScreen"
          component={views.ProfessionalRegisterScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ProfessionalsScreen"
          component={TabRoutes}
          options={{
            headerShown: false,
            headerTitle: props => <LogoTitle {...props} />,
            headerStyle: { alignItems: 'space-between' },
          }}
        />
        <Stack.Screen
          name="MyProfessionalProfile"
          component={TabProfessionalRoutes}
          options={{
            headerShown: false,
            headerTitle: props => <LogoTitle {...props} />,
            headerStyle: { alignItems: 'space-between' },
          }}
        />
        <Stack.Screen
          name="StudentProfileScreen"
          component={views.StudentProfileScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ClassDescriptionScreen"
          component={views.ClassDescriptionScreen}
          options={{ headerShown: false, tabBarVisible: false }}
        />
        <Stack.Screen
          name="TrainingDetailScreen"
          component={views.TrainingDetailScreen}
          options={{ headerShown: false, tabBarVisible: false }}
        />
        <Stack.Screen
          name="StudentTutorialScreen"
          component={views.StudentTutorialScreen}
          options={{ headerShown: false, tabBarVisible: false }}
        />
        <Stack.Screen
          name="ProfessionalTutorialScreen"
          component={views.ProfessionalTutorialScreen}
          options={{ headerShown: false, tabBarVisible: false }}
        />
        <Stack.Screen
          name="PaymentSelectionScreen"
          component={views.PaymentSelectionScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="PaymentWithPagSeguroScreen"
          component={views.PaymentWithPagSeguroScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="PaymentWithBoleto"
          component={views.PaymentWithBoleto}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="PaymentInHands"
          component={views.PaymentInHands}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Routes;
