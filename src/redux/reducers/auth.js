export const Types = {
  SET_USER: 'auth/SET_USER',
  SET_TOKEN: 'auth/SET_TOKEN',
  CLEAR_USER_INFO: 'auth/CLEAR_USER_INFO',
  SET_DEVICE_TOKEN: 'auth/SET_DEVICE_TOKEN',
};

export const Creators = {
  setUser: payload => {
    return {
      type: Types.SET_USER,
      payload,
    };
  },
  setToken: payload => {
    return {
      type: Types.SET_TOKEN,
      payload,
    };
  },
  clearUserInfo: () => {
    return {
      type: Types.CLEAR_USER_INFO,
    };
  },
  setTokenDevice: () => {
    return {
      type: Types.SET_DEVICE_TOKEN,
    };
  },
};

const INITIAL_STATE = {
  user: null,
  token: null,
  deviceToken: null,
};

export default function auth(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.SET_USER:
      return { ...state, user: action.payload };
    case Types.SET_TOKEN:
      return { ...state, token: action.payload };
    case Types.CLEAR_USER_INFO:
      return { user: null, token: null };
    case Types.SET_DEVICE_TOKEN:
      return { ...state, deviceToken: action.payload };
    default:
      return state;
      break;
  }
}
