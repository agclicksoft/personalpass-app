export const bankList = [
  {
    "value": "001",
    "label": "Banco do Brasil S.A."
  },
  {
    "value": "003",
    "label": "Banco da Amazônia S.A."
  },
  {
    "value": "004",
    "label": "Banco do Nordeste do Brasil S.A."
  },
  {
    "value": "007",
    "label": "Banco Nacional de Desenvolvimento Econômico e Social BNDES"
  },
  {
    "value": "010",
    "label": "Credicoamo Crédito Rural Cooperativa"
  },
  {
    "value": "011",
    "label": "Credit Suisse Hedging-Griffo Corretora de Valores S.A."
  },
  {
    "value": "012",
    "label": "Banco Inbursa S.A."
  },
  {
    "value": "014",
    "label": "Natixis Brasil S.A. Banco Múltiplo"
  },
  {
    "value": "015",
    "label": "UBS Brasil Corretora de Câmbio, Títulos e Valores Mobiliários S.A."
  },
  {
    "value": "016",
    "label": "Coop de Créd. Mútuo dos Despachantes de Trânsito de SC e Rio Grande do Sul"
  },
  {
    "value": "017",
    "label": "BNY Mellon Banco S.A."
  },
  {
    "value": "018",
    "label": "Banco Tricury S.A."
  },
  {
    "value": "021",
    "label": "Banestes S.A. Banco do Estado do Espírito Santo"
  },
  {
    "value": "024",
    "label": "Banco Bandepe S.A."
  },
  {
    "value": "025",
    "label": "Banco Alfa S.A."
  },
  {
    "value": "029",
    "label": "Banco Itaú Consignado S.A."
  },
  {
    "value": "033",
    "label": "Banco Santander (Brasil) S. A."
  },
  {
    "value": "036",
    "label": "Banco Bradesco BBI S.A."
  },
  {
    "value": "037",
    "label": "Banco do Estado do Pará S.A."
  },
  {
    "value": "040",
    "label": "Banco Cargill S.A."
  },
  {
    "value": "041",
    "label": "Banco do Estado do Rio Grande do Sul S.A."
  },
  {
    "value": "047",
    "label": "Banco do Estado de Sergipe S.A."
  },
  {
    "value": "060",
    "label": "Confidence Corretora de Câmbio S.A."
  },
  {
    "value": "062",
    "label": "Hipercard Banco Múltiplo S.A."
  },
  {
    "value": "063",
    "label": "Banco Bradescard S.A."
  },
  {
    "value": "064",
    "label": "Goldman Sachs do Brasil  Banco Múltiplo S. A."
  },
  {
    "value": "065",
    "label": "Banco AndBank (Brasil) S.A."
  },
  {
    "value": "066",
    "label": "Banco Morgan Stanley S. A."
  },
  {
    "value": "069",
    "label": "Banco Crefisa S.A."
  },
  {
    "value": "070",
    "label": "Banco de Brasília S.A."
  },
  {
    "value": "074",
    "label": "Banco J. Safra S.A."
  },
  {
    "value": "075",
    "label": "Banco ABN Amro S.A."
  },
  {
    "value": "076",
    "label": "Banco KDB do Brasil S.A."
  },
  {
    "value": "077",
    "label": "Banco Inter S.A."
  },
  {
    "value": "078",
    "label": "Haitong Banco de Investimento do Brasil S.A."
  },
  {
    "value": "079",
    "label": "Banco Original do Agronegócio S.A."
  },
  {
    "value": "080",
    "label": "BT Corretora de Câmbio Ltda."
  },
  {
    "value": "081",
    "label": "BBN Banco Brasileiro de Negocios S.A."
  },
  {
    "value": "082",
    "label": "Banco Topazio S.A."
  },
  {
    "value": "083",
    "label": "Banco da China Brasil S.A."
  },
  {
    "value": "084",
    "label": "Uniprime Norte do Paraná - Cooperativa de Crédito Ltda."
  },
  {
    "value": "085",
    "label": "Cooperativa Central de Crédito Urbano - Cecred"
  },
  {
    "value": "089",
    "label": "Cooperativa de Crédito Rural da Região da Mogiana"
  },
  {
    "value": "091",
    "label": "Central de Cooperativas de Economia e Crédito Mútuo do Est RS - Unicred"
  },
  {
    "value": "092",
    "label": "BRK S.A. Crédito, Financiamento e Investimento"
  },
  {
    "value": "093",
    "label": "Pólocred Sociedade de Crédito ao Microempreendedor e à Empresa de Pequeno Porte"
  },
  {
    "value": "094",
    "label": "Banco Finaxis S.A."
  },
  {
    "value": "095",
    "label": "Banco Confidence de Câmbio S.A."
  },
  {
    "value": "096",
    "label": "Banco BMFBovespa de Serviços de Liquidação e Custódia S/A"
  },
  {
    "value": "097",
    "label": "Cooperativa Central de Crédito Noroeste Brasileiro Ltda - CentralCredi"
  },
  {
    "value": "098",
    "label": "Credialiança Cooperativa de Crédito Rural"
  },
  {
    "value": "099",
    "label": "Uniprime Central – Central Interestadual de Cooperativas de Crédito Ltda."
  },
  {
    "value": "100",
    "label": "Planner Corretora de Valores S.A."
  },
  {
    "value": "101",
    "label": "Renascença Distribuidora de Títulos e Valores Mobiliários Ltda."
  },
  {
    "value": "102",
    "label": "XP Investimentos Corretora de Câmbio Títulos e Valores Mobiliários S.A."
  },
  {
    "value": "104",
    "label": "Caixa Econômica Federal"
  },
  {
    "value": "105",
    "label": "Lecca Crédito, Financiamento e Investimento S/A"
  },
  {
    "value": "107",
    "label": "Banco Bocom BBM S.A."
  },
  {
    "value": "108",
    "label": "PortoCred S.A. Crédito, Financiamento e Investimento"
  },
  {
    "value": "111",
    "label": "Oliveira Trust Distribuidora de Títulos e Valores Mobiliários S.A."
  },
  {
    "value": "113",
    "label": "Magliano S.A. Corretora de Cambio e Valores Mobiliarios"
  },
  {
    "value": "114",
    "label": "Central Cooperativa de Crédito no Estado do Espírito Santo - CECOOP"
  },
  {
    "value": "117",
    "label": "Advanced Corretora de Câmbio Ltda."
  },
  {
    "value": "118",
    "label": "Standard Chartered Bank (Brasil) S.A. Banco de Investimento"
  },
  {
    "value": "119",
    "label": "Banco Western Union do Brasil S.A."
  },
  {
    "value": "120",
    "label": "Banco Rodobens SA"
  },
  {
    "value": "121",
    "label": "Banco Agibank S.A."
  },
  {
    "value": "122",
    "label": "Banco Bradesco BERJ S.A."
  },
  {
    "value": "124",
    "label": "Banco Woori Bank do Brasil S.A."
  },
  {
    "value": "125",
    "label": "Brasil Plural S.A. Banco Múltiplo"
  },
  {
    "value": "126",
    "label": "BR Partners Banco de Investimento S.A."
  },
  {
    "value": "127",
    "label": "Codepe Corretora de Valores e Câmbio S.A."
  },
  {
    "value": "128",
    "label": "MS Bank S.A. Banco de Câmbio"
  },
  {
    "value": "129",
    "label": "UBS Brasil Banco de Investimento S.A."
  },
  {
    "value": "130",
    "label": "Caruana S.A. Sociedade de Crédito, Financiamento e Investimento"
  },
  {
    "value": "131",
    "label": "Tullett Prebon Brasil Corretora de Valores e Câmbio Ltda."
  },
  {
    "value": "132",
    "label": "ICBC do Brasil Banco Múltiplo S.A."
  },
  {
    "value": "133",
    "label": "Confederação Nacional das Cooperativas Centrais de Crédito e Economia Familiar e"
  },
  {
    "value": "134",
    "label": "BGC Liquidez Distribuidora de Títulos e Valores Mobiliários Ltda."
  },
  {
    "value": "135",
    "label": "Gradual Corretora de Câmbio, Títulos e Valores Mobiliários S.A."
  },
  {
    "value": "136",
    "label": "Confederação Nacional das Cooperativas Centrais Unicred Ltda – Unicred do Brasil"
  },
  {
    "value": "137",
    "label": "Multimoney Corretora de Câmbio Ltda"
  },
  {
    "value": "138",
    "label": "Get Money Corretora de Câmbio S.A."
  },
  {
    "value": "139",
    "label": "Intesa Sanpaolo Brasil S.A. - Banco Múltiplo"
  },
  {
    "value": "140",
    "label": "Easynvest - Título Corretora de Valores SA"
  },
  {
    "value": "142",
    "label": "Broker Brasil Corretora de Câmbio Ltda."
  },
  {
    "value": "143",
    "label": "Treviso Corretora de Câmbio S.A."
  },
  {
    "value": "144",
    "label": "Bexs Banco de Câmbio S.A."
  },
  {
    "value": "145",
    "label": "Levycam - Corretora de Câmbio e Valores Ltda."
  },
  {
    "value": "146",
    "label": "Guitta Corretora de Câmbio Ltda."
  },
  {
    "value": "149",
    "label": "Facta Financeira S.A. - Crédito Financiamento e Investimento"
  },
  {
    "value": "157",
    "label": "ICAP do Brasil Corretora de Títulos e Valores Mobiliários Ltda."
  },
  {
    "value": "159",
    "label": "Casa do Crédito S.A. Sociedade de Crédito ao Microempreendedor"
  },
  {
    "value": "163",
    "label": "Commerzbank Brasil S.A. - Banco Múltiplo"
  },
  {
    "value": "169",
    "label": "Banco Olé Bonsucesso Consignado S.A."
  },
  {
    "value": "172",
    "label": "Albatross Corretora de Câmbio e Valores S.A"
  },
  {
    "value": "173",
    "label": "BRL Trust Distribuidora de Títulos e Valores Mobiliários S.A."
  },
  {
    "value": "174",
    "label": "Pernambucanas Financiadora S.A. Crédito, Financiamento e Investimento"
  },
  {
    "value": "177",
    "label": "Guide Investimentos S.A. Corretora de Valores"
  },
  {
    "value": "180",
    "label": "CM Capital Markets Corretora de Câmbio, Títulos e Valores Mobiliários Ltda."
  },
  {
    "value": "182",
    "label": "Dacasa Financeira S/A - Sociedade de Crédito, Financiamento e Investimento"
  },
  {
    "value": "183",
    "label": "Socred S.A. - Sociedade de Crédito ao Microempreendedor"
  },
  {
    "value": "184",
    "label": "Banco Itaú BBA S.A."
  },
  {
    "value": "188",
    "label": "Ativa Investimentos S.A. Corretora de Títulos Câmbio e Valores"
  },
  {
    "value": "189",
    "label": "HS Financeira S/A Crédito, Financiamento e Investimentos"
  },
  {
    "value": "190",
    "label": "Cooperativa de Economia e Crédito Mútuo dos Servidores Públicos Estaduais do Rio"
  },
  {
    "value": "191",
    "label": "Nova Futura Corretora de Títulos e Valores Mobiliários Ltda."
  },
  {
    "value": "194",
    "label": "Parmetal Distribuidora de Títulos e Valores Mobiliários Ltda."
  },
  {
    "value": "196",
    "label": "Fair Corretora de Câmbio S.A."
  },
  {
    "value": "197",
    "label": "Stone Pagamentos S.A."
  },
  {
    "value": "204",
    "label": "Banco Bradesco Cartões S.A."
  },
  {
    "value": "208",
    "label": "Banco BTG Pactual S.A."
  },
  {
    "value": "212",
    "label": "Banco Original S.A."
  },
  {
    "value": "213",
    "label": "Banco Arbi S.A."
  },
  {
    "value": "217",
    "label": "Banco John Deere S.A."
  },
  {
    "value": "218",
    "label": "Banco BS2 S.A."
  },
  {
    "value": "222",
    "label": "Banco Credit Agrícole Brasil S.A."
  },
  {
    "value": "224",
    "label": "Banco Fibra S.A."
  },
  {
    "value": "233",
    "label": "Banco Cifra S.A."
  },
  {
    "value": "237",
    "label": "Banco Bradesco S.A."
  },
  {
    "value": "241",
    "label": "Banco Clássico S.A."
  },
  {
    "value": "243",
    "label": "Banco Máxima S.A."
  },
  {
    "value": "246",
    "label": "Banco ABC Brasil S.A."
  },
  {
    "value": "249",
    "label": "Banco Investcred Unibanco S.A."
  },
  {
    "value": "250",
    "label": "BCV - Banco de Crédito e Varejo S/A"
  },
  {
    "value": "253",
    "label": "Bexs Corretora de Câmbio S/A"
  },
  {
    "value": "254",
    "label": "Parana Banco S. A."
  },
  {
    "value": "260",
    "label": "Nu Pagamentos S.A."
  },
  {
    "value": "265",
    "label": "Banco Fator S.A."
  },
  {
    "value": "266",
    "label": "Banco Cédula S.A."
  },
  {
    "value": "268",
    "label": "Barigui Companhia Hipotecária"
  },
  {
    "value": "269",
    "label": "HSBC Brasil S.A. Banco de Investimento"
  },
  {
    "value": "271",
    "label": "IB Corretora de Câmbio, Títulos e Valores Mobiliários Ltda."
  },
  {
    "value": "300",
    "label": "Banco de la Nacion Argentina"
  },
  {
    "value": "318",
    "label": "Banco BMG S.A."
  },
  {
    "value": "320",
    "label": "China Construction Bank (Brasil) Banco Múltiplo S/A"
  },
  {
    "value": "341",
    "label": "Itaú Unibanco  S.A."
  },
  {
    "value": "366",
    "label": "Banco Société Générale Brasil S.A."
  },
  {
    "value": "370",
    "label": "Banco Mizuho do Brasil S.A."
  },
  {
    "value": "376",
    "label": "Banco J. P. Morgan S. A."
  },
  {
    "value": "389",
    "label": "Banco Mercantil do Brasil S.A."
  },
  {
    "value": "394",
    "label": "Banco Bradesco Financiamentos S.A."
  },
  {
    "value": "399",
    "label": "Kirton Bank S.A. - Banco Múltiplo"
  },
  {
    "value": "412",
    "label": "Banco Capital S. A."
  },
  {
    "value": "422",
    "label": "Banco Safra S.A."
  },
  {
    "value": "456",
    "label": "Banco MUFG Brasil S.A."
  },
  {
    "value": "464",
    "label": "Banco Sumitomo Mitsui Brasileiro S.A."
  },
  {
    "value": "473",
    "label": "Banco Caixa Geral - Brasil S.A."
  },
  {
    "value": "477",
    "label": "Citibank N.A."
  },
  {
    "value": "479",
    "label": "Banco ItauBank S.A."
  },
  {
    "value": "487",
    "label": "Deutsche Bank S.A. - Banco Alemão"
  },
  {
    "value": "488",
    "label": "JPMorgan Chase Bank, National Association"
  },
  {
    "value": "492",
    "label": "ING Bank N.V."
  },
  {
    "value": "494",
    "label": "Banco de La Republica Oriental del Uruguay"
  },
  {
    "value": "495",
    "label": "Banco de La Provincia de Buenos Aires"
  },
  {
    "value": "505",
    "label": "Banco Credit Suisse (Brasil) S.A."
  },
  {
    "value": "545",
    "label": "Senso Corretora de Câmbio e Valores Mobiliários S.A."
  },
  {
    "value": "600",
    "label": "Banco Luso Brasileiro S.A."
  },
  {
    "value": "604",
    "label": "Banco Industrial do Brasil S.A."
  },
  {
    "value": "610",
    "label": "Banco VR S.A."
  },
  {
    "value": "611",
    "label": "Banco Paulista S.A."
  },
  {
    "value": "612",
    "label": "Banco Guanabara S.A."
  },
  {
    "value": "613",
    "label": "Omni Banco S.A."
  },
  {
    "value": "623",
    "label": "Banco Pan S.A."
  },
  {
    "value": "626",
    "label": "Banco Ficsa S. A."
  },
  {
    "value": "630",
    "label": "Banco Intercap S.A."
  },
  {
    "value": "633",
    "label": "Banco Rendimento S.A."
  },
  {
    "value": "634",
    "label": "Banco Triângulo S.A."
  },
  {
    "value": "637",
    "label": "Banco Sofisa S. A."
  },
  {
    "value": "641",
    "label": "Banco Alvorada S.A."
  },
  {
    "value": "643",
    "label": "Banco Pine S.A."
  },
  {
    "value": "652",
    "label": "Itaú Unibanco Holding S.A."
  },
  {
    "value": "653",
    "label": "Banco Indusval S. A."
  },
  {
    "value": "654",
    "label": "Banco A. J. Renner S.A."
  },
  {
    "value": "655",
    "label": "Banco Votorantim S.A."
  },
  {
    "value": "707",
    "label": "Banco Daycoval S.A."
  },
  {
    "value": "712",
    "label": "Banco Ourinvest S.A."
  },
  {
    "value": "719",
    "label": "Banif - Bco Internacional do Funchal (Brasil) S.A."
  },
  {
    "value": "735",
    "label": "Banco Neon S.A."
  },
  {
    "value": "739",
    "label": "Banco Cetelem S.A."
  },
  {
    "value": "741",
    "label": "Banco Ribeirão Preto S.A."
  },
  {
    "value": "743",
    "label": "Banco Semear S.A."
  },
  {
    "value": "745",
    "label": "Banco Citibank S.A."
  },
  {
    "value": "746",
    "label": "Banco Modal S.A."
  },
  {
    "value": "747",
    "label": "Banco Rabobank International Brasil S.A."
  },
  {
    "value": "748",
    "label": "Banco Cooperativo Sicredi S. A."
  },
  {
    "value": "751",
    "label": "Scotiabank Brasil S.A. Banco Múltiplo"
  },
  {
    "value": "752",
    "label": "Banco BNP Paribas Brasil S.A."
  },
  {
    "value": "753",
    "label": "Novo Banco Continental S.A. - Banco Múltiplo"
  },
  {
    "value": "754",
    "label": "Banco Sistema S.A."
  },
  {
    "value": "755",
    "label": "Bank of America Merrill Lynch Banco Múltiplo S.A."
  },
  {
    "value": "756",
    "label": "Banco Cooperativo do Brasil S/A - Bancoob"
  },
  {
    "value": "757",
    "label": "Banco Keb Hana do Brasil S. A."
  }
  // {
  //   label: '001	- BANCO DO BRASIL S.A.',
  //   value: '001',
  //   key: '001',
  // },
  // {
  //   label: "070 - BRB - BANCO DE BRASILIA S.A.",
  //   value: '070',
  //   key: '070',
  // },
  // {
  //   label: "104 - CAIXA ECONOMICA FEDERAL",
  //   value: '104',
  //   key: '104',
  // },
  // {
  //   label: "077 - BANCO INTER S.A.",
  //   value: '077',
  //   key: '077',
  // },
  // {
  //   label: "741 - BANCO RIBEIRAO PRETO S.A.",
  //   value: '741',
  //   key: '741',
  // },
  // {
  //   label: "330 - BANCO BARI DE INVESTIMENTOS E FINANCIAMENTOS S.A.",
  //   value: '330',
  //   key: '330',
  // },
  // {
  //   label: "739 - BANCO CETELEM S.A",
  //   value: '739',
  //   key: '739',
  // },
  // {
  //   label: "743 - BANCO SEMEAR S.A.",
  //   value: '743',
  //   key: '743',
  // },
  // {
  //   label: "096 - BANCO B3 S.A.",
  //   value: '096',
  //   key: '096',
  // },
  // {
  //   label: "747 - BANCO RABOBANK INTERNATIONAL BRASIL S.A.",
  //   value: '747',
  //   key: '747',
  // },
  // {
  //   label: "748 - BANCO COOPERATIVO SICREDI S.A.",
  //   value: '748',
  //   key: '748',
  // },
  // {
  //   label: "752 - BANCO BNP PARIBAS BRASIL S.A.",
  //   value: '752',
  //   key: '752',
  // },
  // {
  //   label: "399 - KIRTON BANK S.A. - BANCO MÚLTIPLO",
  //   value: '399',
  //   key: '399',
  // },
  // {
  //   label: "655 - BANCO BV S.A.",
  //   value: '655',
  //   key: '655',
  // },
  // {
  //   label: "756 - BANCO COOPERATIVO DO BRASIL S.A. - BANCOOB",
  //   value: '756',
  //   key: '756',
  // },
  // {
  //   label: "757 - BANCO KEB HANA DO BRASIL S.A.",
  //   value: '757',
  //   key: '757',
  // },
  // //Código não encontrado
  // // {
  // //   label: "BANCO CATERPILLAR S.",
  // //   value: '',
  // //   key: '',
  // // },
  // {
  //   label: "066 - BANCO MORGAN STANLEY S.A.",
  //   value: '066',
  //   key: '066',
  // },
  // //Código não encontrado
  // // {
  // //   label: "BANCO CNH INDUSTRIAL CAPITAL S.A.",
  // //   value: '',
  // //   key: '',
  // // },
  // {
  //   label: "062 - HIPERCARD BANCO MÚLTIPLO S.A.",
  //   value: '062',
  //   key: '062',
  // },
  // {
  //   label: "074 - BANCO J. SAFRA S.A.",
  //   value: '074',
  //   key: '074',
  // },
  // //Código não encontrado
  // // {
  // //   label: "BANCO TOYOTA DO BRASIL S.A.",
  // //   value: '',
  // //   key: '',
  // // },
  // {
  //   label: "025 - BANCO ALFA S.A.",
  //   value: '025',
  //   key: '025',
  // },
  // //Código não encontrado
  // // {
  // //   label: "BANCO PSA FINANCE BRASIL S.A.",
  // //   value: '',
  // //   key: '',
  // // },
  // {
  //   label: "075 - BANCO ABN AMRO S.A.",
  //   value: '075',
  //   key: '075',
  // },
  // {
  //   label: "040 - BANCO CARGILL S.A",
  //   value: '040',
  //   key: '040',
  // },
  // //Código não encontrado
  // // {
  // //   label: "BANCO HONDA S.A.",
  // //   value: '',
  // //   key: '',
  // // },
  // {
  //   label: "063 - BANCO BRADESCARD S.A",
  //   value: '063',
  //   key: '063',
  // },
  // {
  //   label: "064 - GOLDMAN SACHS DO BRASIL BANCO MULTIPLO S.A.",
  //   value: '064',
  //   key: '064',
  // },
  // {
  //   label: "012 - BANCO INBURSA S.A",
  //   value: '012',
  //   key: '012',
  // },
  // {
  //   label: "003 - BANCO DA AMAZONIA S.",
  //   value: '003',
  //   key: '003',
  // },
  // {
  //   label: "037 - BANCO DO ESTADO DO PARÁ S.A.",
  //   value: '037',
  //   key: '037',
  // },
  // //Código não encontrado
  // // {
  // //   label: "BANCO DE LAGE LANDEN BRASIL S.A.",
  // //   value: '',
  // //   key: '',
  // // },
  // {
  //   label: "036 - BANCO BRADESCO BBI S.A.",
  //   value: '036',
  //   key: '036',
  // },
  // {
  //   label: "394 - BANCO BRADESCO FINANCIAMENTOS S.A.",
  //   value: '394',
  //   key: '394',
  // },
  // {
  //   label: "004 - BANCO DO NORDESTE DO BRASIL S.A.",
  //   value: '004',
  //   key: '004',
  // },
  // //Código não encontrado
  // // {
  // //   label: "BANCO MONEO S.A.",
  // //   value: '',
  // //   key: '',
  // // },
  // {
  //   label: "320 - CHINA CONSTRUCTION BANK (BRASIL) BANCO MÚLTIPLO S/A",
  //   value: '320',
  //   key: '320',
  // },
  // {
  //   label: "076 - BANCO KDB DO BRASIL S.A.",
  //   value: '076',
  //   key: '076',
  // },
  // {
  //   label: "082 - BANCO TOPÁZIO S.A",
  //   value: '082',
  //   key: '082',
  // },
  // {
  //   label: "505 - BANCO CSF S.A.",
  //   value: '505',
  //   key: '505',
  // },
  // {
  //   label: "MONEYCORP BANCO DE CÂMBIO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "STATE STREET BRASIL S.A. – BANCO COMERCIAL",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO ORIGINAL DO AGRONEGÓCIO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO VIPAL S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCOSEGURO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO YAMAHA MOTOR DO BRASIL S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO AGIBANK S.A",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO DA CHINA BRASIL S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO BANDEPE S.A",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "SCANIA BANCO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO RANDON S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "TRAVELEX BANCO DE CÂMBIO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO FINAXIS S.A",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO DO ESTADO DE SERGIPE S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BEXS BANCO DE CÂMBIO S/A",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO WESTERN UNION DO BRASIL S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "PARANÁ BANCO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO BOCOM BBM S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO CAPITAL S.A",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO WOORI BANK DO BRASIL S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "389 - BANCO MERCANTIL DO BRASIL S.A.",
  //   value: '389',
  //   key: '389',
  // },
  // {
  //   label: "BANCO ITAUCARD S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO ITAÚ BBA S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO TRIANGULO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "ICBC DO BRASIL BANCO MÚLTIPLO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "MS BANK S.A. BANCO DE CÂMBIO",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "DEUTSCHE SPARKASSEN LEASING DO BRASIL BANCO MÚLTIPLO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "COMMERZBANK BRASIL S.A. - BANCO MÚLTIPLO",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO KOMATSU DO BRASIL S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO DIGIO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANESTES S.A. BANCO DO ESTADO DO ESPIRITO SANTO.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO ABC BRASIL S.A",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO PACCAR S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "INTL FCSTONE BANCO DE CÂMBIO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "SCOTIABANK BRASIL S.A. BANCO MÚLTIPLO",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO HYUNDAI CAPITAL BRASIL S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO BTG PACTUAL S.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "746 - BANCO MODAL S.A.",
  //   value: '746',
  //   key: '746',
  // },
  // {
  //   label: "BANCO CLASSICO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO C6 S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO GUANABARA S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO INDUSTRIAL DO BRASIL S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO CREDIT SUISSE (BRASIL) S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "300 - BANCO DE LA NACION ARGENTINA",
  //   value: '300',
  //   key: '300',
  // },
  // {
  //   label: "CITIBANK N.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO CEDULA S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO BRADESCO BERJ S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "376 - BANCO J.P. MORGAN S.",
  //   value: '376',
  //   key: '376',
  // },
  // {
  //   label: "BANCO LOSANGO S.A. - BANCO MÚLTIPLO",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO XP S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO CAIXA GERAL - BRASIL S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "745 - BANCO CITIBANK S.A.",
  //   value: '745',
  //   key: '745',
  // },
  // {
  //   label: "BANCO RODOBENS S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO FATOR S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO ITAÚ CONSIGNADO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO MÁXIMA S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO IBM S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO XCMG BRASIL S.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BNY MELLON BANCO S.A",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "DAYCOVAL LEASING - BANCO MÚLTIPLO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "495 - BANCO DE LA PROVINCIA DE BUENOS AIRES",
  //   value: '495',
  //   key: '495',
  // },
  // {
  //   label: "PLURAL S.A. BANCO MÚLTIPLO",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "JPMORGAN CHASE BANK, NATIONAL ASSOCIATION",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO ANDBANK (BRASIL) S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "ING BANK N.V.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO ITAULEASING S.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BCV - BANCO DE CRÉDITO E VAREJO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO HSBC S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO ARBI S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "INTESA SANPAOLO BRASIL S.A. - BANCO MÚLTIPLO",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO TRICURY S.A",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO VOLVO BRASIL S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO SAFRA S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO SMARTBANK S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO FIBRA S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO VOLKSWAGEN S.A",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO LUSO BRASILEIRO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO GM S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO PAN S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO VOTORANTIM S.A",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO ITAUBANK S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO MUFG BRASIL S.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO SUMITOMO MITSUI BRASILEIRO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "341 - ITAÚ UNIBANCO S.A",
  //   value: '341',
  //   key: '341',
  // },
  // {
  //   label: "237 - BANCO BRADESCO S.A.",
  //   value: '237',
  //   key: '237',
  // },
  // {
  //   label: "BANCO MERCEDES-BENZ DO BRASIL S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "OMNI BANCO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "ITAÚ UNIBANCO HOLDING S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO SOFISA S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO INDUSVAL S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "069 - BANCO CREFISA S.A",
  //   value: '069',
  //   key: '069',
  // },
  // {
  //   label: "BANCO MIZUHO DO BRASIL S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO INVESTCRED UNIBANCO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO BMG S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO ITAÚ VEÍCULOS S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO C6 CONSIGNADO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO SOCIETE GENERALE BRASIL S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO PAULISTA S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANK OF AMERICA MERRILL LYNCH BANCO MÚLTIPLO S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO PINE S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO DAYCOVAL S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO FIDIS S/A",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO RCI BRASIL S.A",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "487 - DEUTSCHE BANK S.A. - BANCO ALEMAO",
  //   value: '487',
  //   key: '487',
  // },
  // {
  //   label: "BANCO CIFRA S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO RENDIMENTO S.A",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO BS2 S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "753 - NOVO BANCO CONTINENTAL S.A. - BANCO MÚLTIPLO",
  //   value: '753',
  //   key: '753',
  // },
  // {
  //   label: "BANCO CRÉDIT AGRICOLE BRASIL S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO SISTEMA S.A",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO VR S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO OURINVEST S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO RNX S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "033 - BANCO SANTANDER (BRASIL) S.A.",
  //   value: '033',
  //   key: '033',
  // },
  // {
  //   label: "BANCO FORD S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO JOHN DEERE S.A",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "041 - BANCO DO ESTADO DO RIO GRANDE DO SUL S.A.",
  //   value: '041',
  //   key: '041',
  // },
  // {
  //   label: "BANCO DIGIMAIS S.A.",
  //   value: '',
  //   key: '',
  // },
  // {
  //   label: "BANCO ORIGINAL S.A.",
  //   value: '',
  //   key: '',
  // }
];
