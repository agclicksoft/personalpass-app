import React, { Component } from 'react';
import {
  View,
  Button,
  Text,
  ScrollView,
  Modal,
  TextInput,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';

import { DefaultButton, DefaultInput, ProfileItem } from '../../components';

import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { SafeAreaView } from 'react-native-safe-area-context';

class ConditionsScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showModal: true,
    };
  }

  render() {
    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            paddingHorizontal: 20,
            paddingVertical: 20,
            elevation: 3,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Ionicon
                style={{
                  fontWeight: '900',
                  marginRight: 10,
                }}
                name={'ios-arrow-round-back'}
                size={30}
                color={'#37153D'}
              />
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#37153D',
                  textAlign: 'left',
                }}>
                {' '}
                Termos de uso
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <ScrollView>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'flex-start',
              padding: 0,
            }}>
            <View style={{ height: 10 }} />
            <Text
              style={{
                fontFamily: 'SofiaProLight',
                fontSize: 13,
                textAlign: 'left',
                color: '#2E2E2E',
                padding: 20,
                paddingBottom: 0,
              }}>
              {`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae arcu eget dui fringilla luctus tempus sit amet neque. Morbi semper sapien et porttitor venenatis. Vivamus tincidunt convallis diam eget congue. Proin consectetur accumsan porttitor. Pellentesque pretium, felis at posuere bibendum, sapien tellus semper urna, a dictum ipsum est sit amet est. Curabitur ut risus eget enim pulvinar tincidunt sit amet sit amet lacus. Morbi feugiat suscipit aliquet. Phasellus risus turpis, commodo eu gravida vitae, varius nec lacus. Vivamus vel porttitor neque. Aenean sit amet nulla ipsum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sit amet commodo elit. Donec finibus at neque et tempor. Maecenas eu dui sed augue convallis lacinia eu a quam.
                        `}
            </Text>
            <Text
              style={{
                fontFamily: 'SofiaProLight',
                fontSize: 13,
                textAlign: 'left',
                color: '#2E2E2E',
                padding: 20,
                paddingTop: 0,
                paddingBottom: 0,
              }}>
              {` Mauris faucibus pretium facilisis. Morbi euismod ex semper metus tristique, id pellentesque ipsum fringilla. Curabitur malesuada suscipit tortor. Etiam sit amet lorem laoreet enim congue efficitur in non lectus. Cras porta sagittis purus, id malesuada ligula pretium quis. Sed ac metus sed lorem pretium tincidunt cursus in velit. Donec fringilla, dolor quis fringilla dictum, nulla ex elementum nunc, id euismod velit odio sed lorem. Sed condimentum ipsum felis, id tincidunt tellus fermentum ut. Aenean finibus dui tortor, at vehicula felis pulvinar ac. Mauris congue arcu ex, quis euismod libero sodales at. Integer ullamcorper tempus odio, et efficitur dolor porta vitae. Ut interdum urna sed orci tincidunt, et sagittis sapien interdum. Nullam vitae mollis mi. Nunc malesuada tristique arcu sit amet scelerisque. Praesent quis facilisis quam. Nullam malesuada leo nec orci lacinia tempor. Ut facilisis est nec felis porta, ac accumsan magna rutrum. Etiam gravida pretium tristique. Nunc ut urna erat. Aenean ut tempus lectus. Praesent id nunc at dui suscipit sollicitudin posuere a nibh. Morbi sit amet gravida odio. Donec quis semper ligula, quis vulputate purus. Quisque maximus eros id ligula condimentum, at hendrerit lorem fermentum. Praesent enim leo, feugiat sed luctus et, suscipit in quam. Vestibulum dui diam, feugiat vel nulla et, commodo ultricies dui. Proin venenatis suscipit suscipit. Nullam malesuada leo nec orci lacinia tempor. Ut facilisis est nec felis porta, ac accumsan magna rutrum. Etiam gravida pretium tristique. Nunc ut urna erat. Aenean ut tempus lectus. Praesent id nunc at dui suscipit sollicitudin posuere a nibh. Morbi sit amet gravida odio. Donec quis semper ligula, quis vulputate purus. Quisque maximus eros id ligula condimentum, at hendrerit lorem fermentum. Praesent enim leo, feugiat sed luctus et, suscipit in quam. Vestibulum dui diam, feugiat vel nulla et, commodo ultricies dui. Proin venenatis suscipit suscipit.
                        `}
            </Text>
            <Text
              style={{
                fontFamily: 'SofiaProLight',
                fontSize: 13,
                textAlign: 'left',
                color: '#2E2E2E',
                padding: 20,
                paddingTop: 0,
                paddingBottom: 0,
              }}>
              {` Nullam malesuada leo nec orci lacinia tempor. Ut facilisis est nec felis porta, ac accumsan magna rutrum. Etiam gravida pretium tristique. Nunc ut urna erat. Aenean ut tempus lectus. Praesent id nunc at dui suscipit sollicitudin posuere a nibh. Morbi sit amet gravida odio. Donec quis semper ligula, quis vulputate purus. Quisque maximus eros id ligula condimentum, at hendrerit lorem fermentum. Praesent enim leo, feugiat sed luctus et, suscipit in quam. Vestibulum dui diam, feugiat vel nulla et, commodo ultricies dui. Proin venenatis suscipit suscipit. Nullam malesuada leo nec orci lacinia tempor. Ut facilisis est nec felis porta, ac accumsan magna rutrum. Etiam gravida pretium tristique. Nunc ut urna erat. Aenean ut tempus lectus. Praesent id nunc at dui suscipit sollicitudin posuere a nibh. Morbi sit amet gravida odio. Donec quis semper ligula, quis vulputate purus. Quisque maximus eros id ligula condimentum, at hendrerit lorem fermentum. Praesent enim leo, feugiat sed luctus et, suscipit in quam. Vestibulum dui diam, feugiat vel nulla et, commodo ultricies dui. Proin venenatis suscipit suscipit.
                        `}
            </Text>
          </View>
        </ScrollView>

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: 'white',
            shadowColor: '#000',
            justifyContent: 'center',
            alignItems: 'center',
            padding: 10,
            elevation: 3,
          }}>
          <View style={{ flexDirection: 'column', alignItems: 'center' }}>
            <DefaultButton
              title={'Li e aceito os termos de uso'}
              height={35}
              width={300}
              onPress={() => {
                this.props.navigation.goBack();
              }}
            />
            <Text
              style={{
                fontFamily: 'SofiaProLight',
                fontSize: 11,
                color: '#707070',
              }}
              onPress={() => {
                this.props.navigation.goBack();
              }}>
              Recusar
            </Text>
          </View>
        </View>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(ConditionsScreen);
