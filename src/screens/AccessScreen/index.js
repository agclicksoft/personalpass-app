import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Platform,
  PermissionsAndroid,
  KeyboardAvoidingView,
} from 'react-native';
import { connect } from 'react-redux';
import { showMessage } from 'react-native-flash-message';
import AsyncStorage from '@react-native-community/async-storage';
import Geolocation from 'react-native-geolocation-service';
import RNBootSplash from 'react-native-bootsplash';
import { Creators as authActions } from '../../redux/reducers/auth';
import { ImageBackground, Logo } from './styles';
import { DefaultButton, DefaultInput, LoadingModal } from '../../components';
import loginBg from '../../assets/images/background.png';
import logo from '../../assets/images/logo.png';
import API, { AxiosWithHeaders } from '../../services/api';

class AccessScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      loading: false,
    };
  }

  async componentDidMount() {
    RNBootSplash.show();

    setTimeout(() => {
      RNBootSplash.hide();
    }, 2000);

    if (Platform.OS === 'ios') {
      Geolocation.requestAuthorization('whenInUse');
      this.getCurrentLocation();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Autorização para acessar localização',
            message:
              'Precisamos de acesso para mostrar profissionais próximos à você.',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          this.getCurrentLocation();
        } else {
          console.log('Location permission denied');
        }
      } catch (err) {
        console.warn(err);
      }
    }

    await this.checkLoggedUser();
  }

  getCurrentLocation() {
    Geolocation.getCurrentPosition(
      position => {
        console.log(position);
      },
      error => {
        console.log('map error: ', error);
        console.log(error.code, error.message);
      },
      { enableHighAccuracy: false, timeout: 15000, maximumAge: 10000 },
    );
  }

  async checkLoggedUser() {
    const data = await AsyncStorage.getItem('DATA_KEY');
    const refresh_token = await AsyncStorage.getItem('REFRESH_TOKEN');
    const saved_email = await AsyncStorage.getItem('EMAIL');

    if (saved_email) {
      this.setState({
        email: saved_email,
      });
    }

    if (data && refresh_token) {
      const AxiosInstance = AxiosWithHeaders({
        Authorization: `Bearer ${JSON.parse(data)}`,
      });

      try {
        const response = await AxiosInstance.post('users/refresh', {
          refresh_token: JSON.parse(refresh_token),
        });

        if (!(response && response.status == 401)) {
          await AsyncStorage.setItem(
            'REFRESH_TOKEN',
            JSON.stringify(response.data.refreshToken),
          );
        }

        this.navigateByRole(response);

        return;
      } catch (err) {
        console.log(err);
        RNBootSplash.hide({ fade: true });
      }
    }
    RNBootSplash.hide({ fade: true });
  }

  navigateByRole = async response => {
    try {
      await this.props.setToken(response.data.token);

      const AxiosInstance = AxiosWithHeaders({
        Authorization: `Bearer ${response.data.token}`,
      });

      let getResponse;

      if (response.data.role === 'student') {
        getResponse = await AxiosInstance.get(
          `students/${response.data.student.id}`,
        );
      } else {
        getResponse = await AxiosInstance.get(
          `professionals/${response.data.professional.id}`,
        );
      }
      console.log(response.data);
      console.log(getResponse);
      this.props.setUser(getResponse.data);

      response.data.role === 'student'
        ? this.props.navigation.navigate('ProfessionalsScreen')
        : this.props.navigation.navigate('MyProfessionalProfile');
    } catch (err) {
      console.log(err);
      RNBootSplash.hide({ fade: true });
    }
  };

  login = async () => {
    try {
      this.setState({ loading: true });

      if (!this.state.email || !this.state.password) {
        this.setState({ loading: false });

        showMessage({
          type: 'danger',
          message: 'Erro ao efetuar login',
          description: 'Preencha todas as informações',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });

        return;
      }

      const response = await API.post('users/authenticate', {
        email: this.state.email.replace(/\s+/g, ''),
        password: this.state.password,
      });

      if (response.status === 200) {
        this.setState(
          {
            loading: false,
          },
          async () => {
            await AsyncStorage.setItem(
              'DATA_KEY',
              JSON.stringify(response.data.token),
            );

            await AsyncStorage.setItem(
              'REFRESH_TOKEN',
              JSON.stringify(response.data.refreshToken),
            );

            await AsyncStorage.setItem(
              'EMAIL',
              this.state.email.replace(/\s+/g, ''),
            );

            await this.props.setToken(response.data.token);

            const AxiosInstance = AxiosWithHeaders({
              Authorization: `Bearer ${response.data.token}`,
            });

            let getResponse;

            if (response.data.role === 'student') {
              getResponse = await AxiosInstance.get(
                `students/${response.data.student.id}`,
              );
            } else {
              getResponse = await AxiosInstance.get(
                `professionals/${response.data.professional.id}`,
              );
            }
            console.log(response.data);
            console.log(getResponse);
            this.props.setUser(getResponse.data);
          },
        );
        console.log('Login successed.');

        response.data.role === 'student'
          ? this.props.navigation.navigate('ProfessionalsScreen')
          : this.props.navigation.navigate('MyProfessionalProfile');
      }
    } catch (err) {
      console.log('We got an error', err.response.status);

      if (err.response.status === 401) {
        this.setState({ loading: false });
        showMessage({
          type: 'danger',
          message: 'Erro ao efetuar login',
          description: 'Usuário e/ou senha inválidos',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      } else if (err.response.status === 404) {
        this.setState({ loading: false });
        showMessage({
          type: 'danger',
          message: 'Erro ao efetuar login',
          description: 'Usuário não encontrado',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      } else {
        this.setState({ loading: false });
        showMessage({
          type: 'danger',
          message: 'Erro ao efetuar login',
          description: 'Tente novamente',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      }
    }
  };

  onEmailChanged = email => this.setState({ email: email.toLowerCase() });

  onPasswordChanged = password => this.setState({ password });

  render() {
    return (
      <>
        <ImageBackground source={loginBg}>
          <KeyboardAvoidingView
            behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
            style={{ flex: 1 }}>
            <ScrollView
              contentContainerStyle={{ flexGrow: 1 }}
              showsVerticalScrollIndicator={false}>
              <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                <View
                  style={{
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    flex: 1,
                  }}>
                  <Logo
                    source={logo}
                    style={{ alignItems: 'center', height: 170, width: 170 }}
                    resizeMode="contain"
                  />
                </View>

                <View
                  style={{
                    justifyContent: 'flex-end',
                    backgroundColor: '#F8F8F8',
                    marginTop: 40,
                    padding: 20,
                    borderTopLeftRadius: 40,
                    borderTopRightRadius: 40,
                    flex: 1,
                  }}>
                  <Text
                    style={{
                      fontSize: 23,
                      fontFamily: 'SofiaPro-Medium',
                      textAlign: 'center',
                      padding: 10,
                      color: '#F76E1E',
                    }}>
                    Olá!
                  </Text>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'SofiaPro-Regular',
                      textAlign: 'center',
                      color: '#707070',
                      paddingBottom: 30,
                    }}>
                    Faça login na sua conta.
                  </Text>
                  <View
                    style={{
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <DefaultInput
                      onChangeText={this.onEmailChanged}
                      value={this.state.email}
                      placeholder="E-mail"
                      keyboardType="email-address"
                    />

                    <DefaultInput
                      onChangeText={this.onPasswordChanged}
                      value={this.state.password}
                      secureTextEntry
                      placeholder="Senha"
                    />

                    <DefaultButton
                      onPress={() => {
                        this.login();
                      }}
                      title="Entrar"
                      width={190}
                    />

                    <View
                      style={{
                        justifyContent: 'space-evenly',
                        alignItems: 'center',
                        flexDirection: 'row',
                        margin: 10,
                        marginTop: 30,
                        padding: 5,
                        paddingBottom: 0,
                        height: 25,
                      }}>
                      <Text
                        style={{
                          fontFamily: 'SofiaProLight',
                          fontSize: 12,
                          textAlign: 'center',
                          borderRadius: 20,
                          color: '#37153D',
                        }}>
                        {'Cadastre-se: '}
                      </Text>
                      <Text
                        onPress={() =>
                          this.props.navigation.navigate('RegisterScreen')
                        }
                        style={{
                          fontFamily: 'SofiaPro-Bold',
                          fontSize: 12,
                          textAlign: 'center',
                          borderRadius: 20,
                          color: '#37153D',
                        }}>
                        Aluno.
                      </Text>
                    </View>

                    <View
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        flexDirection: 'row',
                        margin: 0,
                        padding: 5,
                      }}>
                      <Text
                        style={{
                          fontFamily: 'SofiaProLight',
                          fontSize: 12,
                          textAlign: 'center',
                          borderRadius: 20,
                          color: '#37153D',
                        }}>
                        {'Cadastre-se: '}
                      </Text>
                      <Text
                        onPress={() =>
                          this.props.navigation.navigate(
                          'ProfessionalRegisterScreen',
                        )
                        }
                        style={{
                          fontFamily: 'SofiaPro-Bold',
                          fontSize: 12,
                          textAlign: 'center',
                          borderRadius: 20,
                          color: '#37153D',
                        }}>
                        Professor.
                      </Text>
                    </View>

                    <View
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        flexDirection: 'row',
                        margin: 0,
                        padding: 5,
                        paddingBottom: 80,
                      }}>
                      <Text
                        onPress={() =>
                          this.props.navigation.navigate('RecoverPassScreen')
                        }
                        style={{
                          fontFamily: 'SofiaPro-Bold',
                          fontSize: 12,
                          textAlign: 'center',
                          borderRadius: 20,
                          color: '#37153D',
                        }}>
                        Esqueci minha senha.
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </ImageBackground>
        <LoadingModal show={this.state.loading} />
      </>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
});

export default connect(
  mapStateToProps,
  { ...authActions },
)(AccessScreen);
