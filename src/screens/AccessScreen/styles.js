import styled from 'styled-components/native';

export const Container = styled.View``;

export const ImageBackground = styled.ImageBackground`
  height: 100%;
  background-color: white;
`;
export const Logo = styled.Image`
  margin: 30px 0 15px;
`;

export const LoginContent = styled.View`
  min-height: 100%;
  opacity: 1;
  position: absolute;
  width: 100%;
  left: 0;
  right: 0;
  bottom: 0
  justify-content: center;
  background-color: #fff;
  border-top-left-radius: 31px;
  border-top-right-radius: 31px;
  padding-left: 29px;
  padding-right: 29px;
`;
