import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  ActivityIndicator,
} from 'react-native';
import { DefaultButton, LoadingModal } from '../../components';
import { SafeAreaView } from 'react-native-safe-area-context';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { BASE_URL } from '../../services/baseUrl';
import { WebView } from 'react-native-webview';
import { AxiosWithHeaders } from '../../services/api';
import Clipboard from '@react-native-community/clipboard';
import AwesomeAlert from 'react-native-awesome-alerts';
import LottieView from 'lottie-react-native';
import animation from '../../assets/success.json';
import animationError from '../../assets/warn.json';
import CardFlip from 'react-native-card-flip';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import axios from 'axios';
import moment from 'moment';
import {
  PAGSEGURO_EMAIL,
  PAGSEGURO_TOKEN,
} from '../../services/pagseguroToken';

class PaymentWithBoleto extends Component {
  constructor(props) {
    super(props);

    this.webView = null;

    this.state = {
      appointment: '',
      contract_id: '',
      user: '',
      sessionId: '',
      hash: '',
      barCode: '',
      dueDate: '',
      copiedText: false,
      showCardBackView: false,
      loading: false,
    };
  }

  componentDidMount() {
    this.createSession();

    this.setState({
      appointment: this.props.route?.params?.appointment,
      contract_id: this.props.route?.params?.appointment?.contract?.id,
      barCode: this.props.route?.params?.appointment?.contract?.ipte,
      dueDate: this.props.route?.params?.appointment?.contract
        ?.payment_due_date,
      user: this.props.user.user,
    });
  }

  createSession = async () => {
    try {
      let response = await axios.post(
        `https://ws.pagseguro.uol.com.br/v2/sessions?email=${PAGSEGURO_EMAIL}&token=${PAGSEGURO_TOKEN}`,
      );

      if (response.data) {
        this.setState({
          sessionId: response.data
            .toString()
            .replace(
              '<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?><session><id>',
              '',
            )
            .replace('</id></session>', ''),
        });
      }
    } catch (err) {
      console.log(err);
      this.showPaymentError();
    }
  };

  chargePayment = async () => {
    const { token } = this.props;
    const { hash, user, appointment, contract_id } = this.state;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    console.log('token ' + token);
    console.log('hash ' + hash);
    console.log('reference_id ' + contract_id);

    if (
      user.name == '' ||
      user.email == '' ||
      user.cpf == '' ||
      user.contact == '' ||
      user.birthday == ''
    ) {
      this.showProfileError();
      return;
    }

    const phone = user.contact.replace(/[^0-9]/g, '');

    const data = {
      reference: `${parseInt(contract_id)}`,
      firstDueDate: `${moment()
        .add(3, 'days')
        .format('YYYY-MM-DD')}`,
      numberOfPayments: '1',
      periodicity: 'monthly',
      amount: `${appointment.contract?.professionalPlan?.price}`,
      instructions:
        'Boleto referente à contratação de seu pacote de aulas junto ao Personal Access.',
      description: `${
        appointment.contract?.professionalPlan?.plan?.description
      }`,
      customer: {
        document: {
          type: 'CPF',
          value: `${user.cpf.replace(/[^0-9]/g, '')}`,
        },
        name: `${user.name}`,
        email: `${user.email}`,
        phone: {
          areaCode: `${phone.length > 9 ? phone.substring(0, 2) : phone}`,
          number: `${phone.length > 9 ? phone.slice(2) : phone}`,
        },
        address: {
          postalCode: user.cep ? user.cep.replace(/[^0-9]/g, '') : '23040300',
          street: user.address_street
            ? `${user.address_street}`
            : 'Rua não informada',
          number: user.address_number ? `${user.address_number}` : '1',
          district: user.address_neighborhood
            ? `${user.address_neighborhood}`
            : 'Bairro não informado',
          city: user.address_city
            ? `${user.address_city}`
            : 'Cidade Não informada',
          state: user.address_uf ? `${user.address_uf}` : 'RJ',
        },
      },
    };

    try {
      this.setState({ loading: true });

      const response = await axios.post(
        `https://ws.pagseguro.uol.com.br/recurring-payment/boletos?email=${PAGSEGURO_EMAIL}&token=${PAGSEGURO_TOKEN}`,
        data,
      );

      if (response.status == 201) {
        console.log(response.data);

        await AxiosInstance.put(`contracts/${contract_id}`, {
          method_payment: 'BOLETO',
          ipte: response.data.boletos[0].barcode,
          payment_due_date: response.data.boletos[0].dueDate,
        });

        this.setState({
          loading: false,
          barCode: response.data.boletos[0].barcode,
          dueDate: response.data.boletos[0].dueDate,
        });

        this.showAlert();
      }
    } catch (err) {
      this.setState({ loading: false });
      this.showPaymentError();
      console.log(err);
    }
  };

  copyToClipboard = () => {
    Clipboard.setString(this.state.barCode);

    this.setState({
      copiedText: true,
    });

    setTimeout(
      () =>
        this.setState({
          copiedText: false,
        }),
      3000,
    );
  };

  showPaymentError = () => {
    this.setState({
      showPaymentError: true,
    });

    setTimeout(
      () =>
        this.setState({
          showPaymentError: false,
        }),
      3000,
    ); // hide alert after 4s
  };

  showAlert = () => {
    this.setState({
      showAlert: true,
    });

    setTimeout(
      () =>
        this.setState({
          showAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  showProfileError = () => {
    this.setState({
      showProfileError: true,
    });

    setTimeout(
      () =>
        this.setState({
          showProfileError: false,
        }),
      5000,
    ); // hide alert after 4s
  };

  convertPrice = price => {
    return price.toLocaleString('pt-br', {
      style: 'currency',
      currency: 'BRL',
    });
  };

  render() {
    const {
      sessionId,
      showAlert,
      showProfileError,
      showPaymentError,
      appointment,
      copiedText,
    } = this.state;

    const getSenderHash = `
      PagSeguroDirectPayment.setSessionId('${sessionId}');
      PagSeguroDirectPayment.onSenderHashReady(function(response){
          if(response.status == 'error') {
            window.ReactNativeWebView.postMessage(JSON.stringify({
              message: 'response.message'
            }));
            return false;
          }
          var hash = response.senderHash;
          window.ReactNativeWebView.postMessage(JSON.stringify({
            data: hash,
            message: 'senderHash'
          }));
        });
        `;

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            paddingHorizontal: 30,
            paddingVertical: 20,
            elevation: 3,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Ionicon
                style={{
                  fontWeight: '900',
                  marginRight: 10,
                }}
                name={'ios-arrow-round-back'}
                size={30}
                color={'#37153D'}
              />
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#37153D',
                  textAlign: 'left',
                }}>
                {' '}
                Boleto
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>

        <View>
          <WebView
            source={{ uri: `https://personal-access.herokuapp.com/api/pagseguro` }}
            ref={webView => (this.webView = webView)}
            javaScriptEnabled={true}
            injectedJavaScript={getSenderHash}
            onMessage={async event => {
              let response = JSON.parse(event.nativeEvent.data);

              console.log(response);

              if (response.message == 'senderHash') {
                await this.setState({
                  hash: response.data,
                });
              }
            }}
          />
        </View>

        {this.state.loading == true ? (
          <LoadingModal show={this.state.loading} />
        ) : (
          <KeyboardAvoidingView
            behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
            style={{ flex: 1 }}>
            <ScrollView>
              <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                <View style={styles.container}>
                  <CardFlip
                    style={styles.cardContainer}
                    ref={card => (this.card = card)}>
                    <TouchableOpacity
                      activeOpacity={1}
                      style={[styles.card, styles.card1]}
                      onPress={null}>
                      <Image
                        resizeMode="contain"
                        source={require('../../assets/images/file-white.png')}
                        style={{
                          height: 30,
                          width: 30,
                          marginBottom: 15,
                        }}
                      />

                      <View>
                        <Text style={styles.label}>
                          {
                            appointment?.contract?.professionalPlan?.plan
                              ?.description
                          }{' '}
                          - {appointment?.professional?.user?.name}
                        </Text>
                      </View>

                      <Text style={styles.label}>
                        Confirme os dados abaixo:
                      </Text>

                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          width: 250,
                        }}>
                        <Text style={styles.label}>
                          Valor:{' '}
                          {appointment.contract?.professionalPlan?.price
                            ? this.convertPrice(
                                appointment.contract?.professionalPlan?.price,
                              )
                            : null}
                        </Text>

                        <Text style={styles.label}>
                          Venc:{' '}
                          {moment()
                            .add(3, 'days')
                            .format('DD/MM')}
                        </Text>
                      </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                      activeOpacity={1}
                      style={[styles.card, styles.card2]}
                      onPress={null}
                    />
                  </CardFlip>
                </View>

                {(this.state.hash != '' && this.state.barCode == null) ||
                moment() > moment(this.state.dueDate) ? (
                  <View style={{ margin: 20 }}>
                    <DefaultButton
                      title={'Gerar boleto'}
                      width={200}
                      height={35}
                      onPress={() => this.chargePayment()}
                    />
                  </View>
                ) : null}

                {this.state.hash == '' && this.state.barCode == null ? (
                  <View style={{ margin: 20 }}>
                    <ActivityIndicator color={'#37153D'} />
                  </View>
                ) : null}

                {this.state.barCode != '' &&
                moment() < moment(this.state.dueDate) ? (
                  <View style={{ alignItems: 'center', paddingHorizontal: 40 }}>
                    <Text
                      style={{
                        color: '#000',
                        fontSize: 20,
                        fontWeight: 'bold',
                      }}>
                      {appointment.contract?.professionalPlan?.price
                        ? this.convertPrice(
                            appointment.contract?.professionalPlan?.price,
                          )
                        : null}
                    </Text>

                    <Text
                      style={{
                        color: '#999',
                        fontSize: 14,
                        marginVertical: 5,
                      }}>
                      VENCIMENTO{' '}
                      {moment(this.state.dueDate)
                        .add('3', 'hours')
                        .format('DD-MM')}
                    </Text>

                    <Text
                      style={{
                        color: '#000',
                        fontSize: 14,
                        marginVertical: 10,
                        textAlign: 'center',
                      }}>
                      Utilize o número do código de barras abaixo para realizar
                      o pagamento do boleto referente ao seu pacote de aulas:
                    </Text>

                    <Text
                      style={{
                        color: '#000',
                        fontSize: 16,
                        marginVertical: 10,
                        textAlign: 'center',
                      }}>
                      {this.state.barCode}
                    </Text>

                    <View style={{ margin: 20 }}>
                      <DefaultButton
                        title={'Copiar código'}
                        width={200}
                        height={35}
                        onPress={() => this.copyToClipboard()}
                      />
                    </View>
                  </View>
                ) : null}
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        )}

        <AwesomeAlert
          show={showProfileError}
          showProgress={false}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          showConfirmButton={true}
          confirmText="Editar perfil"
          onConfirmPressed={() => this.props.navigation.navigate('MyProfile')}
          confirmButtonStyle={{
            width: 200,
            height: 40,
            backgroundColor: '#37153D',
            borderRadius: 25,
            marginBottom: 20,
          }}
          confirmButtonTextStyle={{
            textAlign: 'center',
            fontFamily: 'SofiaPro-Medium',
            letterSpacing: 0,
            color: '#fff',
            fontSize: 15,
          }}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Oops! Seus dados de perfil estão incompletos. Por favor, volte e
                preencha todas as suas informações.
              </Text>
              <LottieView
                style={{ height: 70 }}
                resizeMode="contain"
                autoSize
                source={animationError}
                autoPlay
                loop={false}
              />
            </View>
          }
        />

        <AwesomeAlert
          show={showPaymentError}
          showProgress={false}
          closeOnHardwareBackPress={false}
          onDismiss={() => this.props.navigation.goBack()}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Oops! Algo de errado aconteceu, tente novamente.
              </Text>
              <LottieView
                style={{ height: 70 }}
                resizeMode="contain"
                autoSize
                source={animationError}
                autoPlay
                loop={false}
              />
            </View>
          }
        />

        <AwesomeAlert
          show={copiedText}
          showProgress={false}
          closeOnHardwareBackPress={false}
          onDismiss={() => this.props.navigation.navigate('MyClasses')}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Código copiado com sucesso!
              </Text>
              <LottieView
                style={{ width: 70, height: 70 }}
                resizeMode="contain"
                autoSize
                source={animation}
                autoPlay
                loop={false}
              />
            </View>
          }
        />

        <AwesomeAlert
          show={showAlert}
          showProgress={false}
          closeOnHardwareBackPress={false}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Boleto gerado com sucesso!
              </Text>
              <LottieView
                style={{ width: 70, height: 70 }}
                resizeMode="contain"
                autoSize
                source={animation}
                autoPlay
                loop={false}
              />
            </View>
          }
        />
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F9F9F9',
    marginVertical: 30,
  },
  cardContainer: {
    width: 318,
    height: 200,
    backgroundColor: '#F9F9F9',
  },
  card: {
    width: 318,
    height: 200,
    backgroundColor: '#FE474C',
    borderRadius: 5,
    shadowColor: 'rgba(0,0,0,0.5)',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.5,
  },
  card1: {
    backgroundColor: '#37153D',
    justifyContent: 'center',
    alignItems: 'flex-start',
    padding: 30,
  },
  card2: {
    backgroundColor: '#37153D',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  label: {
    fontSize: 15,
    color: '#fff',
    marginBottom: 10,
  },
});

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(PaymentWithBoleto);
