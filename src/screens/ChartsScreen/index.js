import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Dimensions,
  TouchableWithoutFeedback,
} from 'react-native';

import { LineChart } from 'react-native-chart-kit';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { SafeAreaView } from 'react-native-safe-area-context';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';

class ChartsScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      state: '',
      district: { name: '', id: '' },
      neighbourhood: { name: '', id: '' },
      optionsState: [],
      optionsDistrict: [],
      optionsNeighbourhood: [],
      plan: { name: '', id: '' },
      optionsPlans: [
        { name: 'Aula avulsa', id: '' },
        { name: 'Pacote de aulas', id: '' },
      ],
    };
  }

  screenTitle = id_screen => {
    switch (id_screen) {
      case 1:
        return ' Preço médio cobrado por bairro';
      case 2:
        return 'Número médio de solicitações por bairro';
      case 3:
        return 'Número de rejeições por bairro';
      case 4:
        return 'Razão entre profissionais e alunos';
    }
  };

  componentDidMount() {
    console.log(this.props.route.params.statistics);
  }

  render() {
    const { id_screen } = this.props.route.params;
    const data = this.props.route.params.statistics;
    const address = this.props.route.params.fullAddress;

    const chartConfig = {
      backgroundGradientFrom: 'rgba(248, 248, 248, 1)',
      backgroundGradientTo: 'rgba(248, 248, 248, 1)',
      decimalPlaces: 2,
      color: (opacity = 1) => `rgba(55, 21, 61, ${opacity})`,
      labelColor: (opacity = 1) => `rgba(112, 112, 112, ${opacity})`,
      style: {
        borderRadius: 0,
      },
      propsForBackgroundLines: {
        strokeWidth: 0.5,
      },
      fillShadowGradient: '#FF993E',
      fillShadowGradientOpacity: 1,
      propsForLabels: {
        fontSize: 11,
        fontFamily: 'SofiaPro-Medium',
        fill: '#37153D',
      },
      strokeWidth: 0.1,
      propsForDots: {
        r: '7',
        strokeWidth: '4',
        stroke: 'rgba(248, 248, 248, 1)',
      },
    };

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            padding: 20,
            elevation: 3,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Ionicon
                style={{
                  fontWeight: '900',
                  marginRight: 10,
                }}
                name={'ios-arrow-round-back'}
                size={30}
                color={'#37153D'}
              />
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#37153D',
                  textAlign: 'left',
                }}>{`Preço médio cobrado por bairro`}</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <View
          style={{ backgroundColor: 'rgba(248, 248, 248, 1)', height: '100%' }}>
          <ScrollView>
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'center',
                backgroundColor: 'rgba(248, 248, 248, 1)',
              }}>
              {id_screen === 1 ? (
                <>
                  <Text
                    style={{
                      fontSize: 18,
                      fontFamily: 'SofiaPro-Medium',
                      color: '#37153D',
                      marginTop: 30,
                    }}>
                    {address.neighbourhood ? `${address.neighbourhood} - ` : ''}{' '}
                    {address.district}
                  </Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginVertical: 5,
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontSize: 16,
                        fontFamily: 'SofiaPro-Regular',
                        color: '#37153D',
                      }}>
                      Pacote:{' '}
                    </Text>
                    <Text
                      style={{
                        fontSize: 16,
                        fontFamily: 'SofiaProLight',
                        color: '#37153D',
                      }}>
                      {address.plan}
                    </Text>
                  </View>

                  <LineChart
                    data={{
                      labels: [
                        'Menor valor',
                        'Valor médio',
                        'Maior valor',
                        'Seu valor',
                      ],
                      datasets: [
                        {
                          data: [data?.min, data?.avg, data?.max, data?.price],
                        },
                      ],
                    }}
                    width={Dimensions.get('window').width - 50} // from react-native
                    height={300}
                    yAxisLabel="R$"
                    xLabelsOffset={5}
                    yLabelsOffset={15}
                    label
                    chartConfig={chartConfig}
                    bezier
                    style={{
                      marginVertical: 50,
                      borderRadius: 0,
                    }}
                  />
                </>
              ) : (
                <>
                  <Text
                    style={{
                      fontSize: 18,
                      fontFamily: 'SofiaPro-Medium',
                      color: '#37153D',
                      marginTop: 20,
                    }}>
                    Tijuca - Rio de Janeiro
                  </Text>
                  <View style={{ flexDirection: 'row', marginBottom: 20 }}>
                    <Text
                      style={{
                        fontSize: 16,
                        fontFamily: 'SofiaPro-Regular',
                        color: '#37153D',
                      }}>
                      Pacote:{' '}
                    </Text>
                    <Text
                      style={{
                        fontSize: 16,
                        fontFamily: 'SofiaProLight',
                        color: '#37153D',
                      }}>
                      aula avulsa
                    </Text>
                  </View>

                  <LineChart
                    data={{
                      labels: ['300', '400', '600', '700', '800'],
                      datasets: [
                        {
                          data: ['', '', '', '', 400],
                        },
                      ],
                    }}
                    width={Dimensions.get('window').width - 20}
                    height={220}
                    yAxisInterval={5}
                    fromZero={true}
                    chartConfig={{
                      backgroundGradientFrom: '#1E2923',
                      backgroundGradientFromOpacity: 0,
                      backgroundGradientTo: '#08130D',
                      backgroundGradientToOpacity: 0.0,
                      color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                      decimalPlaces: 0,
                      fillShadowGradient: '#FF993E',
                      fillShadowGradientOpacity: 0,
                      strokeWidth: 0.1,
                      barPercentage: 0.5,
                      useShadowColorFromDataset: false,
                      propsForLabels: {
                        fontSize: 13,
                        fontFamily: 'SofiaPro-Medium',
                        fill: '#37153D',
                      },
                      propsForDots: {
                        r: '7',
                        strokeWidth: '4',
                        stroke: 'rgba(248, 248, 248, 1)',
                      },
                    }}
                    style={{
                      marginVertical: 8,
                      borderRadius: 0,
                    }}
                  />
                </>
              )}
            </View>
          </ScrollView>
        </View>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(ChartsScreen);
