import React, { Component } from 'react';
import {
  View,
  Button,
  Text,
  ScrollView,
  Modal,
  TextInput,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';

import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import API, { AxiosWithHeaders } from '../../services/api';

class PaymentListScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      bank_type: this.props.user.bank_type ? this.props.user.bank_type : '',
      bank_agency: this.props.user.bank_agency
        ? this.props.user.bank_agency
        : '',
      bank_first_number: this.props.user.bank_number
        ? this.props.user.bank_number.split('-')[0]
        : '',
      bank_number: this.props.user.bank_number
        ? this.props.user.bank_number
        : '',
      bank_name: this.props.user.bank_name ? this.props.user.bank_name : '',
      bank_last_number: this.props.user.bank_number
        ? this.props.user.bank_number.split('-')[1]
        : '',
    };
  }

  saveBankData = async () => {
    const { id } = this.props.user;
    const { user, token } = this.props;
    const { bank_type, bank_agency, bank_name, bank_number } = this.state;

    const AxiosInstace = AxiosWithHeaders({ Authorization: `Bearer ${token}` });

    try {
      const response = await AxiosInstace.post(`professionals/${id}/account`, {
        bank_type,
        bank_agency,
        bank_name,
        bank_number,
      });

      user.bank_type = await response.data.data.bank_type;
      user.bank_agency = await response.data.data.bank_agency;
      user.bank_name = await response.data.data.bank_name;
      user.bank_number = await response.data.data.bank_number;

      console.log(user.bank_type);
      console.log(user.bank_agency);
      console.log(user.bank_name);
      console.log(user.bank_number);

      this.props.setUser(user);

      this.props.navigation.navigate('ProfileProfessionalScreen');
    } catch (err) {
      console.log(err.response);
    }
  };

  render() {
    return (
      <>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            padding: 20,
            elevation: 3,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <Ionicon
              style={{
                fontWeight: '900',
                marginRight: 10,
              }}
              name={'ios-arrow-round-back'}
              size={30}
              color={'#37153D'}
            />
          </TouchableWithoutFeedback>
          <Text
            style={{
              fontFamily: 'SofiaPro-Medium',
              fontSize: 14,
              color: '#37153D',
              textAlign: 'left',
            }}>
            {' '}
            Pagamento
          </Text>
        </View>

        <ScrollView>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
              padding: 0,
            }}>
            <Text
              style={{
                fontFamily: 'SofiaPro-Medium',
                textAlign: 'center',
                fontSize: 18,
                margin: 10,
                color: '#37153D',
              }}>
              Formas de pagamento
            </Text>
            <PaymentItem cardInfo={'•••• •••• •••• 9999'} />
            <AddNewPaymentItem
              onPress={() => this.props.navigation.navigate('AddPaymentScreen')}
            />
          </View>
        </ScrollView>
      </>
    );
  }
}

const PaymentItem = ({ cardInfo }) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'flex-start',
        padding: 20,
        paddingLeft: 40,
        alignItems: 'center',
        borderBottomColor: '#DEDEDE',
        borderBottomWidth: 2,
      }}>
      <Image
        style={{ width: 50, height: 50, resizeMode: 'contain' }}
        source={require('../../assets/images/credicard2.png')}
      />
      <Text
        style={{
          fontFamily: 'SofiaPro-Regular',
          fontSize: 14,
          margin: 10,
          marginLeft: 20,
          color: '#2E2E2E',
        }}>
        {cardInfo}
      </Text>
    </View>
  );
};

const AddNewPaymentItem = ({ onPress }) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View
        style={{
          flexDirection: 'row',
          width: '100%',
          justifyContent: 'flex-start',
          padding: 20,
          paddingLeft: 40,
          alignItems: 'center',
          borderBottomColor: '#DEDEDE',
          borderBottomWidth: 2,
        }}>
        <Ionicon
          style={{
            fontWeight: '900',
            marginRight: 10,
          }}
          name={'ios-add'}
          size={30}
          color={'#F76E1E'}
        />
        <Text
          style={{
            fontFamily: 'SofiaPro-Regular',
            fontSize: 12,
            margin: 10,
            color: '#2E2E2E',
          }}>
          Adicionar forma de pagamento
        </Text>
      </View>
    </TouchableWithoutFeedback>
  );
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(PaymentListScreen);
