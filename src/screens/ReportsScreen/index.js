import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import { Picker } from '@react-native-community/picker';
import { DefaultButton } from '../../components';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { AxiosWithHeaders } from '../../services/api';
import { SafeAreaView } from 'react-native-safe-area-context';
import RNPickerSelect from 'react-native-picker-select';
import converStates from '../../services/convertStates';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import axios from 'axios';

class ReportsScreen extends Component {
  constructor(props) {
    super(props);

    this.getStates();

    this.state = {
      optionsPlans: [],
      optionsState: [],
      optionsDistrict: [],
      optionsNeighbourhood: [],
      plan: '',
      plan_id: '',
      state: '',
      district: '',
      neighbourhood: '',
      resultText: '',
      fullAddress: [],
    };
  }

  getStates = async () => {
    let auxStates = [];
    let ibge = axios.create({
      baseURL: `https://servicodados.ibge.gov.br/api/v1/localidades/estados?orderBy=nome`,
    });

    try {
      const response = await ibge.get();
      let { data } = response;

      data.map(item => {
        return auxStates.push({ name: item.nome, id: item.sigla });
      });

      this.setState({ optionsState: auxStates });
    } catch (err) {
      console.log(err.response);
    }
  };

  getDistricts = async state => {
    let auxDistricts = [];
    let ibge = axios.create({
      baseURL: `https://servicodados.ibge.gov.br/api/v1/localidades/estados/${state}/municipios?orderBy=nome`,
    });

    try {
      const response = await ibge.get(``);

      let { data } = response;

      data.map(item => {
        return auxDistricts.push({ name: item.nome, id: item.id });
      });

      this.setState({ optionsDistrict: auxDistricts });
    } catch (err) {
      console.log(err.response);
    }
  };

  getNeighbourhoods = async district => {
    let auxneighbourhood = [];
    let ibge = axios.create({
      baseURL: `https://servicodados.ibge.gov.br/api/v1/localidades/municipios/${district}/subdistritos?orderBy=nome`,
    });

    try {
      const response = await ibge.get(``);

      let { data } = response;

      data.map(item => {
        return auxneighbourhood.push({ name: item.nome, id: item.id });
      });

      await this.setState({ optionsNeighbourhood: auxneighbourhood });
    } catch (err) {
      console.log(err.response);
    }
  };

  saveLocation = async () => {
    const { token } = this.props;
    const { state, district, neighbourhood, plan_id, plan } = this.state;
    const { id_screen } = this.props.route.params;

    this.setState({
      fullAddress: {
        state: converStates(state),
        district: district.name,
        neighbourhood: neighbourhood?.name,
        plan: plan_id > 1 ? 'Pacote de aulas' : 'Aula avulsa',
      },
    });

    const AxiosInstace = AxiosWithHeaders({ Authorization: `Bearer ${token}` });

    try {
      const response = await AxiosInstace.post('/reports/average', {
        state: converStates(state),
        district: district.name,
        neighborhood: neighbourhood?.name,
        plan_id: plan_id,
      });

      console.log(response.data);

      if (response.data.avg === null) {
        Alert.alert('Não existem dados para este endereço no momento.');
      } else {
        this.props.navigation.navigate('ChartsScreen', {
          id_screen: id_screen,
          statistics: response.data,
          fullAddress: this.state.fullAddress,
        });
      }
    } catch (err) {
      console.log(err.response);
    }
  };

  screenTitle = id_screen => {
    switch (id_screen) {
      case 1:
        return ' Preço médio cobrado por bairro';
      case 2:
        return 'Número médio de solicitações por bairro';
      case 3:
        return 'Número de rejeições por bairro';
      case 4:
        return 'Razão entre profissionais e alunos';
    }
  };

  render() {
    const { id_screen } = this.props.route.params;
    const { state, district, neighbourhood, plan_id } = this.state;

    const style = {
      inputAndroid: {
        width: 280,
        height: 40,
        color: '#000',
        textAlign: 'center',
        fontFamily: 'SofiaProLight',
        fontSize: 18,
        margin: 10,
      },
      inputIOS: {
        width: 280,
        height: 35,
        color: '#000',
        textAlign: 'center',
        fontFamily: 'SofiaProLight',
        fontSize: 18,
        margin: 10,
      },
    };

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            padding: 20,
            elevation: 3,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Ionicon
                style={{
                  fontWeight: '900',
                  marginRight: 10,
                }}
                name={'ios-arrow-round-back'}
                size={30}
                color={'#37153D'}
              />
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#37153D',
                  textAlign: 'left',
                }}>
                {this.screenTitle(id_screen)}
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <ScrollView>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
              padding: 10,
            }}>
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'flex-start',
                marginTop: 30,
                marginBottom: 15,
              }}>
              {id_screen === 1 ? (
                <View
                  style={{
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    marginBottom: 15,
                  }}>
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      fontSize: 14,
                      color: '#2E2E2E',
                      textAlign: 'left',
                      marginLeft: 10,
                      padding: 5,
                    }}>
                    Pacote:
                  </Text>

                  <View
                    style={{
                      backgroundColor: '#fff',
                      flexDirection: 'row',
                      justifyContent: 'center',
                      borderRadius: 50,
                    }}>
                    <RNPickerSelect
                      style={style}
                      placeholder={{
                        label: 'Selecionar pacote',
                      }}
                      doneText={'Selecionar'}
                      useNativeAndroidPickerStyle={false}
                      onValueChange={async (itemValue, itemIndex) => {
                        await this.setState({ plan_id: itemValue });
                      }}
                      value={this.state.plan_id}
                      items={[
                        {
                          label: 'Aula avulsa',
                          value: 1,
                          key: 1,
                        },
                        {
                          label: '8 aulas no mês',
                          value: 3,
                          key: 3,
                        },
                        {
                          label: '10 aulas no mês',
                          value: 16,
                          key: 16,
                        },
                        {
                          label: '12 aulas no mês',
                          value: 4,
                          key: 4,
                        },
                        {
                          label: '15 aulas no mês',
                          value: 15,
                          key: 15,
                        },
                        {
                          label: '16 aulas no mês',
                          value: 5,
                          key: 5,
                        },
                        {
                          label: '20 aulas no mês',
                          value: 6,
                          key: 6,
                        },
                        {
                          label: '24 aulas no mês',
                          value: 7,
                          key: 7,
                        },
                      ]}
                    />
                  </View>
                </View>
              ) : null}

              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#2E2E2E',
                  textAlign: 'left',
                  marginLeft: 10,
                  padding: 5,
                }}>
                Estado:
              </Text>

              <View
                style={{
                  backgroundColor: '#fff',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  borderRadius: 50,
                }}>
                <RNPickerSelect
                  style={style}
                  placeholder={{
                    label: 'Selecionar estado',
                  }}
                  doneText={'Selecionar'}
                  useNativeAndroidPickerStyle={false}
                  onValueChange={async (itemValue, itemIndex) => {
                    await this.setState({ state: itemValue });
                    await this.getDistricts(itemValue);
                  }}
                  value={this.state.state}
                  items={this.state.optionsState.map(item => {
                    return {
                      label: item.name,
                      value: item.id,
                      key: item.id,
                    };
                  })}
                />
              </View>
            </View>

            <View
              style={{
                flexDirection: 'column',
                alignItems: 'flex-start',
                marginBottom: 15,
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#2E2E2E',
                  textAlign: 'left',
                  marginLeft: 10,
                  padding: 5,
                }}>
                Cidade:
              </Text>

              <View
                style={{
                  backgroundColor: '#fff',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  borderRadius: 50,
                }}>
                <RNPickerSelect
                  style={style}
                  placeholder={{
                    label: 'Selecionar cidade',
                  }}
                  doneText={'Selecionar'}
                  useNativeAndroidPickerStyle={false}
                  onValueChange={async (itemValue, itemIndex) => {
                    await this.setState({ district: itemValue });
                    await this.getNeighbourhoods(itemValue.id);
                  }}
                  value={this.state.district}
                  items={this.state.optionsDistrict.map(item => {
                    return {
                      key: item.id,
                      label: item.name,
                      value: item,
                    };
                  })}
                />
              </View>
            </View>

            <View
              style={{
                flexDirection: 'column',
                alignItems: 'flex-start',
                marginBottom: 15,
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#2E2E2E',
                  textAlign: 'left',
                  marginLeft: 10,
                  padding: 5,
                }}>
                Bairro:
              </Text>

              <View
                style={{
                  backgroundColor: '#fff',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  borderRadius: 50,
                }}>
                <RNPickerSelect
                  style={style}
                  placeholder={{
                    label: 'Selecionar bairro',
                  }}
                  doneText={'Selecionar'}
                  useNativeAndroidPickerStyle={false}
                  onValueChange={async (itemValue, itemIndex) => {
                    await this.setState({ neighbourhood: itemValue });
                  }}
                  value={this.state.neighbourhood}
                  items={this.state.optionsNeighbourhood.map(item => {
                    return {
                      key: item.id,
                      label: item.name,
                      value: item,
                    };
                  })}
                />
              </View>
            </View>

            {district != '' ? (
              <DefaultButton
                title={'Continuar'}
                width={180}
                height={35}
                onPress={async () => {
                  switch (id_screen) {
                    case 1:
                      this.saveLocation();
                      break;
                    case 2:
                      await this.setState({ resultText: '15' });
                      break;
                    case 3:
                      await this.setState({ resultText: '15' });
                      break;
                    case 4:
                      this.props.navigation.navigate('ChartsScreen', {
                        id_screen: id_screen,
                      });
                      break;
                  }
                }}
              />
            ) : null}

            {id_screen === 3 && this.state.resultText > '' ? (
              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'center',
                  margin: 20,
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Regular',
                    fontSize: 14,
                    color: '#707070',
                  }}>
                  Número de rejeições no bairro Tijuca:
                </Text>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 21,
                    color: '#F76E1E',
                  }}>{`${this.state.resultText} rejeições`}</Text>
              </View>
            ) : null}

            {id_screen === 2 && this.state.resultText > '' ? (
              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'center',
                  margin: 20,
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Regular',
                    fontSize: 14,
                    color: '#707070',
                  }}>
                  Número de solicitações no bairro Tijuca:
                </Text>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 21,
                    color: '#F76E1E',
                  }}>{`${this.state.resultText} rejeições`}</Text>
              </View>
            ) : null}
          </View>
        </ScrollView>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(ReportsScreen);
