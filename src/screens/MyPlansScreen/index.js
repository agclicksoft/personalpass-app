import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Modal,
  Image,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { AxiosWithHeaders } from '../../services/api';
import { DefaultButton } from '../../components';
import { TextInputMask } from 'react-native-masked-text';
import { SafeAreaView } from 'react-native-safe-area-context';
import { showMessage } from 'react-native-flash-message';
import RNPickerSelect from 'react-native-picker-select';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import AwesomeAlert from 'react-native-awesome-alerts';
import LottieView from 'lottie-react-native';
import animation from '../../assets/success.json';

class MyPlansScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      plan: 1,
      plan_id: null,
      price: '',
      modal: false,
    };
  }

  getPlan = id => {
    switch (id) {
      case 1:
        return 'Aula avulsa';
      case 2:
        return 'Aula avulsa em dupla';
      case 3:
        return 'Pacote de aulas';
      case 4:
        return 'Pacote de aula em dupla';
    }
  };

  showModal = () => {
    this.setState({ modal: !this.state.modal });
  };

  showType = () => {
    return this.state.plan > 2;
  };

  savePlan = async () => {
    const { user, token } = this.props;

    const { plan, plan_id, price } = this.state;

    const AxiosInstace = AxiosWithHeaders({ Authorization: `Bearer ${token}` });

    if (String(price).charAt(0) != 0) {
      try {
        await AxiosInstace.post(`/professionals/plans`, {
          plan: {
            plan_id: plan_id != null ? plan_id : plan,
            price: price,
          },
        });

        this.props.setUser(user);

        this.showModal();
        this.showAlert();
      } catch (err) {
        console.log(err);
        console.log(err.response);
        this.showModal();
        showMessage({
          type: 'danger',
          message: 'Erro inesperado. Tente novamente',
          titleStyle: {
            textAlign: 'center',
          },
        });
      }
    } else {
      Alert.alert('Valor mínimo permitido de R$ 1,00');
    }
  };

  showAlert = () => {
    this.setState({
      showAlert: true,
    });

    setTimeout(
      () =>
        this.setState({
          showAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  render() {
    const { showAlert } = this.state;

    const style = {
      inputAndroid: {
        width: 330,
        height: 40,
        color: '#636363',
        textAlign: 'center',
        fontFamily: 'SofiaPro-Medium',
        fontSize: 20,
        margin: 10,
      },
      inputIOS: {
        width: 330,
        height: 35,
        color: '#636363',
        textAlign: 'center',
        fontFamily: 'SofiaPro-Medium',
        fontSize: 20,
        margin: 10,
      },
    };

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: 20,
            elevation: 3,
          }}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <TouchableWithoutFeedback
              onPress={async () => {
                this.props.navigation.goBack();
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Ionicon
                  style={{
                    fontWeight: '900',
                    marginRight: 10,
                  }}
                  name={'ios-arrow-round-back'}
                  size={30}
                  color={'#37153D'}
                />
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 14,
                    color: '#37153D',
                    textAlign: 'left',
                  }}>
                  Pacote de aulas
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>

        <ScrollView>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'flex-start',
                margin: 20,
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#2E2E2E',
                  textAlign: 'left',
                  marginLeft: 15,
                  margin: 5,
                }}>
                Selecione seu pacote:
              </Text>

              <View
                style={{
                  backgroundColor: '#fff',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  borderRadius: 50,
                }}>
                <RNPickerSelect
                  style={style}
                  placeholder={{
                    label: '',
                  }}
                  doneText={'Selecionar'}
                  useNativeAndroidPickerStyle={false}
                  onValueChange={(itemValue, itemIndex) => {
                    this.setState({ plan: itemValue });
                  }}
                  value={this.state.plan}
                  items={[
                    {
                      label: 'Aula avulsa',
                      value: 1,
                      key: 1,
                    },
                    {
                      label: 'Pacote de aulas',
                      value: 3,
                      key: 3,
                    },
                  ]}
                />
              </View>
            </View>
          </View>

          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <DefaultButton
              title={'Selecionar'}
              width={200}
              onPress={this.state.plan > '' ? this.showModal : null}
            />
          </View>
        </ScrollView>

        <Modal visible={this.state.modal} transparent={true}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              padding: 20,
              justifyContent: 'center',
              backgroundColor: 'rgba(0, 0, 0, 0.7)',
            }}>
            <View
              style={{
                flexDirection: 'column',
                padding: 20,
                justifyContent: 'center',
                borderRadius: 20,
                elevation: 2,
                backgroundColor: '#F8F8F8',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 15,
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 22,
                    textAlign: 'center',
                    flexWrap: 'wrap',
                    color: '#37153D',
                  }}>
                  {this.getPlan(this.state.plan)}
                </Text>
              </View>

              {this.showType() ? (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    marginTop: 30,
                  }}>
                  <View
                    style={{
                      flexDirection: 'column',
                      alignItems: 'flex-start',
                    }}>
                    <Text
                      style={{
                        fontFamily: 'SofiaPro-Medium',
                        fontSize: 14,
                        color: '#2E2E2E',
                        textAlign: 'left',
                        marginLeft: 30,
                        margin: 0,
                      }}>
                      Tipo:
                    </Text>
                    <View
                      style={{
                        fontFamily: 'SofiaPro-Regular',
                        fontSize: 14,
                        textAlign: 'center',
                        backgroundColor: '#fff',
                        color: '#636363',
                        borderWidth: 0.1,
                        borderColor: '#707070',
                        borderRadius: 50,
                        margin: 10,
                        width: 330,
                      }}>
                      <RNPickerSelect
                        style={style}
                        placeholder={{
                          label: 'Selecionar pacote',
                        }}
                        doneText={'Selecionar'}
                        useNativeAndroidPickerStyle={false}
                        onValueChange={async (itemValue, itemIndex) => {
                          await this.setState({ plan_id: itemValue });
                        }}
                        value={this.state.plan_id}
                        items={[
                          {
                            label: '8 aulas no mês',
                            value: 3,
                            key: 3,
                          },
                          {
                            label: '10 aulas no mês',
                            value: 16,
                            key: 16,
                          },
                          {
                            label: '12 aulas no mês',
                            value: 4,
                            key: 4,
                          },
                          {
                            label: '15 aulas no mês',
                            value: 15,
                            key: 15,
                          },
                          {
                            label: '16 aulas no mês',
                            value: 5,
                            key: 5,
                          },
                          {
                            label: '20 aulas no mês',
                            value: 6,
                            key: 6,
                          },
                          {
                            label: '24 aulas no mês',
                            value: 7,
                            key: 7,
                          },
                        ]}
                      />
                    </View>
                  </View>
                </View>
              ) : null}

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 30,
                }}>
                <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      fontSize: 14,
                      color: '#2E2E2E',
                      textAlign: 'left',
                      marginLeft: 30,
                      margin: 0,
                      alignSelf: 'flex-start',
                    }}>
                    Preço:
                  </Text>

                  <TextInputMask
                    type={'money'}
                    value={this.state.price}
                    keyboardType={'decimal-pad'}
                    maxLength={10}
                    style={{
                      fontFamily: 'SofiaPro-Regular',
                      fontSize: 20,
                      textAlign: 'center',
                      backgroundColor: '#fff',
                      color: '#636363',
                      borderWidth: 0.1,
                      borderColor: '#707070',
                      borderRadius: 50,
                      margin: 10,
                      width: 330,
                      padding: 10,
                    }}
                    includeRawValueInChangeText={true}
                    onChangeText={(maskedText, rawText) => {
                      this.setState({
                        price: rawText,
                      });
                    }}
                  />
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 10,
                  elevation: 1,
                }}>
                <DefaultButton
                  title={'Salvar'}
                  onPress={() => {
                    this.savePlan();
                  }}
                  width={200}
                  height={40}
                />
              </View>

              <TouchableWithoutFeedback
                onPress={() => {
                  this.setState({
                    price: '',
                  });
                  this.showModal();
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    marginBottom: 20,
                    marginTop: 5,
                  }}>
                  <Text style={{ fontFamily: 'SofiaProLight', fontSize: 12 }}>
                    Cancelar
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
        </Modal>

        <AwesomeAlert
          show={showAlert}
          showProgress={false}
          title="Pacote criado com sucesso!"
          closeOnHardwareBackPress={false}
          onDismiss={async () => {
            this.props.navigation.goBack();
          }}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <LottieView
              style={{ width: 70, height: 70 }}
              resizeMode="contain"
              autoSize
              source={animation}
              autoPlay
              loop={false}
            />
          }
        />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(MyPlansScreen);
