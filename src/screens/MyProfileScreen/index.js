import React, { Component } from 'react';
import { View, Text, ScrollView, Image } from 'react-native';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { ProfileItem } from '../../components';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { AxiosWithHeaders } from '../../services/api';
import { SafeAreaView } from 'react-native-safe-area-context';
import AsyncStorage from '@react-native-community/async-storage';

class MyProfileScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: this.props.user?.user,
      img_profile:
        'https://static.scrum.org/web/images/profile-placeholder.png',
    };
  }

  componentDidMount() {
    setTimeout(() => this.loadUser(this.props.user?.id), 2000);

    this.focusListener = this.props.navigation.addListener('focus', () =>
      this.loadUser(this.props.user?.id),
    );
  }

  componentWillUnmount() {
    this.focusListener();
  }

  loadUser = async id => {
    const { user, token } = this.props;
    const { img_profile } = this.state;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(`/students/${id}`)
      .then(async response => {
        await this.setState({
          user: response.data,
          img_profile:
            response.data.user.img_profile !== null
              ? `${response.data.user.img_profile}?random=${Math.random()
                  .toString(36)
                  .substring(7)}`
              : `https://static.scrum.org/web/images/profile-placeholder.png`,
        });
      })
      .catch(err => {
        console.log(err);
      });

    user.user.img_profile = img_profile;
    this.props.setUser(user);
  };

  render() {
    const { user, img_profile } = this.state;

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            paddingHorizontal: 30,
            paddingVertical: 20,
            elevation: 3,
          }}>
          <Text
            style={{
              fontFamily: 'SofiaPro-Medium',
              fontSize: 18,
              color: '#37153D',
              textAlign: 'left',
            }}>
            Meu perfil
          </Text>
        </View>
        <ScrollView>
          <View>
            <View
              style={{
                flexDirection: 'row',
                padding: 30,
                backgroundColor: '#F8F8F8',
                elevation: 2,
              }}>
              <Image
                style={{ height: 115, width: 115, borderRadius: 135 }}
                source={{
                  uri: img_profile
                    ? img_profile
                    : 'https://static.scrum.org/web/images/profile-placeholder.png',
                }}
              />
              <View
                style={{
                  flexDirection: 'column',
                  justifyContent: 'center',
                  paddingLeft: 20,
                  flexShrink: 1,
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 20,
                    flexWrap: 'wrap',
                    color: '#37153D',
                  }}>
                  {user?.user?.name}
                </Text>
                <TouchableWithoutFeedback
                  onPress={() =>
                    this.props.navigation.navigate('EditStudentScreen', {
                      student: user,
                      img_profile: img_profile,
                    })
                  }>
                  <Text style={{ fontFamily: 'SofiaProLight', fontSize: 12 }}>
                    Editar perfil
                  </Text>
                </TouchableWithoutFeedback>
              </View>
            </View>

            <ProfileItem
              name={'Perfil público'}
              description={'Visualizar seu perfil público'}
              icon={require('../../assets/images/perfil-publico.png')}
              onPress={() => {
                this.props.navigation.navigate('StudentProfileScreen', {
                  student: user,
                  img_profile: img_profile,
                });
              }}
            />

            <ProfileItem
              onPress={() => {
                this.props.navigation.navigate('PARQScreen');
              }}
              name={'PAR-Q'}
              description={'Questionário de Prontidão para Atividade Física'}
              icon={require('../../assets/images/cardiology.png')}
            />

            <ProfileItem
              name={'Histórico de pagamentos'}
              onPress={() => {
                // this.props.navigation.navigate('PaymentListScreen');
                null;
              }}
              description={'Visualizar aulas pagas'}
              icon={require('../../assets/images/em_breve.png')}
            />

            {/* <ProfileItem
              name={'Anexos'}
              description={'Adicionar anexos'}
              icon={require('../../assets/images/icon.png')}
              onPress={() => {
                this.props.navigation.navigate('AttachmentsScreen');
              }}
            /> */}

            <ProfileItem
              onPress={async () => {
                await AsyncStorage.removeItem('DATA_KEY');
                await AsyncStorage.removeItem('REFRESH_TOKEN');
                this.props.clearUserInfo();
                this.props.navigation.navigate('AccessScreen');
              }}
              name={'Sair'}
              description={'Sair da sua conta'}
              icon={require('../../assets/images/off-icon.png')}
            />
          </View>
        </ScrollView>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(MyProfileScreen);
