import React, { Component } from 'react';
import {
  View,
  Button,
  Text,
  ScrollView,
  Modal,
  TextInput,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';
import FilePickerManager from 'react-native-file-picker';
import DocumentPicker from 'react-native-document-picker';
import axios from 'axios';

import {
  DefaultButton,
  DefaultInput,
  PARQQuestion,
  ProfileItem,
} from '../../components';

import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import API, { AxiosWithHeaders } from '../../services/api';
import Axios from 'axios';
import { BASE_URL } from '../../services/baseUrl';

class AttachmentsScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      attachment: this.props.user?.user?.attachments
        ? this.props.user.user.attachments
        : [],
      file: '',
    };
  }

  componentDidMount() {
    console.log(this.props.user?.user?.attachments);
  }

  loadAttachment = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      this.setState({
        file: res,
      });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        console.log('Picker canceled.');
      } else {
        throw err;
      }
    }
  };

  saveAttachment = async () => {
    const { user, token } = this.props;

    const data = new FormData();

    data.append('attachments', {
      name: this.state.file.fileName,
      type: this.state.file.type,
      uri:
        Platform.OS === 'android'
          ? this.state.file.uri
          : this.state.file.uri.replace('file://', ''),
    });

    try {
      axios({
        url: `${BASE_URL}users/${user.user.id}/attachments`,
        method: 'POST',
        data: data,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${token}`,
        },
      })
        .then(response => {
          this.setState({ attachment: response.data.data.attachments });
          console.log(response.data);
        })
        .catch(function(error) {
          console.log('error from image :' + error);
        });
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    return (
      <>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            padding: 20,
            elevation: 3,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <Ionicon
              style={{
                fontWeight: '900',
                marginRight: 10,
              }}
              name={'ios-arrow-round-back'}
              size={30}
              color={'#37153D'}
            />
          </TouchableWithoutFeedback>
          <Text
            style={{
              fontFamily: 'SofiaPro-Medium',
              fontSize: 14,
              color: '#37153D',
              textAlign: 'left',
            }}>
            {' '}
            Anexos
          </Text>
        </View>

        <ScrollView>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
              padding: 0,
            }}>
            <Text
              style={{
                fontFamily: 'SofiaPro-Medium',
                textAlign: 'center',
                fontSize: 18,
                margin: 20,
                color: '#37153D',
              }}>
              Lista de anexos
            </Text>

            {this.state.attachment.map(item => {
              return <AttachmentItem cardInfo={item.name} />;
            })}

            <View style={{ height: 30 }} />
            <DefaultButton
              title={'Adicionar anexo'}
              onPress={async () => {
                await this.loadAttachment().then(async () => {
                  this.saveAttachment();
                });
              }}
              width={220}
            />
          </View>
        </ScrollView>
      </>
    );
  }
}

const AttachmentItem = ({ cardInfo }) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        padding: 20,
        alignItems: 'center',
        borderBottomColor: '#DEDEDE',
        borderBottomWidth: 2,
      }}>
      <Text
        style={{
          fontFamily: 'SofiaPro-Regular',
          fontSize: 14.5,
          margin: 10,
          marginLeft: 20,
          color: '#2E2E2E',
        }}>
        {cardInfo}
      </Text>
      <Ionicon
        style={{
          fontWeight: '900',
          color: '#37153D',
          transform: [{ rotate: '180deg' }],
          marginRight: 20,
        }}
        name={'ios-arrow-back'}
        size={21}
        color={'red'}
      />
    </View>
  );
};

const AddNewPaymentItem = ({}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'flex-start',
        padding: 20,
        paddingLeft: 40,
        alignItems: 'center',
        borderBottomColor: '#DEDEDE',
        borderBottomWidth: 2,
      }}>
      <Ionicon
        style={{
          fontWeight: '900',
          marginRight: 10,
        }}
        name={'ios-add'}
        size={30}
        color={'#F76E1E'}
      />
      <Text
        style={{
          fontFamily: 'SofiaPro-Regular',
          fontSize: 12,
          margin: 10,
          color: '#2E2E2E',
        }}>
        Adicionar forma de pagamento
      </Text>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(AttachmentsScreen);
