import React, { Component } from 'react';
import {
  View,
  FlatList,
  Text,
  Image,
  TouchableWithoutFeedback,
  RefreshControl,
  Alert,
} from 'react-native';

import { DefaultButton } from '../../components';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { AxiosWithHeaders } from '../../services/api';
import { TextInputMask } from 'react-native-masked-text';
import { SafeAreaView } from 'react-native-safe-area-context';
import AwesomeAlert from 'react-native-awesome-alerts';
import LottieView from 'lottie-react-native';
import animation from '../../assets/success.json';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import avulsa from '../../assets/images/avulsa.png';
import pacote from '../../assets/images/pacote.png';
import editar from '../../assets/images/editar.png';
import excluir from '../../assets/images/cancel.png';

class PlansListScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      professional: this.props.route.params.professional,
      professionalPlans: '',
      plan: '',
      price: '',
      type: '',
      showEditModal: false,
      showExcludeModal: false,
      showAlert: false,
      showDeleteAlert: false,
      refreshing: false,
      choice: '',
      price: '',
    };
  }

  componentDidMount() {
    const { professional } = this.state;

    // await this.loadPlans(professional.id);

    this.focusListener = this.props.navigation.addListener('focus', () =>
      this.loadPlans(professional.id),
    );
  }

  componentWillUnmount() {
    this.focusListener();
  }

  loadPlans = async id => {
    const { user, token } = this.props;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(`/professionals/${id}`)
      .then(response => {
        this.setState({
          professional: response.data,
          professionalPlans: response.data.professionalPlans.sort((a, b) =>
            a.plan.amount > b.plan.amount ? 1 : -1,
          ),
        });
      })
      .catch(err => {
        console.log(err);
      });

    console.log(this.state.professionalPlans);
    this.props.setUser(user);
  };

  _onRefresh = async id => {
    const { user, token } = this.props;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(`/professionals/${id}`)
      .then(response => {
        this.setState({
          professional: response.data,
          professionalPlans: response.data.professionalPlans.sort((a, b) =>
            a.plan.amount > b.plan.amount ? 1 : -1,
          ),
        });
      })
      .catch(err => {
        console.log(err);
      });

    console.log(this.state.professionalPlans);
    this.props.setUser(user);
  };

  savePlan = async () => {
    const { user, token } = this.props;

    const { choice, price } = this.state;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    if (String(price).charAt(0) != 0) {
      try {
        await AxiosInstance.post(`/professionals/plans`, {
          plan: {
            plan_id: choice.plan.id,
            price: price,
          },
        });

        this.setState({
          price: '',
        });
        this.props.setUser(user);
        this.showAlert();
      } catch (err) {
        console.log(err);
      }
    } else {
      Alert.alert('Valor mínimo permitido de R$ 1,00');
    }
  };

  excludePlan = async () => {
    const { user, token } = this.props;

    const { choice } = this.state;

    const AxiosInstace = AxiosWithHeaders({ Authorization: `Bearer ${token}` });

    console.log(choice.id);

    try {
      await AxiosInstace.delete(`/professionals/plans/${choice.id}`);

      this.setState({
        price: '',
      });
      this.props.setUser(user);
      this.showDeleteAlert();
    } catch (err) {
      console.log(err);
    }
  };

  showAlert = () => {
    this.setState({
      showAlert: true,
    });

    setTimeout(
      () =>
        this.setState({
          showAlert: false,
        }),
      2000,
    ); // hide alert after 4s
  };

  showDeleteAlert = () => {
    this.setState({
      showDeleteAlert: true,
    });

    setTimeout(
      () =>
        this.setState({
          showDeleteAlert: false,
        }),
      2000,
    ); // hide alert after 4s
  };

  convertPrice = price => {
    return price.toLocaleString('pt-br', {
      style: 'currency',
      currency: 'BRL',
    });
  };

  render() {
    const {
      professional,
      professionalPlans,
      showEditModal,
      showExcludeModal,
      showAlert,
      showDeleteAlert,
    } = this.state;

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingHorizontal: 20,
            paddingVertical: 20,
            elevation: 3,
          }}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <TouchableWithoutFeedback
              onPress={async () => {
                this.props.navigation.goBack();
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Ionicon
                  style={{
                    fontWeight: '900',
                    marginRight: 10,
                  }}
                  name={'ios-arrow-round-back'}
                  size={30}
                  color={'#37153D'}
                />
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 14,
                    color: '#37153D',
                    textAlign: 'left',
                  }}>
                  Pacotes de serviços
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          <DefaultButton
            title={'Novo pacote'}
            width={120}
            height={35}
            margin={'0px'}
            onPress={() => {
              this.props.navigation.navigate('MyPlansScreen', {
                professional: professional,
              });
            }}
          />
        </View>

        <View>
          {professionalPlans != '' ? (
            <FlatList
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={async () =>
                    await this._onRefresh(this.state.professional.id)
                  }
                />
              }
              style={{ marginBottom: 80 }}
              showsVerticalScrollIndicator={false}
              data={professionalPlans}
              keyExtractor={plan => String(plan.id)}
              renderItem={({ item }) => (
                <PlanItem
                  source={item.plan.amount > 1 ? pacote : avulsa}
                  title={item.plan.description}
                  price={item.price ? this.convertPrice(item.price) : null}
                  type={
                    item.plan.amount == 1
                      ? `${item.plan.amount} única aula`
                      : `${item.plan.amount} aulas no mês`
                  }
                  onPressEdit={() =>
                    this.setState({
                      showEditModal: true,
                      choice: item,
                    })
                  }
                  onPressExclude={() =>
                    this.setState({
                      showExcludeModal: true,
                      choice: item,
                    })
                  }
                />
              )}
            />
          ) : (
            <View
              style={{
                marginVertical: 100,
                marginHorizontal: 20,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <ItemModal item={'Você ainda não possui planos cadastrados.'} />
            </View>
          )}
        </View>

        <AwesomeAlert
          show={showEditModal}
          showProgress={false}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          contentContainerStyle={{
            backgroundColor: '#F8F8F8',
            borderRadius: 25,
          }}
          customView={
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: 300,
                backgroundColor: '#F8F8F8',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 15,
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 22,
                    textAlign: 'center',
                    flexWrap: 'wrap',
                    color: '#37153D',
                  }}>
                  {this.state.choice?.plan?.description}
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 30,
                }}>
                <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      fontSize: 14,
                      color: '#2E2E2E',
                      textAlign: 'left',
                      marginLeft: 30,
                      margin: 0,
                      alignSelf: 'flex-start',
                    }}>
                    Novo preço:
                  </Text>

                  <TextInputMask
                    type={'money'}
                    value={this.state.price}
                    keyboardType={'decimal-pad'}
                    maxLength={10}
                    style={{
                      fontFamily: 'SofiaPro-Regular',
                      fontSize: 20,
                      textAlign: 'center',
                      backgroundColor: '#fff',
                      color: '#636363',
                      borderWidth: 0.1,
                      borderColor: '#707070',
                      borderRadius: 50,
                      margin: 10,
                      width: 270,
                      padding: 10,
                    }}
                    includeRawValueInChangeText={true}
                    onChangeText={(maskedText, rawText) => {
                      this.setState({
                        price: rawText,
                      });
                    }}
                  />
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                }}>
                <DefaultButton
                  onPress={() => {
                    this.setState({
                      showEditModal: false,
                    });
                    this.savePlan();
                  }}
                  title={'Salvar'}
                  width={200}
                  height={40}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                }}>
                <TouchableWithoutFeedback
                  onPress={() =>
                    this.setState({
                      showEditModal: false,
                    })
                  }>
                  <Text style={{ marginTop: 10, color: '#999' }}>Voltar</Text>
                </TouchableWithoutFeedback>
              </View>
            </View>
          }
        />

        <AwesomeAlert
          show={showExcludeModal}
          showProgress={false}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          contentContainerStyle={{
            backgroundColor: '#F8F8F8',
            borderRadius: 25,
          }}
          customView={
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: 300,
                backgroundColor: '#F8F8F8',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 15,
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 22,
                    textAlign: 'center',
                    flexWrap: 'wrap',
                    color: '#37153D',
                  }}>
                  {this.state.choice?.plan?.description}
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 20,
                }}>
                <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      fontSize: 17,
                      color: '#2E2E2E',
                      margin: 0,
                    }}>
                    Preço atual
                  </Text>
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      fontSize: 20,
                      color: '#F76E1E',
                      margin: 0,
                    }}>
                    R$ {this.state.choice?.price}
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                }}>
                <DefaultButton
                  onPress={() => {
                    this.setState({
                      showExcludeModal: false,
                    });
                    this.excludePlan();
                  }}
                  backgroundColor={'#D22020'}
                  title={'Excluir plano'}
                  width={200}
                  height={40}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                }}>
                <TouchableWithoutFeedback
                  onPress={() =>
                    this.setState({
                      showExcludeModal: false,
                    })
                  }>
                  <Text style={{ marginTop: 10, color: '#999' }}>Voltar</Text>
                </TouchableWithoutFeedback>
              </View>
            </View>
          }
        />

        <AwesomeAlert
          show={showAlert}
          showProgress={false}
          title="Pacote atualizado com sucesso!"
          closeOnHardwareBackPress={false}
          onDismiss={async () => {
            await this._onRefresh(this.state.professional.id);
          }}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <LottieView
              style={{ width: 70, height: 70 }}
              resizeMode="contain"
              autoSize
              source={animation}
              autoPlay
              loop={false}
            />
          }
        />

        <AwesomeAlert
          show={showDeleteAlert}
          showProgress={false}
          title="Pacote deletado com sucesso!"
          closeOnHardwareBackPress={false}
          onDismiss={async () => {
            await this._onRefresh(this.state.professional.id);
          }}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <LottieView
              style={{ width: 70, height: 70 }}
              resizeMode="contain"
              autoSize
              source={animation}
              autoPlay
              loop={false}
            />
          }
        />
      </>
    );
  }
}

const PlanItem = ({
  type,
  title,
  price,
  source,
  onPressEdit,
  onPressExclude,
}) => {
  return (
    <>
      <View
        style={{
          flexDirection: 'column',
          alignItems: 'center',
          padding: 20,
          margin: 10,
          marginLeft: 20,
          marginRight: 20,
          borderRadius: 10,
          elevation: 1,
          backgroundColor: '#fff',
          position: 'relative',
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            flex: 1,
          }}>
          <Image style={{ margin: 20 }} source={source} />
          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              marginLeft: 20,
              flex: 4,
            }}>
            <Text
              style={{
                fontFamily: 'SofiaPro-Medium',
                fontSize: 18,
                textAlign: 'left',
                flexWrap: 'wrap',
                color: '#37153D',
              }}>
              {title}
            </Text>

            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Preço:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {price}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Tipo:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {type}
              </Text>
            </View>
          </View>
        </View>

        <View
          style={{
            position: 'absolute',
            flexDirection: 'row',
            right: 0,
            marginTop: 10,
            marginRight: 10,
          }}>
          <TouchableWithoutFeedback onPress={onPressEdit}>
            <Image
              style={{ height: 20, width: 20, marginRight: 5, marginTop: 5 }}
              source={editar}
              resizeMode={'contain'}
            />
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={onPressExclude}>
            <Image
              style={{
                height: 20,
                width: 20,
                marginRight: 5,
                marginLeft: 5,
                marginTop: 5,
              }}
              source={excluir}
              resizeMode={'contain'}
            />
          </TouchableWithoutFeedback>
        </View>
      </View>
    </>
  );
};

const ItemModal = ({ item, answer }) => {
  return (
    <View style={{ flexDirection: 'row', justifyContent: 'center', margin: 2 }}>
      <Text
        style={{
          fontFamily: 'SofiaPro-Regular',
          textAlign: 'center',
          flexWrap: 'wrap',
          color: '#F76E1E',
          fontSize: 13,
        }}>
        {item}
      </Text>
      <Text
        style={{
          fontFamily: 'SofiaProLight',
          flexWrap: 'wrap',
          color: '#2E2E2E',
          fontSize: 13,
        }}>
        {answer}
      </Text>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(PlansListScreen);
