import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  FlatList,
  TouchableWithoutFeedback,
  TextInput,
} from 'react-native';

import { CheckBox } from 'react-native-elements';
import { DefaultButton } from '../../components';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { AxiosWithHeaders } from '../../services/api';
import { SafeAreaView } from 'react-native-safe-area-context';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import AwesomeAlert from 'react-native-awesome-alerts';
import LottieView from 'lottie-react-native';
import animation from '../../assets/success.json';

class ClassDescriptionScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showAlert: false,
      showModal: true,
      groupings: '',
      modalities: '',
      optionsGroupings: [
        { description: 'Peito' },
        { description: 'Costas' },
        { description: 'Bíceps' },
        { description: 'Antebraço' },
        { description: 'Tríceps' },
        { description: 'Ombros' },
        { description: 'Abdômen' },
        { description: 'Lombar' },
        { description: 'Quadríceps' },
        { description: 'Posterior' },
        { description: 'Glúteos' },
        { description: 'Panturrilha' },
      ],
      optionsModalities: [
        { description: 'Musculação' },
        { description: 'Aeróbico' },
        { description: 'HIIT' },
        { description: 'Circuito' },
        { description: 'Alongamento' },
        { description: 'Mobilidade' },
      ],
      note: '',
    };
  }

  async componentDidMount() {
    console.log(this.props.route?.params?.classId);
  }

  checkGroupings = async item => {
    const { groupings } = this.state;
    let newArr = [];

    if (!groupings.includes(item)) {
      newArr = [...groupings, item];
    } else {
      newArr = groupings.filter(a => a !== item);
    }

    await this.setState({
      groupings: newArr,
    });

    await console.log(groupings);
  };

  checkModalities = async item => {
    const { modalities } = this.state;
    let newArr = [];

    if (!modalities.includes(item)) {
      newArr = [...modalities, item];
    } else {
      newArr = modalities.filter(a => a !== item);
    }

    await this.setState({
      modalities: newArr,
    });

    await console.log(modalities);
  };

  saveTrainingDescription = async () => {
    const { user, token } = this.props;
    const id = this.props.route?.params?.classId;
    const { groupings, modalities, note } = this.state;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    try {
      AxiosInstance.put(`scheduled-services/${id}/status`, {
        status: 'CONCLUIDO',
      });

      const response = await AxiosInstance.post(
        `/scheduled/${id}/description`,
        {
          groupings: groupings
            .map(e => {
              return e;
            })
            .toString(),
          modality: modalities
            .map(e => {
              return e;
            })
            .toString(),
          note: note,
        },
      );
      console.log(response.data);
      this.props.setUser(user);
      this.showAlert();
    } catch (err) {
      console.log(err);
    }
  };

  showAlert = () => {
    this.setState({
      showAlert: true,
    });

    setTimeout(
      () =>
        this.setState({
          showAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  render() {
    const {
      groupings,
      modalities,
      optionsGroupings,
      optionsModalities,
      showAlert,
      showModal,
      note,
    } = this.state;

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: 20,
            elevation: 3,
          }}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <TouchableWithoutFeedback
              onPress={async () => {
                this.props.navigation.goBack();
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Ionicon
                  style={{
                    fontWeight: '900',
                    marginRight: 10,
                  }}
                  name={'ios-arrow-round-back'}
                  size={30}
                  color={'#37153D'}
                />
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 14,
                    color: '#37153D',
                    textAlign: 'left',
                  }}>
                  {' '}
                  Descrição de treino
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>

        <ScrollView
          style={{
            paddingHorizontal: 20,
          }}>
          <Text
            style={{
              padding: 10,
              marginLeft: 10,
              fontFamily: 'SofiaPro-Medium',
              fontSize: 16,
              color: '#2E2E2E',
            }}>
            Grupamentos
          </Text>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={optionsGroupings}
            keyExtractor={group => String(group.description)}
            renderItem={({ item }) => (
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'flex-start',

                  width: '100%',
                }}>
                <CheckBox
                  title={item?.description}
                  textStyle={{
                    color: '#707070',
                    fontFamily: 'SofiaProLight',
                    fontSize: 14,
                    borderWidth: 0.1,
                  }}
                  containerStyle={{
                    width: '95%',
                    backgroundColor: 'transparent',
                    borderColor: 'transparent',
                  }}
                  checkedIcon="square"
                  uncheckedIcon="square"
                  checkedColor="#37153D"
                  uncheckedColor="#fff"
                  onPress={() => this.checkGroupings(item?.description)}
                  checked={groupings.includes(item?.description)}
                />
              </View>
            )}
          />

          <Text
            style={{
              padding: 10,
              marginLeft: 10,
              fontFamily: 'SofiaPro-Medium',
              fontSize: 16,
              color: '#2E2E2E',
            }}>
            Modalidade
          </Text>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={optionsModalities}
            keyExtractor={modality => String(modality.description)}
            renderItem={({ item }) => (
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'flex-start',

                  width: '100%',
                }}>
                <CheckBox
                  title={item?.description}
                  textStyle={{
                    color: '#707070',
                    fontFamily: 'SofiaProLight',
                    fontSize: 14,
                    borderWidth: 0.1,
                  }}
                  containerStyle={{
                    width: '95%',
                    backgroundColor: 'transparent',
                    borderColor: 'transparent',
                  }}
                  checkedIcon="square"
                  uncheckedIcon="square"
                  checkedColor="#37153D"
                  uncheckedColor="#fff"
                  onPress={() => this.checkModalities(item?.description)}
                  checked={modalities.includes(item?.description)}
                />
              </View>
            )}
          />

          <CustomInput
            question={'Observações: '}
            answer={note}
            multiline={true}
            height={300}
            borderRadius={30}
            padding={20}
            onChangeText={text => this.setState({ note: text })}
          />

          <View style={{ margin: 10, marginBottom: 30, alignSelf: 'center' }}>
            <DefaultButton
              title={'Salvar'}
              width={200}
              height={40}
              onPress={() => this.saveTrainingDescription()}
            />
          </View>
        </ScrollView>

        <AwesomeAlert
          show={showAlert}
          showProgress={false}
          closeOnHardwareBackPress={false}
          onDismiss={async () => {
            this.props.navigation.navigate('TrainingDetailScreen', {
              classId: this.props.route?.params?.classId,
            });
          }}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Descrição de treino salva com sucesso!
              </Text>
              <LottieView
                style={{ width: 70, height: 70 }}
                resizeMode="contain"
                autoSize
                source={animation}
                autoPlay
                loop={false}
              />
            </View>
          }
        />

        <AwesomeAlert
          show={showModal}
          showProgress={false}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          confirmButtonColor="#DD6B55"
          onCancelPressed={() => {
            this.setState({
              modalPlan: !this.state.showModal,
            });
          }}
          overlayStyle={{
            backgroundColor: 'rgba(0, 0, 0, 0.4)',
          }}
          contentContainerStyle={{
            maxWidth: '100%',
            minWidth: '80%',
            height: '20%',
            borderRadius: 25,
            justifyContent: 'center',
          }}
          customView={
            <ScrollView showsVerticalScrollIndicator={false}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginBottom: 15,
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 22,
                    textAlign: 'center',
                    flexWrap: 'wrap',
                    color: '#37153D',
                  }}>
                  Aula concluída!
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}>
                {this.state.plan != '' ? (
                  <DefaultButton
                    title={'Incluir descrição de treino'}
                    onPress={() => {
                      this.setState({
                        showModal: !this.state.showModal,
                      });
                    }}
                    width={220}
                    height={40}
                    backgroundColor={'#37153D'}
                  />
                ) : null}
              </View>
            </ScrollView>
          }
        />
      </>
    );
  }
}

const CustomInput = ({
  question,
  answer,
  height,
  width,
  keyboardType,
  maxLength,
  onChangeText,
  placeholder,
  padding,
}) => {
  return (
    <View
      style={{
        flexDirection: 'column',
        padding: 10,
        margin: 10,
      }}>
      <Text
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaPro-Medium',
          margin: 10,
          fontSize: 13,
        }}>
        {question}
      </Text>
      <TextInput
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaProLight',
          fontSize: 13,
          backgroundColor: '#fff',
          borderRadius: 20,
          borderWidth: 0.1,
          paddingLeft: 15,
          paddingRight: 15,
          width: '100%',
          height: height,
          padding: padding,
          textAlignVertical: 'top',
        }}
        value={answer}
        maxLength={maxLength}
        keyboardType={keyboardType}
        onChangeText={onChangeText}
        placeholder={placeholder}
        autoCapitalize={false}
        multiline={true}
      />
    </View>
  );
};

CustomInput.defaultProps = {
  placeholder: 'Não informado',
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(ClassDescriptionScreen);
