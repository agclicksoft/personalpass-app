import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
} from 'react-native';
import { DefaultButton, LoadingModal } from '../../components';
import { SafeAreaView } from 'react-native-safe-area-context';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { AxiosWithHeaders } from '../../services/api';
import AwesomeAlert from 'react-native-awesome-alerts';
import LottieView from 'lottie-react-native';
import animation from '../../assets/success.json';
import animationError from '../../assets/warn.json';
import CardFlip from 'react-native-card-flip';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';

class PaymentInHands extends Component {
  constructor(props) {
    super(props);

    this.state = {
      appointment: '',
      contract_id: '',
      user: '',
      loading: false,
    };
  }

  componentDidMount() {
    this.setState({
      appointment: this.props.route?.params?.appointment,
      contract_id: this.props.route?.params?.appointment?.contract?.id,
      user: this.props.user.user,
    });
  }

  confirmPaymentInHands = async () => {
    const { token } = this.props;
    const { user, contract_id } = this.state;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    if (
      user.name == '' ||
      user.email == '' ||
      user.cpf == '' ||
      user.contact == '' ||
      user.birthday == ''
    ) {
      this.showProfileError();
      return;
    }

    try {
      this.setState({ loading: true });

      const response = await AxiosInstance.put(`contracts/${contract_id}`, {
        method_payment: 'MAOS',
      });

      if (response.status == 204) {
        this.setState({
          loading: false,
        });
        this.showAlert();
      }
    } catch (err) {
      this.setState({ loading: false });
      this.showConfirmationError();
      console.log(err);
    }
  };

  showAlert = () => {
    this.setState({
      showAlert: true,
    });

    setTimeout(
      () =>
        this.setState({
          showAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  showProfileError = () => {
    this.setState({
      showProfileError: true,
    });

    setTimeout(
      () =>
        this.setState({
          showProfileError: false,
        }),
      5000,
    ); // hide alert after 4s
  };

  showConfirmationError = () => {
    this.setState({
      showConfirmationError: true,
    });

    setTimeout(
      () =>
        this.setState({
          showConfirmationError: false,
        }),
      3000,
    ); // hide alert after 4s
  };

  convertPrice = price => {
    return price.toLocaleString('pt-br', {
      style: 'currency',
      currency: 'BRL',
    });
  };

  render() {
    const {
      showAlert,
      showProfileError,
      showConfirmationError,
      appointment,
    } = this.state;

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            paddingHorizontal: 30,
            paddingVertical: 20,
            elevation: 3,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Ionicon
                style={{
                  fontWeight: '900',
                  marginRight: 10,
                }}
                name={'ios-arrow-round-back'}
                size={30}
                color={'#37153D'}
              />
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#37153D',
                  textAlign: 'left',
                }}>
                {' '}
                Pagamento em mãos
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>

        {this.state.loading == true ? (
          <LoadingModal show={this.state.loading} />
        ) : (
          <KeyboardAvoidingView
            behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
            style={{ flex: 1 }}>
            <ScrollView>
              <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                <View style={styles.container}>
                  <CardFlip
                    style={styles.cardContainer}
                    ref={card => (this.card = card)}>
                    <TouchableOpacity
                      activeOpacity={1}
                      style={[styles.card, styles.card1]}
                      onPress={null}>
                      <Image
                        resizeMode="contain"
                        source={require('../../assets/images/verified-white.png')}
                        style={{
                          height: 30,
                          width: 30,
                          marginBottom: 15,
                        }}
                      />

                      <View>
                        <Text style={styles.label}>
                          {
                            appointment?.contract?.professionalPlan?.plan
                              ?.description
                          }{' '}
                          - {appointment?.professional?.user?.name}
                        </Text>
                      </View>

                      <Text style={styles.label}>
                        Confirme os dados abaixo:
                      </Text>

                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          width: 250,
                        }}>
                        <Text style={styles.label}>
                          Valor:{' '}
                          {appointment.contract?.professionalPlan?.price
                            ? this.convertPrice(
                                appointment.contract?.professionalPlan?.price,
                              )
                            : null}
                        </Text>
                      </View>

                      <Text style={styles.label}>
                        Pagar em mãos no dia da aula.
                      </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      activeOpacity={1}
                      style={[styles.card, styles.card2]}
                      onPress={null}
                    />
                  </CardFlip>
                </View>

                <View style={{ margin: 20 }}>
                  <DefaultButton
                    title={'Confirmar'}
                    width={200}
                    height={35}
                    onPress={() => this.confirmPaymentInHands()}
                  />
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        )}

        <AwesomeAlert
          show={showProfileError}
          showProgress={false}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          showConfirmButton={true}
          confirmText="Editar perfil"
          onConfirmPressed={() => this.props.navigation.navigate('MyProfile')}
          confirmButtonStyle={{
            width: 200,
            height: 40,
            backgroundColor: '#37153D',
            borderRadius: 25,
            marginBottom: 20,
          }}
          confirmButtonTextStyle={{
            textAlign: 'center',
            fontFamily: 'SofiaPro-Medium',
            letterSpacing: 0,
            color: '#fff',
            fontSize: 15,
          }}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Oops! Seus dados de perfil estão incompletos. Por favor, volte e
                preencha todas as suas informações.
              </Text>
              <LottieView
                style={{ height: 70 }}
                resizeMode="contain"
                autoSize
                source={animationError}
                autoPlay
                loop={false}
              />
            </View>
          }
        />

        <AwesomeAlert
          show={showAlert}
          showProgress={false}
          closeOnHardwareBackPress={false}
          onDismiss={() => this.props.navigation.navigate('MyClasses')}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Aula confirmada!
              </Text>
              <LottieView
                style={{ width: 70, height: 70 }}
                resizeMode="contain"
                autoSize
                source={animation}
                autoPlay
                loop={false}
              />
            </View>
          }
        />

        <AwesomeAlert
          show={showConfirmationError}
          showProgress={false}
          closeOnHardwareBackPress={false}
          onDismiss={() => this.props.navigation.goBack()}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Oops! Algo de errado aconteceu, tente novamente.
              </Text>
              <LottieView
                style={{ height: 70 }}
                resizeMode="contain"
                autoSize
                source={animationError}
                autoPlay
                loop={false}
              />
            </View>
          }
        />
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F9F9F9',
    marginVertical: 30,
  },
  cardContainer: {
    width: 318,
    height: 250,
    backgroundColor: '#F9F9F9',
  },
  card: {
    width: 318,
    height: 250,
    backgroundColor: '#FE474C',
    borderRadius: 5,
    shadowColor: 'rgba(0,0,0,0.5)',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.5,
  },
  card1: {
    backgroundColor: '#37153D',
    justifyContent: 'center',
    alignItems: 'flex-start',
    padding: 30,
  },
  card2: {
    backgroundColor: '#37153D',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  label: {
    fontSize: 15,
    color: '#fff',
    marginBottom: 10,
  },
});

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(PaymentInHands);
