import React, { Component } from 'react';
import {
  View,
  Text,
  Modal,
  FlatList,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';

import { CheckBox } from 'react-native-elements';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { AxiosWithHeaders } from '../../services/api';
import { DefaultButton, SimpleSearch, SearchInput } from '../../components';
import { SafeAreaView } from 'react-native-safe-area-context';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import AwesomeAlert from 'react-native-awesome-alerts';
import LottieView from 'lottie-react-native';
import animation from '../../assets/success.json';

class GymsScreen extends Component {
  constructor(props) {
    super(props);

    const fullAddress = this.props.route.params.fullAddress;

    this.state = {
      fullAddress: fullAddress,
      gyms: [],
      optionsGyms: fullAddress.gyms,
      searchText: '',
      showModal: true,
      showAlert: false,
    };
  }

  async componentDidMount() {
    setTimeout(() => this.loadGyms(), 1000);
  }

  loadGyms = async () => {
    const { token, user } = this.props;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(`/professionals/${user.id}`)
      .then(response => {
        this.setState({
          gyms: response.data.gyms.map(e => e.id),
        });
        console.log(response.data.gyms);
      })
      .catch(err => {
        console.log(err);
      });
  };

  saveGyms = async () => {
    const { user, token } = this.props;
    const { id } = this.props.user;
    const { gyms } = this.state;

    const AxiosInstace = AxiosWithHeaders({ Authorization: `Bearer ${token}` });

    try {
      await AxiosInstace.post(`professionals/${id}/gyms`, {
        gyms: gyms,
      });

      this.props.setUser(user);
      this.showAlert();
    } catch (err) {
      console.log(err.response.data);
    }
  };

  checkItem = async item => {
    const { gyms } = this.state;
    let newArr = [];

    if (!gyms.includes(item)) {
      newArr = [...gyms, item];
    } else {
      newArr = gyms.filter(a => a !== item);
    }
    await this.setState({ gyms: newArr }, () =>
      console.log('updated state', newArr),
    );
  };

  onSearchTextChanged = text => this.setState({ searchText: text });

  showAlert = () => {
    this.setState({
      showAlert: true,
    });

    setTimeout(
      () =>
        this.setState({
          showAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  render() {
    const {
      fullAddress,
      gyms,
      optionsGyms,
      searchText,
      showAlert,
    } = this.state;

    var gymsList = optionsGyms;

    gymsList = gymsList.filter(result => {
      let regex = new RegExp(searchText.toLowerCase()).test(
        result?.name?.toLowerCase(),
      );
      return regex;
    });

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: 20,
            elevation: 3,
          }}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <TouchableWithoutFeedback
              onPress={() => {
                this.props.navigation.goBack();
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Ionicon
                  style={{
                    fontWeight: '900',
                    marginRight: 10,
                  }}
                  name={'ios-arrow-round-back'}
                  size={30}
                  color={'#37153D'}
                />
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 14,
                    color: '#37153D',
                    textAlign: 'left',
                  }}>
                  {' '}
                  Academias
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          <DefaultButton
            title={'Salvar'}
            width={100}
            height={35}
            margin={'0px'}
            onPress={() => {
              this.saveGyms();
            }}
          />
        </View>
        <View>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'flex-start',
              padding: 0,
            }}>
            <View style={{ height: 10 }} />
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                margin: 20,
              }}>
              <SimpleSearch
                value={this.state.searchText}
                onChangeText={this.onSearchTextChanged}
              />

              <TouchableWithoutFeedback
                onPress={() =>
                  this.props.navigation.navigate('NewGymScreen', {
                    address: fullAddress.data,
                  })
                }>
                <Ionicon
                  style={{
                    fontWeight: '1000',
                  }}
                  name={'ios-add'}
                  size={40}
                  color={'#F76E1E'}
                />
              </TouchableWithoutFeedback>
            </View>
          </View>
        </View>

        <FlatList
          showsVerticalScrollIndicator={false}
          data={gymsList}
          extraData={gyms}
          keyExtractor={gym => String(gym.id)}
          renderItem={({ item }) => (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
                padding: 5,
                borderBottomWidth: 1,
                borderColor: '#E9E9E9',
                width: '100%',
              }}>
              <CheckBox
                title={item?.name}
                textStyle={{
                  color: '#707070',
                  fontFamily: 'SofiaPro-Regular',
                  margin: 5,
                  fontSize: 13,
                  borderWidth: 0.1,
                }}
                containerStyle={{
                  width: '95%',
                  backgroundColor: 'transparent',
                  borderColor: 'transparent',
                }}
                checkedIcon="circle"
                uncheckedIcon="circle"
                checkedColor="#37153D"
                onPress={() => this.checkItem(item?.id)}
                checked={gyms.includes(item?.id)}
              />
            </View>
          )}
        />

        <Modal
          visible={this.state.showModal}
          onRequestClose={this.closeSolicitationModal}
          onPressOut={this.closeSolicitationModal}
          transparent={true}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              padding: 20,
              justifyContent: 'center',
              backgroundColor: 'rgba(0, 0, 0, 0.7)',
            }}>
            <View
              style={{
                flexDirection: 'column',
                padding: 20,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 20,
                elevation: 2,
                backgroundColor: '#FFF',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 13,
                  fontFamily: 'SofiaProLight',
                  width: 200,
                  marginBottom: 10,
                }}>
                Selecione as academias que você deseja trabalhar.
              </Text>

              <DefaultButton
                title={'Continuar'}
                width={180}
                height={35}
                onPress={() => {
                  this.setState({ showModal: false });
                }}
              />
            </View>
          </View>
        </Modal>

        <AwesomeAlert
          show={showAlert}
          showProgress={false}
          closeOnHardwareBackPress={false}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Academias atualizadas com sucesso!
              </Text>
              <LottieView
                style={{ width: 70, height: 70 }}
                resizeMode="contain"
                autoSize
                source={animation}
                autoPlay
                loop={false}
              />
            </View>
          }
        />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(GymsScreen);
