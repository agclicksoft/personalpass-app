import React, { Component } from 'react';
import FormData from 'form-data';
import {
  View,
  Text,
  ScrollView,
  TextInput,
  Image,
  TouchableWithoutFeedback,
  Platform,
  Alert,
  KeyboardAvoidingView,
} from 'react-native';

import { DefaultButton } from '../../components';
import { TextInputMask } from 'react-native-masked-text';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { AxiosWithHeaders } from '../../services/api';
import { SafeAreaView } from 'react-native-safe-area-context';
import { showMessage } from 'react-native-flash-message';
import birthdayIsValid from '../../services/checkBirthday';
import ImagePicker from 'react-native-image-picker';
import axios from 'axios';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import AwesomeAlert from 'react-native-awesome-alerts';
import LottieView from 'lottie-react-native';
import animation from '../../assets/success.json';
import cameraImage from '../../assets/images/camera.png';
import { BASE_URL } from '../../services/baseUrl';

class EditStudentScreen extends Component {
  constructor(props) {
    super(props);

    const student = this.props.route.params.student;
    const img_profile = this.props.route.params.img_profile;

    this.state = {
      name: student?.user?.name ? student.user.name : '',
      cpf: student?.user?.cpf ? student.user.cpf : '',
      birthday: student?.user?.birthday ? student.user.birthday : '',
      email: student?.user?.email ? student.user.email : '',
      password: '',
      contact: student?.user?.contact ? student.user.contact : '',
      emergency_phone: student?.user?.emergency_phone
        ? student.user.emergency_phone
        : '',
      optionsGenre: [
        { id: '', text: 'Prefiro não informar' },
        { id: 'M', text: 'Masculino' },
        { id: 'F', text: 'Feminino' },
      ],
      genre: student?.user?.genre ? student.user.genre : '',
      img_profile: img_profile
        ? { uri: img_profile }
        : {
            uri: 'https://static.scrum.org/web/images/profile-placeholder.png',
          },
      img_upload: '',
      cep: student?.user?.address_zipcode ? student.user.address_zipcode : '',
      address_city: student?.user?.address_city
        ? student.user.address_city
        : '',
      address_uf: student?.user?.address_uf ? student.user.address_uf : '',
      address_neighborhood: student?.user?.address_neighborhood
        ? student.user.address_neighborhood
        : '',
      address_street: student?.user?.address_street
        ? student.user.address_street
        : '',
      address_number: student?.user?.address_number
        ? student.user.address_number
        : '',
      address_complement: student?.user?.address_complement
        ? student.user.address_complement
        : '',
      showAlert: false,
    };
  }

  chooseFile = () => {
    var options = {
      title: 'Escolher imagem',
      cancelButtonTitle: 'Cancelar',
      customButtons: [],
      takePhotoButtonTitle: 'Tirar foto',
      chooseFromLibraryButtonTitle: 'Escolher imagem existente',

      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
      allowsEditing: true,
      aspect: [4, 4],
      quality: 0.4,
    };

    ImagePicker.launchImageLibrary(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        this.setState({
          img_profile: response,
          img_upload: response,
        });
        console.log(this.state.img_upload);
      }
    });
  };

  saveAvatar = async () => {
    if (this.state.img_upload !== '') {
      const { token } = this.props;
      const { img_upload } = this.state;

      const data = new FormData();
      data.append('profile', {
        name: 'profile-img',
        type: img_upload?.type,
        uri:
          Platform.OS === 'android'
            ? img_upload?.uri
            : img_upload?.uri.replace('file://', ''),
      });

      try {
        await axios({
          url: `${BASE_URL}users/profile`,
          method: 'POST',
          data: data,
          headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
            Authorization: `Bearer ${token}`,
          },
        })
          .then(function(response) {
            console.log('response :', response);
          })
          .catch(function(error) {
            console.log('error from image :' + error);
          });
      } catch (err) {
        console.log(err);
      }
    }
  };

  saveProfile = async () => {
    const { user, token } = this.props;
    const { id, user_id } = this.props.user;
    const {
      name,
      cpf,
      birthday,
      email,
      contact,
      emergency_phone,
      cep,
      address_uf,
      address_city,
      address_neighborhood,
      address_street,
      address_number,
      address_complement,
      img_profile,
    } = this.state;

    const AxiosInstace = AxiosWithHeaders({ Authorization: `Bearer ${token}` });

    if (birthdayIsValid(this.state.birthday)) {
      try {
        await this.saveAvatar();

        await AxiosInstace.put(`students/${id}`, {
          id: id,
          user_id: user_id,
          user: {
            id: user_id,
            email: email,
            birthday: birthday,
            cpf: cpf,
            name: name,
            emergency_phone: emergency_phone,
            contact: contact,
            address_zipcode: cep,
            address_street: address_street,
            address_number: address_number,
            address_complement: address_complement,
            address_city: address_city,
            address_uf: address_uf,
            address_neighborhood: address_neighborhood,
          },
        });

        user.user.name = name;
        user.user.img_profile = img_profile;
        user.user.cpf = cpf;
        user.user.birthday = birthday;
        user.user.email = email;
        user.user.contact = contact;
        user.user.emergency_phone = emergency_phone;
        user.user.cep = cep;
        user.user.address_street = address_street;
        user.user.address_number = address_number;
        user.user.address_complement = address_complement;
        user.user.address_city = address_city;
        user.user.address_uf = address_uf;
        user.user.address_neighborhood = address_neighborhood;

        this.props.setUser(user);
        this.showAlert();
      } catch (err) {
        console.log(err.response);
        showMessage({
          type: 'danger',
          message: 'Erro inesperado. Tente novamente',
          titleStyle: {
            textAlign: 'center',
          },
        });
      }
    } else {
      return Alert.alert('Atenção', 'Insira uma data de nascimento válida.');
    }
  };

  saveNewPassword = async () => {
    const { user, token } = this.props;

    const { password } = this.state;

    if (!this.state.password) {
      showMessage({
        type: 'danger',
        message: 'Favor informar nova senha',
        titleStyle: {
          textAlign: 'center',
        },
      });
      return;
    }

    const AxiosInstace = AxiosWithHeaders({ Authorization: `Bearer ${token}` });

    try {
      await AxiosInstace.post(`/users/change-password`, {
        password: password,
      });

      this.showSuccess();
      this.setState({
        password: '',
      });
      this.props.setUser(user);
    } catch (err) {
      console.log(err);
      showMessage({
        type: 'danger',
        message: 'Erro inesperado. Tente novamente',
        titleStyle: {
          textAlign: 'center',
        },
      });
    }
  };

  showAlert = () => {
    this.setState({
      showAlert: true,
    });

    setTimeout(
      () =>
        this.setState({
          showAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  showSuccess = () => {
    this.setState({
      showSuccess: true,
    });

    setTimeout(
      () =>
        this.setState({
          showSuccess: false,
          changePassAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  showPassChange = () => {
    this.setState({ changePassAlert: !this.state.changePassAlert });
  };

  render() {
    const { showAlert, changePassAlert, showSuccess } = this.state;

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            paddingHorizontal: 20,
            paddingVertical: 20,
            elevation: 3,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Ionicon
                style={{
                  fontWeight: '900',
                  marginRight: 10,
                }}
                name={'ios-arrow-round-back'}
                size={30}
                color={'#37153D'}
              />
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#37153D',
                  textAlign: 'left',
                }}>
                {' '}
                Editar perfil
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <KeyboardAvoidingView
          behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
          style={{ flex: 1 }}>
          <ScrollView>
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'center',
              }}>
              <View style={{ position: 'relative' }}>
                <Image
                  style={{
                    height: 200,
                    width: 200,
                    borderRadius: 100,
                    margin: 20,
                  }}
                  source={this.state.img_profile}
                />
                <TouchableWithoutFeedback onPress={() => this.chooseFile()}>
                  <Image
                    resizeMode="contain"
                    source={cameraImage}
                    style={{
                      position: 'absolute',
                      top: 75,
                      left: 100,
                      width: 50,
                    }}
                  />
                </TouchableWithoutFeedback>
              </View>

              <CustomInput
                question={'Nome: '}
                answer={this.state.name}
                onChangeText={text => this.setState({ name: text })}
              />

              <MaskedInput
                question={'CPF: '}
                type={'cpf'}
                keyboardType={'number-pad'}
                answer={this.state.cpf}
                onChangeText={text => this.setState({ cpf: text })}
              />

              <MaskedInput
                question={'Data de nascimento: '}
                type={'datetime'}
                maxLength={10}
                keyboardType={'number-pad'}
                answer={this.state.birthday}
                onChangeText={text => this.setState({ birthday: text })}
              />

              <CustomInput
                question={'Email: '}
                answer={this.state.email}
                onChangeText={text => this.setState({ email: text })}
              />

              <CustomInput
                question={'Senha: '}
                answer={'xxxxxxxxxxxx'}
                height={40}
                secureTextEntry={true}
                password={true}
                editable={false}
                onChangePassPress={this.showPassChange}
              />

              <MaskedInput
                question={'Telefone/Celular: '}
                type={'cel-phone'}
                keyboardType={'number-pad'}
                options={{
                  maskType: 'BRL',
                  withDDD: true,
                  dddMask: '(99) ',
                }}
                answer={this.state.contact}
                onChangeText={text => this.setState({ contact: text })}
              />

              <MaskedInput
                question={'Telefone de emergências: '}
                type={'cel-phone'}
                keyboardType={'number-pad'}
                options={{
                  maskType: 'BRL',
                  withDDD: true,
                  dddMask: '(99) ',
                }}
                answer={this.state.emergency_phone}
                onChangeText={text => this.setState({ emergency_phone: text })}
              />

              <MaskedInput
                question={'CEP: '}
                type={'zip-code'}
                keyboardType={'number-pad'}
                answer={this.state.cep}
                onChangeText={async text => {
                  await this.setState({ cep: text });
                  if (text.length == 9) {
                    let cep = await axios.get(
                      `https://viacep.com.br/ws/${text}/json/`,
                    );

                    console.log(cep.data);

                    if (cep.data.erro) {
                      return Alert.alert(
                        'Atenção',
                        'Não foi possível encontrar o CEP informado. Preencha as informações manualmente',
                      );
                    }

                    await this.setState({
                      address_city: cep.data.localidade,
                      address_uf: cep.data.uf,
                      address_neighborhood: cep.data.bairro,
                      address_street: cep.data.logradouro,
                      address_complement: cep.data.complemento,
                    });
                  }
                }}
              />

              <CustomInput
                question={'Estado: '}
                answer={this.state.address_city}
                onChangeText={async text =>
                  await this.setState({ address_city: text })
                }
              />
              <CustomInput
                question={'Cidade: '}
                answer={this.state.address_uf}
                onChangeText={async text =>
                  await this.setState({ address_uf: text })
                }
              />
              <CustomInput
                question={'Bairro: '}
                answer={this.state.address_neighborhood}
                onChangeText={async text =>
                  await this.setState({ address_neighborhood: text })
                }
              />
              <CustomInput
                question={'Endereço: '}
                answer={this.state.address_street}
                onChangeText={async text =>
                  await this.setState({ address_street: text })
                }
              />
              <CustomInput
                question={'Número: '}
                answer={this.state.address_number}
                keyboardType={'number-pad'}
                onChangeText={async text =>
                  await this.setState({ address_number: text })
                }
              />
              <CustomInput
                question={'Complemento: '}
                answer={this.state.address_complement}
                onChangeText={async text =>
                  await this.setState({ address_complement: text })
                }
              />

              <View style={{ margin: 20 }}>
                <DefaultButton
                  title={'Salvar'}
                  width={200}
                  height={40}
                  onPress={async () => {
                    await this.saveProfile();
                  }}
                />
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>

        <AwesomeAlert
          show={changePassAlert}
          showProgress={false}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          contentContainerStyle={{
            maxHeight: '80%',
            borderRadius: 25,
            backgroundColor: '#f8f8f8',
            justifyContent: 'center',
          }}
          customView={
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: 300,
                backgroundColor: '#F8F8F8',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 20,
                }}>
                <View
                  style={{
                    flexDirection: 'column',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      fontSize: 17,
                      color: '#2E2E2E',
                      margin: 0,
                    }}>
                    Nova senha:
                  </Text>
                  <CustomInput
                    question={null}
                    answer={this.state.password}
                    height={40}
                    secureTextEntry={true}
                    onChangeText={text => this.setState({ password: text })}
                    width={200}
                  />
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                  marginTop: 20,
                }}>
                <DefaultButton
                  onPress={() => {
                    this.setState({
                      changePassAlert: false,
                    });
                    this.saveNewPassword();
                  }}
                  backgroundColor={'#37153D'}
                  title={'Salvar'}
                  width={200}
                  height={40}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                }}>
                <TouchableWithoutFeedback
                  onPress={() =>
                    this.setState({
                      changePassAlert: false,
                    })
                  }>
                  <Text style={{ marginTop: 10, color: '#999' }}>Cancelar</Text>
                </TouchableWithoutFeedback>
              </View>
            </View>
          }
        />

        <AwesomeAlert
          show={showAlert}
          showProgress={false}
          closeOnHardwareBackPress={false}
          onDismiss={async () => {
            this.props.navigation.goBack();
          }}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Perfil atualizado com sucesso!
              </Text>
              <LottieView
                style={{ width: 70, height: 70 }}
                resizeMode="contain"
                autoSize
                source={animation}
                autoPlay
                loop={false}
              />
            </View>
          }
        />

        <AwesomeAlert
          show={showSuccess}
          showProgress={false}
          closeOnHardwareBackPress={false}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Senha atualizada com sucesso!
              </Text>
              <LottieView
                style={{ width: 70, height: 70 }}
                resizeMode="contain"
                autoSize
                source={animation}
                autoPlay
                loop={false}
              />
            </View>
          }
        />
      </>
    );
  }
}

const MaskedInput = ({
  question,
  answer,
  width,
  keyboardType,
  maxLength,
  onChangeText,
  placeholder,
  type,
  options,
}) => {
  return (
    <View style={{ flexDirection: 'column', marginLeft: 10, marginRight: 10 }}>
      <Text
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaPro-Medium',
          margin: 10,
          fontSize: 13,
        }}>
        {question}
      </Text>
      <TextInputMask
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaProLight',
          fontSize: 13,
          backgroundColor: '#fff',
          height: 40,
          borderRadius: 50,
          borderWidth: 0.1,
          paddingLeft: 30,
          paddingRight: 30,
          width: width ? width : 300,
        }}
        maxLength={maxLength}
        type={type}
        value={answer}
        keyboardType={keyboardType}
        onChangeText={onChangeText}
        options={options}
        placeholder={placeholder}
      />
    </View>
  );
};

const CustomInput = ({
  question,
  answer,
  width,
  onPress,
  keyboardType,
  maxLength,
  onChangeText,
  placeholder,
  secureTextEntry,
  password,
  onChangePassPress,
  editable,
}) => {
  return (
    <View style={{ flexDirection: 'column', marginLeft: 10, marginRight: 10 }}>
      <Text
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaPro-Medium',
          margin: 10,
          fontSize: 13,
        }}>
        {question}
      </Text>

      <View
        style={{
          flexDirection: 'row',
          position: 'relative',
        }}>
        <TextInput
          style={{
            color: '#2E2E2E',
            fontFamily: 'SofiaProLight',
            fontSize: 13,
            backgroundColor: '#fff',
            height: 40,
            borderRadius: 50,
            borderWidth: 0.1,
            paddingLeft: 30,
            paddingRight: 30,
            width: width ? width : 300,
          }}
          value={answer}
          maxLength={maxLength}
          keyboardType={keyboardType}
          onChangeText={onChangeText}
          placeholder={placeholder}
          secureTextEntry={secureTextEntry}
          editable={editable}
        />

        {password == true ? (
          <TouchableWithoutFeedback onPress={onChangePassPress}>
            <Text
              style={{
                position: 'absolute',
                right: 10,
                alignSelf: 'center',
                fontSize: 11,
                color: '#37153D',
                fontFamily: 'SofiaProLight',
                padding: 10,
              }}>
              Alterar
            </Text>
          </TouchableWithoutFeedback>
        ) : null}
      </View>
    </View>
  );
};

CustomInput.defaultProps = {
  placeholder: 'Não informado',
};

MaskedInput.defaultProps = {
  placeholder: 'Não informado',
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(EditStudentScreen);
