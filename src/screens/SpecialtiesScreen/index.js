import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  FlatList,
  TouchableWithoutFeedback,
} from 'react-native';

import { CheckBox } from 'react-native-elements';
import { DefaultButton } from '../../components';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { AxiosWithHeaders } from '../../services/api';
import { SafeAreaView } from 'react-native-safe-area-context';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import AwesomeAlert from 'react-native-awesome-alerts';
import LottieView from 'lottie-react-native';
import animation from '../../assets/success.json';

class SpecialtiesScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showAlert: false,
      specialties: this.props.user.specialties.map(e => e.description),
      optionsSpecialties: [
        { description: 'Hipertrofia' },
        { description: 'Emagrecimento' },
        { description: 'Condicionamento Físico' },
        { description: 'Preparação Física' },
        { description: 'Yoga' },
        { description: 'Pilates' },
        { description: 'Gestante' },
        { description: 'Kids' },
        { description: 'Atividades para Idosos' },
        { description: 'Dança' },
      ],
    };
  }

  async UNSAFE_componentWillMount() {
    await this.loadUser(this.props.user.id);
  }

  loadUser = async id => {
    const { user, token } = this.props;
    // const { specialties } = this.state;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(`/professionals/${id}`)
      .then(response => {
        this.setState({
          specialties: response.data.specialties.map(e => e.description),
        });
      })
      .catch(err => {
        console.log(err);
      });

    this.props.setUser(user);
  };

  checkItem = async item => {
    const { specialties } = this.state;
    let newArr = [];

    if (!specialties.includes(item)) {
      newArr = [...specialties, item];
    } else {
      newArr = specialties.filter(a => a !== item);
    }

    await this.setState({
      specialties: newArr,
    });

    console.log(specialties);
  };

  saveSpecialty = async () => {
    const { user, token } = this.props;
    const { id } = this.props.user;
    const { specialties } = this.state;

    const AxiosInstace = AxiosWithHeaders({ Authorization: `Bearer ${token}` });

    try {
      const response = await AxiosInstace.post(
        `professionals/${id}/specialties`,
        {
          specialties: specialties.map(e => {
            return { description: e };
          }),
        },
      );

      this.props.setUser(user);
      this.showAlert();
      console.log(response.data.data);
    } catch (err) {
      console.log(err.response.data);
    }
  };

  showAlert = () => {
    this.setState({
      showAlert: true,
    });

    setTimeout(
      () =>
        this.setState({
          showAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  render() {
    const { specialties, optionsSpecialties, showAlert } = this.state;

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: 20,
            elevation: 3,
          }}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <TouchableWithoutFeedback
              onPress={async () => {
                this.props.navigation.goBack();
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Ionicon
                  style={{
                    fontWeight: '900',
                    marginRight: 10,
                  }}
                  name={'ios-arrow-round-back'}
                  size={30}
                  color={'#37153D'}
                />
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 14,
                    color: '#37153D',
                    textAlign: 'left',
                  }}>
                  {' '}
                  Especialidades
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          <DefaultButton
            title={'Salvar'}
            width={100}
            height={35}
            margin={'0px'}
            onPress={() => {
              this.saveSpecialty();
            }}
          />
        </View>
        <ScrollView>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={optionsSpecialties}
            extraData={specialties}
            keyExtractor={specialty => String(specialty.description)}
            renderItem={({ item }) => (
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'flex-start',
                  borderBottomWidth: 1,
                  borderColor: '#E9E9E9',
                  width: '100%',
                }}>
                <CheckBox
                  title={item?.description}
                  textStyle={{
                    color: '#707070',
                    fontFamily: 'SofiaPro-Regular',
                    fontSize: 13,
                    borderWidth: 0.1,
                  }}
                  containerStyle={{
                    width: '95%',
                    backgroundColor: 'transparent',
                    borderColor: 'transparent',
                  }}
                  checkedIcon="circle"
                  uncheckedIcon="circle"
                  checkedColor="#37153D"
                  uncheckedColor="#fff"
                  onPress={() => this.checkItem(item?.description)}
                  checked={specialties.includes(item?.description)}
                />
              </View>
            )}
          />
        </ScrollView>

        <AwesomeAlert
          show={showAlert}
          showProgress={false}
          closeOnHardwareBackPress={false}
          onDismiss={async () => {
            this.props.navigation.goBack();
          }}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Especialidades atualizadas com sucesso!
              </Text>
              <LottieView
                style={{ width: 70, height: 70 }}
                resizeMode="contain"
                autoSize
                source={animation}
                autoPlay
                loop={false}
              />
            </View>
          }
        />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(SpecialtiesScreen);
