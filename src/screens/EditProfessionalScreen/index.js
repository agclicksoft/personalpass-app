import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  TextInput,
  Image,
  TouchableWithoutFeedback,
  Platform,
  Alert,
  KeyboardAvoidingView,
} from 'react-native';

import { DefaultButton, DefaultInput } from '../../components';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { AxiosWithHeaders } from '../../services/api';
import { TextInputMask } from 'react-native-masked-text';
import { SafeAreaView } from 'react-native-safe-area-context';
import { showMessage } from 'react-native-flash-message';
import birthdayIsValid from '../../services/checkBirthday';
import isUserInstagramValid from '../../services/checkInstagram';
import FormData from 'form-data';
import ImagePicker from 'react-native-image-picker';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import axios from 'axios';
import AwesomeAlert from 'react-native-awesome-alerts';
import LottieView from 'lottie-react-native';
import animation from '../../assets/success.json';
import cameraImage from '../../assets/images/camera.png';
import { BASE_URL } from '../../services/baseUrl';

class EditProfessionalScreen extends Component {
  constructor(props) {
    super(props);

    const professional = this.props.route.params.professional;
    const img_profile = this.props.route.params.img_profile;

    this.state = {
      doc_professional: professional?.doc_professional
        ? professional.doc_professional
        : '',
      name: professional?.user?.name ? professional.user.name : '',
      email: professional?.user?.email ? professional.user.email : '',
      password: '',
      cpf: professional?.user?.cpf ? professional.user.cpf : '',
      birthday: professional?.user?.birthday ? professional.user.birthday : '',
      contact: professional?.user?.contact ? professional.user.contact : '',
      emergency_phone: professional?.user?.emergency_phone
        ? professional.user.emergency_phone
        : '',
      bibliography: professional?.bibliography ? professional.bibliography : '',
      img_profile: img_profile
        ? { uri: img_profile }
        : {
            uri: 'https://static.scrum.org/web/images/profile-placeholder.png',
          },
      img_upload: '',
      social_network: professional?.instagram ? professional.instagram : '',
    };
  }

  chooseFile = () => {
    var options = {
      title: 'Escolher imagem',
      cancelButtonTitle: 'Cancelar',
      customButtons: [],
      takePhotoButtonTitle: 'Tirar foto',
      chooseFromLibraryButtonTitle: 'Escolher imagem existente',

      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
      allowsEditing: true,
      aspect: [4, 4],
      quality: 0.4,
    };

    ImagePicker.launchImageLibrary(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        this.setState({
          img_profile: response,
          img_upload: response,
        });
      }
    });
  };

  saveAvatar = async () => {
    if (this.state.img_upload !== '') {
      const { token } = this.props;
      const { img_upload } = this.state;

      const data = new FormData();
      data.append('profile', {
        name: 'profile-img',
        type: img_upload?.type,
        uri:
          Platform.OS === 'android'
            ? img_upload?.uri
            : img_upload?.uri.replace('file://', ''),
      });

      try {
        await axios({
          url: `${BASE_URL}users/profile`,
          method: 'POST',
          data: data,
          headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
            Authorization: `Bearer ${token}`,
          },
        })
          .then(function(response) {
            console.log('response :', response);
          })
          .catch(function(error) {
            console.log('error from image :' + error);
          });
      } catch (err) {
        console.log(err);
      }
    }
  };

  saveProfile = async () => {
    const { user, token } = this.props;
    const { id, user_id } = this.props.user;
    const {
      doc_professional,
      name,
      cpf,
      birthday,
      email,
      contact,
      emergency_phone,
      social_network,
      cep,
      address_uf,
      address_city,
      address_neighborhood,
      address_street,
      address_number,
      address_complement,
      bibliography,
    } = this.state;

    const AxiosInstace = AxiosWithHeaders({ Authorization: `Bearer ${token}` });

    if (birthdayIsValid(this.state.birthday)) {
      try {
        await this.saveAvatar();

        await AxiosInstace.put(`professionals/${id}`, {
          id: id,
          user_id: user_id,
          doc_professional: doc_professional,
          bibliography: bibliography,
          instagram: social_network,
          user: {
            id: user_id,
            email: email,
            birthday: birthday,
            cpf: cpf,
            name: name,
            emergency_phone: emergency_phone,
            contact: contact,
            address_zipcode: cep,
            address_street: address_street,
            address_number: address_number,
            address_complement: address_complement,
            address_city: address_city,
            address_uf: address_uf,
            address_neighborhood: address_neighborhood,
          },
        });

        user.user.name = name;
        user.user.cpf = cpf;
        user.user.birthday = birthday;
        user.user.email = email;
        user.user.contact = contact;
        user.user.emergency_phone = emergency_phone;
        user.user.instagram = social_network;
        user.user.cep = cep;
        user.user.address_street = address_street;
        user.user.address_number = address_number;
        user.user.address_complement = address_complement;
        user.user.address_city = address_city;
        user.user.address_uf = address_uf;
        user.user.address_neighborhood = address_neighborhood;

        this.props.setUser(user);
        this.showAlert();
      } catch (err) {
        console.log(err);
        showMessage({
          type: 'danger',
          message: 'Erro inesperado. Tente novamente',
          titleStyle: {
            textAlign: 'center',
          },
        });
      }
    } else {
      return Alert.alert('Atenção', 'Insira uma data de nascimento válida.');
    }
  };

  saveNewPassword = async () => {
    const { user, token } = this.props;

    const { password } = this.state;

    if (!this.state.password) {
      showMessage({
        type: 'danger',
        message: 'Favor informar nova senha',
        titleStyle: {
          textAlign: 'center',
        },
      });
      return;
    }

    const AxiosInstace = AxiosWithHeaders({ Authorization: `Bearer ${token}` });

    try {
      await AxiosInstace.post(`/users/change-password`, {
        password: password,
      });

      this.showSuccess();
      this.setState({
        password: '',
      });
      this.props.setUser(user);
    } catch (err) {
      console.log(err);
    }
  };

  showAlert = () => {
    this.setState({
      showAlert: true,
    });

    setTimeout(
      () =>
        this.setState({
          showAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  showSuccess = () => {
    this.setState({
      showSuccess: true,
    });

    setTimeout(
      () =>
        this.setState({
          showSuccess: false,
          changePassAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  showPassChange = () => {
    this.setState({ changePassAlert: !this.state.changePassAlert });
  };

  render() {
    const { showAlert, changePassAlert, showSuccess } = this.state;

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            paddingHorizontal: 20,
            paddingVertical: 20,
            elevation: 3,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Ionicon
                style={{
                  fontWeight: '900',
                  marginRight: 10,
                }}
                name={'ios-arrow-round-back'}
                size={30}
                color={'#37153D'}
              />
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#37153D',
                  textAlign: 'left',
                }}>
                Editar perfil
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <KeyboardAvoidingView
          behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
          style={{ flex: 1 }}>
          <ScrollView>
            <View style={{ flexDirection: 'column', alignItems: 'center' }}>
              <View style={{ position: 'relative' }}>
                <Image
                  style={{
                    height: 200,
                    width: 200,
                    borderRadius: 100,
                    margin: 20,
                  }}
                  source={this.state.img_profile}
                />
                <TouchableWithoutFeedback onPress={() => this.chooseFile()}>
                  <Image
                    resizeMode="contain"
                    source={cameraImage}
                    style={{
                      position: 'absolute',
                      top: 75,
                      left: 100,
                      width: 50,
                    }}
                  />
                </TouchableWithoutFeedback>
              </View>

              <MaskedInput
                type={'custom'}
                question={'CREF/CONFEF: '}
                options={{
                  mask: '999.999-A/AA',
                }}
                answer={this.state.doc_professional}
                onChangeText={text => this.setState({ doc_professional: text })}
              />

              <CustomInput
                question={'Nome: '}
                answer={this.state.name}
                onChangeText={text => this.setState({ name: text })}
                height={40}
              />
              <CustomInput
                question={'Email: '}
                answer={this.state.email}
                onChangeText={text => this.setState({ email: text })}
                height={40}
              />
              <CustomInput
                question={'Senha: '}
                answer={'xxxxxxxxxxxx'}
                height={40}
                secureTextEntry={true}
                password={true}
                editable={false}
                onChangePassPress={this.showPassChange}
              />
              <MaskedInput
                question={'CPF: '}
                type={'cpf'}
                keyboardType={'number-pad'}
                answer={this.state.cpf}
                onChangeText={text => this.setState({ cpf: text })}
              />
              <MaskedInput
                question={'Data de nascimento: '}
                type={'datetime'}
                maxLength={10}
                keyboardType={'number-pad'}
                answer={this.state.birthday}
                onChangeText={text => this.setState({ birthday: text })}
              />
              <MaskedInput
                question={'Telefone/Celular: '}
                type={'cel-phone'}
                keyboardType={'number-pad'}
                options={{
                  maskType: 'BRL',
                  withDDD: true,
                  dddMask: '(99) ',
                }}
                answer={this.state.contact}
                onChangeText={text => this.setState({ contact: text })}
              />

              <MaskedInput
                question={'Telefone de emergências: '}
                type={'cel-phone'}
                keyboardType={'number-pad'}
                options={{
                  maskType: 'BRL',
                  withDDD: true,
                  dddMask: '(99) ',
                }}
                answer={this.state.emergency_phone}
                onChangeText={text => this.setState({ emergency_phone: text })}
              />

              <CustomInput
                question={'Instagram: '}
                answer={this.state.social_network}
                onChangeText={text => this.setState({ social_network: text })}
                placeholder={'Ex.: hugosilvaoficial'}
                height={40}
              />

              <CustomInput
                question={'Biografia: '}
                answer={this.state.bibliography}
                height={300}
                borderRadius={30}
                multline={true}
                padding={20}
                onChangeText={text => this.setState({ bibliography: text })}
              />

              <View style={{ margin: 20 }}>
                <DefaultButton
                  title={'Salvar'}
                  width={200}
                  height={40}
                  onPress={async () => {
                    if (
                      isUserInstagramValid(this.state.social_network) === null
                    ) {
                      Alert.alert(
                        'Instagram inválido. Favor preencher conforme o exemplo.',
                      );
                    } else {
                      {
                        await this.setState({
                          social_network: isUserInstagramValid(
                            this.state.social_network,
                          ),
                        });
                        console.log(
                          isUserInstagramValid(this.state.social_network),
                        );
                        this.saveProfile();
                      }
                    }
                  }}
                />
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>

        <AwesomeAlert
          show={changePassAlert}
          showProgress={false}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          contentContainerStyle={{
            maxHeight: '80%',
            borderRadius: 25,
            backgroundColor: '#f8f8f8',
            justifyContent: 'center',
          }}
          customView={
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: 300,
                backgroundColor: '#F8F8F8',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 20,
                }}>
                <View
                  style={{
                    flexDirection: 'column',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      fontSize: 17,
                      color: '#2E2E2E',
                      margin: 0,
                    }}>
                    Nova senha:
                  </Text>
                  <CustomInput
                    question={null}
                    answer={this.state.password}
                    height={40}
                    secureTextEntry={true}
                    onChangeText={text => this.setState({ password: text })}
                    width={200}
                  />
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                  marginTop: 20,
                }}>
                <DefaultButton
                  onPress={() => {
                    this.setState({
                      changePassAlert: false,
                    });
                    this.saveNewPassword();
                  }}
                  backgroundColor={'#37153D'}
                  title={'Salvar'}
                  width={200}
                  height={40}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                }}>
                <TouchableWithoutFeedback
                  onPress={() =>
                    this.setState({
                      changePassAlert: false,
                    })
                  }>
                  <Text style={{ marginTop: 10, color: '#999' }}>Cancelar</Text>
                </TouchableWithoutFeedback>
              </View>
            </View>
          }
        />

        <AwesomeAlert
          show={showAlert}
          showProgress={false}
          closeOnHardwareBackPress={false}
          onDismiss={async () => {
            this.props.navigation.goBack();
          }}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Perfil atualizado com sucesso!
              </Text>
              <LottieView
                style={{ width: 70, height: 70 }}
                resizeMode="contain"
                autoSize
                source={animation}
                autoPlay
                loop={false}
              />
            </View>
          }
        />

        <AwesomeAlert
          show={showSuccess}
          showProgress={false}
          closeOnHardwareBackPress={false}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Senha atualizada com sucesso!
              </Text>
              <LottieView
                style={{ width: 70, height: 70 }}
                resizeMode="contain"
                autoSize
                source={animation}
                autoPlay
                loop={false}
              />
            </View>
          }
        />
      </>
    );
  }
}

const MaskedInput = ({
  question,
  answer,
  width,
  keyboardType,
  onChangeText,
  placeholder,
  type,
  options,
  maxLength,
}) => {
  return (
    <View style={{ flexDirection: 'column', marginLeft: 10, marginRight: 10 }}>
      <Text
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaPro-Medium',
          margin: 10,
          fontSize: 13,
        }}>
        {question}
      </Text>

      <TextInputMask
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaProLight',
          fontSize: 13,
          backgroundColor: '#fff',
          height: 40,
          borderRadius: 50,
          borderWidth: 0.1,
          paddingLeft: 30,
          paddingRight: 30,
          width: width ? width : 300,
        }}
        maxLength={maxLength}
        type={type}
        value={answer}
        keyboardType={keyboardType}
        onChangeText={onChangeText}
        options={options}
        placeholder={placeholder}
      />
    </View>
  );
};

const CustomInput = ({
  question,
  answer,
  height,
  width,
  keyboardType,
  maxLength,
  onChangeText,
  placeholder,
  multline,
  padding,
  secureTextEntry,
  password,
  onChangePassPress,
  editable,
}) => {
  return (
    <View
      style={{
        flexDirection: 'column',
        marginLeft: 10,
        marginRight: 10,
      }}>
      <Text
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaPro-Medium',
          margin: 10,
          fontSize: 13,
        }}>
        {question}
      </Text>

      <View
        style={{
          flexDirection: 'row',
          position: 'relative',
        }}>
        <TextInput
          style={{
            color: '#2E2E2E',
            fontFamily: 'SofiaProLight',
            fontSize: 13,
            backgroundColor: '#fff',
            borderRadius: 50,
            borderWidth: 0.1,
            paddingLeft: 30,
            paddingRight: 30,
            width: width ? width : 300,
            height: height,
            textAlignVertical: 'top',
            paddingTop: padding,
          }}
          multiline={multline}
          value={answer}
          maxLength={maxLength}
          keyboardType={keyboardType}
          onChangeText={onChangeText}
          placeholder={placeholder}
          autoCapitalize={false}
          secureTextEntry={secureTextEntry}
          editable={editable}
        />

        {password == true ? (
          <TouchableWithoutFeedback onPress={onChangePassPress}>
            <Text
              style={{
                position: 'absolute',
                right: 10,
                alignSelf: 'center',
                fontSize: 11,
                color: '#37153D',
                fontFamily: 'SofiaProLight',
                padding: 10,
              }}>
              Alterar
            </Text>
          </TouchableWithoutFeedback>
        ) : null}
      </View>
    </View>
  );
};

CustomInput.defaultProps = {
  placeholder: 'Não informado',
};

MaskedInput.defaultProps = {
  placeholder: 'Não informado',
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(EditProfessionalScreen);
