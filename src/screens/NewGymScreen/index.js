import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableWithoutFeedback,
  ScrollView,
  TextInput,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { AxiosWithHeaders } from '../../services/api';
import { TextInputMask } from 'react-native-masked-text';
import { DefaultButton } from '../../components';
import { SafeAreaView } from 'react-native-safe-area-context';
import { showMessage } from 'react-native-flash-message';
import Geocoder from 'react-native-geocoder';
import convertStates from '../../services/convertStates';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import AwesomeAlert from 'react-native-awesome-alerts';
import LottieView from 'lottie-react-native';
import animation from '../../assets/success.json';
import { PLACES_API } from '../../services/placesAPI';

class NewGymScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      addressId: '',
      name: '',
      cnpj: '',
      cep: '',
      address_state: '',
      address_district: '',
      address_neighbourhood: '',
      address_street: '',
      address_number: '',
      showAlert: false,
    };
  }

  async componentDidMount() {
    console.log(this.props.route.params.address);
    const address = await this.props.route.params.address;
    this.setState({
      addressId: address.id,
    });
  }

  getPosition = async (lat, lng) => {
    var pos = {
      lat: lat,
      lng: lng,
    };

    Geocoder.geocodePosition(pos)
      .then(async res => {
        await this.setState({
          cep: res[0].postalCode,
          address_street: res[0]?.streetName,
          address_number: res[0].streetNumber,
          address_state: res[0].adminArea,
          address_district: res[0].subAdminArea,
          address_neighbourhood: res[0].subLocality,
        });

        if (Platform.OS === 'ios') {
          await this.setState({
            address_state: convertStates(res[0].adminArea),
            address_district: res[0].locality,
          });
        }
      })
      .catch(err => console.log(err));
  };

  saveGym = async gym => {
    const { user, token } = this.props;
    const { id } = this.props.user;
    const {
      name,
      cnpj,
      cep,
      address_street,
      address_number,
      address_neighbourhood,
      address_district,
      address_state,
    } = this.state;

    if (!name) {
      showMessage({
        type: 'danger',
        message: 'Por favor preencher o nome e endereço da academia',
        titleStyle: {
          textAlign: 'center',
        },
      });
      return;
    }

    const AxiosInstace = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    try {
      const response = await AxiosInstace.put(
        `/professionals/${id}/service-locations/${gym}`,
        {
          name: name,
          cnpj: cnpj ?? null,
          address_zipcode: cep ?? null,
          address_street: address_street ?? null,
          address_number: address_number ?? null,
          address_neighborhood: address_neighbourhood ?? null,
          address_city: address_district ?? null,
          address_state: address_state ?? null,
        },
      );

      console.log(response.data);

      this.props.setUser(user);
      this.showAlert();
    } catch (err) {
      console.log(err.response);
      showMessage({
        type: 'danger',
        message: 'Erro inesperado. Tente novamente',
        titleStyle: {
          textAlign: 'center',
        },
      });
    }
  };

  showAlert = () => {
    this.setState({
      showAlert: true,
    });

    setTimeout(
      () =>
        this.setState({
          showAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  render() {
    const { addressId, name, cnpj, showAlert } = this.state;

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: 20,
            elevation: 3,
          }}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <TouchableWithoutFeedback
              onPress={() => {
                this.props.navigation.goBack();
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Ionicon
                  style={{
                    fontWeight: '900',
                    marginRight: 10,
                  }}
                  name={'ios-arrow-round-back'}
                  size={30}
                  color={'#37153D'}
                />
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 14,
                    color: '#37153D',
                    textAlign: 'left',
                  }}>
                  {' '}
                  Adicionar academia
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          <DefaultButton
            title={'Salvar'}
            width={100}
            height={35}
            margin={'0px'}
            onPress={() => {
              this.saveGym(addressId);
            }}
          />
        </View>

        <KeyboardAvoidingView
          behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
          style={{ flex: 1 }}>
          <ScrollView keyboardShouldPersistTaps="always">
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'center',
                marginVertical: 15,
              }}>
              <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                <View style={{ flexDirection: 'row', position: 'relative' }}>
                  <CustomInput
                    question={'Nome: '}
                    answer={name}
                    onChangeText={text => this.setState({ name: text })}
                  />
                  {!name ? (
                    <Text
                      style={{
                        marginTop: 50,
                        marginRight: 25,
                        right: 0,
                        color: 'red',
                        position: 'absolute',
                      }}>
                      *
                    </Text>
                  ) : null}
                </View>

                <MaskedInput
                  question={'CNPJ: '}
                  type={'cnpj'}
                  keyboardType={'number-pad'}
                  answer={cnpj}
                  onChangeText={text => this.setState({ cnpj: text })}
                />
                <Text
                  style={{
                    fontSize: 10,
                    marginTop: 5,
                    color: '#2E2E2E',
                    opacity: 0.5,
                  }}>
                  Caso saiba, nos informe por favor. Não obrigatório.
                </Text>

                <View
                  style={{
                    flexDirection: 'column',
                    marginLeft: 10,
                    marginRight: 10,
                  }}>
                  <Text
                    style={{
                      color: '#2E2E2E',
                      fontFamily: 'SofiaPro-Medium',
                      marginTop: 10,
                      marginLeft: 10,
                      fontSize: 13,
                      textAlign: 'left',
                    }}>
                    Endereço:
                  </Text>

                  <GooglePlacesAutocomplete
                    placeholder="Pesquisar endereço"
                    fetchDetails={true}
                    enablePoweredByContainer={false}
                    minLength={6}
                    onPress={async (data, details = null) => {
                      var lat = details.geometry.location.lat;
                      var lng = details.geometry.location.lng;

                      await this.getPosition(lat, lng);
                    }}
                    query={{
                      key: PLACES_API,
                      language: 'pt-BR',
                      components: 'country:br',
                    }}
                    styles={{
                      textInputContainer: {
                        backgroundColor: 'rgba(0,0,0,0)',
                        borderTopWidth: 0,
                        borderBottomWidth: 0,
                        maxWidth: 300,
                        minWidth: 300,
                      },
                      textInput: {
                        marginLeft: 0,
                        marginRight: 0,
                        color: '#636363',
                        fontFamily: 'SofiaProLight',
                        fontSize: 13,
                        backgroundColor: '#fff',
                        height: 40,
                        borderRadius: 50,
                        borderWidth: 0.1,
                        paddingLeft: 30,
                        paddingRight: 30,
                      },
                      listView: {
                        maxWidth: 300,
                        marginTop: 10,
                      },
                    }}
                    placeholderTextColor="#CBCBCD"
                  />
                </View>
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>

        <AwesomeAlert
          show={showAlert}
          showProgress={false}
          title="Academia salva com sucesso!"
          closeOnHardwareBackPress={false}
          onDismiss={async () => {
            this.props.navigation.navigate('ProfileProfessionalScreen');
          }}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <LottieView
              style={{ width: 70, height: 70 }}
              resizeMode="contain"
              autoSize
              source={animation}
              autoPlay
              loop={false}
            />
          }
        />
      </>
    );
  }
}

const MaskedInput = ({
  question,
  answer,
  width,
  keyboardType,
  onChangeText,
  placeholder,
  type,
  options,
}) => {
  return (
    <View style={{ flexDirection: 'column', marginLeft: 10, marginRight: 10 }}>
      <Text
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaPro-Medium',
          margin: 10,
          fontSize: 13,
        }}>
        {question}
      </Text>
      <TextInputMask
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaProLight',
          fontSize: 13,
          backgroundColor: '#fff',
          height: 40,
          borderRadius: 50,
          borderWidth: 0.1,
          paddingLeft: 30,
          paddingRight: 30,
          width: width ? width : 300,
        }}
        type={type}
        value={answer}
        keyboardType={keyboardType}
        onChangeText={onChangeText}
        options={options}
        placeholder={placeholder}
      />
    </View>
  );
};

const CustomInput = ({
  question,
  answer,
  width,
  keyboardType,
  maxLength,
  onChangeText,
  placeholder,
}) => {
  return (
    <View style={{ flexDirection: 'column', marginLeft: 10, marginRight: 10 }}>
      <Text
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaPro-Medium',
          margin: 10,
          fontSize: 13,
        }}>
        {question}
      </Text>
      <TextInput
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaProLight',
          fontSize: 13,
          backgroundColor: '#fff',
          height: 40,
          borderRadius: 50,
          borderWidth: 0.1,
          paddingLeft: 30,
          paddingRight: 30,
          width: width ? width : 300,
        }}
        value={answer}
        maxLength={maxLength}
        keyboardType={keyboardType}
        onChangeText={onChangeText}
        placeholder={placeholder}
      />
    </View>
  );
};

CustomInput.defaultProps = {
  placeholder: 'Não informado',
};

MaskedInput.defaultProps = {
  placeholder: 'Não informado',
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(NewGymScreen);
