import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  TextInput,
  TouchableWithoutFeedback,
} from 'react-native';

import { DefaultButton } from '../../components';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { AxiosWithHeaders } from '../../services/api';
import { SafeAreaView } from 'react-native-safe-area-context';
import RNPickerSelect from 'react-native-picker-select';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import AwesomeAlert from 'react-native-awesome-alerts';
import LottieView from 'lottie-react-native';
import animation from '../../assets/success.json';
import { bankList } from '../../utils/bankList';

class BankScreen extends Component {
  constructor(props) {
    super(props);

    const professional = this.props.route.params.professional;

    this.state = {
      bank_number: professional?.bank_number ? professional.bank_number : '',
      bank_agency: professional?.bank_agency ? professional.bank_agency : '',
      bank_type: professional?.bank_type ? professional.bank_type : '',
      bank_name: professional?.bank_name ? professional.bank_name : '',
    };
  }

  saveBankData = async () => {
    const { user, token } = this.props;
    const { id } = this.props.user;
    const { bank_number, bank_agency, bank_type, bank_name } = this.state;

    const AxiosInstace = AxiosWithHeaders({ Authorization: `Bearer ${token}` });

    try {
      await AxiosInstace.post(`professionals/${id}/account`, {
        bank_number,
        bank_agency,
        bank_type,
        bank_name,
      });

      user.bank_number = bank_number;
      user.bank_agency = bank_agency;
      user.bank_type = bank_type;
      user.bank_name = bank_name;
    } catch (err) {
      console.log(err);
    }

    this.props.setUser(user);
    this.showAlert();
  };

  showAlert = () => {
    this.setState({
      showAlert: true,
    });

    setTimeout(
      () =>
        this.setState({
          showAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  render() {
    const { showAlert } = this.state;

    const style = {
      inputAndroid: {
        width: 280,
        height: 40,
        color: '#000',
        textAlign: 'center',
        fontFamily: 'SofiaProLight',
        fontSize: 18,
        margin: 10,
      },
      inputIOS: {
        width: 280,
        height: 35,
        color: '#000',
        textAlign: 'center',
        fontFamily: 'SofiaProLight',
        fontSize: 18,
        margin: 10,
      },
    };

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            paddingHorizontal: 20,
            paddingVertical: 20,
            elevation: 3,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Ionicon
                style={{
                  fontWeight: '900',
                  marginRight: 10,
                }}
                name={'ios-arrow-round-back'}
                size={30}
                color={'#37153D'}
              />
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#37153D',
                  textAlign: 'left',
                }}>
                Conta bancária
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <ScrollView>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
              padding: 0,
            }}>
            <View style={{ height: 10 }} />

            <View
              style={{
                flexDirection: 'column',
                alignItems: 'flex-start',
                marginTop: 30,
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#2E2E2E',
                  textAlign: 'left',
                  marginLeft: 10,
                  padding: 5,
                }}>
                Tipo de conta:
              </Text>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  backgroundColor: '#fff',
                  borderColor: '#707070',
                  borderWidth: 0.1,
                  borderRadius: 30,
                  width: 300,
                  height: 50,
                }}>
                <RNPickerSelect
                  style={style}
                  placeholder={{
                    label: 'Selecionar conta',
                  }}
                  doneText={'Selecionar'}
                  useNativeAndroidPickerStyle={false}
                  onValueChange={(itemValue, itemIndex) => {
                    this.setState({ bank_type: itemValue });
                  }}
                  value={this.state.bank_type}
                  items={[
                    {
                      label: 'Corrente',
                      value: 'corrente',
                      key: 'corrente',
                    },
                    {
                      label: 'Poupança',
                      value: 'poupanca',
                      key: 'poupanca',
                    },
                  ]}
                />
              </View>
            </View>

            <View
              style={{
                flexDirection: 'column',
                alignItems: 'flex-start',
                marginTop: 30,
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#2E2E2E',
                  textAlign: 'left',
                  marginLeft: 10,
                  padding: 5,
                }}>
                Banco:
              </Text>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  backgroundColor: '#fff',
                  borderColor: '#707070',
                  borderWidth: 0.1,
                  borderRadius: 30,
                  width: 300,
                  height: 50,
                }}>
                <RNPickerSelect
                  style={style}
                  placeholder={{
                    label: 'Selecionar banco',
                  }}
                  doneText={'Selecionar'}
                  useNativeAndroidPickerStyle={false}
                  onValueChange={(itemValue, itemIndex) => {
                    this.setState({ bank_name: itemValue });
                  }}
                  value={this.state.bank_name}
                  items={bankList}
                />
              </View>
            </View>

            <View
              style={{
                flexDirection: 'column',
                alignItems: 'flex-start',
                marginTop: 30,
                marginBottom: 15,
              }}>
              <View style={{ flexDirection: 'row' }}>
                <CustomInput
                  question={'Agência: '}
                  keyboardType={'numeric'}
                  maxLength={15}
                  width={140}
                  answer={this.state.bank_agency}
                  onChangeText={text => {
                    this.setState({ bank_agency: text });
                  }}
                />
                <CustomInput
                  question={'Conta: '}
                  keyboardType={'numeric'}
                  maxLength={15}
                  width={140}
                  answer={this.state.bank_number}
                  onChangeText={async text => {
                    await this.setState({ bank_number: text });
                  }}
                />
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <DefaultButton
                title={'Salvar'}
                width={180}
                height={35}
                margin={'20px'}
                onPress={() => {
                  this.saveBankData();
                }}
              />
            </View>
          </View>
        </ScrollView>

        <AwesomeAlert
          show={showAlert}
          showProgress={false}
          closeOnHardwareBackPress={false}
          onDismiss={async () => {
            this.props.navigation.goBack();
          }}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Dados atualizados com sucesso!
              </Text>
              <LottieView
                style={{ width: 70, height: 70 }}
                resizeMode="contain"
                autoSize
                source={animation}
                autoPlay
                loop={false}
              />
            </View>
          }
        />
      </>
    );
  }
}

const CustomInput = ({
  question,
  answer,
  width,
  onPress,
  keyboardType,
  maxLength,
  onChangeText,
  placeholder,
}) => {
  return (
    <View style={{ flexDirection: 'column', marginLeft: 10, marginRight: 10 }}>
      <Text
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaPro-Medium',
          margin: 10,
          fontSize: 13,
        }}>
        {question}
      </Text>
      <TextInput
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaProLight',
          fontSize: 18,
          backgroundColor: '#fff',
          height: 40,
          borderRadius: 50,
          borderWidth: 0.1,
          paddingLeft: 15,
          paddingRight: 15,
          width: width ? width : 100,
        }}
        value={answer}
        maxLength={maxLength}
        keyboardType={keyboardType}
        onChangeText={onChangeText}
        placeholder={placeholder}
      />
    </View>
  );
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(BankScreen);
