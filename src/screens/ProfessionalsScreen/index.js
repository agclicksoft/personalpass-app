import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  RefreshControl,
  Image,
  TouchableNativeFeedback,
  Platform,
  Alert,
  TextInput,
  KeyboardAvoidingView
} from 'react-native';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import {
  DefaultButton,
  LoadingModal,
  ProfessionalItem,
  SearchInput,
} from '../../components';
import { AxiosWithHeaders } from '../../services/api';
import { SafeAreaView } from 'react-native-safe-area-context';
import LottieView from 'lottie-react-native';
import RNBootSplash from 'react-native-bootsplash';
import Geolocation from 'react-native-geolocation-service';
import Geocoder from 'react-native-geocoder';
import convertStates from '../../services/convertStates';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import barLogo from '../../assets/images/bar-logo.png';
import mapSrc from '../../assets/images/maps-and-flags.png';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import AwesomeAlert from 'react-native-awesome-alerts';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { PLACES_API } from '../../services/placesAPI';
import animation from '../../assets/success.json';
import { showMessage } from 'react-native-flash-message';

class ProfessionalsScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      address: '',
      street: '',
      streetNumber: '',
      state: '',
      district: '',
      neighborhood: '',
      searchContent: '',
      professionalsList: [],
      searchText: '',
      showModal: false,
      loading: true,
      refreshing: false,
      changePassAlert: false,
      password: '',
      page: 1,
      lastPage: 1,
      querySaved: '',
    };
  }


  async componentDidMount() {
    await RNBootSplash.hide({ duration: 500 }), 2000;

    try {
      await this.getPosition();
    } catch (err) {
      console.log(err);
    }
  }

  async UNSAFE_componentWillReceiveProps(nextProps) {
    try {
      const { query } = nextProps.route.params ? nextProps.route.params : '';
      this.setState({ querySaved: query, page: 1 });
      this.loadProfessionals(query);
    } catch (err) {
      console.log(err);
    }
  }

  onSearchTextChanged = text => this.setState({ searchText: text });

  getPosition = async (lat, lng) => {
    const { query } = this.props.route.params ? this.props.route.params : '';
    this.setState({ querySaved: query });

    Geolocation.getCurrentPosition(
      position => {
        var pos = {
          lat: lat ?? position.coords.latitude,
          lng: lng ?? position.coords.longitude,
        };

        Geocoder.geocodePosition(pos)
          .then(async res => {
            await this.setState({
              address: `${
                res[0]?.streetName != null ? `${res[0].streetName}, ` : ''
              }${
                res[0]?.streetNumber != null ? `${res[0].streetNumber} - ` : ''
              }${res[0].subLocality}`,
              street: res[0]?.streetName,
              streetNumber: res[0].streetNumber,
              state: res[0].adminArea,
              district: res[0].subAdminArea,
              neighborhood: res[0].subLocality,
            });

            if (Platform.OS === 'ios') {
              await this.setState({
                state: convertStates(res[0].adminArea),
                district: res[0].locality,
              });
            }


            this.setState({
              changePassAlert: this.props?.user?.user?.is_redefinition,
            });

            this.loadProfessionals(query);
          })
          .catch(err => console.log(err));
      },
      error => {
        this.setState({
          error: error.code,
        }),
          error.code === 1
            ? Alert.alert(
                'Por favor, ative o acesso do aplicativo à sua localização, assim você terá uma melhor experiência.',
              )
            : console.log(error);
      },
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );

    this.loadProfessionals(query);
  };

  _onRefresh = async () => {
    await this.setState({ professionalsList: [], loading: true, page: 1 });
    await this.getPosition();
  };

  loadProfessionals = async query => {
    const { token } = this.props;
    const { page, state, district, neighborhood } = this.state;

    await this.setState({ professionalsList: [], loading: true });

    if (!query)
      query = `state=${state}&district=${district}&neighborhood=${neighborhood}`;
    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    console.log(query);

    await AxiosInstance.get(`/professionals?page=${page}&${query}`)
      .then(response => {
        this.setState({
          professionalsList: response.data.data,
          lastPage: response.data.lastPage,
          loading: false,
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  loadOnScroll = async query => {
    const { token } = this.props;
    const { page, state, district, neighborhood } = this.state;

    if (!query)
      query = `state=${state}&district=${district}&neighborhood=${neighborhood}`;
    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(`/professionals?page=${page}&${query}`)
      .then(response => {
        let aux = this.state.professionalsList;
        response.data.data.map(item => {
          aux.push(item);
        });

        this.setState({
          professionalsList: aux,
          lastPage: response.data.lastPage,
          loading: false,
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  clearLocalityFilter = async query => {
    const { token } = this.props;
    const { page } = this.state;

    await this.setState({
      professionalsList: [],
      loading: true,
      querySaved: '',
    });

    // if (!query) query = '';
    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(`/professionals?page=${page}`)
      .then(response => {
        console.log('Professionals search succeeded.');
        console.log(query);

        this.setState({
          professionalsList: response.data.data,
          lastPage: response.data.lastPage,
          loading: false,
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  showPassChange = () => {
    this.setState({ changePassAlert: !this.state.changePassAlert });
  };

  showSuccess = () => {
    this.setState({
      showSuccess: true,
    });

    setTimeout(
      () =>
        this.setState({
          showSuccess: false,
          changePassAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  saveNewPassword = async () => {
    const { user, token } = this.props;
    const { id, user_id } = this.props.user;

    const { password } = this.state;

    if (!password) {
      showMessage({
        type: 'danger',
        message: 'Favor informar nova senha',
        titleStyle: {
          textAlign: 'center',
        },
      });

      this.setState({
        changePassAlert: true,
      })

      return;
    }

    const AxiosInstace = AxiosWithHeaders({ Authorization: `Bearer ${token}` });

    try {
      await AxiosInstace.post(`/users/change-password`, {
        password: password,
      });

      this.showSuccess();
      this.setState({
        password: '',
      });

      await AxiosInstace.put(`/students/${id}`, {
        id: id,
        user_id: user_id,
        user: {
          id: user_id,
          is_redefinition: false
        },
      });

      user.user.is_redefinition = false;
      this.props.setUser(user);
    } catch (err) {
      console.log(err);
      showMessage({
        type: 'danger',
        message: 'Erro inesperado. Tente novamente',
        titleStyle: {
          textAlign: 'center',
        },
      });
    }
  };

  render() {
    const {
      professionalsList,
      searchText,
      loading,
      querySaved,
      showModal,
      changePassAlert,
      showSuccess
    } = this.state;

    var professionals = professionalsList;

    professionals = professionalsList.filter(result => {
      let regex = new RegExp(searchText.toLowerCase()).test(
        result.user?.name?.toLowerCase(),
      );
      return regex;
    });

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#fff' }} />
        <View
          style={{
            backgroundColor: '#fff',
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2.1,
            },
            shadowOpacity: 0.2,
            shadowRadius: 1.41,
            elevation: 2,
            alignItems: 'center',
            padding: 32,
          }}>
          <Image
            style={{ width: 180, height: 25, resizeMode: 'contain' }}
            source={barLogo}
          />
        </View>

        <View
          style={{
            marginHorizontal: 20,
          }}>
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: '#512758',
              padding: 5,
              borderRadius: 100,
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              marginTop: 15,
            }}>
            <View style={{ flex: 1, padding: 8, marginLeft: 10 }}>
              <Image
                style={{ height: 35, width: 35 }}
                source={mapSrc}
                resizeMode="contain"
              />
            </View>
            {this.state.address ? (
              <View style={{ flex: 7, padding: 8, marginRight: 30 }}>
                <Text
                  style={{
                    fontFamily: 'SofiaProLight',
                    fontSize: 11,
                    color: '#f8f8f8',
                  }}>
                  Você está em:
                </Text>

                <Text
                  style={{
                    fontFamily: 'SofiaProLight',
                    fontSize: 13,
                    color: '#f8f8f8',
                  }}>
                  {this.state.address
                    ? this.state.address
                    : 'Não foi possível encontrar sua localização.'}
                </Text>
              </View>
            ) : (
              <View style={{ flex: 7, padding: 8 }}>
                <Text
                  style={{
                    fontFamily: 'SofiaProLight',
                    fontSize: 11,
                    color: '#f8f8f8',
                  }}>
                  {this.state.error == 1
                    ? 'Não foi possível acessar sua localização.'
                    : 'Carregando localização...'}
                </Text>
                <Text
                  style={{
                    fontFamily: 'SofiaProLight',
                    fontSize: 13,
                    color: '#f8f8f8',
                  }}>
                  {this.state.error == 1
                    ? 'Verifique se a mesma está ativa nas configurações do seu aparelho.'
                    : 'Por favor aguarde.'}
                </Text>
              </View>
            )}

            <TouchableWithoutFeedback
              style={{ padding: 20, alignItems: 'center' }}
              onPress={async () => {
                this.setState({
                  showModal: true,
                });
              }}>
              <Ionicon
                style={{
                  fontWeight: '900',
                  transform: [{ rotate: '270deg' }],
                }}
                name={'ios-arrow-back'}
                size={21}
                color={'#FFFFFF'}
              />
            </TouchableWithoutFeedback>
          </View>

          <View style={{ margin: 15 }}>
            <Text
              style={{
                fontFamily: 'SofiaPro-Regular',
                fontSize: 15,
                color: '#F76E1E',
                textAlign: 'left',
              }}>
              Encontre profissionais perto de você...
            </Text>
          </View>

          <SearchInput
            value={this.state.searchText}
            onChangeText={this.onSearchTextChanged}
            filterAction={() => {
              this.props.navigation.navigate('FilterScreen');
            }}
          />
        </View>

        <ScrollView
          style={{
            backgroundColor: '#F2F2F2',
          }}
          contentContainerStyle={{ flexGrow: 1 }}
          onScroll={async ({ nativeEvent }) => {
            if (isCloseToBottom(nativeEvent)) {
              console.log('Close to to bottom');
              if (
                !this.state.loading &&
                this.state.page < this.state.lastPage
              ) {
                let auxPage = this.state.page;

                auxPage++;

                await this.setState({ page: auxPage }, () => {
                  console.log('QUERY SALVA É:', querySaved);
                  this.loadOnScroll(querySaved);
                });
              }
            }
          }}
          scrollEventThrottle={160}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={async () => await this._onRefresh()}
            />
          }>
          <View
            style={{
              alignItems: 'center',
              marginHorizontal: 20,
              marginBottom: 20,
              flex: 1,
            }}>
            {this.state.querySaved ? (
              <TouchableNativeFeedback
                onPress={async () => {
                  await this.setState({ querySaved: '', page: 1 });
                  await this.loadProfessionals();
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alginItems: 'center',
                    justifyContent: 'space-between',
                    padding: 5,
                    borderRadius: 50,
                    paddingHorizontal: 20,
                    backgroundColor: '#F76E1E',
                    margin: 10,
                  }}>
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Regular',
                      marginLeft: 5,
                      fontSize: 15,
                      color: '#fff',
                      textAlign: 'left',
                    }}>
                    Limpar filtro de pesquisa
                  </Text>
                </View>
              </TouchableNativeFeedback>
            ) : null}

            {professionals.map(item => {
              return item ? (
                <ProfessionalItem
                  key={item.user.id}
                  name={item.user.name}
                  avatar={{
                    uri: item?.user?.img_profile
                      ? item?.user?.img_profile + '?' + new Date().getTime()
                      : 'https://static.scrum.org/web/images/profile-placeholder.png',
                  }}
                  specialties={item.specialties
                    .map(e => e.description)
                    .join(', ')}
                  description={item.bibliography}
                  onPress={() => {
                    this.props.navigation.navigate(
                      'ProfessionalProfileScreen',
                      { professional: item },
                    );
                  }}
                />
              ) : null;
            })}

            {professionals == '' && loading == false ? (
              <View style={{ alignItems: 'center', marginTop: 70 }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Regular',
                    flexWrap: 'wrap',
                    color: '#F76E1E',
                    fontSize: 13,
                    textAlign: 'center',
                  }}>
                  Não há nenhum profissional perto de você neste momento.
                </Text>
                <DefaultButton
                  title={'Retirar filtro de localidade'}
                  onPress={async () => {
                    await this.clearLocalityFilter(querySaved);
                  }}
                  width={220}
                  height={40}
                />
              </View>
            ) : null}
          </View>
        </ScrollView>

        <LoadingModal show={this.state.loading} />

        <AwesomeAlert
          show={showModal}
          showProgress={false}
          closeOnTouchOutside
          closeOnHardwareBackPress
          showCancelButton
          showConfirmButton={
            this.state.street != '' &&
            this.state.streetNumber != '' &&
            this.state.neighborhood != '' &&
            this.state.district != ''
              ? true
              : false
          }
          onConfirmPressed={() => {
            this.setState({
              address: `${
                this.state.street != null ? `${this.state.street}, ` : ''
              }${
                this.state.streetNumber != null
                  ? `${this.state.streetNumber} - `
                  : ''
              }${this.state.neighborhood}`,

              showModal: !this.state.showModal,
            });

            this.loadProfessionals();
          }}
          onCancelPressed={() => {
            this.setState({
              showModal: !this.state.showModal,
            });
          }}
          confirmText={'Confirmar'}
          cancelText={'Voltar'}
          actionContainerStyle={{
            flexDirection: 'column-reverse',
            alignItems: 'center',
            justifyContent: 'center',
          }}
          confirmButtonStyle={{
            width: 200,
            height: 40,
            backgroundColor: '#F76E1E',
            borderRadius: 25,
          }}
          cancelButtonStyle={{
            width: 200,
            height: 40,
            backgroundColor: 'transparent',
            borderRadius: 25,
          }}
          confirmButtonTextStyle={{
            textAlign: 'center',
            fontFamily: 'SofiaPro-Medium',
            letterSpacing: 0,
            color: '#fff',
            fontSize: 15,
          }}
          cancelButtonTextStyle={{
            textAlign: 'center',
            fontFamily: 'SofiaPro-Medium',
            letterSpacing: 0,
            color: '#707070',
            fontSize: 15,
          }}
          contentStyle={{
            backgroundColor: '#f8f8f8',
            borderRadius: 30,
            maxHeight: '80%',
          }}
          contentContainerStyle={{
            borderRadius: 30,
            backgroundColor: '#f8f8f8',
            maxHeight: '70%',
            marginTop: 40
          }}
          customView={
            <View>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 22,
                  color: '#37153D',
                  textAlign: 'center',
                  marginBottom: 20,
                }}>
                Insira a localização desejada
              </Text>

              <GooglePlacesAutocomplete
                  placeholder="Pesquisar endereço"
                  fetchDetails={true}
                  enablePoweredByContainer={false}
                  minLength={6}
                  onPress={async (data, details = null) => {
                    var lat = details.geometry.location.lat;
                    var lng = details.geometry.location.lng;

                    await this.getPosition(lat, lng);
                  }}
                  query={{
                    key: PLACES_API,
                    language: 'pt-BR',
                    components: 'country:br',
                  }}
                  styles={{
                    textInputContainer: {
                      backgroundColor: 'rgba(0,0,0,0)',
                      borderTopWidth: 0,
                      borderBottomWidth: 0,
                      maxWidth: '100%',
                      minWidth: '100%',
                    },
                    textInput: {
                      marginLeft: 0,
                      marginRight: 0,
                      color: '#636363',
                      fontFamily: 'SofiaProLight',
                      fontSize: 13,
                      backgroundColor: '#fff',
                      height: 40,
                      borderRadius: 50,
                      borderWidth: 0.1,
                      paddingLeft: 30,
                      paddingRight: 30,
                    },
                    listView: {
                      maxWidth: '100%',
                      marginTop: 10,
                      backgroundColor: '#fff',
                    },
                    container: {
                      alignSelf: 'center',
                    },
                  }}
                  placeholderTextColor="#CBCBCD"
                />
            </View>
          }
        />

        <AwesomeAlert
          show={changePassAlert}
          showProgress={false}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          contentContainerStyle={{
            borderRadius: 25,
            backgroundColor: '#f8f8f8',
            justifyContent: 'center',
          }}
          customView={
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: 300,
                backgroundColor: '#F8F8F8',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}>
                <View
                  style={{
                    flexDirection: 'column',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      fontSize: 17,
                      color: '#2E2E2E',
                      margin: 0,
                    }}>
                    Insira aqui sua nova senha:
                  </Text>
                  <CustomInput
                    question={null}
                    answer={this.state.password}
                    height={40}
                    secureTextEntry
                    onChangeText={text => this.setState({ password: text })}
                    width={200}
                  />
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                  marginTop: 20,
                }}>
                <DefaultButton
                  onPress={() => {
                    this.setState({
                      changePassAlert: false,
                    });
                    this.saveNewPassword();
                  }}
                  backgroundColor={'#37153D'}
                  title={'Salvar'}
                  width={200}
                  height={40}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                }}>
                <TouchableWithoutFeedback
                  onPress={() =>
                    this.setState({
                      changePassAlert: false,
                    })
                  }>
                  <Text style={{ marginTop: 10, color: '#999' }}>Deixar para depois</Text>
                </TouchableWithoutFeedback>
              </View>
            </View>
          }
        />

        <AwesomeAlert
          show={showSuccess}
          showProgress={false}
          closeOnHardwareBackPress={false}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Senha atualizada com sucesso!
              </Text>
              <LottieView
                style={{ width: 70, height: 70 }}
                resizeMode="contain"
                autoSize
                source={animation}
                autoPlay
                loop={false}
              />
            </View>
          }
        />
      </>
    );
  }
}

const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
  const paddingToBottom = 20;
  return (
    layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom
  );
};

const CustomInput = ({
  question,
  answer,
  width,
  onPress,
  keyboardType,
  maxLength,
  onChangeText,
  placeholder,
  secureTextEntry,
  password,
  onChangePassPress,
  editable,
}) => {
  return (
    <View style={{ flexDirection: 'column', marginLeft: 10, marginRight: 10 }}>
      <Text
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaPro-Medium',
          margin: 10,
          fontSize: 13,
        }}>
        {question}
      </Text>

      <View
        style={{
          flexDirection: 'row',
          position: 'relative',
        }}>
        <TextInput
          style={{
            color: '#2E2E2E',
            fontFamily: 'SofiaProLight',
            fontSize: 13,
            backgroundColor: '#fff',
            height: 40,
            borderRadius: 50,
            borderWidth: 0.1,
            paddingLeft: 30,
            paddingRight: 30,
            width: width ? width : 300,
          }}
          value={answer}
          maxLength={maxLength}
          keyboardType={keyboardType}
          onChangeText={onChangeText}
          placeholder={placeholder}
          secureTextEntry={secureTextEntry}
          editable={editable}
        />

        {password == true ? (
          <TouchableWithoutFeedback onPress={onChangePassPress}>
            <Text
              style={{
                position: 'absolute',
                right: 10,
                alignSelf: 'center',
                fontSize: 11,
                color: '#37153D',
                fontFamily: 'SofiaProLight',
                padding: 10,
              }}>
              Alterar
            </Text>
          </TouchableWithoutFeedback>
        ) : null}
      </View>
    </View>
  );
};

CustomInput.defaultProps = {
  placeholder: 'Não informado',
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(ProfessionalsScreen);
