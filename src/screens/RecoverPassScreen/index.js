import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Alert,
  KeyboardAvoidingView,
} from 'react-native';
import { DefaultButton, DefaultInput, LoadingModal } from '../../components';
import { showMessage } from 'react-native-flash-message';
import { ImageBackground, Logo } from './styles';
import loginBg from '../../assets/images/background.png';
import logo from '../../assets/images/logo.png';
import API, { AxiosWithHeaders } from '../../services/api';

class RecoverPassScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      loading: false,
    };
  }

  onEmailChanged = email => this.setState({ email });

  recover = async () => {
    const { email } = this.state;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer`,
    });

    if (email == '') {
      showMessage({
        type: 'danger',
        message: 'Favor informar seu e-mail',
        titleStyle: {
          textAlign: 'center',
        },
      });

      return;
    }

    try {
      this.setState({ loading: true });
      const response = await AxiosInstance.post(`/users/forgot-password`, {
        email: email,
      });

      if (response.status === 204) {
        this.setState({
          loading: false,
        });

        showMessage({
          type: 'success',
          message: 'Tudo certo!',
          description: 'Verifique seu e-mail.',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });

        this.props.navigation.navigate('AccessScreen');
      }
    } catch (err) {
      console.log('We got an error', err.response);
      this.setState({
        loading: false,
      });

      if (err.response.status === 404) {
        showMessage({
          type: 'danger',
          message: 'Usuário não encontrado',
          description: 'Tente novamente',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      } else {
        showMessage({
          type: 'danger',
          message: 'Erro inesperado',
          description: 'Tente novamente',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      }
    }
  };

  render() {
    return (
      <>
        <ImageBackground source={loginBg}>
          <KeyboardAvoidingView
            behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
            style={{ flex: 1 }}>
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
              <View style={{ flex: 1, justifyContent: 'center' }}>
                <View
                  style={{
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    flex: 1,
                  }}>
                  <Logo
                    source={logo}
                    style={{ alignItems: 'center', height: 170, width: 170 }}
                    resizeMode="contain"
                  />
                </View>

                <View
                  style={{
                    backgroundColor: '#F8F8F8',
                    marginTop: 40,
                    padding: 20,
                    paddingBottom: 60,
                    borderTopLeftRadius: 40,
                    borderTopRightRadius: 40,
                    flex: 1,
                  }}>
                  <Text
                    style={{
                      fontSize: 23,
                      fontFamily: 'SofiaPro-Medium',
                      textAlign: 'center',
                      padding: 10,
                      color: '#F76E1E',
                    }}>{`Esqueci minha senha`}</Text>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'SofiaPro-Regular',
                      textAlign: 'center',
                      color: '#707070',
                      paddingBottom: 30,
                    }}>{`Nos informe seu e-mail de cadastro.`}</Text>
                  <View
                    style={{
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <DefaultInput
                      onChangeText={this.onEmailChanged}
                      value={this.state.email}
                      placeholder={'E-mail'}
                    />

                    <DefaultButton
                      onPress={() => {
                        this.recover();
                      }}
                      title={'Recuperar senha'}
                      width={190}
                    />

                    <View
                      style={{
                        justifyContent: 'center',
                        flexDirection: 'row',
                        alignItems: 'center',
                        margin: 10,
                        padding: 5,
                      }}>
                      <Text
                        style={{
                          fontFamily: 'SofiaProLight',
                          fontSize: 12,
                          textAlign: 'center',
                          borderRadius: 20,
                          color: '#37153D',
                        }}>{`Lembrou seu acesso?`}</Text>
                      <Text
                        onPress={() =>
                          this.props.navigation.navigate('AccessScreen')
                        }
                        style={{
                          fontFamily: 'SofiaPro-Bold',
                          fontSize: 12,
                          textAlign: 'center',
                          borderRadius: 20,
                          color: '#37153D',
                        }}>{` Entre agora.`}</Text>
                    </View>
                  </View>
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </ImageBackground>

        <LoadingModal show={this.state.loading} />
      </>
    );
  }
}

export default RecoverPassScreen;
