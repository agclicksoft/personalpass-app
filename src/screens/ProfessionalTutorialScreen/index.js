import React, { Component } from 'react';
import { Image, Dimensions } from 'react-native';
import { Creators as authActions } from '../../redux/reducers/auth';
import { connect } from 'react-redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import Onboarding from 'react-native-onboarding-swiper';
import logo from '../../assets/images/logo.png';
import profile from '../../assets/images/ProfessionalOnboarding/edit-profile.gif';
import agenda from '../../assets/images/ProfessionalOnboarding/agenda.gif';
import gyms from '../../assets/images/ProfessionalOnboarding/gyms.gif';
import specialty from '../../assets/images/ProfessionalOnboarding/specialty.gif';
import plans from '../../assets/images/ProfessionalOnboarding/plans.gif';
import bank from '../../assets/images/ProfessionalOnboarding/bank.gif';
import oitava from '../../assets/images/ProfessionalOnboarding/oitava.png';

const dimensions = Dimensions.get('window');
const imageHeight = Math.round((dimensions.width * 9) / 16);

class ProfessionalTutorialScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <Onboarding
          nextLabel={'Avançar'}
          showSkip={false}
          containerStyles={{
            justifyContent: 'flex-start',
          }}
          titleStyles={{
            paddingHorizontal: 20,
            paddingVertical: 0,
            fontSize: 22,
          }}
          imageContainerStyles={{
            paddingVertical: 20,
          }}
          onDone={() => this.props.navigation.navigate('MyProfessionalProfile')}
          pages={[
            {
              backgroundColor: '#37153D',
              image: (
                <>
                  <SafeAreaView style={{ backgroundColor: '#37153D' }} />
                  <Image
                    source={logo}
                    resizeMode={'contain'}
                    style={{ height: 150, marginTop: 100, marginBottom: 100 }}
                  />
                </>
              ),
              title: 'Olá! Seja bem-vindo(a)!',
              subtitle: 'Vamos lhe guiar em seus primeiros passos.',
            },
            {
              backgroundColor: '#fe6e58',
              image: (
                <>
                  <SafeAreaView style={{ backgroundColor: '#fe6e58' }} />
                  <Image
                    source={profile}
                    resizeMode={'contain'}
                    // style={{ height: 400, marginTop: 30 }}
                    style={{ height: imageHeight * 1.5, marginTop: 30 }}
                  />
                </>
              ),
              title:
                'Primeiro, você deve preencher as informações em seu perfil.',
              subtitle:
                'Para isto, clique em editar perfil e preencha todos os campos necessários.',
            },
            {
              backgroundColor: '#d6e6f2',
              image: (
                <>
                  <SafeAreaView style={{ backgroundColor: '#d6e6f2' }} />
                  <Image
                    source={agenda}
                    resizeMode={'contain'}
                    // style={{ height: 400, marginTop: 30 }}
                    style={{ height: imageHeight * 1.5, marginTop: 30 }}
                  />
                </>
              ),
              title: 'O segundo passo é preencher sua agenda.',
              subtitle:
                'Selecione os dias e horários em que estará disponível.',
            },
            {
              backgroundColor: '#febf63',
              image: (
                <>
                  <SafeAreaView style={{ backgroundColor: '#febf63' }} />
                  <Image
                    source={gyms}
                    resizeMode={'contain'}
                    // style={{ height: 400, marginTop: 30 }}
                    style={{ height: imageHeight * 1.5, marginTop: 30 }}
                  />
                </>
              ),
              title: 'Quais serão seus locais de atendimento?',
              subtitle:
                'Na aba locais de atendimento escolha entre as academias disponíveis. Caso a sua não se encontre na lista, você pode adicioná-la! ',
            },
            {
              backgroundColor: '#aa96da',
              image: (
                <>
                  <SafeAreaView style={{ backgroundColor: '#aa96da' }} />
                  <Image
                    source={specialty}
                    resizeMode={'contain'}
                    // style={{ height: 400, marginTop: 30 }}
                    style={{ height: imageHeight * 1.5, marginTop: 30 }}
                  />
                </>
              ),
              title: 'Agora acesse a aba Especialidades.',
              subtitle:
                'Selecione suas especialidades de destaque para que seus alunos possam lhe encontrar com maior facilidade!',
            },
            {
              backgroundColor: '#c8f4de',
              image: (
                <>
                  <SafeAreaView style={{ backgroundColor: '#c8f4de' }} />
                  <Image
                    source={plans}
                    resizeMode={'contain'}
                    // style={{ height: 400, marginTop: 30 }}
                    style={{ height: imageHeight * 1.5, marginTop: 30 }}
                  />
                </>
              ),
              title: 'Agora acesse Pacotes de serviços.',
              subtitle:
                'Para que seu perfil seja exibido, você deve cadastrar ao menos um plano de aula avulsa e um pacote de aulas!',
            },
            {
              backgroundColor: '#ffc8c8',
              image: (
                <>
                  <SafeAreaView style={{ backgroundColor: '#ffc8c8' }} />
                  <Image
                    source={bank}
                    resizeMode={'contain'}
                    // style={{ height: 400, marginTop: 30 }}
                    style={{ height: imageHeight * 1.5, marginTop: 30 }}
                  />
                </>
              ),
              title: 'E por último, acesse a aba Conta bancária.',
              subtitle:
                'Nela, você cadastrará sua conta para recebimento quando os pagamentos acontecerem por nossa plataforma.',
            },
            {
              backgroundColor: '#99b898',
              image: (
                <>
                  <SafeAreaView style={{ backgroundColor: '#99b898' }} />
                  <Image
                    source={oitava}
                    resizeMode={'contain'}
                    // style={{ height: 400, marginTop: 30 }}
                    style={{ height: imageHeight * 1.5, marginTop: 30 }}
                  />
                </>
              ),
              title: 'Pronto!',
              subtitle:
                'Com essas informações preenchidas, basta ficar de olho em Solicitações para dar boas vindas aos seus novos alunos!',
            },
          ]}
        />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(ProfessionalTutorialScreen);
