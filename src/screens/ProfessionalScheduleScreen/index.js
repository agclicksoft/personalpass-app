import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';

import { DefaultButton } from '../../components';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { AxiosWithHeaders } from '../../services/api';
import { SafeAreaView } from 'react-native-safe-area-context';
import RNPickerSelect from 'react-native-picker-select';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import AwesomeAlert from 'react-native-awesome-alerts';
import LottieView from 'lottie-react-native';
import animation from '../../assets/success.json';

class ProfessionalScheduleScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      weekDay: [
        {
          key: 'SEGUNDA',
          name: 'Segunda-Feira',
          isSelected: false,
          hours: [],
        },
        {
          key: 'TERCA',
          name: 'Terça-Feira',
          isSelected: false,
          hours: [],
        },
        {
          key: 'QUARTA',
          name: 'Quarta-Feira',
          isSelected: false,
          hours: [],
        },
        {
          key: 'QUINTA',
          name: 'Quinta-Feira',
          isSelected: false,
          hours: [],
        },
        {
          key: 'SEXTA',
          name: 'Sexta-Feira',
          isSelected: false,
          hours: [],
        },
        {
          key: 'SABADO',
          name: 'Sábado',
          isSelected: false,
          hours: [],
        },
        {
          key: 'DOMINGO',
          name: 'Domingo',
          isSelected: false,
          hours: [],
        },
      ],
      newPeriod: [],
      schedules: [],
    };
  }

  async componentDidMount() {
    await this.loadSchedules();
  }

  loadSchedules = async () => {
    const { token } = this.props;
    const { weekDay } = this.state;
    const id = this.props.user.id;

    let aux = weekDay;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(`/schedules/${id}/format`)
      .then(response => {
        response.data.map(e => {
          var index = weekDay.findIndex(f => f.key == e.key);
          aux[index].hours = e.hours;
          aux[index].isSelected = true;
        });

        this.setState({
          weekDay: aux,
        });
      })

      .catch(err => {
        console.log(err);
      });
  };

  addHours = key => {
    const { weekDay } = this.state;

    weekDay[weekDay.findIndex(e => e.key == key)].hours.push({
      period_of: '',
      period_until: '',
    });

    this.setState({
      weekDay: weekDay,
    });
  };

  removeHour = (index, key) => {
    console.log(index, key);
    const { weekDay } = this.state;

    weekDay[weekDay.findIndex(e => e.key == key)].hours.splice(index, 1);

    this.setState({
      weekDay: weekDay,
    });
  };

  prepareItem = item => {
    const { week_day, period_of, period_until } = item;
    return {
      week_day: week_day,
      period_of: period_of,
      period_until: period_until,
    };
  };

  saveSchedule = async () => {
    const { user, token } = this.props;
    const { id } = this.props.user;
    const { weekDay } = this.state;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    try {
      await AxiosInstance.post(`/schedules/${id}/format`, {
        schedulesFormat: weekDay,
      });

      this.props.setUser(user);
      this.showAlert();
    } catch (err) {
      console.log(err.response.data);
    }
  };

  showAlert = () => {
    this.setState({
      showAlert: true,
    });

    setTimeout(
      () =>
        this.setState({
          showAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  render() {
    const { showAlert } = this.state;

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            paddingHorizontal: 20,
            paddingVertical: 20,
            elevation: 3,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Ionicon
                style={{
                  fontWeight: '900',
                  marginRight: 10,
                }}
                name={'ios-arrow-round-back'}
                size={30}
                color={'#37153D'}
              />
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#37153D',
                  textAlign: 'left',
                }}>
                Agenda
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <ScrollView>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'flex-start',
              paddingHorizontal: 45,
              paddingVertical: 20,
            }}>
            {this.state.weekDay.map(e => {
              return (
                <View
                  style={{
                    flexDirection: 'column',
                    width: 150,
                    marginVertical: 15,
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      width: 200,
                    }}>
                    {e.isSelected ? (
                      <>
                        <TouchableWithoutFeedback
                          onPress={async () =>
                            await this.setState(() => {
                              e.hours = [];
                              e.isSelected = !e.isSelected;
                              e.isSelected ? this.addHours(e.key) : null;
                              console.log(this.state.weekDay);
                              return { e };
                            })
                          }>
                          <View
                            style={{
                              flexDirection: 'row',
                              backgroundColor: '#FF993E',
                              borderColor: '#FF993E',
                              borderWidth: 1,
                              borderRadius: 5,
                              padding: 3,
                            }}>
                            <Image
                              source={require('../../assets/images/checkbox-checked.png')}
                            />
                          </View>
                        </TouchableWithoutFeedback>

                        <Text
                          style={{
                            marginLeft: 15,
                            fontFamily: 'SofiaPro-Regular',
                            color: '#4D4D4D',
                            fontSize: 14,
                            width: 100,
                          }}>
                          {e.name}
                        </Text>
                        <View
                          style={{
                            marginLeft: 20,
                            paddingHorizontal: 6,
                          }}>
                          <TouchableWithoutFeedback
                            onPress={() => this.addHours(e.key)}>
                            <Ionicon
                              name={'ios-add'}
                              size={33}
                              color={'#F76E1E'}
                            />
                          </TouchableWithoutFeedback>
                        </View>
                      </>
                    ) : (
                      <>
                        <TouchableWithoutFeedback
                          onPress={async () =>
                            await this.setState(() => {
                              e.hours = [];
                              e.isSelected = !e.isSelected;
                              e.isSelected ? this.addHours(e.key) : null;
                              console.log(this.state.weekDay);
                              return { e };
                            })
                          }>
                          <View
                            style={{
                              borderColor: '#AFC1C4',
                              borderWidth: 1,
                              borderRadius: 5,
                              padding: 3,
                            }}>
                            <Image
                              source={require('../../assets/images/checkbox.png')}
                            />
                          </View>
                        </TouchableWithoutFeedback>

                        <Text
                          style={{
                            marginLeft: 15,
                            fontFamily: 'SofiaPro-Regular',
                            color: '#4D4D4D',
                            fontSize: 14,
                          }}>
                          {e.name}
                        </Text>
                      </>
                    )}
                  </View>

                  {e.hours?.map((f, index) => {
                    return (
                      <HourItem
                        show={e.isSelected}
                        day={f}
                        onChangePeriodOf={async text =>
                          await this.setState(() => {
                            f.period_of = text;
                            return { f };
                          })
                        }
                        onChangePeriodUntil={async text =>
                          await this.setState(() => {
                            f.period_until = text;
                            return { f };
                          })
                        }
                        onPressHour={() => this.removeHour(index, e.key)}
                      />
                    );
                  })}
                </View>
              );
            })}
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: 30,
            }}>
            <DefaultButton
              title={'Salvar'}
              width={180}
              height={35}
              onPress={async () => {
                await this.saveSchedule();
              }}
            />
          </View>
        </ScrollView>

        <AwesomeAlert
          show={showAlert}
          showProgress={false}
          closeOnHardwareBackPress={false}
          onDismiss={async () => {
            this.props.navigation.goBack();
          }}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Agenda atualizada com sucesso!
              </Text>
              <LottieView
                style={{ width: 70, height: 70 }}
                resizeMode="contain"
                autoSize
                source={animation}
                autoPlay
                loop={false}
              />
            </View>
          }
        />
      </>
    );
  }
}

const HourItem = ({
  day,
  onPressHour,
  show,
  onChangePeriodOf,
  onChangePeriodUntil,
}) => {
  let period_of = [];
  for (let i = 6; i < 24; i++) {
    period_of.push({
      label: `${i < 10 ? '0' + i : i}:00`,
      value: `${i < 10 ? '0' + i : i}:00:00`,
    });
  }

  let period_until = [];
  if (day.period_of)
    for (let i = parseInt(day.period_of.slice(0, 2)) + 1; i < 24; i++) {
      period_until.push({
        label: `${i < 10 ? '0' + i : i}:00`,
        value: `${i < 10 ? '0' + i : i}:00:00`,
      });
    }

  return (
    <>
      {show ? (
        <View
          style={{
            flexDirection: 'row',
            marginVertical: 10,
            alignItems: 'center',
          }}>
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: '#fff',
              borderColor: '#707070',
              borderWidth: 0.1,
              borderRadius: 20,
              padding: 5,
            }}>
            <RNPickerSelect
              style={style}
              placeholder={{
                label: 'De',
              }}
              doneText={'Selecionar'}
              useNativeAndroidPickerStyle={false}
              onValueChange={(itemValue, itemIndex) => {
                onChangePeriodOf(itemValue);
                console.log(itemValue);
              }}
              value={day.period_of}
              items={period_of}
            />
          </View>

          <Text style={{ marginVertical: 20, marginHorizontal: 10 }}>───</Text>

          <View
            style={{
              flexDirection: 'row',
              backgroundColor: '#fff',
              borderColor: '#707070',
              borderWidth: 0.1,
              borderRadius: 20,
              padding: 5,
            }}>
            <RNPickerSelect
              style={style}
              placeholder={{
                label: 'Até',
              }}
              doneText={'Selecionar'}
              useNativeAndroidPickerStyle={false}
              onValueChange={(itemValue, itemIndex) => {
                onChangePeriodUntil(itemValue);
                console.log(itemValue);
              }}
              value={day.period_until}
              items={period_until}
            />
          </View>

          <TouchableWithoutFeedback onPress={onPressHour}>
            <Ionicon
              style={{
                marginLeft: 20,
              }}
              name={'ios-remove'}
              size={33}
              color={'#F76E1E'}
            />
          </TouchableWithoutFeedback>
        </View>
      ) : null}
    </>
  );
};

const style = {
  inputAndroid: {
    width: 70,
    height: 40,
    color: '#000',
    textAlign: 'center',
    fontFamily: 'SofiaPro-Regular',
    fontSize: 20,
    margin: 10,
  },
  inputIOS: {
    width: 70,
    height: 35,
    color: '#000',
    textAlign: 'center',
    fontFamily: 'SofiaPro-Regular',
    fontSize: 20,
    margin: 10,
  },
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(ProfessionalScheduleScreen);
