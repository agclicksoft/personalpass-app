import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableWithoutFeedback,
  Linking,
  FlatList,
  Alert,
} from 'react-native';
import { LoadingModal, DefaultButton } from '../../components';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { AxiosWithHeaders } from '../../services/api';
import { SafeAreaView } from 'react-native-safe-area-context';
import { TextInputMask } from 'react-native-masked-text';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import moment from 'moment';
import AwesomeAlert from 'react-native-awesome-alerts';
import LottieView from 'lottie-react-native';
import animation from '../../assets/warn.json';
import animationClass from '../../assets/success.json';
import whatsapp from '../../assets/images/whatsapp.png';
import email from '../../assets/images/email.png';
import phone from '../../assets/images/phone.png';
import emergencia from '../../assets/images/emergencia.png';
import pacote from '../../assets/images/pacote.png';

class StudentProfileScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      showParq: this.props.user?.user?.role != 'student' ? false : true,
      showAlert: false,
      refreshing: false,
      solicitationModal: false,
      tokenModal: false,
      choice: {},
      index: '',
      file: '',
      fileName: '',
    };
  }
  async componentDidMount() {
    await this.getMedicalAttachment();
  }

  showAlert = () => {
    this.setState({
      showAlert: true,
    });

    setTimeout(
      () =>
        this.setState({
          showAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  openSolicitationModal = (item, index) => {
    this.setState({ solicitationModal: true, choice: item, index: index });
  };

  closeSolicitationModal = () => {
    this.setState({ solicitationModal: !this.state.solicitationModal });
  };

  save = async () => {
    const { token } = this.props;

    try {
      const AxiosInstance = AxiosWithHeaders({
        Authorization: `Bearer ${token}`,
      });

      AxiosInstance.put(`scheduled-services/${this.state.choice.id}/status`, {
        status: 'CONFIRMADO',
      })
        .then(async response => {
          console.log(response.data);
          this.closeSolicitationModal();
          this.props.navigation.goBack();
        })
        .catch(err => {
          this.closeSolicitationModal();
          console.log(err);
        });
    } catch (err) {
      this.closeSolicitationModal();
      console.log(err);
    }
  };

  cancel = async () => {
    const { token } = this.props;

    try {
      const AxiosInstance = AxiosWithHeaders({
        Authorization: `Bearer ${token}`,
      });

      AxiosInstance.put(`scheduled-services/${this.state.choice.id}/status`, {
        status: 'CANCELADO',
      })
        .then(async response => {
          console.log(response.data);
          this.closeSolicitationModal();
          this.props.navigation.goBack();
        })
        .catch(err => {
          console.log(err);
          this.closeSolicitationModal();
        });
    } catch (err) {
      console.log(err);
      this.closeSolicitationModal();
    }
  };

  getMedicalAttachment = async () => {
    const { token } = this.props;

    let file = '';

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(`/students/${this.props.route?.params?.student.id}`)
      .then(async response => {
        await response.data.user.faqAnswered.find(item =>
          item.faq_question_id == 10
            ? (file = item.attachments[0])
            : (file = ''),
        );

        this.setState({
          file: file ? file : '',
          fileName: file.name ? file.name : '',
        });
      })
      .catch(err => {
        console.log(err);
      });

    console.log(this.state.file);
  };

  startClass = async () => {
    const { user, token } = this.props;

    const { code, choice } = this.state;

    const AxiosInstace = AxiosWithHeaders({ Authorization: `Bearer ${token}` });

    try {
      const response = await AxiosInstace.post(
        `/scheduled-services/${choice.id}/give-present`,
        {
          tk_auth_service: code,
        },
      );

      if (response.status === 200) {
        this.showSuccess();
        choice.status = 'andamento';
      }
    } catch (err) {
      console.log('We got an error', err);

      if (err.response && err.response.status === 401) {
        Alert.alert(
          'Oops!',
          'Código incorreto, tente novamente.',
          [{ text: 'OK' }],
          {
            cancelable: false,
          },
        );
      }
    }
  };

  showSuccess = () => {
    this.setState({
      showSuccess: true,
    });

    setTimeout(
      () =>
        this.setState({
          showSuccess: false,
          changePassAlert: false,
          tokenModal: false,
        }),
      5000,
    ); // hide alert after 4s
  };

  render() {
    const {
      solicitationModal,
      showAlert,
      tokenModal,
      showSuccess,
    } = this.state;
    const student = this.props.route?.params?.student;
    const img_profile = this.props.route?.params?.img_profile;
    const appointment = this.props.route?.params?.choice;

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#37153D' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#37153D',
            shadowColor: '#000',
            alignItems: 'center',
            paddingHorizontal: 20,
            paddingVertical: 20,
            paddingTop: 20,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Ionicon
                style={{
                  fontWeight: '900',
                  marginRight: 10,
                }}
                name={'ios-arrow-round-back'}
                size={30}
                color={'#F8F8F8'}
              />

              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#F8F8F8',
                  textAlign: 'left',
                }}>
                {' '}
                Perfil público
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>

        <ScrollView
          contentContainerStyle={{
            flexGrow: 1,
            justifyContent: 'flex-start',
            backgroundColor: '#37153D',
          }}>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
              backgroundColor: '#F8F8F8',
              marginTop: 0,
              padding: 20,
              paddingBottom: 90,
              borderTopLeftRadius: 40,
              borderTopRightRadius: 40,
            }}>
            <Image
              style={{ height: 200, width: 200, borderRadius: 135 }}
              source={{
                uri: img_profile
                  ? img_profile
                  : 'https://static.scrum.org/web/images/profile-placeholder.png',
              }}
            />
            <Text
              style={{
                color: '#37153D',
                fontSize: 22,
                fontFamily: 'SofiaPro-Medium',
                marginTop: 20,
              }}>
              {student?.user?.name}
            </Text>

            <View style={{ flexDirection: 'row' }}>
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: 'SofiaProLight',
                  fontSize: 16,
                  color: '#707070',
                  marginTop: 5,
                }}>
                {moment().diff(student?.user?.birthday, 'years') > 0
                  ? moment().diff(student?.user?.birthday, 'years') + ' anos'
                  : ''}
              </Text>
            </View>

            {appointment?.amount == 1 ? (
              <SolicitationItem
                singleAppointment={true}
                date={moment
                  .parseZone(appointment.scheduledServices[0].date)
                  .format('DD/MM/YYYY - HH:mm')}
                gym={appointment.scheduledServices[0].establishment.name}
                plan={appointment.professionalPlan.plan.description}
                status={appointment.scheduledServices[0].status.toLowerCase()}
                methodPayment={appointment.method_payment}
                onPress={async () => {
                  if (appointment.scheduledServices[0].status == 'PENDENTE') {
                    this.openSolicitationModal(
                      appointment.scheduledServices[0],
                    );
                  } else if (
                    appointment.scheduledServices[0].status == 'DISPONIVEL'
                  ) {
                    this.props.navigation.navigate('ClassDescriptionScreen', {
                      class: appointment.scheduledServices[0],
                      classId: appointment.scheduledServices[0].id,
                    });
                  }
                }}
              />
            ) : null}

            {this.props.user?.user?.role != 'student' ? (
              <View style={{ marginTop: 10 }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    width: 190,
                  }}>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      Linking.canOpenURL('whatsapp://send?text=Oi').then(
                        supported => {
                          if (supported) {
                            return Linking.openURL(
                              `whatsapp://send?phone=55${
                                student?.user.contact
                              }&text=Olá, você me contratou no Personal Access, tudo bem?`,
                            );
                          } else {
                            return Linking.openURL(
                              `https://api.whatsapp.com/send?phone=55${
                                student?.user.contact
                              }&text=Olá, você me contratou no Personal Access, tudo bem?`,
                            );
                          }
                        },
                      )
                    }>
                    <Image
                      source={whatsapp}
                      style={{ width: 40, height: 40, marginVertical: 10 }}
                      resizeMode="contain"
                    />
                  </TouchableWithoutFeedback>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      Linking.openURL(
                        `mailto:${
                          student.user?.email
                        }?subject=Olá, você me contratou pelo Personal Access!`,
                      )
                    }>
                    <Image
                      source={email}
                      style={{ width: 40, height: 40, marginVertical: 10 }}
                      resizeMode="contain"
                    />
                  </TouchableWithoutFeedback>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      Linking.openURL(`tel:${student.user?.contact}`)
                    }>
                    <Image
                      source={phone}
                      style={{ width: 40, height: 40, marginVertical: 10 }}
                      resizeMode="contain"
                    />
                  </TouchableWithoutFeedback>

                  <TouchableWithoutFeedback
                    onPress={() =>
                      Linking.openURL(`tel:${student.user?.emergency_phone}`)
                    }>
                    <Image
                      source={emergencia}
                      style={{ width: 40, height: 40, marginVertical: 10 }}
                      resizeMode="contain"
                    />
                  </TouchableWithoutFeedback>
                </View>
              </View>
            ) : null}

            {this.props.user?.user?.role != 'student' ? (
              <TouchableWithoutFeedback
                onPress={() =>
                  this.setState({
                    showParq: !this.state.showParq,
                  })
                }>
                <Text
                  style={{
                    margin: 20,
                    fontFamily: 'SofiaPro-Regular',
                    fontSize: 18,
                    color: '#F76E1E',
                    textDecorationLine: 'underline',
                  }}>
                  {`Visualizar PAR-Q`}
                </Text>
              </TouchableWithoutFeedback>
            ) : null}

            {this.state.showParq == true ? (
              <View
                style={{
                  backgroundColor: '#fff',
                  borderRadius: 20,
                  padding: 20,
                  marginTop: 20,
                  width: '95%',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    borderRadius: 30,
                    width: 280,
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      marginBottom: 20,
                      fontFamily: 'SofiaPro-Regular',
                      fontSize: 18,
                      color: '#F76E1E',
                    }}>{`PAR-Q:`}</Text>

                  <View
                    style={{
                      flexDirection: 'row',
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      width: 200,
                      margin: 10,
                    }}>
                    {student?.user?.faqAnswered.map(item => {
                      return item.faqQuestion.id == 1 ? (
                        <CustomQuestion
                          key={item.id}
                          flexDirection={'row'}
                          question={`${item.faqQuestion.question}: `}
                          answer={`${item.answered} kg`}
                          visible={item.answered > ''}
                        />
                      ) : null;
                    })}

                    <View style={{ width: 20 }} />

                    {student?.user?.faqAnswered.map(item => {
                      return item.faqQuestion.id == 2 ? (
                        <CustomQuestion
                          key={item.id}
                          flexDirection={'row'}
                          question={`${item.faqQuestion.question}: `}
                          answer={`${item.answered} m`}
                          visible={item.answered > ''}
                        />
                      ) : null;
                    })}
                  </View>

                  {student?.user?.faqAnswered.map(item => {
                    return item.faqQuestion.id > 2 ? (
                      <CustomQuestion
                        key={item.id}
                        question={item.faqQuestion.question}
                        answer={item.answered}
                        visible={item.answered > ''}
                      />
                    ) : null;
                  })}

                  {this.state.fileName != '' ? (
                    <View
                      style={{
                        alignSelf: 'center',
                        flexDirection: 'column',
                      }}>
                      <AttachmentItem
                        hasFile={this.state.file.path !== '' ? true : false}
                        cardInfo={this.state.fileName}
                        onPress={() =>
                          this.state.file.path != ''
                            ? Linking.openURL(this.state.file.path)
                            : ''
                        }
                      />
                    </View>
                  ) : null}
                </View>
              </View>
            ) : null}

            {appointment?.amount !== 1 &&
            this.props.user?.user.role != 'student' ? (
              <View
                style={{
                  borderTopWidth: 1,
                  borderTopColor: 'rgba(112, 112, 112, 0.1)',
                  width: '100%',
                  alignItems: 'center',
                  marginVertical: 15,
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 20,
                    color: '#37153D',
                    marginVertical: 15,
                  }}>
                  Agendamentos
                </Text>

                <FlatList
                  ItemSeparatorComponent={renderSeparator}
                  showsVerticalScrollIndicator={false}
                  data={appointment?.scheduledServices.sort((a, b) =>
                    new Date(a.date) > new Date(b.date) ? 1 : -1,
                  )}
                  keyExtractor={lesson => String(lesson.id)}
                  renderItem={({ item, index }) => (
                    <SolicitationItem
                      singleAppointment={false}
                      date={moment
                        .parseZone(item.date)
                        .format('DD/MM/YYYY - HH:mm')}
                      gym={item.establishment.name}
                      status={item.status.toLowerCase()}
                      methodPayment={item.method_payment}
                      onPress={async () => {
                        if (item.status == 'PENDENTE') {
                          this.openSolicitationModal(item, index);
                        } else if (item.status == 'DISPONIVEL') {
                          await this.setState({
                            choice: item,
                            tokenModal: !this.state.tokenModal,
                          });
                        } else if (item.status == 'andamento') {
                          this.props.navigation.navigate(
                            'ClassDescriptionScreen',
                            {
                              class: item,
                              classId: item.id,
                            },
                          );
                        } else if (item.status == 'CONCLUIDO') {
                          this.props.navigation.navigate(
                            'TrainingDetailScreen',
                            {
                              class: item,
                              classId: item.id,
                            },
                          );
                        }
                      }}
                    />
                  )}
                />
              </View>
            ) : null}
          </View>
        </ScrollView>

        <LoadingModal show={this.state.loading} />

        {this.props.user?.user?.role == 'student' ? (
          <AwesomeAlert
            show={showAlert}
            showProgress={false}
            closeOnTouchOutside={true}
            contentContainerStyle={{
              padding: 0,
            }}
            customView={
              <View
                style={{
                  padding: 5,
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 14,
                    textAlign: 'center',
                    flexWrap: 'wrap',
                  }}>
                  Apenas o professor contratado tem acesso ao seu perfil.
                </Text>
                <LottieView
                  style={{ width: 50, height: 50 }}
                  resizeMode="contain"
                  autoSize
                  source={animation}
                  autoPlay
                  loop={false}
                />
              </View>
            }
          />
        ) : null}

        <AwesomeAlert
          show={solicitationModal}
          showProgress={false}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          customView={
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: 300,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 15,
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 22,
                    textAlign: 'center',
                    flexWrap: 'wrap',
                    color: '#37153D',
                  }}>
                  Solicitação
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                }}>
                <DefaultButton
                  onPress={() => this.save()}
                  title={'Confirmar aula'}
                  width={200}
                  height={40}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                }}>
                <DefaultButton
                  onPress={() => {
                    this.cancel();
                  }}
                  title={'Cancelar aula'}
                  backgroundColor={'#D22020'}
                  width={200}
                  height={40}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                }}>
                <TouchableWithoutFeedback
                  onPress={() => this.closeSolicitationModal()}>
                  <Text style={{ marginTop: 10, color: '#999' }}>Voltar</Text>
                </TouchableWithoutFeedback>
              </View>
            </View>
          }
        />

        <AwesomeAlert
          show={tokenModal}
          showProgress={false}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          contentContainerStyle={{
            backgroundColor: '#F8F8F8',
            borderRadius: 25,
          }}
          customView={
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: 300,
                backgroundColor: '#F8F8F8',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 20,
                }}>
                <View
                  style={{
                    flexDirection: 'column',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      fontSize: 22,
                      textAlign: 'center',
                      flexWrap: 'wrap',
                      color: '#37153D',
                      paddingHorizontal: 10,
                    }}>
                    Digite aqui o código informado pelo aluno:
                  </Text>

                  <MaskedInput
                    question={null}
                    type={'custom'}
                    options={{
                      mask: '999999',
                    }}
                    keyboardType={'number-pad'}
                    answer={this.state.code}
                    onChangeText={text => this.setState({ code: text })}
                  />
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                  marginTop: 20,
                }}>
                <DefaultButton
                  onPress={() => {
                    this.setState({
                      changePassAlert: false,
                    });
                    this.startClass();
                  }}
                  backgroundColor={'#37153D'}
                  title={'Confirmar'}
                  width={200}
                  height={40}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                }}>
                <TouchableWithoutFeedback
                  onPress={() =>
                    this.setState({
                      tokenModal: false,
                    })
                  }>
                  <Text style={{ marginTop: 10, color: '#999' }}>Voltar</Text>
                </TouchableWithoutFeedback>
              </View>
            </View>
          }
        />

        <AwesomeAlert
          show={showSuccess}
          showProgress={false}
          closeOnHardwareBackPress={false}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View style={{ alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  color: '#37153D',
                }}>
                Aula iniciada! Após a finalização, por favor prossiga para
                Concluir aula.
              </Text>

              <LottieView
                style={{ width: 70, height: 70 }}
                resizeMode="contain"
                autoSize
                source={animationClass}
                autoPlay
                loop={false}
              />
            </View>
          }
        />
      </>
    );
  }
}

const AttachmentItem = ({ cardInfo, onPress, hasFile }) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        width: 180,
        alignItems: 'center',
        alignSelf: 'center',
        borderRadius: 50,
      }}>
      <Text
        style={{
          fontFamily: 'SofiaPro-Regular',
          fontSize: 14.5,
          color: '#2E2E2E',
          textAlign: 'center',
        }}>
        {cardInfo}
      </Text>
      {hasFile == true ? (
        <TouchableWithoutFeedback onPress={onPress}>
          <Ionicon
            style={{
              fontWeight: '900',
            }}
            name={'ios-download'}
            size={28}
            color={'#37153D'}
          />
        </TouchableWithoutFeedback>
      ) : null}
    </View>
  );
};

let renderSeparator = () => (
  <View
    style={{
      height: 15,
    }}
  />
);

let getStatusTitle = text => {
  switch (text) {
    case 'em andamento':
      return 'Em andamento';
    case 'concluido':
      return 'Concluída';
    case 'pendente':
      return 'Responder solicitação';
    case 'PAGO':
      return 'Pagamento confirmado';
    case 'confirmado':
      return 'Confirmado';
    case 'cancelado':
      return 'Cancelada';
    case 'disponivel':
      return 'Concluir aula';
    default:
      return 'Responder';
  }
};

let getMultipleAppointmentTitle = text => {
  switch (text) {
    case 'em andamento':
      return 'Em andamento';
    case 'concluido':
      return 'Concluído';
    case 'pendente':
      return 'Responder solicitação';
    case 'confirmado':
      return 'Confirmado';
    case 'cancelado':
      return 'Cancelado';
    case 'disponivel':
      return 'Iniciar aula';
    case 'andamento':
      return 'Concluir aula';
    default:
      return 'Responder';
  }
};

let getStatusPayment = text => {
  switch (text) {
    case 'CARTAO':
      return 'Cartão de crédito';
    case 'PENDENTE':
      return 'Pendente';
    case 'MAOS':
      return 'Em mãos';
    case 'BOLETO':
      return 'Em mãos';
    default:
      return 'Não identificado';
  }
};

let getStatusBG = text => {
  switch (text) {
    case 'pendente':
      return '#37153D';
    case 'andamento':
      return '#F76E1E';
    case 'disponivel':
      return '#F76E1E';
    default:
      return '#E9E9E9';
  }
};

let getStatusColor = text => {
  switch (text) {
    case 'em andamento':
      return '#F76E1E';
    case 'pendente':
      return '#E9E9E9';
    case 'concluido':
      return '#37153D';
    case 'confirmado':
      return '#049526';
    case 'PAGO':
      return '#049526';
    case 'cancelado':
      return '#D22020';
    case 'andamento':
      return '#fff';
    case 'disponivel':
      return '#fff';
    default:
      return '';
  }
};

const SolicitationItem = ({
  onPress,
  status,
  date,
  gym,
  plan,
  singleAppointment,
  methodPayment,
}) => {
  return (
    <>
      <View
        style={{
          flexDirection: 'column',
          alignItems: 'center',
          margin: 10,
          borderRadius: 10,
          elevation: 1,
          backgroundColor: '#fff',
          padding: 30,
        }}>
        <View
          style={{
            flexDirection: 'column',
            alignItems: 'center',
            paddingTop: 10,
            paddingBottom: 0,
            borderRadius: 10,
          }}>
          <View
            style={{
              flexDirection: 'row',
            }}>
            {singleAppointment === false ? (
              <Image
                style={{ height: 65, width: 65, padding: 5 }}
                source={pacote}
                resizeMode={'contain'}
              />
            ) : null}
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'space-evenly',
                alignItems:
                  singleAppointment === true ? 'center' : 'flex-start',
                marginLeft: 10,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Regular',
                    flexWrap: 'wrap',
                    color: '#F76E1E',
                    fontSize: 14,
                  }}>
                  Data/hora:{' '}
                </Text>
                <Text
                  style={{
                    fontFamily: 'SofiaProLight',
                    flexWrap: 'wrap',
                    color: '#2E2E2E',
                    fontSize: 14,
                  }}>
                  {date}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 5,
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Regular',
                    flexWrap: 'wrap',
                    color: '#F76E1E',
                    fontSize: 14,
                  }}>
                  Local:{' '}
                </Text>
                <Text
                  style={{
                    fontFamily: 'SofiaProLight',
                    flexWrap: 'wrap',
                    color: '#2E2E2E',
                    fontSize: 14,
                  }}>
                  {gym}
                </Text>
              </View>
              {singleAppointment == true ? (
                <>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginTop: 5,
                    }}>
                    <Text
                      style={{
                        fontFamily: 'SofiaPro-Regular',
                        flexWrap: 'wrap',
                        color: '#F76E1E',
                        fontSize: 13,
                      }}>
                      Pacote:{' '}
                    </Text>
                    <Text
                      style={{
                        fontFamily: 'SofiaProLight',
                        flexWrap: 'wrap',
                        color: '#2E2E2E',
                        fontSize: 13,
                      }}>
                      {plan}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginTop: 5,
                    }}>
                    <Text
                      style={{
                        fontFamily: 'SofiaPro-Regular',
                        flexWrap: 'wrap',
                        color: '#F76E1E',
                        fontSize: 13,
                      }}>
                      Pagamento:{' '}
                    </Text>
                    <Text
                      style={{
                        fontFamily: 'SofiaProLight',
                        flexWrap: 'wrap',
                        color: '#2E2E2E',
                        fontSize: 13,
                      }}>
                      {getStatusPayment(methodPayment ?? 'PENDENTE')}
                    </Text>
                  </View>
                </>
              ) : null}
            </View>
          </View>
        </View>

        <View
          style={{
            width: '100%',
            alignItems: 'center',
            marginTop: 10,
          }}>
          <DefaultButton
            title={
              singleAppointment === true
                ? getStatusTitle(status)
                : getMultipleAppointmentTitle(status)
            }
            onPress={onPress}
            backgroundColor={getStatusBG(status)}
            fontColor={getStatusColor(status)}
            width={220}
            height={35}
          />
        </View>
      </View>
    </>
  );
};

const CustomQuestion = ({ question, answer, visible, flexDirection }) => {
  return visible ? (
    <View
      style={{
        flexDirection: flexDirection,
        margin: 10,
        alignItems: 'center',
      }}>
      <Text
        style={{
          textAlign: 'center',
          fontFamily: 'SofiaPro-Regular',
          fontSize: 12,
          color: '#222',
        }}>
        {question}
      </Text>
      <Text
        style={{
          textAlign: 'center',
          fontFamily: 'SofiaProLight',
          fontSize: 12,
          color: '#707070',
          margin: 5,
        }}>
        {answer}
      </Text>
    </View>
  ) : null;
};

CustomQuestion.defaultProps = {
  visible: true,
};

const MaskedInput = ({
  question,
  answer,
  width,
  keyboardType,
  onChangeText,
  placeholder,
  type,
  options,
  maxLength,
}) => {
  return (
    <View style={{ flexDirection: 'column', marginLeft: 10, marginRight: 10 }}>
      <Text
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaPro-Medium',
          margin: 10,
          fontSize: 13,
        }}>
        {question}
      </Text>

      <TextInputMask
        style={{
          color: '#2E2E2E',
          fontSize: 18,
          backgroundColor: '#fff',
          height: 40,
          borderRadius: 50,
          borderWidth: 0.1,
          paddingLeft: 30,
          paddingRight: 30,
          width: width ? width : 200,
          textAlign: 'center',
        }}
        maxLength={maxLength}
        type={type}
        value={answer}
        keyboardType={keyboardType}
        onChangeText={onChangeText}
        options={options}
        placeholder={placeholder}
      />
    </View>
  );
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(StudentProfileScreen);
