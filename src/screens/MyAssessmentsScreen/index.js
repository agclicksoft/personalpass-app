import React, { Component } from 'react';
import {
  View,
  Button,
  Text,
  ScrollView,
  Modal,
  TextInput,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';

import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';

import {
  DefaultButton,
  DefaultInput,
  ProfileItem,
  SolicitationItem,
} from '../../components';
import avaliationSrc from '../../assets/images/avaliation.png';
import avatar from '../../assets/images/woman-placeholder2.png';

class MyAssessmentsScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      solicitationModal: false,
    };
  }

  openSolicitationModal = () => {
    this.setState({ solicitationModal: true });
  };

  closeSolicitationModal = () => {
    this.setState({ solicitationModal: false });
  };

  render() {
    return (
      <>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            padding: 20,
            elevation: 3,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <Ionicon
              style={{
                fontWeight: '900',
                marginRight: 10,
              }}
              name={'ios-arrow-round-back'}
              size={30}
              color={'#37153D'}
            />
          </TouchableWithoutFeedback>
          <Text
            style={{
              fontFamily: 'SofiaPro-Medium',
              fontSize: 14,
              color: '#37153D',
              textAlign: 'left',
            }}>
            {' '}
            Minhas avaliações
          </Text>
        </View>
        <ScrollView>
          <View>
            <AssessmentItem
              name={'Maria Santos'}
              created_at={'23/11/2019'}
              assessment={
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi neque nisl, pharetra non blandit vitae, tempus quis quam. Vestibulum leo lorem, auctor a feugiat non.'
              }
              rating={'5,0'}
            />

            <AssessmentItem
              name={'Maria Santos'}
              created_at={'23/11/2019'}
              assessment={
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi neque nisl, pharetra non blandit vitae, tempus quis quam. Vestibulum leo lorem, auctor a feugiat non.'
              }
              rating={'5,0'}
            />

            <AssessmentItem
              name={'Maria Santos'}
              created_at={'23/11/2019'}
              assessment={
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi neque nisl, pharetra non blandit vitae, tempus quis quam. Vestibulum leo lorem, auctor a feugiat non.'
              }
              rating={'5,0'}
            />

            <AssessmentItem
              name={'Maria Santos'}
              created_at={'23/11/2019'}
              assessment={
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi neque nisl, pharetra non blandit vitae, tempus quis quam. Vestibulum leo lorem, auctor a feugiat non.'
              }
              rating={'5,0'}
            />

            <AssessmentItem
              name={'Maria Santos'}
              created_at={'23/11/2019'}
              assessment={
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi neque nisl, pharetra non blandit vitae, tempus quis quam. Vestibulum leo lorem, auctor a feugiat non.'
              }
              rating={'5,0'}
            />

            <AssessmentItem
              name={'Maria Santos'}
              created_at={'23/11/2019'}
              assessment={
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi neque nisl, pharetra non blandit vitae, tempus quis quam. Vestibulum leo lorem, auctor a feugiat non.'
              }
              rating={'5,0'}
            />
          </View>
        </ScrollView>
      </>
    );
  }
}

const AssessmentItem = ({ name, created_at, assessment, rating }) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff',
        padding: 20,
        margin: 15,
        borderRadius: 10,
        elevation: 1,
      }}>
      <View>
        <Image
          style={{ height: 135, width: 135, borderRadius: 135 }}
          source={avatar}
        />
      </View>
      <View style={{ flexDirection: 'column', marginLeft: 10 }}>
        <Text
          style={{
            fontFamily: 'SofiaPro-Medium',
            fontSize: 20,
            color: '#37153D',
          }}>
          {name}
        </Text>
        <Text
          style={{
            fontFamily: 'SofiaProLight',
            fontSize: 11,
            color: '#707070',
            marginTop: -5,
          }}>
          {created_at}
        </Text>
        <Text
          style={{
            fontFamily: 'SofiaProLight',
            fontSize: 11,
            color: '#707070',
            width: 210,
            flexWrap: 'wrap',
            padding: 5,
            paddingLeft: 0,
            textAlign: 'left',
            marginTop: -5,
          }}>
          {assessment}
        </Text>
        <View
          style={{
            flexDirection: 'row',
            alignSelf: 'flex-start',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: '#F2F2F2',
            marginTop: 5,
            borderRadius: 5,
            padding: 5,
            paddingLeft: 15,
            paddingRight: 15,
          }}>
          <Image
            style={{ height: 20, width: 20 }}
            resizeMode="contain"
            source={avaliationSrc}
          />
          <Text
            style={{
              marginLeft: 6,
              fontFamily: 'SofiaProLight',
              fontSize: 11,
              color: '#F76E1E',
            }}>
            {rating}
          </Text>
        </View>
      </View>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(MyAssessmentsScreen);
