import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  ScrollView,
  RefreshControl,
  TouchableWithoutFeedback,
} from 'react-native';
import { ClassItem, DefaultButton, LoadingModal } from '../../components';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { AxiosWithHeaders } from '../../services/api';
import { SafeAreaView } from 'react-native-safe-area-context';
import RNPickerSelect from 'react-native-picker-select';
import AwesomeAlert from 'react-native-awesome-alerts';
import moment from 'moment';
import 'moment-timezone';

class MyClassesScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      lessons: [],
      loading: true,
      refreshing: false,
      choice: '',
      code: '',
      tokenModal: false,
      showModal: false,
      showFilter: false,
      status: '',
      filtering: false,
      page: 1,
      optionsStatus: [
        { description: 'Pendente', value: 'PENDENTE', checked: false },
        { description: 'Confirmada', value: 'CONFIRMADO', checked: false },
        { description: 'Concluída', value: 'CONCLUIDO', checked: false },
        { description: 'Estornada', value: 'ESTORNADO', checked: false },
        { description: 'Cancelada', value: 'CANCELADO', checked: false },
      ],
    };
  }

  async componentDidMount() {
    await this.loadLessons(this.props.user.id);

    this.focusListener = this.props.navigation.addListener('focus', () =>
      this.onRefresh(),
    );
  }

  componentWillUnmount() {
    this.focusListener();
  }

  loadLessons = async id => {
    const { token } = this.props;
    const { page } = this.state;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(`/student/${id}/schedules-service?page=${page}`)
      .then(response => {
        this.setState({
          lessons:
            this.state.page === 1
              ? response.data.data
              : [...this.state.lessons, ...response.data.data],
          loading: false,
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  loadOnScroll = async () => {
    if (!this.state.filtering)
      this.setState(
        {
          page: this.state.page + 1,
        },
        () => this.loadLessons(this.props.user.id),
      );
  };

  onRefresh = async () => {
    await this.setState({ lessons: [], loading: true, page: 1 });
    await this.loadLessons(this.props.user.id);
  };

  filter = async lessonStatus => {
    const { token } = this.props;

    await this.setState({ lessons: [], loading: true, page: 1 });

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(
      `/student/${this.props.user.id}/schedules-service?page=${
        this.state.page
      }&perPage=40`,
    )
      .then(response => {
        this.setState({
          lessons: response.data.data.filter(e => e.status === lessonStatus),
          loading: false,
        });
      })
      .catch(err => {
        console.log('Error:' + err);
      });
  };

  cancel = async () => {
    const { token } = this.props;

    try {
      const AxiosInstance = AxiosWithHeaders({
        Authorization: `Bearer ${token}`,
      });

      AxiosInstance.put(`scheduled-services/${this.state.choice.id}/status`, {
        status: 'CANCELADO',
      })
        .then(async response => {
          console.log(response.data.data);
          this.onRefresh();
        })
        .catch(err => {
          console.log(err);
        });
    } catch (err) {
      console.log(err);
    }
  };

  openTokenModal = () => {
    this.setState({ tokenModal: !this.state.tokenModal });
  };

  render() {
    const { lessons, showModal, showFilter, tokenModal } = this.state;

    const isValid = moment().diff(this.state.choice.date, 'days');

    console.log(isValid);

    const style = {
      inputAndroid: {
        width: 190,
        height: 40,
        color: '#707070',
        paddingHorizontal: 10,
        fontFamily: 'SofiaPro-Medium',
        fontSize: 16,
      },
      inputIOS: {
        width: 150,
        height: 35,
        color: '#707070',
        paddingHorizontal: 10,
        textAlign: 'center',
        fontFamily: 'SofiaPro-Medium',
        fontSize: 16,
      },
    };

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingHorizontal: 30,
            paddingVertical: 20,
            elevation: 3,
          }}>
          <Text
            style={{
              fontFamily: 'SofiaPro-Medium',
              fontSize: 18,
              flexWrap: 'wrap',
              color: '#37153D',
            }}>
            Minhas aulas
          </Text>

          <DefaultButton
            title={'Filtrar'}
            width={100}
            height={35}
            margin={'0px'}
            onPress={() => {
              this.setState({
                showFilter: true,
                status: '',
              });
            }}
          />
        </View>

        {this.state.filtering == true ? (
          <TouchableWithoutFeedback
            onPress={() => {
              this.setState({
                filtering: false,
                loading: true,
                lessons: [],
              });
              this.loadLessons(this.props.user.id);
            }}>
            <View
              style={{
                alginItems: 'center',
                justifyContent: 'center',
                alignSelf: 'center',
                borderRadius: 50,
                paddingHorizontal: 15,
                paddingVertical: 5,
                backgroundColor: '#F76E1E',
                margin: 10,
                width: 135,
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  marginLeft: 5,
                  fontSize: 15,
                  color: '#fff',
                  textAlign: 'center',
                }}>
                Limpar filtro
              </Text>
            </View>
          </TouchableWithoutFeedback>
        ) : null}

        {lessons != '' ? (
          <FlatList
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={async () => await this.onRefresh()}
              />
            }
            onEndReachedThreshold={0}
            onEndReached={this.loadOnScroll}
            showsVerticalScrollIndicator={false}
            data={lessons}
            keyExtractor={lesson => String(lesson.id)}
            renderItem={({ item }) => (
              <ClassItem
                date={moment.parseZone(item.date).format('DD/MM/YYYY - HH:mm')}
                gym={item.establishment.name}
                personal={item.professional.user.name}
                avatar={{
                  uri: item.professional.user.img_profile
                    ? `${
                        item.professional.user.img_profile
                      }?random=${Math.random()
                        .toString(36)
                        .substring(7)}`
                    : 'https://static.scrum.org/web/images/profile-placeholder.png',
                }}
                plan={item.contract.professionalPlan.plan.description}
                status={
                  item.contract.status_payment == 'PAGO' &&
                  item.status == 'CONFIRMADO'
                    ? 'PAGO'
                    : item.status == 'CONFIRMADO' &&
                      item.contract.method_payment == 'MAOS'
                    ? 'MAOS'
                    : item.status == 'CONFIRMADO' &&
                      item.contract.status_payment != 'PAGO'
                    ? 'Aguardando pagamento'
                    : item.status
                }
                methodPayment={item.contract.method_payment}
                onPress={async () => {
                  if (item.status == 'CONCLUIDO') {
                    this.props.navigation.navigate('TrainingDetailScreen', {
                      class: item,
                      classId: item.id,
                    });
                  } else if (item.status == 'PENDENTE') {
                    await this.setState({
                      choice: item,
                      showModal: true,
                    });
                  } else if (item.status == 'DISPONIVEL') {
                    await this.setState({
                      choice: item,
                    });

                    await this.openTokenModal();
                  } else if (
                    item.status == 'CONFIRMADO' &&
                    item.contract.status_payment != 'PAGO'
                  ) {
                    this.props.navigation.navigate('PaymentSelectionScreen', {
                      class: item,
                      classId: item.id,
                    });
                  }
                }}
              />
            )}
          />
        ) : (
          <View
            style={{
              marginTop: 20,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {this.state.filtering === false && this.state.loading === false ? (
              <>
                <ItemModal item={'Nenhuma aula encontrada.'} />

                <TouchableWithoutFeedback
                  onPress={() => {
                    this.setState({
                      filtering: false,
                      loading: true,
                    });
                    this.loadLessons(this.props.user.id);
                  }}>
                  <View
                    style={{
                      alginItems: 'center',
                      justifyContent: 'center',
                      alignSelf: 'center',
                      borderRadius: 50,
                      paddingHorizontal: 15,
                      paddingVertical: 5,
                      backgroundColor: '#F76E1E',
                      margin: 10,
                      width: 135,
                    }}>
                    <Text
                      style={{
                        fontFamily: 'SofiaPro-Regular',
                        marginLeft: 5,
                        fontSize: 15,
                        color: '#fff',
                        textAlign: 'center',
                      }}>
                      Recarregar
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </>
            ) : null}

            {this.state.filtering === true && this.state.loading === false ? (
              <ItemModal item={'Nenhuma aula encontrada.'} />
            ) : null}
          </View>
        )}

        <LoadingModal show={this.state.loading} />

        <AwesomeAlert
          show={showModal}
          showProgress={false}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          overlayStyle={{
            backgroundColor: 'rgba(0, 0, 0, 0.4)',
          }}
          contentContainerStyle={{
            maxWidth: '80%',
            minWidth: '70%',
            height: 280,
            margin: 10,
            borderRadius: 30,
            justifyContent: 'center',
          }}
          customView={
            <ScrollView showsVerticalScrollIndicator={false}>
              <View
                style={{
                  flexDirection: 'column',
                  justifyContent: 'center',
                  marginVertical: 15,
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 22,
                    textAlign: 'center',
                    flexWrap: 'wrap',
                    color: '#37153D',
                  }}>
                  Sua aula ainda não foi confirmada.
                </Text>

                {isValid ? (
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      fontSize: 15,
                      textAlign: 'center',
                      flexWrap: 'wrap',
                      color: 'green',
                      marginTop: 5,
                    }}>
                    Essa aula será devolvida caso cancelada com 24h de
                    antecedência.
                  </Text>
                ) : (
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      fontSize: 15,
                      textAlign: 'center',
                      flexWrap: 'wrap',
                      color: 'red',
                      marginTop: 5,
                    }}>
                    Essa aula não será devolvida, pois expirou o prazo de 24h de
                    antecedência.
                  </Text>
                )}
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 10,
                }}>
                {this.state.plan != '' ? (
                  <DefaultButton
                    title={'Aguardar confirmação'}
                    onPress={() => {
                      this.setState({
                        showModal: !this.state.showModal,
                      });
                    }}
                    width={200}
                    height={40}
                    backgroundColor={'#F76E1E'}
                  />
                ) : null}
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}>
                <TouchableWithoutFeedback
                  onPress={() => {
                    this.cancel();
                    this.setState({
                      showModal: !this.state.showModal,
                    });
                  }}>
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      marginTop: 10,
                      color: '#707070',
                    }}>
                    Cancelar agendamento
                  </Text>
                </TouchableWithoutFeedback>
              </View>
            </ScrollView>
          }
        />

        <AwesomeAlert
          show={showFilter}
          showProgress={false}
          closeOnHardwareBackPress={false}
          overlayStyle={{
            backgroundColor: 'rgba(0, 0, 0, 0.4)',
          }}
          contentContainerStyle={{
            maxWidth: '80%',
            minWidth: '70%',
            height: 260,
            margin: 10,
            borderRadius: 30,
            justifyContent: 'center',
          }}
          customView={
            <ScrollView showsVerticalScrollIndicator={false}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginVertical: 15,
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 22,
                    textAlign: 'center',
                    flexWrap: 'wrap',
                    color: '#37153D',
                  }}>
                  Status
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginVertical: 10,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#FAFAFA',
                    borderColor: '#707070',
                    borderWidth: 0.1,
                    borderRadius: 30,
                    width: 200,
                    height: 50,
                  }}>
                  <RNPickerSelect
                    style={style}
                    placeholder={{
                      label: 'Selecionar',
                    }}
                    doneText={'Selecionar'}
                    useNativeAndroidPickerStyle={false}
                    onValueChange={async (itemValue, itemIndex) => {
                      await this.setState({ status: itemValue });
                    }}
                    value={this.state.status}
                    items={this.state.optionsStatus.map(item => {
                      return {
                        label: item.description,
                        value: item.value,
                        key: item.value,
                      };
                    })}
                  />
                </View>
              </View>

              <View
                style={{
                  alignSelf: 'center',
                }}>
                <DefaultButton
                  title={'Filtrar'}
                  onPress={() => {
                    this.setState({
                      showFilter: !this.state.showFilter,
                      filtering: true,
                    });
                    this.filter(this.state.status);
                  }}
                  width={200}
                  height={40}
                  backgroundColor={'#F76E1E'}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 5,
                }}>
                <TouchableWithoutFeedback
                  onPress={() => {
                    this.setState({
                      showFilter: !this.state.showFilter,
                    });
                  }}>
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      color: '#707070',
                    }}>
                    Cancelar
                  </Text>
                </TouchableWithoutFeedback>
              </View>
            </ScrollView>
          }
        />

        <AwesomeAlert
          show={tokenModal}
          showProgress={false}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          contentContainerStyle={{
            backgroundColor: '#F8F8F8',
            borderRadius: 25,
          }}
          customView={
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: 300,
                backgroundColor: '#F8F8F8',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 20,
                }}>
                <View
                  style={{
                    flexDirection: 'column',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      fontSize: 22,
                      textAlign: 'center',
                      flexWrap: 'wrap',
                      color: '#37153D',
                      paddingHorizontal: 10,
                    }}>
                    Informe o seguinte código ao seu Personal:
                  </Text>

                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      fontSize: 22,
                      textAlign: 'center',
                      flexWrap: 'wrap',
                      color: '#F76E1E',
                      marginVertical: 15,
                    }}>
                    {this.state.choice.tk_auth_service}
                  </Text>

                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      fontSize: 16,
                      textAlign: 'center',
                      flexWrap: 'wrap',
                      color: '#37153D',
                      padding: 10,
                    }}>
                    Este código é sua garantia de segurança de que a aula não
                    possa ser iniciada sem que você esteja presente.
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                }}>
                <TouchableWithoutFeedback
                  onPress={() =>
                    this.setState({
                      tokenModal: false,
                    })
                  }>
                  <Text style={{ marginTop: 10, color: '#999' }}>Voltar</Text>
                </TouchableWithoutFeedback>
              </View>
            </View>
          }
        />
      </>
    );
  }
}

const ItemModal = ({ item, answer }) => {
  return (
    <View style={{ flexDirection: 'row', justifyContent: 'center', margin: 2 }}>
      <Text
        style={{
          fontFamily: 'SofiaPro-Regular',
          flexWrap: 'wrap',
          color: '#F76E1E',
          fontSize: 13,
        }}>
        {item}
      </Text>
      <Text
        style={{
          fontFamily: 'SofiaProLight',
          flexWrap: 'wrap',
          color: '#2E2E2E',
          fontSize: 13,
        }}>
        {answer}
      </Text>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(MyClassesScreen);
