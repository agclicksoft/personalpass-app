import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableWithoutFeedback,
  TextInput
} from 'react-native';

import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { AxiosWithHeaders } from '../../services/api';
import { ProfileItem, DefaultButton } from '../../components';
import { SafeAreaView } from 'react-native-safe-area-context';
import { showMessage } from 'react-native-flash-message';
import RNBootSplash from 'react-native-bootsplash';
import AsyncStorage from '@react-native-community/async-storage';
import LottieView from 'lottie-react-native';
import animation from '../../assets/success.json';
import AwesomeAlert from 'react-native-awesome-alerts';

class ProfileProfessionalScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: this.props?.user?.user,
      img_profile: this.props?.user?.user?.img_profile,
      changePassAlert: false,
      password: '',
    };
  }

  async componentDidMount() {
    setTimeout(() => RNBootSplash.hide({ duration: 300 }), 2000);
    setTimeout(() => this.loadUser(this.props?.user?.id), 2000);
    await setTimeout(() => {
      this.setState({
        changePassAlert: this.props?.user?.user?.is_redefinition,
      })
    }, 2000);

    this.focusListener = this.props.navigation.addListener('focus', () =>
      this.loadUser(this.props?.user?.id),
    );
  }

  componentWillUnmount() {
    this.focusListener();
  }

  loadUser = async id => {
    const { user, token } = this.props;
    const { img_profile } = this.state;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(`/professionals/${id}`)
      .then(async response => {
        await this.setState({
          user: response.data,
          img_profile:
            response.data.user.img_profile !== null
              ? `${response.data.user.img_profile}?random=${Math.random()
                  .toString(36)
                  .substring(7)}`
              : `https://static.scrum.org/web/images/profile-placeholder.png`,
        });
      })
      .catch(err => {
        console.log(err);
      });

    user.user.img_profile = img_profile;
    this.props.setUser(user);
  };

  showPassChange = () => {
    this.setState({ changePassAlert: !this.state.changePassAlert });
  };

  showSuccess = () => {
    this.setState({
      showSuccess: true,
    });

    setTimeout(
      () =>
        this.setState({
          showSuccess: false,
          changePassAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  saveNewPassword = async () => {
    const { user, token } = this.props;
    const { id, user_id } = this.props.user;

    const { password } = this.state;

    if (!password) {
      showMessage({
        type: 'danger',
        message: 'Favor informar nova senha',
        titleStyle: {
          textAlign: 'center',
        },
      });

      this.setState({
        changePassAlert: true,
      })

      return;
    }

    const AxiosInstace = AxiosWithHeaders({ Authorization: `Bearer ${token}` });

    try {
      await AxiosInstace.post(`/users/change-password`, {
        password: password,
      });

      this.showSuccess();
      this.setState({
        password: '',
      });

      await AxiosInstace.put(`/professionals/${id}`, {
        id: id,
        user_id: user_id,
        user: {
          id: user_id,
          is_redefinition: false
        },
      });

      user.user.is_redefinition = false;
      this.props.setUser(user);
    } catch (err) {
      console.log(err);
      showMessage({
        type: 'danger',
        message: 'Erro inesperado. Tente novamente',
        titleStyle: {
          textAlign: 'center',
        },
      });
    }
  };

  render() {
    const { user, img_profile, changePassAlert, showSuccess } = this.state;

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            paddingHorizontal: 30,
            paddingVertical: 20,
            elevation: 3,
          }}>
          <Text
            style={{
              fontFamily: 'SofiaPro-Medium',
              fontSize: 18,
              color: '#37153D',
              textAlign: 'left',
            }}>
            Meu perfil
          </Text>
        </View>
        <ScrollView>
          <View>
            <View
              style={{
                flexDirection: 'row',
                padding: 30,
                backgroundColor: '#F8F8F8',
                elevation: 2,
              }}>
              <Image
                style={{ height: 115, width: 115, borderRadius: 135 }}
                source={{
                  uri: img_profile
                    ? img_profile
                    : 'https://static.scrum.org/web/images/profile-placeholder.png',
                }}
              />
              <View
                style={{
                  flexDirection: 'column',
                  justifyContent: 'center',
                  paddingLeft: 20,
                  flexShrink: 1,
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 20,
                    flexWrap: 'wrap',
                    color: '#37153D',
                  }}>
                  {user?.user?.name}
                </Text>
                <TouchableWithoutFeedback
                  onPress={() => {
                    this.props.navigation.navigate('EditProfessionalScreen', {
                      professional: user,
                      img_profile: img_profile,
                    });
                  }}>
                  <Text style={{ fontFamily: 'SofiaProLight', fontSize: 12 }}>
                    Editar perfil
                  </Text>
                </TouchableWithoutFeedback>
              </View>
            </View>

            <ProfileItem
              name={'Perfil público'}
              description={'Visualizar seu perfil público'}
              icon={require('../../assets/images/perfil-publico.png')}
              onPress={() => {
                this.props.navigation.navigate('ProfessionalProfileScreen', {
                  professional: user,
                  img_profile: img_profile,
                });
              }}
            />

            <ProfileItem
              name={'Agenda'}
              description={'Selecionar dias e horários de atendimento'}
              icon={require('../../assets/images/interface.png')}
              onPress={() => {
                this.props.navigation.navigate('ProfessionalScheduleScreen', {
                  professional: user,
                });
              }}
            />

            <ProfileItem
              name={'Locais de atendimento'}
              description={'Selecionar locais'}
              icon={require('../../assets/images/place.png')}
              onPress={() => {
                this.props.navigation.navigate('LocationsScreen');
              }}
            />

            <ProfileItem
              name={'Especialidades'}
              description={'Selecionar suas especialidades'}
              icon={require('../../assets/images/verified.png')}
              onPress={() => {
                this.props.navigation.navigate('SpecialtiesScreen');
              }}
            />

            <ProfileItem
              name={'Pacotes de serviços'}
              description={'Gerenciar opções de pacotes'}
              icon={require('../../assets/images/cube.png')}
              onPress={() => {
                this.props.navigation.navigate('PlansListScreen', {
                  professional: user,
                });
              }}
            />

            {/* <ProfileItem
              name={'Minhas avaliações'}
              description={'Visualizar avaliações'}
              icon={require('../../assets/images/favorites.png')}
              onPress={() => {
                this.props.navigation.navigate('MyAssessmentsScreen');
              }}
            /> */}

            <ProfileItem
              name={'Conta bancária'}
              description={'Editar conta para recebimento'}
              icon={require('../../assets/images/coin.png')}
              onPress={() => {
                this.props.navigation.navigate('BankScreen', {
                  professional: user,
                });
              }}
            />

            <ProfileItem
              name={'Termos de uso'}
              description={'Visualizar termos'}
              icon={require('../../assets/images/file.png')}
              onPress={() => {
                this.props.navigation.navigate('ConditionsScreen');
              }}
            />

            <ProfileItem
              onPress={async () => {
                await AsyncStorage.removeItem('DATA_KEY');
                await AsyncStorage.removeItem('REFRESH_TOKEN');
                this.props.clearUserInfo();
                this.props.navigation.navigate('AccessScreen');
              }}
              name={'Sair'}
              description={'Sair da sua conta'}
              icon={require('../../assets/images/off-icon.png')}
            />
          </View>
        </ScrollView>

        <AwesomeAlert
          show={changePassAlert}
          showProgress={false}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          contentContainerStyle={{
            borderRadius: 25,
            backgroundColor: '#f8f8f8',
            justifyContent: 'center',
          }}
          customView={
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: 300,
                backgroundColor: '#F8F8F8',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}>
                <View
                  style={{
                    flexDirection: 'column',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      fontSize: 17,
                      color: '#2E2E2E',
                      margin: 0,
                    }}>
                    Insira aqui sua nova senha:
                  </Text>
                  <CustomInput
                    question={null}
                    answer={this.state.password}
                    height={40}
                    secureTextEntry
                    onChangeText={text => this.setState({ password: text })}
                    width={200}
                  />
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                  marginTop: 20,
                }}>
                <DefaultButton
                  onPress={() => {
                    this.setState({
                      changePassAlert: false,
                    });
                    this.saveNewPassword();
                  }}
                  backgroundColor={'#37153D'}
                  title={'Salvar'}
                  width={200}
                  height={40}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                }}>
                <TouchableWithoutFeedback
                  onPress={() =>
                    this.setState({
                      changePassAlert: false,
                    })
                  }>
                  <Text style={{ marginTop: 10, color: '#999' }}>Deixar para depois</Text>
                </TouchableWithoutFeedback>
              </View>
            </View>
          }
        />

        <AwesomeAlert
          show={showSuccess}
          showProgress={false}
          closeOnHardwareBackPress={false}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Senha atualizada com sucesso!
              </Text>
              <LottieView
                style={{ width: 70, height: 70 }}
                resizeMode="contain"
                autoSize
                source={animation}
                autoPlay
                loop={false}
              />
            </View>
          }
        />
      </>
    );
  }
}

const CustomInput = ({
  question,
  answer,
  width,
  onPress,
  keyboardType,
  maxLength,
  onChangeText,
  placeholder,
  secureTextEntry,
  password,
  onChangePassPress,
  editable,
}) => {
  return (
    <View style={{ flexDirection: 'column', marginLeft: 10, marginRight: 10 }}>
      <Text
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaPro-Medium',
          margin: 10,
          fontSize: 13,
        }}>
        {question}
      </Text>

      <View
        style={{
          flexDirection: 'row',
          position: 'relative',
        }}>
        <TextInput
          style={{
            color: '#2E2E2E',
            fontFamily: 'SofiaProLight',
            fontSize: 13,
            backgroundColor: '#fff',
            height: 40,
            borderRadius: 50,
            borderWidth: 0.1,
            paddingLeft: 30,
            paddingRight: 30,
            width: width ? width : 300,
          }}
          value={answer}
          maxLength={maxLength}
          keyboardType={keyboardType}
          onChangeText={onChangeText}
          placeholder={placeholder}
          secureTextEntry={secureTextEntry}
          editable={editable}
        />

        {password == true ? (
          <TouchableWithoutFeedback onPress={onChangePassPress}>
            <Text
              style={{
                position: 'absolute',
                right: 10,
                alignSelf: 'center',
                fontSize: 11,
                color: '#37153D',
                fontFamily: 'SofiaProLight',
                padding: 10,
              }}>
              Alterar
            </Text>
          </TouchableWithoutFeedback>
        ) : null}
      </View>
    </View>
  );
};

CustomInput.defaultProps = {
  placeholder: 'Não informado',
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(ProfileProfessionalScreen);
