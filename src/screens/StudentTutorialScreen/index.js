import React, { Component } from 'react';
import { Image, Dimensions } from 'react-native';
import { Creators as authActions } from '../../redux/reducers/auth';
import { connect } from 'react-redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import Onboarding from 'react-native-onboarding-swiper';
import logo from '../../assets/images/logo.png';
import profile from '../../assets/images/StudentOnboarding/edit-profile.gif';
import parq from '../../assets/images/StudentOnboarding/parq.gif';
import publicProfile from '../../assets/images/StudentOnboarding/public-profile.gif';
import schedule from '../../assets/images/StudentOnboarding/schedule.gif';

const dimensions = Dimensions.get('window');
const imageHeight = Math.round((dimensions.width * 9) / 16);

class StudentTutorialScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <Onboarding
          nextLabel={'Avançar'}
          showSkip={false}
          containerStyles={{
            justifyContent: 'flex-start',
          }}
          titleStyles={{
            paddingHorizontal: 20,
            fontSize: 22,
          }}
          imageContainerStyles={{
            paddingVertical: 20,
          }}
          onDone={() => this.props.navigation.navigate('ProfessionalsScreen')}
          pages={[
            {
              backgroundColor: '#37153D',
              image: (
                <>
                  <SafeAreaView style={{ backgroundColor: '#37153D' }} />
                  <Image
                    source={logo}
                    resizeMode={'contain'}
                    style={{ height: 150, marginTop: 100, marginBottom: 100 }}
                  />
                </>
              ),
              title: 'Olá! Seja bem-vindo(a)!',
              subtitle: 'Vamos lhe guiar em seus primeiros passos.',
            },
            {
              backgroundColor: '#fe6e58',
              image: (
                <>
                  <SafeAreaView style={{ backgroundColor: '#fe6e58' }} />
                  <Image
                    source={profile}
                    resizeMode={'contain'}
                    // style={{ height: 400, marginTop: 30 }}
                    style={{ height: imageHeight * 1.5, marginTop: 30 }}
                  />
                </>
              ),
              title:
                'Primeiro, você deve preencher as informações em seu perfil.',
              subtitle:
                'Para isto, clique em editar perfil e preencha todos os campos necessários.',
            },
            {
              backgroundColor: '#8fcfd1',
              image: (
                <>
                  <SafeAreaView style={{ backgroundColor: '#8fcfd1' }} />
                  <Image
                    source={parq}
                    resizeMode={'contain'}
                    // style={{ height: 400, marginTop: 30 }}
                    style={{ height: imageHeight * 1.5, marginTop: 30 }}
                  />
                </>
              ),
              title: 'O segundo passo é preencher as informações na aba PAR-Q.',
              subtitle:
                'Este é um questionário completo de Prontidão para Atividade Física, essencial para seu futuro personal saber detalhes sobre você.',
            },
            {
              backgroundColor: '#febf63',
              image: (
                <>
                  <SafeAreaView style={{ backgroundColor: '#febf63' }} />
                  <Image
                    source={publicProfile}
                    resizeMode={'contain'}
                    // style={{ height: 400, marginTop: 30 }}
                    style={{ height: imageHeight * 1.5, marginTop: 30 }}
                  />
                </>
              ),
              title: 'Importante lembrar!',
              subtitle:
                'Apenas o personal contratado por você pode ter acesso ao seu perfil público!',
            },
            {
              backgroundColor: '#99b898',
              image: (
                <>
                  <SafeAreaView style={{ backgroundColor: '#99b898' }} />
                  <Image
                    source={schedule}
                    resizeMode={'contain'}
                    // style={{ height: 400, marginTop: 30 }}
                    style={{ height: imageHeight * 1.5, marginTop: 30 }}
                  />
                </>
              ),
              title: 'Pronto!',
              subtitle:
                'Com essas informações preenchidas, você pode acessar o menu principal, selecionar o personal e agendar sua primeira aula!',
            },
          ]}
        />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(StudentTutorialScreen);
