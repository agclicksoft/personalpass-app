import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';
import { DefaultButton } from '../../components';
import { SafeAreaView } from 'react-native-safe-area-context';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import moment from 'moment';
import 'moment-timezone';

class PaymentSelectionScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      lesson: '',
      daysTilAppointment: '',
    };
  }

  async componentDidMount() {
    this.setState({
      lesson: this.props.route?.params?.class,
    });

    this.differenceInDays();
  }

  convertPrice = price => {
    return price.toLocaleString('pt-br', {
      style: 'currency',
      currency: 'BRL',
    });
  };

  differenceInDays = () => {
    var a = moment(this.props.route?.params?.class?.date);
    var b = moment();

    this.setState({
      daysTilAppointment: a.diff(b, 'days'),
    });
    console.log(a.diff(b, 'days'));
  };

  render() {
    const appointment = this.state.lesson;

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            paddingHorizontal: 30,
            paddingVertical: 20,
            elevation: 3,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Ionicon
                style={{
                  fontWeight: '900',
                  marginRight: 10,
                }}
                name={'ios-arrow-round-back'}
                size={30}
                color={'#37153D'}
              />
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#37153D',
                  textAlign: 'left',
                }}>
                {' '}
                Pagamento
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>

        <ScrollView>
          <View style={{ flexDirection: 'column' }}>
            <Text
              style={{
                fontFamily: 'SofiaPro-Medium',
                fontSize: 18,
                flexWrap: 'wrap',
                color: '#37153D',
                textAlign: 'center',
                padding: 20,
              }}>
              Detalhes do agendamento
            </Text>
            <ClassItem
              date={moment
                .parseZone(appointment?.date)
                .format('DD/MM/YYYY - HH:mm')}
              gym={appointment?.establishment?.name}
              personal={appointment?.professional?.user.name}
              plan={appointment?.contract?.professionalPlan?.plan?.description}
              status={
                appointment.contract?.professionalPlan?.price
                  ? this.convertPrice(
                      appointment.contract?.professionalPlan?.price,
                    )
                  : null
              }
              onPress={null}
            />

            <View style={{ height: 20 }} />

            <MenuItem
              name={'Cartão de crédito'}
              icon={require('../../assets/images/credit-card.png')}
              onPress={() => {
                this.props.navigation.navigate('PaymentWithPagSeguroScreen', {
                  appointment: appointment,
                });
              }}
            />

            {this.state.daysTilAppointment >= 5 &&
            this.state.daysTilAppointment != '' ? (
              <MenuItem
                name={'Boleto'}
                icon={require('../../assets/images/file.png')}
                onPress={() => {
                  this.props.navigation.navigate('PaymentWithBoleto', {
                    appointment: appointment,
                  });
                }}
              />
            ) : null}

            <MenuItem
              name={'Pagar no dia'}
              icon={require('../../assets/images/verified.png')}
              onPress={() => {
                this.props.navigation.navigate('PaymentInHands', {
                  appointment: appointment,
                });
              }}
            />
          </View>
        </ScrollView>
      </>
    );
  }
}

const MenuItem = ({ name, icon, onPress }) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          backgroundColor: '#fafafa',
          borderBottomWidth: 0.3,
          borderBottomColor: '#00000030',
          padding: 30,
        }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Image
            source={icon}
            style={{ width: 30, height: 30 }}
            resizeMode="contain"
          />
          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              paddingLeft: 15,
              flexShrink: 1,
            }}>
            <Text
              style={{
                fontFamily: 'SofiaPro-Medium',
                fontSize: 16,
                flexWrap: 'wrap',
                color: '#37153D',
              }}>
              {name}
            </Text>
          </View>
        </View>
        <Ionicon
          style={{
            fontWeight: '900',
            color: 'rgba(112,112,112,0.5)',
            transform: [{ rotate: '180deg' }],
          }}
          name={'ios-arrow-back'}
          size={21}
          color={'red'}
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

const ClassItem = ({ date, gym, personal, onPress, status, plan, avatar }) => {
  return (
    <>
      <View
        style={{
          flexDirection: 'column',
          alignItems: 'center',
          paddingTop: 20,
          paddingBottom: 10,
          margin: 10,
          borderRadius: 10,
          elevation: 1,
          backgroundColor: '#fff',
        }}>
        <View
          style={{
            flexDirection: 'column',
            alignItems: 'center',
            width: 320,
          }}>
          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'space-evenly',
              height: 100,
            }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Data/hora:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {date}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Local:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {gym}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Personal:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {personal}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Pacote:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {plan}
              </Text>
            </View>
          </View>
        </View>

        <View
          style={{
            borderTopWidth: 1,
            borderTopColor: 'rgba(112, 112, 112, 0.1)',
            padding: 5,
            width: '100%',
            alignItems: 'center',
            marginTop: 15,
          }}>
          <DefaultButton
            title={status}
            onPress={onPress}
            backgroundColor={'#F76E1E'}
            fontColor={'#fff'}
            width={250}
            height={35}
          />
        </View>
      </View>
    </>
  );
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(PaymentSelectionScreen);
