import AccessScreen from './AccessScreen';
import RecoverPassScreen from './RecoverPassScreen';
import PaymentSelectionScreen from './PaymentSelectionScreen';
import PaymentWithPagSeguroScreen from './PaymentWithPagSeguroScreen';
import PaymentWithBoleto from './PaymentWithBoleto';
import PaymentInHands from './PaymentInHands';
import AttachmentsScreen from './AttachmentsScreen';
import BankScreen from './BankScreen';
import ChartsScreen from './ChartsScreen';
import ConditionsScreen from './ConditionsScreen';
import EditStudentScreen from './EditStudentScreen';
import EditProfessionalScreen from './EditProfessionalScreen';
import Example from './Example';
import FilterScreen from './FilterScreen';
import GymsScreen from './GymsScreen';
import NewGymScreen from './NewGymScreen';
import LocationsScreen from './LocationsScreen';
import MyAssessmentsScreen from './MyAssessmentsScreen';
import MyClassesScreen from './MyClassesScreen';
import MyPlansScreen from './MyPlansScreen';
import MyProfileScreen from './MyProfileScreen';
import PARQScreen from './PARQScreen';
import PaymentListScreen from './PaymentListScreen';
import PlansListScreen from './PlansListScreen';
import ProfessionalProfileScreen from './ProfessionalProfileScreen';
import ProfessionalRegisterScreen from './ProfessionalRegisterScreen';
import ProfileProfessionalScreen from './ProfileProfessionalScreen';
import ProfessionalScheduleScreen from './ProfessionalScheduleScreen';
import ProfessionalsScreen from './ProfessionalsScreen';
import RegisterScreen from './RegisterScreen';
import ReportsScreen from './ReportsScreen';
import SolicitationsScreen from './SolicitationsScreen';
import SpecialtiesScreen from './SpecialtiesScreen';
import StatisticsScreen from './StatisticsScreen';
import StudentProfileScreen from './StudentProfileScreen';
import ClassDescriptionScreen from './ClassDescriptionScreen';
import TrainingDetailScreen from './TrainingDetailScreen';
import StudentTutorialScreen from './StudentTutorialScreen';
import ProfessionalTutorialScreen from './ProfessionalTutorialScreen';

export {
  AccessScreen,
  RecoverPassScreen,
  PaymentSelectionScreen,
  PaymentWithPagSeguroScreen,
  PaymentWithBoleto,
  PaymentInHands,
  AttachmentsScreen,
  BankScreen,
  ChartsScreen,
  ConditionsScreen,
  EditStudentScreen,
  EditProfessionalScreen,
  Example,
  FilterScreen,
  MyClassesScreen,
  GymsScreen,
  NewGymScreen,
  LocationsScreen,
  MyAssessmentsScreen,
  MyPlansScreen,
  MyProfileScreen,
  PARQScreen,
  PaymentListScreen,
  PlansListScreen,
  ProfessionalRegisterScreen,
  ProfessionalProfileScreen,
  ProfessionalsScreen,
  ProfessionalScheduleScreen,
  ProfileProfessionalScreen,
  RegisterScreen,
  ReportsScreen,
  SolicitationsScreen,
  SpecialtiesScreen,
  StatisticsScreen,
  StudentProfileScreen,
  ClassDescriptionScreen,
  TrainingDetailScreen,
  StudentTutorialScreen,
  ProfessionalTutorialScreen,
};
