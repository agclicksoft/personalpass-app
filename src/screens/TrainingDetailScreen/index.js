import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableWithoutFeedback,
  Linking,
} from 'react-native';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { AxiosWithHeaders } from '../../services/api';
import { SafeAreaView } from 'react-native-safe-area-context';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import moment from 'moment';
import whatsapp from '../../assets/images/whatsapp.png';
import email from '../../assets/images/email.png';
import phone from '../../assets/images/phone.png';
import emergencia from '../../assets/images/emergencia.png';
import pacote from '../../assets/images/pacote.png';

class TrainingDetailScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showParq: false,
      lesson: '',
      student: '',
      img_profile: '',
      professional: '',
      professionalImgProfile: '',
    };
  }
  async componentDidMount() {
    await this.loadLesson();
  }

  loadLesson = async () => {
    const { token } = this.props;
    const id = this.props.route?.params?.classId;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(`/scheduled-services/${id}`)
      .then(response => {
        this.setState({
          lesson: response.data,
          student: response.data.classes[0].student,
          img_profile: response.data.classes[0].student.user.img_profile,
          professional: response.data.professional,
          professionalImgProfile: response.data.professional.user.img_profile,
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  goBack = () => {
    this.props.route?.params?.class != null
      ? this.props.navigation.pop()
      : this.props.navigation.pop(3);
  };

  showAlert = () => {
    this.setState({
      showAlert: true,
    });

    setTimeout(
      () =>
        this.setState({
          showAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  render() {
    const {
      lesson,
      student,
      img_profile,
      professional,
      professionalImgProfile,
    } = this.state;

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#37153D' }} />

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#37153D',
            shadowColor: '#000',
            alignItems: 'center',
            paddingHorizontal: 20,
            paddingVertical: 20,
            paddingTop: 20,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.goBack();
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Ionicon
                style={{
                  fontWeight: '900',
                  marginRight: 10,
                }}
                name={'ios-arrow-round-back'}
                size={30}
                color={'#F8F8F8'}
              />

              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#F8F8F8',
                  textAlign: 'left',
                }}>
                {' '}
                Aula concluída
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>

        <ScrollView
          contentContainerStyle={{
            flexGrow: 1,
            justifyContent: 'flex-start',
            backgroundColor: '#37153D',
          }}>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
              backgroundColor: '#F8F8F8',
              marginTop: 0,
              padding: 20,
              paddingBottom: 90,
              borderTopLeftRadius: 40,
              borderTopRightRadius: 40,
            }}>
            <Image
              style={{ height: 200, width: 200, borderRadius: 135 }}
              source={{
                uri:
                  this.props.user?.user?.role == 'student'
                    ? professionalImgProfile
                    : img_profile,
              }}
            />
            <Text
              style={{
                color: '#37153D',
                fontSize: 22,
                fontFamily: 'SofiaPro-Medium',
                marginTop: 20,
              }}>
              {this.props.user?.user?.role == 'student'
                ? professional?.user?.name
                : student?.user?.name}
            </Text>

            {this.props.user?.user?.role == 'professional' ? (
              <View style={{ flexDirection: 'row' }}>
                <Text
                  style={{
                    textAlign: 'center',
                    fontFamily: 'SofiaProLight',
                    fontSize: 16,
                    color: '#707070',
                    marginTop: 5,
                  }}>
                  {moment().diff(student?.user?.birthday, 'years')} anos
                </Text>
              </View>
            ) : (
              <View style={{ flexDirection: 'column' }}>
                <CustomQuestion
                  flexDirection={'row'}
                  question={`CREF/CONFEF: `}
                  answer={professional?.doc_professional?.toUpperCase()}
                  color={'#F76E1E'}
                />
              </View>
            )}

            <SolicitationItem
              singleAppointment={true}
              date={moment.parseZone(lesson.date).format('DD/MM/YYYY - HH:mm')}
              gym={lesson.establishment?.name}
              plan={lesson.contract?.professionalPlan?.plan?.description}
              status={lesson.status?.toLowerCase()}
            />

            {this.props.user?.user?.role != 'student' ? (
              <View style={{ marginTop: 10 }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    width: 190,
                  }}>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      Linking.canOpenURL('whatsapp://send?text=Oi').then(
                        supported => {
                          if (supported) {
                            return Linking.openURL(
                              `whatsapp://send?phone=55${
                                student?.user.contact
                              }&text=Olá, você me contratou no Personal Access, tudo bem?`,
                            );
                          } else {
                            return Linking.openURL(
                              `https://api.whatsapp.com/send?phone=55${
                                student?.user.contact
                              }&text=Olá, você me contratou no Personal Access, tudo bem?`,
                            );
                          }
                        },
                      )
                    }>
                    <Image
                      source={whatsapp}
                      style={{ width: 40, height: 40, marginVertical: 10 }}
                      resizeMode="contain"
                    />
                  </TouchableWithoutFeedback>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      Linking.openURL(
                        `mailto:${
                          student.user?.email
                        }?subject=Olá, você me contratou pelo Personal Access!`,
                      )
                    }>
                    <Image
                      source={email}
                      style={{ width: 40, height: 40, marginVertical: 10 }}
                      resizeMode="contain"
                    />
                  </TouchableWithoutFeedback>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      Linking.openURL(`tel:${student.user?.contact}`)
                    }>
                    <Image
                      source={phone}
                      style={{ width: 40, height: 40, marginVertical: 10 }}
                      resizeMode="contain"
                    />
                  </TouchableWithoutFeedback>

                  <TouchableWithoutFeedback
                    onPress={() =>
                      Linking.openURL(`tel:${student.user?.emergency_phone}`)
                    }>
                    <Image
                      source={emergencia}
                      style={{ width: 40, height: 40, marginVertical: 10 }}
                      resizeMode="contain"
                    />
                  </TouchableWithoutFeedback>
                </View>
              </View>
            ) : (
              <View style={{ marginTop: 10 }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    width: 150,
                  }}>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      Linking.canOpenURL('whatsapp://send?text=Oi').then(
                        supported => {
                          if (supported) {
                            return Linking.openURL(
                              `whatsapp://send?phone=55${
                                professional?.user.contact
                              }&text=Olá, te contratei pelo Personal Access, tudo bem?`,
                            );
                          } else {
                            return Linking.openURL(
                              `https://api.whatsapp.com/send?phone=55${
                                professional?.user.contact
                              }&text=Olá, te contratei pelo Personal Access, tudo bem?`,
                            );
                          }
                        },
                      )
                    }>
                    <Image
                      source={whatsapp}
                      style={{ width: 40, height: 40, marginVertical: 10 }}
                      resizeMode="contain"
                    />
                  </TouchableWithoutFeedback>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      Linking.openURL(
                        `mailto:${
                          professional.user?.email
                        }?subject=Olá, te contratei pelo Personal Access!`,
                      )
                    }>
                    <Image
                      source={email}
                      style={{ width: 40, height: 40, marginVertical: 10 }}
                      resizeMode="contain"
                    />
                  </TouchableWithoutFeedback>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      Linking.openURL(`tel:${professional.user?.contact}`)
                    }>
                    <Image
                      source={phone}
                      style={{ width: 40, height: 40, marginVertical: 10 }}
                      resizeMode="contain"
                    />
                  </TouchableWithoutFeedback>
                </View>
              </View>
            )}

            {this.props.user?.user?.role != 'student' ? (
              <TouchableWithoutFeedback
                onPress={() =>
                  this.setState({
                    showParq: !this.state.showParq,
                  })
                }>
                <Text
                  style={{
                    margin: 20,
                    fontFamily: 'SofiaPro-Regular',
                    fontSize: 18,
                    color: '#F76E1E',
                    textDecorationLine: 'underline',
                  }}>
                  {`Visualizar PAR-Q`}
                </Text>
              </TouchableWithoutFeedback>
            ) : null}

            {this.state.showParq == true ? (
              <View
                style={{
                  backgroundColor: '#fff',
                  borderRadius: 20,
                  padding: 20,
                  marginTop: 20,
                  width: '95%',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    borderRadius: 30,
                    width: 280,
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      marginBottom: 20,
                      fontFamily: 'SofiaPro-Regular',
                      fontSize: 18,
                      color: '#F76E1E',
                    }}>{`PAR-Q:`}</Text>

                  <View
                    style={{
                      flexDirection: 'row',
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      width: 200,
                      margin: 10,
                    }}>
                    {student?.user?.faqAnswered.map(item => {
                      return item.faqQuestion.id == 1 ? (
                        <CustomQuestion
                          flexDirection={'row'}
                          question={`${item.faqQuestion.question}: `}
                          answer={`${item.answered} kg`}
                          visible={item.answered > ''}
                        />
                      ) : null;
                    })}

                    <View style={{ width: 20 }} />
                    {student?.user?.faqAnswered.map(item => {
                      return item.faqQuestion.id == 2 ? (
                        <CustomQuestion
                          flexDirection={'row'}
                          question={`${item.faqQuestion.question}: `}
                          answer={`${item.answered} m`}
                          visible={item.answered > ''}
                        />
                      ) : null;
                    })}
                  </View>

                  {student?.user?.faqAnswered.map(item => {
                    return item.faqQuestion.id > 2 ? (
                      <CustomQuestion
                        question={item.faqQuestion.question}
                        answer={item.answered}
                        visible={item.answered > ''}
                      />
                    ) : null;
                  })}
                </View>
              </View>
            ) : null}

            <View
              style={{
                borderTopWidth: 1,
                borderTopColor: 'rgba(112, 112, 112, 0.1)',
                width: '100%',
                alignItems: 'center',
                marginVertical: 15,
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 20,
                  color: '#37153D',
                  marginVertical: 15,
                }}>
                Descrição de treino
              </Text>

              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'center',
                  margin: 10,
                  borderRadius: 10,
                  elevation: 1,
                  backgroundColor: '#fff',
                  paddingHorizontal: 30,
                }}>
                <CustomQuestion
                  flexDirection={'column'}
                  question={`Grupamentos: `}
                  answer={
                    lesson?.trainingDescription?.groupings != null
                      ? lesson.trainingDescription.groupings.replace(/,/g, ', ')
                      : 'Não informado'
                  }
                />
                <CustomQuestion
                  flexDirection={'column'}
                  question={`Modalidade: `}
                  answer={
                    lesson?.trainingDescription?.modality != null
                      ? lesson.trainingDescription.modality.replace(/,/g, ', ')
                      : 'Não informado'
                  }
                />
                <CustomQuestion
                  flexDirection={'column'}
                  question={`Observações: `}
                  answer={
                    lesson?.trainingDescription?.note != null
                      ? lesson.trainingDescription.note
                      : 'Não informado'
                  }
                />
              </View>
            </View>
          </View>
        </ScrollView>
      </>
    );
  }
}

const SolicitationItem = ({
  date,
  gym,
  plan,
  singleAppointment,
  methodPayment,
}) => {
  return (
    <>
      <View
        style={{
          flexDirection: 'column',
          alignItems: 'center',
          margin: 10,
          borderRadius: 10,
          elevation: 1,
          backgroundColor: '#fff',
          padding: 30,
        }}>
        <View
          style={{
            flexDirection: 'column',
            alignItems: 'center',
            paddingTop: 10,
            paddingBottom: 0,
            borderRadius: 10,
          }}>
          <View
            style={{
              flexDirection: 'row',
            }}>
            {singleAppointment === false ? (
              <Image
                style={{ height: 65, width: 65, padding: 5 }}
                source={pacote}
                resizeMode={'contain'}
              />
            ) : null}
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'space-evenly',
                alignItems:
                  singleAppointment === true ? 'center' : 'flex-start',
                marginLeft: 10,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Regular',
                    flexWrap: 'wrap',
                    color: '#F76E1E',
                    fontSize: 14,
                  }}>
                  Data/hora:{' '}
                </Text>
                <Text
                  style={{
                    fontFamily: 'SofiaProLight',
                    flexWrap: 'wrap',
                    color: '#2E2E2E',
                    fontSize: 14,
                  }}>
                  {date}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 5,
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Regular',
                    flexWrap: 'wrap',
                    color: '#F76E1E',
                    fontSize: 14,
                  }}>
                  Local:{' '}
                </Text>
                <Text
                  style={{
                    fontFamily: 'SofiaProLight',
                    flexWrap: 'wrap',
                    color: '#2E2E2E',
                    fontSize: 14,
                  }}>
                  {gym}
                </Text>
              </View>
              {singleAppointment == true ? (
                <>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginTop: 5,
                    }}>
                    <Text
                      style={{
                        fontFamily: 'SofiaPro-Regular',
                        flexWrap: 'wrap',
                        color: '#F76E1E',
                        fontSize: 13,
                      }}>
                      Pacote:{' '}
                    </Text>
                    <Text
                      style={{
                        fontFamily: 'SofiaProLight',
                        flexWrap: 'wrap',
                        color: '#2E2E2E',
                        fontSize: 13,
                      }}>
                      {plan}
                    </Text>
                  </View>
                </>
              ) : null}
            </View>
          </View>
        </View>
      </View>
    </>
  );
};

const CustomQuestion = ({
  question,
  answer,
  color,
  visible,
  flexDirection,
}) => {
  return visible ? (
    <View
      style={{
        flexDirection: flexDirection,
        margin: 10,
        alignItems: 'center',
      }}>
      <Text
        style={{
          textAlign: 'center',
          fontFamily: 'SofiaPro-Regular',
          fontSize: 12,
          color: color ? color : '#222',
        }}>
        {question}
      </Text>
      <Text
        style={{
          textAlign: 'center',
          fontFamily: 'SofiaProLight',
          fontSize: 12,
          color: '#707070',
          margin: 5,
        }}>
        {answer}
      </Text>
    </View>
  ) : null;
};

CustomQuestion.defaultProps = {
  visible: true,
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(TrainingDetailScreen);
