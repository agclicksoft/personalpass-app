import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import { DefaultButton } from '../../components';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { SafeAreaView } from 'react-native-safe-area-context';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import Geocoder from 'react-native-geocoder';
import convertStates from '../../services/convertStates';
import { PLACES_API } from '../../services/placesAPI';

class FilterScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      optionsSpecialties: [
        { description: 'Hipertrofia', checked: false },
        { description: 'Emagrecimento', checked: false },
        { description: 'Condicionamento Físico', checked: false },
        { description: 'Preparação Física', checked: false },
        { description: 'Yoga', checked: false },
        { description: 'Pilates', checked: false },
        { description: 'Gestante', checked: false },
        { description: 'Kids', checked: false },
        { description: 'Atividades para Idosos', checked: false },
        { description: 'Dança', checked: false },
      ],
      state: '',
      district: '',
      neighbourhood: '',
    };
  }

  toggleSpecialty = async index => {
    let aux = this.state.optionsSpecialties;

    aux[index] = {
      description: aux[index].description,
      checked: !aux[index].checked,
    };

    await this.setState({ optionsSpecialties: aux });
  };

  getPosition = async (lat, lng) => {
    var pos = {
      lat: lat,
      lng: lng,
    };

    Geocoder.geocodePosition(pos)
      .then(async res => {
        await this.setState({
          state: res[0].adminArea,
          district: res[0].subAdminArea,
          neighbourhood: res[0].subLocality,
        });

        if (Platform.OS === 'ios') {
          await this.setState({
            state: convertStates(res[0].adminArea),
            district: res[0].locality,
          });
        }
      })
      .catch(err => console.log(err));
  };

  getSpecialtyQuery = async (filterType, filterOptions) => {
    let query = `${filterType}=`;
    let empty = true;
    filterOptions.map(item => {
      if (item.checked) {
        query += item.description + ', ';
        empty = false;
      }
    });
    !empty ? (query = query.slice(0, query.length - 2)) : null;

    return !empty ? query : '';
  };

  getPlanQuery = async (filterType, filterOptions) => {
    let query = `${filterType}=`;
    let filters = '';
    let empty = true;
    filterOptions.map(item => {
      if (item.checked) {
        empty = false;

        switch (item.description) {
          case 'Aula avulsa':
            filters += '1,2';
            break;
          case 'Pacote de aulas':
            filters += ',3,4';
            break;
          default:
            filters += '';
        }
      }
    });

    filters[0] == ',' ? (filters = filters.slice(1, filters.length)) : null;
    query += filters;

    return !empty ? query : '';
  };

  getLocationQuery = type => {
    switch (type) {
      case 'state':
        return this.state.state ? `${type}=${this.state.state}` : '';
      case 'district':
        return this.state.district ? `${type}=${this.state.district}` : '';
      case 'neighbourhood':
        return this.state.neighbourhood
          ? `${type}=${this.state.neighbourhood}`
          : '';
    }
  };

  getAllQueries = async () => {
    let aux = '';
    aux +=
      (await this.getSpecialtyQuery(
        'specialty',
        this.state.optionsSpecialties,
      )) != ''
        ? await this.getSpecialtyQuery(
            'specialty',
            this.state.optionsSpecialties,
          )
        : '';
    aux += (await this.getLocationQuery('state'))
      ? '&' + (await this.getLocationQuery('state'))
      : '';
    aux += (await this.getLocationQuery('district'))
      ? '&' + (await this.getLocationQuery('district'))
      : '';
    aux += (await this.getLocationQuery('neighbourhood'))
      ? '&' + (await this.getLocationQuery('neighbourhood'))
      : '';

    return aux;
  };

  render() {
    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingHorizontal: 20,
            paddingVertical: 20,
            elevation: 3,
          }}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <TouchableWithoutFeedback
              onPress={() => {
                this.props.navigation.goBack();
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Ionicon
                  style={{
                    fontWeight: '900',
                    marginRight: 10,
                  }}
                  name={'ios-arrow-round-back'}
                  size={30}
                  color={'#37153D'}
                />
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 14,
                    color: '#37153D',
                    textAlign: 'left',
                  }}>
                  {' '}
                  Filtros de busca
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          <DefaultButton
            title={'Aplicar'}
            width={100}
            height={35}
            margin={'0px'}
            onPress={async () => {
              let query = await this.getAllQueries();
              this.props.navigation.navigate('ProfessionalsList', { query });
            }}
          />
        </View>
        <ScrollView
          keyboardShouldPersistTaps="always"
          contentContainerStyle={{ flexGrow: 1 }}
          showsVerticalScrollIndicator={false}>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'flex-start',
              backgroundColor: '#FAFAFA',
              paddingBottom: 20,
              flex: 1,
            }}>
            <View style={{ height: 10 }} />

            <Text
              style={{
                fontFamily: 'SofiaProLight',
                fontSize: 14,
                color: '#F76E1E',
                margin: 20,
              }}>
              Locais de atendimento:
            </Text>

            <View
              style={{
                flexDirection: 'column',
                width: '100%',
                marginBottom: 10,
                padding: 0,
              }}>
              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'flex-start',
                  marginTop: 0,
                }}>
                <GooglePlacesAutocomplete
                  placeholder="Pesquisar endereço"
                  fetchDetails={true}
                  enablePoweredByContainer={false}
                  minLength={6}
                  onPress={async (data, details = null) => {
                    var lat = details.geometry.location.lat;
                    var lng = details.geometry.location.lng;

                    await this.getPosition(lat, lng);
                  }}
                  query={{
                    key: PLACES_API,
                    language: 'pt-BR',
                    components: 'country:br',
                  }}
                  styles={{
                    textInputContainer: {
                      backgroundColor: 'rgba(0,0,0,0)',
                      borderTopWidth: 0,
                      borderBottomWidth: 0,
                      maxWidth: '90%',
                      minWidth: '90%',
                    },
                    textInput: {
                      marginLeft: 0,
                      marginRight: 0,
                      color: '#636363',
                      fontFamily: 'SofiaProLight',
                      fontSize: 13,
                      backgroundColor: '#fff',
                      height: 40,
                      borderRadius: 50,
                      borderWidth: 0.1,
                      paddingLeft: 30,
                      paddingRight: 30,
                    },
                    listView: {
                      maxWidth: '90%',
                      marginTop: 10,
                      backgroundColor: '#fff',
                    },
                    container: {
                      alignSelf: 'center',
                    },
                  }}
                  placeholderTextColor="#CBCBCD"
                />
              </View>
            </View>

            <View
              style={{
                // position: 'absolute',
                // marginTop: 130,
                width: '100%',
                zIndex: -5,
                backgroundColor: '#FAFAFA',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  fontSize: 14,
                  color: '#F76E1E',
                  margin: 20,
                }}>
                Especialidades:
              </Text>

              <View
                style={{
                  width: '100%',
                  backgroundColor: '#FAFAFA',
                  padding: 0,
                  marginBottom: 40,
                }}>
                {this.state.optionsSpecialties.map((item, index) => {
                  return (
                    <Checkbox
                      title={item.description}
                      checked={item.checked}
                      onPress={async () => {
                        this.toggleSpecialty(index);
                      }}
                    />
                  );
                })}
              </View>
            </View>
          </View>
        </ScrollView>
      </>
    );
  }
}

const Checkbox = ({ title, checked, onPress }) => {
  return (
    <>
      <TouchableWithoutFeedback onPress={onPress}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            borderBottomColor: '#DEDEDE',
            borderBottomWidth: 1,
            padding: 10,
            paddingLeft: 20,
          }}>
          <View
            style={{
              height: 22,
              width: 22,
              borderRadius: 50,
              backgroundColor: checked ? '#37153D' : '#BCBCBC',
              marginRight: 5,
            }}
          />
          <Text
            style={{
              fontFamily: 'SofiaPro-Regular',
              fontSize: 13,
              color: '#707070',
              marginLeft: 5,
            }}>
            {title}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    </>
  );
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(FilterScreen);
