import React, { Component } from 'react';
import { View, Text, ScrollView, Image } from 'react-native';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import cancel from '../../assets/images/cancel.png';
import location from '../../assets/images/location.png';
import team from '../../assets/images/team.png';
import cost from '../../assets/images/cost.png';

class StatisticsScreen extends Component {
  render() {
    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            paddingHorizontal: 30,
            paddingVertical: 20,
            elevation: 3,
          }}>
          <Text
            style={{
              fontFamily: 'SofiaPro-Medium',
              fontSize: 18,
              color: '#37153D',
              textAlign: 'left',
            }}>
            Estatísticas
          </Text>
        </View>

        <View style={{ backgroundColor: '#F8F8F8', height: '100%' }}>
          <ScrollView>
            <View>
              <StatisticItem
                source={cost}
                name={'Preço médio cobrado por bairro'}
                onPress={() =>
                  this.props.navigation.navigate('ReportsScreen', {
                    id_screen: 1,
                  })
                }
              />

              {this.props.user?.user?.id == 1 ? (
                <View>
                  <StatisticItem
                    source={location}
                    name={'Número médio de solicitações por bairro'}
                    onPress={() =>
                      this.props.navigation.navigate('ReportsScreen', {
                        id_screen: 2,
                      })
                    }
                  />
                  <StatisticItem
                    source={cancel}
                    name={'Número de rejeições por bairro'}
                    onPress={() =>
                      this.props.navigation.navigate('ReportsScreen', {
                        id_screen: 3,
                      })
                    }
                  />
                  <StatisticItem
                    source={team}
                    name={'Razão entre profissionais e alunos'}
                    onPress={() =>
                      this.props.navigation.navigate('ReportsScreen', {
                        id_screen: 4,
                      })
                    }
                  />
                </View>
              ) : null}
            </View>
          </ScrollView>
        </View>
      </>
    );
  }
}

const StatisticItem = ({ name, onPress, source }) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View
        style={{
          backgroundColor: 'red',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          backgroundColor: '#fafafa',
          borderBottomWidth: 1,
          borderBottomColor: '#DEDEDE',
          padding: 30,
        }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Image
            style={{ height: 36, width: 36, marginRight: 10 }}
            resizeMode={'contain'}
            source={source}
          />
          <Text
            style={{
              fontFamily: 'SofiaPro-Medium',
              fontSize: 14,
              flexWrap: 'wrap',
              color: '#37153D',
            }}>
            {name}
          </Text>
        </View>

        <Ionicon
          style={{
            fontWeight: '900',
            color: 'rgba(112,112,112,0.5)',
            transform: [{ rotate: '180deg' }],
          }}
          name={'ios-arrow-back'}
          size={26}
          color={'red'}
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(StatisticsScreen);
