import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import { DefaultButton } from '../../components';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { AxiosWithHeaders } from '../../services/api';
import { SafeAreaView } from 'react-native-safe-area-context';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { showMessage } from 'react-native-flash-message';
import Geocoder from 'react-native-geocoder';
import convertStates from '../../services/convertStates';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import { PLACES_API } from '../../services/placesAPI';

class LocationsScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      state: null,
      district: null,
      neighbourhood: null,
    };
  }

  getPosition = async (lat, lng) => {
    var pos = {
      lat: lat,
      lng: lng,
    };

    Geocoder.geocodePosition(pos)
      .then(async res => {
        await this.setState({
          state: res[0].adminArea,
          district: res[0].subAdminArea,
          neighbourhood: res[0].subLocality,
        });

        if (Platform.OS === 'ios') {
          await this.setState({
            state: convertStates(res[0].adminArea),
            district: res[0].locality,
          });
        }
      })
      .catch(err => console.log(err));
  };

  saveLocation = async () => {
    const { user, token } = this.props;
    const { id } = this.props.user;
    const { state, district, neighbourhood } = this.state;

    const AxiosInstace = AxiosWithHeaders({ Authorization: `Bearer ${token}` });

    if (!state || !state || !neighbourhood) {
      this.setState({ loading: false });

      showMessage({
        type: 'danger',
        message: 'Por favor insira o endereço no campo abaixo',
        titleStyle: {
          textAlign: 'center',
        },
      });

      return;
    }

    try {
      const response = await AxiosInstace.post(
        `professionals/${id}/service-locations`,
        {
          professionalServiceLocations: {
            state: state ?? null,
            district: district ?? null,
            neighborhood: neighbourhood ?? null,
          },
        },
      );

      this.props.setUser(user);
      await this.setState({
        state: null,
        district: null,
        neighbourhood: null,
      });
      this.GooglePlacesRef.setAddressText('');
      this.props.navigation.navigate('GymsScreen', {
        fullAddress: response.data,
      });
    } catch (err) {
      console.log(err.response);
    }
  };

  render() {
    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: 20,
            elevation: 3,
          }}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <TouchableWithoutFeedback
              onPress={() => {
                this.props.navigation.goBack();
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Ionicon
                  style={{
                    fontWeight: '900',
                    marginRight: 10,
                  }}
                  name={'ios-arrow-round-back'}
                  size={30}
                  color={'#37153D'}
                />
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 14,
                    color: '#37153D',
                    textAlign: 'left',
                  }}>
                  {' '}
                  Locais de atendimento
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          <DefaultButton
            title={'Continuar'}
            width={100}
            height={35}
            margin={'0px'}
            onPress={() => {
              this.saveLocation();
            }}
          />
        </View>
        <ScrollView keyboardShouldPersistTaps="always">
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
              padding: 10,
            }}>
            <View
              style={{
                flexDirection: 'column',
                marginLeft: 10,
                marginRight: 10,
              }}>
              <Text
                style={{
                  color: '#2E2E2E',
                  fontFamily: 'SofiaPro-Medium',
                  marginTop: 10,
                  marginLeft: 10,
                  fontSize: 13,
                  textAlign: 'left',
                }}>
                Endereço:
              </Text>
              <GooglePlacesAutocomplete
                ref={instance => {
                  this.GooglePlacesRef = instance;
                }}
                placeholder="Pesquisar academias pelo endereço"
                fetchDetails={true}
                enablePoweredByContainer={false}
                minLength={6}
                onPress={async (data, details = null) => {
                  var lat = details.geometry.location.lat;
                  var lng = details.geometry.location.lng;

                  await this.getPosition(lat, lng);
                }}
                query={{
                  key: PLACES_API,
                  language: 'pt-BR',
                  components: 'country:br',
                }}
                styles={{
                  textInputContainer: {
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderTopWidth: 0,
                    borderBottomWidth: 0,
                    maxWidth: 300,
                    minWidth: 300,
                  },
                  textInput: {
                    marginLeft: 0,
                    marginRight: 0,
                    color: '#636363',
                    fontFamily: 'SofiaProLight',
                    fontSize: 13,
                    backgroundColor: '#fff',
                    height: 40,
                    borderRadius: 50,
                    borderWidth: 0.1,
                    paddingLeft: 30,
                    paddingRight: 30,
                  },
                  listView: {
                    maxWidth: 300,
                    marginTop: 10,
                  },
                }}
                placeholderTextColor="#CBCBCD"
              />
            </View>
          </View>
        </ScrollView>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(LocationsScreen);
