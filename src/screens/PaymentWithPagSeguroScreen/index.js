import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  ActivityIndicator,
} from 'react-native';
import { DefaultButton, LoadingModal } from '../../components';
import { SafeAreaView } from 'react-native-safe-area-context';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { WebView } from 'react-native-webview';
import { BASE_URL } from '../../services/baseUrl';
import { AxiosWithHeaders } from '../../services/api';
import { TextInputMask } from 'react-native-masked-text';
import AwesomeAlert from 'react-native-awesome-alerts';
import LottieView from 'lottie-react-native';
import animation from '../../assets/success.json';
import animationError from '../../assets/warn.json';
import CardFlip from 'react-native-card-flip';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import axios from 'axios';
import {
  PAGSEGURO_EMAIL,
  PAGSEGURO_TOKEN,
} from '../../services/pagseguroToken';

class PaymentWithPagSeguroScreen extends Component {
  constructor(props) {
    super(props);

    this.webView = null;

    this.state = {
      appointment: '',
      contract_id: '',
      user: '',
      card_number: '',
      validity: '',
      cvv: '',
      owner_name: '',
      brand: '',
      expirationMonth: '',
      expirationYear: '',
      sessionId: '',
      hash: '',
      cardToken: '',
      showCardBackView: false,
      loading: false,
    };
  }

  componentDidMount() {
    this.createSession();

    this.setState({
      appointment: this.props.route?.params?.appointment,
      contract_id: this.props.route?.params?.appointment?.contract?.id,
      user: this.props.user.user,
    });
  }

  createSession = async () => {
    try {
      let response = await axios.post(
        `https://ws.pagseguro.uol.com.br/v2/sessions?email=${PAGSEGURO_EMAIL}&token=${PAGSEGURO_TOKEN}`,
      );

      if (response.data) {
        this.setState({
          sessionId: response.data
            .toString()
            .replace(
              '<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?><session><id>',
              '',
            )
            .replace('</id></session>', ''),
        });
      }
    } catch (err) {
      console.log(err);
      this.showPaymentError();
    }
  };

  createCardHash = async () => {
    const {
      card_number,
      brand,
      cvv,
      validity,
      sessionId,
      owner_name,
    } = this.state;

    if (
      card_number == '' ||
      brand == '' ||
      cvv == '' ||
      validity == '' ||
      owner_name == ''
    ) {
      this.showCreditCardError();
      return;
    }

    const cardNumber = card_number.replace(/[^0-9]/g, '');
    const expirationMonth = validity.replace(/[^0-9]/g, '').substring(0, 2);
    const expirationYear = validity.replace(/[^0-9]/g, '').substring(2, 6);

    const getCardToken = `
    PagSeguroDirectPayment.setSessionId('${sessionId}');

    PagSeguroDirectPayment.createCardToken({
        cardNumber: '${cardNumber}',
        brand: '${brand}',
        cvv: '${cvv}',
        expirationMonth: '${expirationMonth}',
        expirationYear: '${expirationYear}',
        success: function (response) {
           window.ReactNativeWebView.postMessage(
            JSON.stringify({
              data: response.card.token,
              message: 'cardToken',
            })
          );
        },
        error: function(response) {
          window.ReactNativeWebView.postMessage(
            JSON.stringify({
              message: response,
            })
          );
        },
      });
    `;

    this.webView.injectJavaScript(getCardToken);
  };

  chargePayment = async () => {
    const { token } = this.props;

    const {
      cardToken,
      hash,
      user,
      appointment,
      contract_id,
      owner_name,
    } = this.state;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    console.log('token ' + token);
    console.log('cardToken ' + cardToken);
    console.log('hash ' + hash);
    console.log('reference_id ' + contract_id);

    if (
      user.name == '' ||
      user.email == '' ||
      user.cpf == '' ||
      user.contact == '' ||
      user.birthday == ''
    ) {
      this.showProfileError();
      return;
    }

    const phone = user.contact.replace(/[^0-9]/g, '');

    this.setState({ loading: true });

    try {
      const response = await AxiosInstance.post(`pagseguro/charge`, {
        sender: {
          name: owner_name,
          email: user.email,
          cpf_cnpj: user.cpf.replace(/[^0-9]/g, ''),
          area_code: phone.length > 9 ? phone.substring(0, 2) : phone,
          phone: phone.length > 9 ? phone.slice(2) : phone,
          birth_date: user.birthday,
        },
        shipping: {
          street: user.address_street
            ? `${user.address_street}`
            : 'Rua não informada',
          number: user.address_number ? `${user.address_number}` : '1',
          district: user.address_neighborhood
            ? `${user.address_neighborhood}`
            : 'Bairro não informado',
          city: user.address_city
            ? `${user.address_city}`
            : 'Cidade Não informada',
          state: user.address_uf ? `${user.address_uf}` : 'RJ',
          postal_code: user.cep ? user.cep.replace(/[^0-9]/g, '') : '23040300',
          same_for_billing: true,
        },
        item: {
          qtde: '1',
          value: appointment.contract?.professionalPlan?.price,
          description:
            appointment.contract?.professionalPlan?.plan?.description,
        },
        method: 'creditCard',
        credit_card_token: cardToken,
        value: appointment.contract?.professionalPlan?.price,
        hash: hash,
        reference: `${parseInt(contract_id)}`,
      });

      if (response.status == 200) {
        console.log(response.data);

        await AxiosInstance.put(`contracts/${contract_id}`, {
          method_payment: 'CARTAO',
        });

        this.setState({ loading: false });
        this.showAlert();
      }
    } catch (err) {
      console.log(err);
      this.setState({ loading: false });
      this.showPaymentError();
    }
  };

  updateBrand() {
    const { card_number } = this.state;

    if (card_number.startsWith('4')) {
      this.setState({
        brand: 'visa',
      });
    } else if (card_number.startsWith('5')) {
      this.setState({
        brand: 'mastercard',
      });
    }
  }

  showPaymentError = () => {
    this.setState({
      showPaymentError: true,
    });

    setTimeout(
      () =>
        this.setState({
          showPaymentError: false,
        }),
      3000,
    ); // hide alert after 4s
  };

  showAlert = () => {
    this.setState({
      showAlert: true,
    });

    setTimeout(
      () =>
        this.setState({
          showAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  showCreditCardError = () => {
    this.setState({
      showCreditCardError: true,
    });

    setTimeout(
      () =>
        this.setState({
          showCreditCardError: false,
        }),
      5000,
    ); // hide alert after 4s
  };

  showProfileError = () => {
    this.setState({
      showProfileError: true,
    });

    setTimeout(
      () =>
        this.setState({
          showProfileError: false,
        }),
      5000,
    ); // hide alert after 4s
  };

  render() {
    const {
      sessionId,
      showAlert,
      showProfileError,
      showPaymentError,
      showCreditCardError,
    } = this.state;

    const getSenderHash = `
      PagSeguroDirectPayment.setSessionId('${sessionId}');
      PagSeguroDirectPayment.onSenderHashReady(function(response){
          if(response.status == 'error') {
            window.ReactNativeWebView.postMessage(JSON.stringify({
              message: 'response.message'
            }));
            return false;
          }
          var hash = response.senderHash;
          window.ReactNativeWebView.postMessage(JSON.stringify({
            data: hash,
            message: 'senderHash'
          }));
        });
        `;

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            paddingHorizontal: 30,
            paddingVertical: 20,
            elevation: 3,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Ionicon
                style={{
                  fontWeight: '900',
                  marginRight: 10,
                }}
                name={'ios-arrow-round-back'}
                size={30}
                color={'#37153D'}
              />
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#37153D',
                  textAlign: 'left',
                }}>
                {' '}
                Cartão de crédito
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>

        <View>
          <WebView
            source={{ uri: `https://personal-access.herokuapp.com/api/pagseguro` }}
            ref={webView => (this.webView = webView)}
            javaScriptEnabled={true}
            injectedJavaScript={getSenderHash}
            onMessage={async event => {
              let response = JSON.parse(event.nativeEvent.data);

              console.log(response);

              if (response.message == 'senderHash') {
                await this.setState({
                  hash: response.data,
                });
              } else if (response.message == 'cardToken') {
                await this.setState({
                  cardToken: response.data,
                });

                this.chargePayment();
              }
            }}
          />
        </View>

        {this.state.loading == true ? (
          <LoadingModal show={this.state.loading} />
        ) : (
          <KeyboardAvoidingView
            behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
            style={{ flex: 1 }}>
            <ScrollView>
              <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                <View style={styles.container}>
                  <CardFlip
                    style={styles.cardContainer}
                    ref={card => (this.card = card)}>
                    <TouchableOpacity
                      activeOpacity={1}
                      style={[styles.card, styles.card1]}
                      onPress={null}>
                      {this.state.card_number.startsWith('5') ? (
                        <Image
                          resizeMode="contain"
                          source={require('../../assets/images/mastercard.png')}
                          style={{
                            width: 50,
                            marginBottom: 10,
                          }}
                        />
                      ) : null}

                      {this.state.card_number.startsWith('4') ? (
                        <Image
                          resizeMode="contain"
                          source={require('../../assets/images/visa.png')}
                          style={{
                            width: 50,
                            marginBottom: 10,
                          }}
                        />
                      ) : null}

                      <View>
                        <Text style={{ color: '#fff' }}>
                          {this.state.card_number}
                        </Text>
                      </View>

                      <View
                        style={{
                          width: 330,
                          marginTop: 10,
                          flexDirection: 'row',
                          justifyContent: 'flex-start',
                        }}>
                        <Text style={{ color: '#fff', width: 200 }}>
                          {this.state.owner_name.toUpperCase()}
                        </Text>

                        <Text style={{ color: '#fff', width: 70 }}>
                          {this.state.validity}
                        </Text>
                      </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                      activeOpacity={1}
                      style={[styles.card, styles.card2]}
                      onPress={null}>
                      <View
                        style={{
                          width: 315,
                          height: 30,
                          backgroundColor: '#000',
                        }}
                      />

                      <View
                        style={{
                          backgroundColor: '#fff',
                          padding: 10,
                          margin: 10,
                          height: 40,
                          width: 80,
                        }}>
                        <Text
                          style={{
                            color: '#000',
                            textAlign: 'center',
                          }}>
                          {this.state.cvv ? this.state.cvv : 'xxx'}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </CardFlip>
                </View>

                <MaskedInput
                  question={'Número do cartão: '}
                  type={'credit-card'}
                  options={{
                    obfuscated: false,
                  }}
                  keyboardType={'decimal-pad'}
                  answer={this.state.card_number}
                  width={330}
                  placeholder={'4546 5456 4564 6546'}
                  onChangeText={text => {
                    this.setState({ card_number: text });

                    this.updateBrand();
                  }}
                  onPress={() => {
                    if (this.state.showCardBackView == true) {
                      this.setState({
                        showCardBackView: false,
                      });
                      this.card.flip();
                    }
                  }}
                />

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    width: 330,
                  }}>
                  <MaskedInput
                    type={'custom'}
                    question={'Validade: '}
                    options={{
                      mask: '99/9999',
                    }}
                    keyboardType={'decimal-pad'}
                    answer={this.state.validity}
                    onChangeText={text => this.setState({ validity: text })}
                    width={150}
                    placeholder={'12/2026'}
                    onPress={() => {
                      if (this.state.showCardBackView == true) {
                        this.setState({
                          showCardBackView: false,
                        });
                        this.card.flip();
                      }
                    }}
                  />

                  <MaskedInput
                    type={'custom'}
                    question={'CVV: '}
                    options={{
                      mask: '999',
                    }}
                    keyboardType={'decimal-pad'}
                    answer={this.state.cvv}
                    onChangeText={text => this.setState({ cvv: text })}
                    width={150}
                    placeholder={'123'}
                    onPress={() => {
                      if (this.state.showCardBackView == false) {
                        this.setState({
                          showCardBackView: true,
                        });
                        this.card.flip();
                      }
                    }}
                  />
                </View>

                <CustomInput
                  question={'Nome do titular:'}
                  answer={this.state.owner_name}
                  width={330}
                  onChangeText={text => this.setState({ owner_name: text })}
                  placeholder={'Nome como aparece em seu cartão'}
                  onPress={() => {
                    if (this.state.showCardBackView == true) {
                      this.setState({
                        showCardBackView: false,
                      });
                      this.card.flip();
                    }
                  }}
                />

                <View style={{ margin: 20 }}>
                  {this.state.hash != '' ? (
                    <DefaultButton
                      title={'Confirmar pagamento'}
                      width={200}
                      height={35}
                      onPress={() => this.createCardHash()}
                    />
                  ) : (
                    <ActivityIndicator
                      color={'#37153D'}
                      style={{ margin: 20 }}
                    />
                  )}
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        )}

        <AwesomeAlert
          show={showCreditCardError}
          showProgress={false}
          closeOnHardwareBackPress={false}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Oops! Favor preencher todos os campos.
              </Text>
              <LottieView
                style={{ height: 70 }}
                resizeMode="contain"
                autoSize
                source={animationError}
                autoPlay
                loop={false}
              />
            </View>
          }
        />

        <AwesomeAlert
          show={showProfileError}
          showProgress={false}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          showConfirmButton={true}
          confirmText="Editar perfil"
          onConfirmPressed={() => this.props.navigation.navigate('MyProfile')}
          confirmButtonStyle={{
            width: 200,
            height: 40,
            backgroundColor: '#37153D',
            borderRadius: 25,
            marginBottom: 20,
          }}
          confirmButtonTextStyle={{
            textAlign: 'center',
            fontFamily: 'SofiaPro-Medium',
            letterSpacing: 0,
            color: '#fff',
            fontSize: 15,
          }}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Oops! Seus dados de perfil estão incompletos. Por favor, volte e
                preencha todas as suas informações.
              </Text>
              <LottieView
                style={{ height: 70 }}
                resizeMode="contain"
                autoSize
                source={animationError}
                autoPlay
                loop={false}
              />
            </View>
          }
        />

        <AwesomeAlert
          show={showPaymentError}
          showProgress={false}
          closeOnHardwareBackPress={false}
          onDismiss={() => this.props.navigation.goBack()}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Oops! Algo de errado aconteceu, tente novamente.
              </Text>
              <LottieView
                style={{ height: 70 }}
                resizeMode="contain"
                autoSize
                source={animationError}
                autoPlay
                loop={false}
              />
            </View>
          }
        />

        <AwesomeAlert
          show={showAlert}
          showProgress={false}
          closeOnHardwareBackPress={false}
          onDismiss={async () => {
            this.props.navigation.navigate('MyClasses');
          }}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Pagamento realizado com sucesso!
              </Text>
              <LottieView
                style={{ width: 70, height: 70 }}
                resizeMode="contain"
                autoSize
                source={animation}
                autoPlay
                loop={false}
              />
            </View>
          }
        />
      </>
    );
  }
}

const CustomInput = ({
  question,
  answer,
  width,
  onPress,
  keyboardType,
  maxLength,
  onChangeText,
  placeholder,
}) => {
  return (
    <View style={{ flexDirection: 'column', marginLeft: 0, marginRight: 0 }}>
      <Text
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaPro-Medium',
          margin: 10,
          fontSize: 13,
        }}>
        {question}
      </Text>
      <TextInput
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaProLight',
          fontSize: 13,
          backgroundColor: '#fff',
          height: 40,
          borderRadius: 50,
          borderWidth: 0.1,
          paddingLeft: 30,
          paddingRight: 30,
          width: width ? width : 100,
        }}
        value={answer}
        maxLength={maxLength}
        keyboardType={keyboardType}
        onChangeText={onChangeText}
        placeholder={placeholder}
        placeholderTextColor={'#999999'}
        onFocus={onPress}
      />
    </View>
  );
};

const MaskedInput = ({
  question,
  answer,
  width,
  keyboardType,
  maxLength,
  onChangeText,
  placeholder,
  type,
  options,
  onPress,
  includeRawText,
}) => {
  return (
    <View style={{ flexDirection: 'column', marginLeft: 0, marginRight: 0 }}>
      <Text
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaPro-Medium',
          margin: 10,
          fontSize: 13,
        }}>
        {question}
      </Text>
      <TextInputMask
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaProLight',
          fontSize: 13,
          backgroundColor: '#fff',
          height: 40,
          borderRadius: 50,
          borderWidth: 0.1,
          paddingLeft: 30,
          paddingRight: 30,
          width: width ? width : 100,
        }}
        maxLength={maxLength}
        type={type}
        value={answer}
        keyboardType={keyboardType}
        onChangeText={onChangeText}
        options={options}
        placeholder={placeholder}
        onFocus={onPress}
        includeRawValueInChangeText={includeRawText}
        placeholderTextColor={'#999999'}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F9F9F9',
    marginVertical: 30,
  },
  cardContainer: {
    width: 318,
    height: 186,
    backgroundColor: '#F9F9F9',
  },
  card: {
    width: 318,
    height: 186,
    backgroundColor: '#FE474C',
    borderRadius: 5,
    shadowColor: 'rgba(0,0,0,0.5)',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.5,
  },
  card1: {
    backgroundColor: '#37153D',
    justifyContent: 'center',
    alignItems: 'flex-start',
    padding: 30,
  },
  card2: {
    backgroundColor: '#37153D',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  label: {
    textAlign: 'center',
    fontSize: 55,
    fontFamily: 'System',
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
});

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(PaymentWithPagSeguroScreen);
