import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableWithoutFeedback,
  YellowBox,
  FlatList,
  Linking,
} from 'react-native';

import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { DefaultButton } from '../../components';
import { SafeAreaView } from 'react-native-safe-area-context';
import { AxiosWithHeaders } from '../../services/api';
import RNPickerSelect from 'react-native-picker-select';
import avaliationSrc from '../../assets/images/avaliation.png';
import AwesomeAlert from 'react-native-awesome-alerts';
import LottieView from 'lottie-react-native';
import animation from '../../assets/success.json';
import avulsa from '../../assets/images/avulsa.png';
import pacote from '../../assets/images/pacote.png';
import moment from 'moment';
import _ from 'lodash';

YellowBox.ignoreWarnings([
  'VirtualizedLists should never be nested', // TODO: Remove when fixed
]);

LocaleConfig.locales['pt-BR'] = {
  monthNames: [
    'Janeiro',
    'Fevereiro',
    'Março',
    'Abril',
    'Maio',
    'Junho',
    'Julho',
    'Agosto',
    'Setembro',
    'Outubro',
    'Novembro',
    'Dezembro',
  ],
  monthNamesShort: [
    'Jan.',
    'Fev.',
    'Mar.',
    'Abr.',
    'Mai.',
    'Jun.',
    'Jul.',
    'Ago.',
    'Set.',
    'Out.',
    'Nov.',
    'Dez.',
  ],
  dayNames: [
    'Domingo',
    'Segunda',
    'Terça',
    'Quarta',
    'Quinta',
    'Sexta',
    'Sábado',
  ],
  dayNamesShort: ['Dom.', 'Seg.', 'Ter.', 'Qua.', 'Qui.', 'Sex.', 'Sáb.'],
};
LocaleConfig.defaultLocale = 'pt-BR';

class ProfessionalProfileScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      professional: null,
      professional_id: null,
      professionalPlans: [],
      individual: true,
      plan: '',
      currentPlan: '',
      modal: false,
      modalPlan: false,
      showConfirmation: false,
      date: '',
      week_day: '',
      hour: '',
      gym: '',
      selected: '',
      optionsHour: [],
      optionsGyms: [],
    };
  }

  async componentDidMount() {
    await this.loadProfessional(this.props.route.params.professional.id);
  }

  loadProfessional = async id => {
    const { token } = this.props;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(`/professionals/${id}`)
      .then(response => {
        response.data;
        this.setState({
          professional: response.data,
          professional_id: id,
          professionalPlans: response.data.professionalPlans.sort((a, b) =>
            a.plan.amount > b.plan.amount ? 1 : -1,
          ),
          plan: response.data.professionalPlans[0].id,
          optionsGyms: response.data.gyms,
        });
        console.log(this.state.optionsGyms);
      })
      .catch(err => {
        console.log(err);
      });
  };

  getExistingPlan = async id => {
    const { token, user } = this.props;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    try {
      await AxiosInstance.get(`/contracts/${user.id}/isvalid/${id}`)
        .then(response => {
          response.data;
          this.setState({
            currentPlan: response.data,
          });
          console.log(response.data);
        })
        .catch(err => {
          console.log(err);
        });
      console.log(this.state.currentPlan);
    } catch (err) {
      console.log(err);
    }
  };

  getAvailableDates = (startDate, endDate, daysToEnable) => {
    const enabledDates = {};
    const start = moment(startDate);
    const end = moment(endDate);
    for (let m = moment(start); m.diff(end, 'days') <= 0; m.add(1, 'days')) {
      if (_.includes(daysToEnable, m.weekday())) {
        enabledDates[m.format('YYYY-MM-DD')] = { disabled: false };
      }
    }
    return enabledDates;
  };

  getScheduleHours = async date => {
    const { token } = this.props;
    const { professional_id } = this.state;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(
      `/professionals/${professional_id}/schedules/${date}`,
    )
      .then(response => {
        this.setState({
          optionsHour: response.data,
        });
        console.log(response.data);
      })
      .catch(err => {
        console.log(err);
      });
  };

  getDaysAvailable = weekday => {
    switch (weekday) {
      case 'DOMINGO':
        return 0;
      case 'SEGUNDA':
        return 1;
      case 'TERCA':
        return 2;
      case 'QUARTA':
        return 3;
      case 'QUINTA':
        return 4;
      case 'SEXTA':
        return 5;
      case 'SABADO':
        return 6;
      default:
        return 'Dia inválido';
    }
  };

  saveSolicitation = () => {
    const { user, token } = this.props;

    let body = {
      professional_id: this.state.professional_id,
      establishment_id: this.state.gym,
      date: `${this.state.date} ${this.state.hour}`,
      student_id: user.id,
      plan_id: this.state.plan,
    };

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    try {
      AxiosInstance.post('/scheduled-services', body)
        .then(response => {
          console.log(response);
          console.log(body);
        })
        .catch(err => {
          console.log(err.response.data);
        });

      this.showConfirmation();
      this.setState({
        week_day: '',
        hour: '',
        gym: '',
        selected: '',
        plan: '',
        date: '',
        optionsHour: '',
        currentPlan: '',
      });
      this.loadProfessional(this.props.route.params.professional.id);
    } catch (err) {
      console.log(err);
    }
  };

  convertPrice = price => {
    return price.toLocaleString('pt-br', {
      style: 'currency',
      currency: 'BRL',
    });
  };

  showConfirmation = () => {
    this.setState({
      showConfirmation: true,
    });

    setTimeout(
      () =>
        this.setState({
          showConfirmation: false,
        }),
      6000,
    ); // hide alert after 4s
  };

  render() {
    const professional = this.props.route.params.professional;

    const {
      showConfirmation,
      modalPlan,
      plan,
      currentPlan,
      hour,
      gym,
      professionalPlans,
      selected,
      optionsHour,
      optionsGyms,
    } = this.state;

    const style = {
      inputAndroid: {
        width: 290,
        height: 50,
        color: '#fff',
        paddingHorizontal: 10,
        paddingVertical: 10,
        marginVertical: 10,
        borderRadius: 25,
        fontFamily: 'SofiaPro-Medium',
        fontSize: 16,
        backgroundColor: this.state.gym != '' ? '#37153D' : '#999',
      },
      inputIOS: {
        width: 290,
        height: 50,
        color: '#fff',
        paddingHorizontal: 10,
        paddingVertical: 10,
        marginVertical: 10,
        borderRadius: 25,
        alignSelf: 'center',
        textAlign: 'center',
        fontFamily: 'SofiaPro-Medium',
        backgroundColor: this.state.gym != '' ? '#37153D' : '#999',
        fontSize: 16,
      },
    };

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#37153D' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#37153D',
            shadowColor: '#000',
            alignItems: 'center',
            padding: 15,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Ionicon
                style={{
                  fontWeight: '900',
                  marginRight: 10,
                }}
                name={'ios-arrow-round-back'}
                size={30}
                color={'#F8F8F8'}
              />
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#F8F8F8',
                  textAlign: 'left',
                }}>
                Profissional
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>

        <ScrollView
          contentContainerStyle={{
            flexGrow: 1,
            justifyContent: 'flex-end',
            backgroundColor: '#37153D',
          }}>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
              backgroundColor: '#F8F8F8',
              marginTop: 0,
              padding: 20,
              paddingBottom: 60,
              borderTopLeftRadius: 40,
              borderTopRightRadius: 40,
            }}>
            <Image
              style={{ height: 200, width: 200, borderRadius: 135 }}
              source={{
                uri: professional.user.img_profile
                  ? professional.user.img_profile + '?' + new Date().getTime()
                  : 'https://static.scrum.org/web/images/profile-placeholder.png',
              }}
            />

            <Text
              style={{
                color: '#37153D',
                fontSize: 22,
                fontFamily: 'SofiaPro-Medium',
                marginTop: 20,
              }}>
              {professional.user.name}
            </Text>

            <View style={{ flexDirection: 'row' }}>
              <Text
                style={{
                  marginLeft: 6,
                  fontFamily: 'SofiaPro-Regular',
                  fontSize: 12,
                  color: '#F76E1E',
                }}>{`CREF/CONFEF:`}</Text>
              <Text
                style={{
                  marginLeft: 6,
                  fontFamily: 'SofiaProLight',
                  fontSize: 12,
                  color: '#2E2E2E',
                }}>
                {professional.doc_professional
                  ? professional.doc_professional.toUpperCase()
                  : 'Não informado'}
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                justifyContent: 'space-between',
                alignItems: 'center',
                backgroundColor: '#F2F2F2',
                marginTop: 5,
                marginBottom: 10,
                borderRadius: 5,
                padding: 6,
              }}>
              <Image
                style={{ height: 20, width: 20, marginRight: 5 }}
                resizeMode="contain"
                source={avaliationSrc}
              />
              <Text
                style={{
                  marginLeft: 6,
                  fontFamily: 'SofiaProLight',
                  fontSize: 12,
                  color: '#F76E1E',
                }}>{`Não avaliado`}</Text>
            </View>

            {this.props.user?.user?.role != 'professional' ? (
              <DefaultButton
                title={'Agendar'}
                onPress={async () => {
                  await this.getExistingPlan(
                    this.props.route.params.professional.id,
                  );

                  {
                    this.state.currentPlan == ''
                      ? await this.setState({
                          modalPlan: !this.state.modalPlan,
                        })
                      : await this.setState({ modal: !this.state.modal });
                  }
                }}
                width={190}
              />
            ) : null}

            <View
              style={{
                backgroundColor: '#FFF',
                width: '95%',
                borderRadius: 30,
                padding: 20,
                marginTop: 20,
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: 'SofiaPro-Regular',
                  fontSize: 14,
                  color: '#F76E1E',
                }}>{`Especialidade:`}</Text>
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: 'SofiaProLight',
                  fontSize: 12,
                  color: '#707070',
                  marginBottom: 20,
                  textTransform: 'capitalize',
                }}>
                {professional.specialties.map(e => e.description).join(', ')}
              </Text>
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: 'SofiaPro-Regular',
                  fontSize: 14,
                  color: '#F76E1E',
                }}>{`Locais de atendimento:`}</Text>
              <Text
                numberOfLines={2}
                style={{
                  textAlign: 'center',
                  fontFamily: 'SofiaProLight',
                  fontSize: 12,
                  color: '#707070',
                  marginBottom: 20,
                }}>
                {optionsGyms?.map(e => e.name).join(', ')}
              </Text>
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: 'SofiaPro-Regular',
                  fontSize: 14,
                  color: '#F76E1E',
                }}>{`Biografia:`}</Text>
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: 'SofiaProLight',
                  fontSize: 12,
                  color: '#707070',
                  marginBottom: 20,
                }}>
                {professional.bibliography
                  ? professional.bibliography
                  : 'Não informado'}
              </Text>
              <Text
                style={{
                  textAlign: 'center',
                  fontFamily: 'SofiaPro-Regular',
                  fontSize: 14,
                  color: '#F76E1E',
                }}>{`Instagram:`}</Text>
              {professional.instagram != null ? (
                <TouchableWithoutFeedback
                  onPress={() => {
                    Linking.canOpenURL(
                      `instagram://user?username=${professional?.instagram}`,
                    ).then(supported => {
                      if (supported) {
                        return Linking.openURL(
                          `instagram://user?username=${
                            professional?.instagram
                          }`,
                        );
                      } else {
                        return Linking.openURL(
                          `https://instagram.com/${professional?.instagram}`,
                        );
                      }
                    });
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignSelf: 'center',
                      alignItems: 'center',
                      backgroundColor: 'rgba(247, 110, 30, 0.3)',
                      paddingHorizontal: 15,
                      paddingVertical: 2,
                      borderRadius: 25,
                      marginTop: 5,
                    }}>
                    <Ionicon
                      style={{
                        fontWeight: '900',
                        marginRight: 10,
                      }}
                      name={'logo-instagram'}
                      size={30}
                      color={'#000'}
                    />
                    <Text
                      style={{
                        textAlign: 'center',
                        fontFamily: 'SofiaProLight',
                        fontSize: 12,
                        color: '#707070',
                      }}>
                      {professional.instagram ? professional.instagram : ''}
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              ) : (
                <Text
                  style={{
                    textAlign: 'center',
                    fontFamily: 'SofiaProLight',
                    fontSize: 12,
                    color: '#707070',
                  }}>
                  {professional.instagram
                    ? professional.instagram
                    : 'Não informado'}
                </Text>
              )}
            </View>
          </View>
        </ScrollView>

        {/* Escolha de planos */}

        <AwesomeAlert
          show={modalPlan}
          showProgress={false}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          showCancelButton
          showConfirmButton={this.state.plan != '' ? true : false}
          onConfirmPressed={() => {
            this.setState({
              modal: !this.state.modal,
              modalPlan: !this.state.modalPlan,
            });
          }}
          onCancelPressed={() => {
            this.setState({
              modalPlan: !this.state.modalPlan,
              plan: '',
            });
          }}
          confirmText={'Confirmar'}
          cancelText={'Cancelar'}
          actionContainerStyle={{
            flexDirection: 'column-reverse',
            alignItems: 'center',
          }}
          confirmButtonStyle={{
            width: 200,
            height: 40,
            backgroundColor: '#F76E1E',
            borderRadius: 25,
          }}
          cancelButtonStyle={{
            width: 200,
            height: 40,
            backgroundColor: '#707070',
            borderRadius: 25,
          }}
          confirmButtonTextStyle={{
            textAlign: 'center',
            fontFamily: 'SofiaPro-Medium',
            letterSpacing: 0,
            color: '#fff',
            fontSize: 15,
          }}
          cancelButtonTextStyle={{
            textAlign: 'center',
            fontFamily: 'SofiaPro-Medium',
            letterSpacing: 0,
            color: '#fff',
            fontSize: 15,
          }}
          title={'Selecione seu plano de aulas'}
          titleStyle={{
            fontFamily: 'SofiaPro-Medium',
            fontSize: 20,
            textAlign: 'center',
            flexWrap: 'wrap',
            color: '#37153D',
          }}
          contentStyle={{
            maxWidth: '95%',
            maxHeight: '75%',
          }}
          contentContainerStyle={{
            maxWidth: '95%',
            borderRadius: 30,
            justifyContent: 'center',
          }}
          customView={
            <ScrollView showsVerticalScrollIndicator={false}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 22,
                    textAlign: 'center',
                    flexWrap: 'wrap',
                    color: '#37153D',
                  }}>
                  {/* Selecione seu plano de aulas */}
                </Text>
              </View>

              {/* <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <DefaultButton
                  title={'Individual'}
                  width={'120px'}
                  height={'50px'}
                  backgroundColor={
                    this.state.individual === true ? '#37153D' : '#999'
                  }
                  onPress={async () => {
                    await this.setState({
                      individual: !this.state.individual,
                      plan: '',
                    });
                  }}
                />
                <DefaultButton
                  title={'Em dupla'}
                  width={'120px'}
                  height={'50px'}
                  backgroundColor={
                    this.state.individual !== true ? '#37153D' : '#999'
                  }
                  onPress={async () => {
                    await this.setState({
                      individual: !this.state.individual,
                      plan: '',
                    });
                  }}
                />
              </View> */}

              <View>
                {this.state.individual === true ? (
                  <View
                    style={{
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <FlatList
                      showsVerticalScrollIndicator={false}
                      data={professionalPlans}
                      keyExtractor={plan => String(plan.id)}
                      renderItem={({ item }) => (
                        <ClassItem
                          source={item.plan.amount > 1 ? pacote : avulsa}
                          plan={item.plan.description}
                          price={
                            item.price ? this.convertPrice(item.price) : null
                          }
                          amount={
                            item.plan.amount > 1
                              ? `${item.plan.amount} aulas.`
                              : `${item.plan.amount} aula.`
                          }
                          backgroundColor={plan == item.id ? '#37153D' : '#999'}
                          opacity={plan == item.id ? 1 : 0.5}
                          onPress={async () => {
                            await this.setState({
                              plan: item.id,
                            });
                          }}
                        />
                      )}
                    />
                  </View>
                ) : (
                  <View
                    style={{
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginTop: 10,
                    }}>
                    <FlatList
                      showsVerticalScrollIndicator={false}
                      data={professionalPlans?.filter(e => e.plan.duo === true)}
                      keyExtractor={plan => String(plan.id)}
                      renderItem={({ item }) => (
                        <ClassItem
                          plan={item.plan.description}
                          price={
                            item.price ? this.convertPrice(item.price) : null
                          }
                          amount={
                            item.plan.amount > 1
                              ? `${item.plan.amount} aulas.`
                              : `${item.plan.amount} aula.`
                          }
                          backgroundColor={plan == item.id ? '#37153D' : '#999'}
                          opacity={plan == item.id ? 1 : 0.5}
                          onPress={async () => {
                            await this.setState({
                              plan: item.id,
                            });
                          }}
                        />
                      )}
                    />
                  </View>
                )}
              </View>
            </ScrollView>
          }
        />

        {/* Calendário */}

        <AwesomeAlert
          show={this.state.modal}
          showProgress={false}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          showCancelButton
          showConfirmButton={
            this.state.hour != '' &&
            this.state.gym != '' &&
            this.state.gym != null
              ? true
              : false
          }
          onConfirmPressed={() => {
            this.saveSolicitation();
            this.setState({
              modal: !this.state.modal,
            });
          }}
          onCancelPressed={() => {
            this.setState({
              modal: !this.state.modal,
              plan: '',
              date: '',
              selected: '',
              hour: '',
              optionsHour: '',
              gym: '',
            });
          }}
          confirmText={'Confirmar'}
          cancelText={'Cancelar'}
          actionContainerStyle={{
            flexDirection: 'column-reverse',
            alignItems: 'center',
          }}
          confirmButtonStyle={{
            width: 200,
            height: 40,
            backgroundColor: '#F76E1E',
            borderRadius: 25,
          }}
          cancelButtonStyle={{
            width: 200,
            height: 40,
            backgroundColor: '#707070',
            borderRadius: 25,
          }}
          confirmButtonTextStyle={{
            textAlign: 'center',
            fontFamily: 'SofiaPro-Medium',
            letterSpacing: 0,
            color: '#fff',
            fontSize: 15,
          }}
          cancelButtonTextStyle={{
            textAlign: 'center',
            fontFamily: 'SofiaPro-Medium',
            letterSpacing: 0,
            color: '#fff',
            fontSize: 15,
          }}
          contentStyle={{
            maxWidth: '95%',
            maxHeight: '80%',
          }}
          contentContainerStyle={{
            maxWidth: '95%',
            borderRadius: 30,
            justifyContent: 'center',
          }}
          customView={
            <ScrollView
              showsVerticalScrollIndicator={false}
              ref={ref => {
                this.ListView_Ref = ref;
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 15,
                  marginBottom: 15,
                }}>
                {currentPlan == '' ? (
                  <TouchableWithoutFeedback
                    onPress={async () => {
                      await this.setState({
                        modal: !this.state.modal,
                        modalPlan: !this.state.modalPlan,
                        date: '',
                        selected: '',
                        hour: '',
                        optionsHour: '',
                      });
                    }}>
                    <Ionicon
                      style={{
                        fontWeight: '900',
                        marginRight: 10,
                        position: 'absolute',
                        left: 0,
                      }}
                      name={'ios-arrow-round-back'}
                      size={30}
                      color={'#000'}
                    />
                  </TouchableWithoutFeedback>
                ) : null}
                <View style={{ flexDirection: 'column', width: 220 }}>
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      fontSize: 22,
                      textAlign: 'center',
                      flexWrap: 'wrap',
                      color: '#37153D',
                    }}>
                    Agendar
                  </Text>

                  {currentPlan != '' ? (
                    <View>
                      <Text
                        style={{
                          fontFamily: 'SofiaPro-Medium',
                          fontSize: 18,
                          textAlign: 'center',
                          flexWrap: 'wrap',
                          color: '#37153D',
                          marginTop: 5,
                        }}>
                        Você ainda possui
                      </Text>
                      <Text
                        style={{
                          fontFamily: 'SofiaPro-Medium',
                          fontSize: 18,
                          textAlign: 'center',
                          flexWrap: 'wrap',
                          color: '#F76E1E',
                          marginBottom: 5,
                        }}>
                        {currentPlan.amount - currentPlan.amount_used > 1
                          ? `${currentPlan.amount -
                              currentPlan.amount_used} aulas disponíveis`
                          : `${currentPlan.amount -
                              currentPlan.amount_used} aula disponível`}
                      </Text>
                    </View>
                  ) : null}
                </View>
              </View>

              <View style={{ marginTop: 5 }}>
                <View>
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      fontSize: 14,
                      color: '#F76E1E',
                      marginLeft: 35,
                      marginBottom: 10,
                    }}>
                    Selecione a data:
                  </Text>

                  <View
                    style={{
                      backgroundColor: '#FFFFFF',
                      borderWidth: 0.1,
                      borderColor: '#707070',
                      borderRadius: 20,
                      margin: 15,
                      padding: 10,
                    }}>
                    <Calendar
                      theme={{
                        arrowColor: '#37153D',
                        textDayFontFamily: 'SofiaPro-Medium',
                        selectedDayTextColor: '#fff',
                        selectedDayBackgroundColor: '#37153D',
                        'stylesheet.day.basic': {
                          base: {
                            width: 25,
                            height: 25,
                            alignItems: 'center',
                          },
                          text: {
                            marginTop: 2,
                            fontFamily: 'SofiaPro-Medium',
                          },
                        },
                        'stylesheet.calendar.header': {
                          monthText: {
                            margin: 0,
                          },
                          arrow: {
                            padding: 2,
                          },
                        },
                      }}
                      disabledByDefault
                      disableAllTouchEventsForDisabledDays
                      hideExtraDays
                      markedDates={{
                        ...this.getAvailableDates(
                          moment().add(2, 'day'),
                          moment().add(2, 'month'),
                          this.state.professional?.schedules.map(e =>
                            this.getDaysAvailable(e.week_day),
                          ),
                        ),
                        [this.state.selected]: {
                          selected: true,
                        },
                      }}
                      onDayPress={async date => {
                        this.setState({
                          date: await date.dateString,
                          selected: date.dateString,
                          hour: '',
                        });

                        await this.getScheduleHours(date.dateString);
                        await this.ListView_Ref.scrollToEnd({
                          animated: true,
                        });
                      }}
                    />
                  </View>
                </View>

                <View>
                  {optionsHour != '' ? (
                    <View>
                      <Text
                        style={{
                          fontFamily: 'SofiaPro-Medium',
                          fontSize: 14,
                          color: '#F76E1E',
                          marginLeft: 35,
                          marginBottom: 10,
                        }}>
                        Selecione o horário:
                      </Text>
                      <FlatList
                        numColumns={3}
                        showsVerticalScrollIndicator={false}
                        data={optionsHour}
                        keyExtractor={hour => String(hour)}
                        renderItem={({ item }) => (
                          <DefaultButton
                            title={item}
                            width={'90px'}
                            height={'50px'}
                            margin={'5px'}
                            backgroundColor={hour == item ? '#37153D' : '#999'}
                            onPress={async () => {
                              await this.setState({ hour: item });
                            }}
                          />
                        )}
                      />

                      <Text
                        style={{
                          fontFamily: 'SofiaPro-Medium',
                          fontSize: 14,
                          color: '#F76E1E',
                          marginLeft: 35,
                          marginBottom: 10,
                        }}>
                        Selecione o local:
                      </Text>

                      <RNPickerSelect
                        style={style}
                        placeholder={{
                          label: 'Selecionar academia',
                        }}
                        doneText={'Selecionar'}
                        useNativeAndroidPickerStyle={false}
                        onValueChange={async (itemValue, itemIndex) => {
                          await this.setState({ gym: itemValue });
                        }}
                        value={this.state.state}
                        items={this.state.optionsGyms.map(item => {
                          return {
                            label: item.name,
                            value: item.id,
                            key: item.id,
                          };
                        })}
                      />

                      {/* <FlatList
                        numColumns={2}
                        showsVerticalScrollIndicator={false}
                        data={optionsGyms}
                        keyExtractor={gym => String(gym.id)}
                        renderItem={({ item }) => (
                          <DefaultButton
                            title={item.name}
                            width={'150px'}
                            height={'50px'}
                            margin={'5px'}
                            backgroundColor={
                              gym == item.id ? '#37153D' : '#999'
                            }
                            onPress={async () => {
                              await this.setState({ gym: item.id });
                            }}
                          />
                        )}
                      /> */}
                    </View>
                  ) : selected != '' ? (
                    <Text
                      style={{
                        fontFamily: 'SofiaPro-Medium',
                        fontSize: 14,
                        color: '#F76E1E',
                        marginLeft: 35,
                        marginBottom: 10,
                      }}>
                      Não existem horários disponíveis.
                    </Text>
                  ) : null}
                </View>
              </View>
            </ScrollView>
          }
        />

        {/* Confirmação de agendamento */}

        <AwesomeAlert
          show={showConfirmation}
          showProgress={false}
          closeOnHardwareBackPress={false}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                Aula agendada com sucesso! Agende uma nova ou prossiga para
                Minhas Aulas para realizar o pagamento.
              </Text>
              <LottieView
                style={{ width: 70, height: 70 }}
                resizeMode="contain"
                autoSize
                source={animation}
                autoPlay
                loop={false}
              />
            </View>
          }
        />
      </>
    );
  }
}

const ClassItem = ({
  plan,
  price,
  amount,
  onPress,
  backgroundColor,
  opacity,
  source,
}) => {
  return (
    <>
      <TouchableWithoutFeedback onPress={onPress}>
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 20,
            backgroundColor: '#fff',
            opacity: opacity,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <Image
              style={{ height: 67, width: 67 }}
              resizeMode="contain"
              source={source}
            />
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                marginLeft: 20,
                width: 115,
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                {plan}
              </Text>

              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  color: '#F76E1E',
                  fontSize: 20,
                }}>
                {price}
              </Text>

              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                {amount}
              </Text>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </>
  );
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(ProfessionalProfileScreen);
