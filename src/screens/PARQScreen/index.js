import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableWithoutFeedback,
  Platform,
  Linking,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { QuickQuestion, DefaultButton, PARQQuestion } from '../../components';
import { TextInputMask } from 'react-native-masked-text';
import { AxiosWithHeaders } from '../../services/api';
import { SafeAreaView } from 'react-native-safe-area-context';
import Ionicon from 'react-native-vector-icons/dist/Ionicons';
import DocumentPicker from 'react-native-document-picker';
import AwesomeAlert from 'react-native-awesome-alerts';
import LottieView from 'lottie-react-native';
import animation from '../../assets/success.json';
import axios from 'axios';
import { BASE_URL } from '../../services/baseUrl';

class PARQScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showAlert: false,
      diabetesAnswer: 'Não',
      symptoms: [
        { text: 'Tontura', value: false },
        { text: 'Enjoo', value: false },
        { text: 'Falta de ar', value: false },
        { text: 'Nada', value: true },
      ],
      weight: '',
      height: '',
      objective: '',
      otherDiscomfort: '',
      haveDiabetic: 'Não',
      diabeticStage: '',
      haveHypertension: 'Não',
      hypertensionMax: '',
      haveHeartComplications: 'Não',
      heartComplications: '',
      haveRespiratoryComplications: 'Não',
      respiratoryComplications: '',
      useMedications: 'Não',
      whichMedications: '',
      haveRecentSurgeries: 'Não',
      recentSurgeries: '',
      physicalDiscomfort: '',
      havePhysicalDiscomfort: 'Não',
      medicalRestriction: '',
      haveMedicalRestriction: 'Não',
      loading: false,
      faqAnswered: [],
      PARQForm: [],
      attachment: false,
      file: '',
      fileName: '',
    };
  }

  componentDidMount() {
    this.getUserFaq();
    this.getMedicalAttachment();
  }

  getMedicalAttachment = async () => {
    const { token } = this.props;

    let file = '';

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(`/students/${this.props.user?.id}`)
      .then(async response => {
        await response.data.user.faqAnswered.find(item =>
          item.faq_question_id == 10
            ? (file = item.attachments[0])
            : (file = ''),
        );

        this.setState({
          file: file ? file : '',
          fileName: file.name ? file.name : '',
          attachment: false,
        });
        console.log(this.state.file);
      })
      .catch(err => {
        console.log(err);
      });
  };

  setSymptom = option => {
    if (option.text === 'Nada') {
      this.setState({
        symptoms: [
          { text: 'Tontura', value: false },
          { text: 'Enjoo', value: false },
          { text: 'Falta de ar', value: false },
          { text: 'Nada', value: true },
        ],
      });
    } else {
      let optionItem = this.state.symptoms.filter(result => {
        return result.text == option.text;
      });
      optionItem[0].value = !option.value;
      this.state.symptoms.map(item =>
        option.text == item.text ? optionItem : item,
      );

      if (optionItem[0].value !== 'Nada') {
        this.state.symptoms
          .filter(e => e.text === 'Nada')
          .map(f => (f.value = false));
      }

      this.setState({ symptoms: this.state.symptoms });
    }

    this.getSymptoms();
  };

  getSymptoms = () => {
    let finalAnswer = '';

    this.state.symptoms
      .filter(item => {
        return item.value;
      })
      .map(result => {
        return (finalAnswer += result.text + ', ');
      });

    return finalAnswer;
  };

  setUserSymptoms = symptomsItem => {
    let auxSymptoms = [];
    for (let i = 0; i < this.state.symptoms.length; i++) {
      auxSymptoms.push({
        text: this.state.symptoms[i].text,
        value: new RegExp(this.state.symptoms[i].text).test(symptomsItem),
      });
    }

    return this.setState({ symptoms: auxSymptoms });
  };

  onChangeText = text => {
    this.setState({ diabetesAnswer: text });
  };

  getUserFaq = async () => {
    this.props.user.user.faqAnswered.map(item => {
      switch (item.faq_question_id) {
        case 1:
          this.setState({ weight: item.answered });
          break;

        case 2:
          this.setState({ height: item.answered });
          break;

        case 3:
          this.setState({ objective: item.answered });
          break;

        case 4:
          this.setUserSymptoms(item.answered);
          break;

        case 5:
          this.setState({ otherDiscomfort: item.answered });
          break;

        case 6:
          this.setState({ haveDiabetic: item.answered });
          break;

        case 7:
          this.setState({ diabeticStage: item.answered });
          break;

        case 8:
          this.setState({ haveHypertension: item.answered });
          break;

        case 9:
          this.setState({ hypertensionMax: item.answered });
          break;

        case 10:
          this.setState({ haveHeartComplications: item.answered });
          break;

        case 11:
          this.setState({ heartComplications: item.answered });
          break;

        case 12:
          this.setState({ haveRespiratoryComplications: item.answered });
          break;

        case 13:
          this.setState({ respiratoryComplications: item.answered });
          break;

        case 14:
          this.setState({ useMedications: item.answered });
          break;

        case 15:
          this.setState({ whichMedications: item.answered });
          break;

        case 16:
          this.setState({ haveRecentSurgeries: item.answered });
          break;

        case 17:
          this.setState({ recentSurgeries: item.answered });
          break;

        case 18:
          this.setState({ havePhysicalDiscomfort: item.answered });
          break;

        case 19:
          this.setState({ physicalDiscomfort: item.answered });
          break;

        case 20:
          this.setState({ haveMedicalRestriction: item.answered });
          break;

        case 21:
          this.setState({ medicalRestriction: item.answered });
          break;
      }
    });
  };

  getFaqAnswered = () => {
    let faq = [];
    faq.push({
      faq_question_id: 1,
      user_id: this.props.user.id,
      answered: this.state.weight,
    });
    faq.push({
      faq_question_id: 2,
      user_id: this.props.user.id,
      answered: this.state.height,
    });
    faq.push({
      faq_question_id: 3,
      user_id: this.props.user.id,
      answered: this.state.objective,
    });
    faq.push({
      faq_question_id: 4,
      user_id: this.props.user.id,
      answered: this.getSymptoms(),
    });
    faq.push({
      faq_question_id: 5,
      user_id: this.props.user.id,
      answered: this.state.otherDiscomfort,
    });
    faq.push({
      faq_question_id: 6,
      user_id: this.props.user.id,
      answered: this.state.haveDiabetic,
    });
    faq.push({
      faq_question_id: 7,
      user_id: this.props.user.id,
      answered: this.state.diabeticStage,
    });
    faq.push({
      faq_question_id: 8,
      user_id: this.props.user.id,
      answered: this.state.haveHypertension,
    });
    faq.push({
      faq_question_id: 9,
      user_id: this.props.user.id,
      answered: this.state.hypertensionMax,
    });
    faq.push({
      faq_question_id: 10,
      user_id: this.props.user.id,
      answered: this.state.haveHeartComplications,
    });
    faq.push({
      faq_question_id: 11,
      user_id: this.props.user.id,
      answered: this.state.heartComplications,
    });
    faq.push({
      faq_question_id: 12,
      user_id: this.props.user.id,
      answered: this.state.haveRespiratoryComplications,
    });
    faq.push({
      faq_question_id: 13,
      user_id: this.props.user.id,
      answered: this.state.respiratoryComplications,
    });
    faq.push({
      faq_question_id: 14,
      user_id: this.props.user.id,
      answered: this.state.useMedications,
    });
    faq.push({
      faq_question_id: 15,
      user_id: this.props.user.id,
      answered: this.state.whichMedications,
    });
    faq.push({
      faq_question_id: 16,
      user_id: this.props.user.id,
      answered: this.state.haveRecentSurgeries,
    });
    faq.push({
      faq_question_id: 17,
      user_id: this.props.user.id,
      answered: this.state.recentSurgeries,
    });
    faq.push({
      faq_question_id: 18,
      user_id: this.props.user.id,
      answered: this.state.havePhysicalDiscomfort,
    });
    faq.push({
      faq_question_id: 19,
      user_id: this.props.user.id,
      answered: this.state.physicalDiscomfort,
    });
    faq.push({
      faq_question_id: 20,
      user_id: this.props.user.id,
      answered: this.state.haveMedicalRestriction,
    });
    faq.push({
      faq_question_id: 21,
      user_id: this.props.user.id,
      answered: this.state.medicalRestriction,
    });
    this.setState({ faqAnswered: faq });
  };

  showAlert = () => {
    this.setState({
      showAlert: true,
    });

    setTimeout(
      () =>
        this.setState({
          showAlert: false,
        }),
      4000,
    ); // hide alert after 4s
  };

  saveFaq = async () => {
    const { user } = this.props;
    const { weight, height, objective, havePhysicalDiscomfort } = this.state;

    if (
      weight == '' ||
      height == '' ||
      objective == '' ||
      havePhysicalDiscomfort == ''
    ) {
      Alert.alert('Por favor preencher todos os campos obrigatórios.');
    } else {
      try {
        await this.getFaqAnswered();

        const AxiosInstance = AxiosWithHeaders({
          Authorization: `Bearer ${this.props.token}`,
        });

        await AxiosInstance.put('/student/faq-question', {
          faqAnswered: this.state.faqAnswered,
        })
          .then(async res => {
            if (this.state.fileName == 'atestado-cardiaco') {
              let heartComplicationsId = '';
              await res.data.data.filter(e => {
                e.faq_question_id == 10 ? (heartComplicationsId = e.id) : null;
              });
              await this.saveAttachment(heartComplicationsId);
            }

            user.user.faqAnswered = res.data.data;
            this.props.setUser(user);
            this.showAlert();
          })
          .catch(err => {
            console.log('Erro\n', 'err.response');
          });
      } catch (err) {
        console.log('Algo deu errado!\n', err);
      }
    }
  };

  onChangeObjective = text => this.setState({ objective: text });

  setAnswerCheckbox = answer => {
    let questionItem = this.state.PARQForm.filter(item => {
      return item.id == answer.id;
    });

    questionItem[0].answered =
      questionItem[0].answered == 'Sim' ? 'Não' : 'Sim';

    this.state.PARQForm.map(item =>
      item.id === answer.id ? questionItem : item,
    );
    this.setState({
      PARQForm: this.state.PARQForm,
    });
  };

  loadAttachment = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      this.setState({
        file: res,
        fileName: 'atestado-cardiaco',
        attachment: true,
      });
      console.log(res.uri, res.type, res.name, res.size);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        console.log('Picker canceled.');
      } else {
        throw err;
      }
    }
  };

  saveAttachment = async id => {
    const { token } = this.props;
    const { file, fileName } = this.state;

    if (
      this.state.haveHeartComplications == 'Sim' &&
      this.state.attachment === true
    ) {
      const data = new FormData();
      data.append('attachments', {
        name: fileName,
        type: file.type,
        uri:
          Platform.OS === 'android'
            ? file.uri
            : file.uri.replace('file://', ''),
      });

      try {
        axios({
          url: `${BASE_URL}faq-questions/${id}`,
          method: 'POST',
          data: data,
          headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
            Authorization: `Bearer ${token}`,
          },
        })
          .then(response => {
            console.log(response);
          })
          .catch(function(error) {
            console.log('error from image :' + error);
          });
      } catch (err) {
        console.log(err);
      }
    }
  };

  render() {
    const { showAlert } = this.state;

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            paddingHorizontal: 20,
            paddingVertical: 20,
            elevation: 3,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Ionicon
                style={{
                  fontWeight: '900',
                  marginRight: 10,
                }}
                name={'ios-arrow-round-back'}
                size={30}
                color={'#37153D'}
              />
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 14,
                  color: '#37153D',
                  textAlign: 'left',
                }}>
                {' '}
                Meu perfil
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>

        <ScrollView>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              width: '100%',
              paddingLeft: 20,
              paddingRight: 20,
              backgroundColor: '#F8F8F8',
            }}>
            <Text
              style={{
                fontFamily: 'SofiaPro-Medium',
                fontSize: 16,
                margin: 15,
                width: 300,
                flexWrap: 'wrap',
                color: '#37153D',
                textAlign: 'center',
              }}>
              Questionário de Prontidão para Atividade Física (PAR-Q)
            </Text>

            <View
              style={{
                flexDirection: 'column',
                marginLeft: 80,
                marginRight: 80,
                width: '100%',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View style={{ flexDirection: 'row', position: 'relative' }}>
                  <PARQQuestionMasked
                    type={'custom'}
                    options={{
                      mask: '999',
                    }}
                    question={'Peso:'}
                    keyboardType={'decimal-pad'}
                    answer={this.state.weight}
                    onChangeText={text => this.setState({ weight: text })}
                  />
                  {this.state.weight == '' ? (
                    <Text
                      style={{
                        marginTop: 50,
                        marginRight: 25,
                        right: 0,
                        color: 'red',
                        position: 'absolute',
                      }}>
                      *
                    </Text>
                  ) : null}
                </View>

                <View style={{ flexDirection: 'row', position: 'relative' }}>
                  <PARQQuestionMasked
                    type={'custom'}
                    options={{
                      mask: '9,99',
                    }}
                    question={'Altura:'}
                    keyboardType={'decimal-pad'}
                    answer={this.state.height}
                    onChangeText={text => {
                      this.setState({ height: text });
                    }}
                  />
                  {this.state.height == '' ? (
                    <Text
                      style={{
                        marginTop: 50,
                        marginRight: 25,
                        right: 0,
                        color: 'red',
                        position: 'absolute',
                      }}>
                      *
                    </Text>
                  ) : null}
                </View>
              </View>

              <View style={{ position: 'relative' }}>
                <PARQQuestion
                  question={'Qual seu objetivo?'}
                  answer={this.state.objective}
                  onChangeText={this.onChangeObjective}
                />

                {this.state.objective == '' ? (
                  <Text
                    style={{
                      marginTop: 50,
                      marginRight: 25,
                      right: 0,
                      color: 'red',
                      position: 'absolute',
                    }}>
                    *
                  </Text>
                ) : null}
              </View>

              {
                <View
                  style={{
                    flexDirection: 'column',
                    marginLeft: 20,
                    marginRight: 20,
                    alignItems: 'flex-start',
                  }}>
                  <Text
                    style={{
                      color: '#2E2E2E',
                      fontFamily: 'SofiaPro-Medium',
                      textAlign: 'left',
                      margin: 10,
                      fontSize: 13,
                    }}>
                    {
                      'Na prática de atividades físicas, você sente algum desses sintomas?'
                    }
                  </Text>

                  <View
                    style={{
                      flexDirection: 'column',
                      marginLeft: 20,
                      alignItems: 'flex-start',
                      justifyContent: 'center',
                    }}>
                    {this.state.symptoms.map(result => {
                      return (
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          <TouchableWithoutFeedback
                            onPress={() => this.setSymptom(result)}>
                            <View
                              style={{
                                backgroundColor: result.value
                                  ? '#37153D'
                                  : '#FFFFFF',
                                height: 24,
                                width: 24,
                                borderRadius: 20,
                              }}
                            />
                          </TouchableWithoutFeedback>

                          <Text
                            style={{
                              color: '#2E2E2E',
                              fontFamily: 'SofiaPro-Medium',
                              margin: 10,
                              fontSize: 13,
                              borderWidth: 0.1,
                            }}>
                            {result.text}
                          </Text>
                        </View>
                      );
                    })}
                  </View>
                </View>
              }

              <View style={{ position: 'relative' }}>
                <PARQQuestion
                  question={'Algum outro desconforto?'}
                  answer={this.state.otherDiscomfort}
                  onChangeText={text =>
                    this.setState({ otherDiscomfort: text })
                  }
                />

                {this.state.otherDiscomfort == '' ? (
                  <Text
                    style={{
                      marginTop: 50,
                      marginRight: 25,
                      right: 0,
                      color: 'red',
                      position: 'absolute',
                    }}>
                    *
                  </Text>
                ) : null}
              </View>

              <QuickQuestion
                question={'Diabético?'}
                answer={this.state.haveDiabetic}
                onPress={() =>
                  this.setState({
                    haveDiabetic:
                      this.state.haveDiabetic == 'Sim' ? 'Não' : 'Sim',
                  })
                }
              />

              <View style={{ position: 'relative' }}>
                {this.state.haveDiabetic == 'Sim' ? (
                  <PARQQuestion
                    question={'Qual estágio?'}
                    answer={this.state.diabeticStage}
                    onChangeText={text =>
                      this.setState({ diabeticStage: text })
                    }
                  />
                ) : null}

                {this.state.haveDiabetic == 'Sim' &&
                this.state.diabeticStage == '' ? (
                  <Text
                    style={{
                      marginTop: 50,
                      marginRight: 25,
                      right: 0,
                      color: 'red',
                      position: 'absolute',
                    }}>
                    *
                  </Text>
                ) : null}
              </View>

              <QuickQuestion
                question={'Hipertenso?'}
                answer={this.state.haveHypertension}
                onPress={() =>
                  this.setState({
                    haveHypertension:
                      this.state.haveHypertension == 'Sim' ? 'Não' : 'Sim',
                  })
                }
              />

              <View style={{ position: 'relative' }}>
                {this.state.haveHypertension == 'Sim' ? (
                  <PARQQuestion
                    question={'Qual a máxima?'}
                    answer={this.state.hypertensionMax}
                    onChangeText={text =>
                      this.setState({ hypertensionMax: text })
                    }
                  />
                ) : null}

                {this.state.haveHypertension == 'Sim' &&
                this.state.hypertensionMax == '' ? (
                  <Text
                    style={{
                      marginTop: 50,
                      marginRight: 25,
                      right: 0,
                      color: 'red',
                      position: 'absolute',
                    }}>
                    *
                  </Text>
                ) : null}
              </View>

              <QuickQuestion
                question={'Possui algum problema cardíaco?'}
                answer={this.state.haveHeartComplications}
                onPress={() =>
                  this.setState({
                    haveHeartComplications:
                      this.state.haveHeartComplications == 'Sim'
                        ? 'Não'
                        : 'Sim',
                  })
                }
              />

              <View style={{ position: 'relative' }}>
                {this.state.haveHeartComplications == 'Sim' ? (
                  <View>
                    <PARQQuestion
                      question={'Qual?'}
                      answer={this.state.heartComplications}
                      onChangeText={text =>
                        this.setState({ heartComplications: text })
                      }
                    />

                    {this.state.fileName != '' ? (
                      <AttachmentItem
                        hasFile={this.state.file.path !== '' ? true : false}
                        cardInfo={this.state.fileName}
                        onPress={() =>
                          this.state.file.path != ''
                            ? Linking.openURL(this.state.file.path)
                            : ''
                        }
                      />
                    ) : null}

                    <View
                      style={{
                        alignSelf: 'center',
                      }}>
                      <DefaultButton
                        title={'Anexar atestado médico '}
                        onPress={async () => {
                          await this.loadAttachment();
                        }}
                        width={220}
                      />
                    </View>
                  </View>
                ) : null}

                {this.state.haveHeartComplications == 'Sim' &&
                this.state.heartComplications == '' ? (
                  <Text
                    style={{
                      marginTop: 50,
                      marginRight: 25,
                      right: 0,
                      color: 'red',
                      position: 'absolute',
                    }}>
                    *
                  </Text>
                ) : null}
              </View>

              <QuickQuestion
                question={'Possui algum problema respiratório?'}
                answer={this.state.haveRespiratoryComplications}
                onPress={() =>
                  this.setState({
                    haveRespiratoryComplications:
                      this.state.haveRespiratoryComplications == 'Sim'
                        ? 'Não'
                        : 'Sim',
                  })
                }
              />

              <View style={{ position: 'relative' }}>
                {this.state.haveRespiratoryComplications == 'Sim' ? (
                  <PARQQuestion
                    question={'Qual?'}
                    answer={this.state.respiratoryComplications}
                    onChangeText={text =>
                      this.setState({ respiratoryComplications: text })
                    }
                  />
                ) : null}

                {this.state.haveRespiratoryComplications == 'Sim' &&
                this.state.respiratoryComplications == '' ? (
                  <Text
                    style={{
                      marginTop: 50,
                      marginRight: 25,
                      right: 0,
                      color: 'red',
                      position: 'absolute',
                    }}>
                    *
                  </Text>
                ) : null}
              </View>

              <QuickQuestion
                question={'Utiliza algum tipo de medicamento ou drogas?'}
                answer={this.state.useMedications}
                onPress={() =>
                  this.setState({
                    useMedications:
                      this.state.useMedications == 'Sim' ? 'Não' : 'Sim',
                  })
                }
              />

              <View style={{ position: 'relative' }}>
                {this.state.useMedications == 'Sim' ? (
                  <PARQQuestion
                    question={'Qual?'}
                    answer={this.state.whichMedications}
                    onChangeText={text =>
                      this.setState({ whichMedications: text })
                    }
                  />
                ) : null}

                {this.state.useMedications == 'Sim' &&
                this.state.whichMedications == '' ? (
                  <Text
                    style={{
                      marginTop: 50,
                      marginRight: 25,
                      right: 0,
                      color: 'red',
                      position: 'absolute',
                    }}>
                    *
                  </Text>
                ) : null}
              </View>

              <QuickQuestion
                question={'Fez alguma cirurgia nos últimos 6 meses?'}
                answer={this.state.haveRecentSurgeries}
                onPress={() =>
                  this.setState({
                    haveRecentSurgeries:
                      this.state.haveRecentSurgeries == 'Sim' ? 'Não' : 'Sim',
                  })
                }
              />

              <View style={{ position: 'relative' }}>
                {this.state.haveRecentSurgeries == 'Sim' ? (
                  <PARQQuestion
                    question={'Qual?'}
                    answer={this.state.recentSurgeries}
                    onChangeText={text =>
                      this.setState({ recentSurgeries: text })
                    }
                  />
                ) : null}

                {this.state.haveRecentSurgeries == 'Sim' &&
                this.state.recentSurgeries == '' ? (
                  <Text
                    style={{
                      marginTop: 50,
                      marginRight: 25,
                      right: 0,
                      color: 'red',
                      position: 'absolute',
                    }}>
                    *
                  </Text>
                ) : null}
              </View>

              <QuickQuestion
                question={
                  'Sente incômodo ou dores em articulações e/ou coluna na prática de atividade física?'
                }
                answer={this.state.havePhysicalDiscomfort}
                onPress={text =>
                  this.setState({
                    havePhysicalDiscomfort:
                      this.state.havePhysicalDiscomfort == 'Sim'
                        ? 'Não'
                        : 'Sim',
                  })
                }
              />

              <View style={{ position: 'relative' }}>
                {this.state.havePhysicalDiscomfort == 'Sim' ? (
                  <PARQQuestion
                    question={'Qual?'}
                    answer={this.state.physicalDiscomfort}
                    onChangeText={text =>
                      this.setState({ physicalDiscomfort: text })
                    }
                  />
                ) : null}

                {this.state.havePhysicalDiscomfort == 'Sim' &&
                this.state.physicalDiscomfort == '' ? (
                  <Text
                    style={{
                      marginTop: 50,
                      marginRight: 25,
                      right: 0,
                      color: 'red',
                      position: 'absolute',
                    }}>
                    *
                  </Text>
                ) : null}
              </View>

              <QuickQuestion
                question={
                  'Tem alguma restrição ou recomendação médica para a prática de exercícios físicos?'
                }
                answer={this.state.haveMedicalRestriction}
                onPress={text =>
                  this.setState({
                    haveMedicalRestriction:
                      this.state.haveMedicalRestriction == 'Sim'
                        ? 'Não'
                        : 'Sim',
                  })
                }
              />

              <View style={{ position: 'relative' }}>
                {this.state.haveMedicalRestriction == 'Sim' ? (
                  <PARQQuestion
                    question={'Qual?'}
                    answer={this.state.medicalRestriction}
                    onChangeText={text =>
                      this.setState({ medicalRestriction: text })
                    }
                  />
                ) : null}

                {this.state.haveMedicalRestriction == 'Sim' &&
                this.state.medicalRestriction == '' ? (
                  <Text
                    style={{
                      marginTop: 50,
                      marginRight: 25,
                      right: 0,
                      color: 'red',
                      position: 'absolute',
                    }}>
                    *
                  </Text>
                ) : null}
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  margin: 20,
                }}>
                <DefaultButton
                  onPress={async () => {
                    this.saveFaq();
                    this.setUserSymptoms();
                  }}
                  title={'Salvar'}
                  width={190}
                />
              </View>
            </View>
          </View>
        </ScrollView>

        <AwesomeAlert
          show={showAlert}
          showProgress={false}
          closeOnHardwareBackPress={false}
          onDismiss={async () => {
            this.props.navigation.goBack();
          }}
          contentContainerStyle={{
            padding: 0,
          }}
          customView={
            <View
              style={{
                padding: 5,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Medium',
                  fontSize: 16,
                  textAlign: 'center',
                  flexWrap: 'wrap',
                  margin: 5,
                }}>
                PAR-Q atualizado com sucesso!
              </Text>
              <LottieView
                style={{ width: 70, height: 70 }}
                resizeMode="contain"
                autoSize
                source={animation}
                autoPlay
                loop={false}
              />
            </View>
          }
        />
      </>
    );
  }
}

const PARQQuestionMasked = ({
  question,
  answer,
  width,
  onPress,
  keyboardType,
  maxLength,
  onChangeText,
  type,
  options,
  placeholder,
}) => {
  return (
    <View style={{ flexDirection: 'column', marginLeft: 20, marginRight: 20 }}>
      <Text
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaPro-Medium',
          margin: 10,
          fontSize: 13,
        }}>
        {question}
      </Text>
      <TextInputMask
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaProLight',
          fontSize: 13,
          backgroundColor: '#fff',
          height: 40,
          borderRadius: 50,
          borderWidth: 0.1,
          paddingLeft: 20,
          flex: 1,
          width: 80,
        }}
        type={type}
        value={answer}
        keyboardType={keyboardType}
        onChangeText={onChangeText}
        options={options}
        placeholder={placeholder}
      />
    </View>
  );
};

PARQQuestionMasked.defaultProps = {
  width: 150,
  keyboardType: 'default',
  maxLength: 300,
};

const AttachmentItem = ({ cardInfo, onPress, hasFile }) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: 180,
        paddingHorizontal: 15,
        alignItems: 'center',
        alignSelf: 'center',
        borderRadius: 50,
        marginTop: 10,
      }}>
      <Text
        style={{
          fontFamily: 'SofiaPro-Regular',
          fontSize: 14.5,
          color: '#2E2E2E',
          textAlign: 'center',
          margin: 10,
        }}>
        {cardInfo}
      </Text>
      {hasFile == true ? (
        <TouchableWithoutFeedback onPress={onPress}>
          <Ionicon
            style={{
              fontWeight: '900',
            }}
            name={'ios-download'}
            size={28}
            color={'#37153D'}
          />
        </TouchableWithoutFeedback>
      ) : null}
    </View>
  );
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(PARQScreen);
