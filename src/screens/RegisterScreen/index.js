import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Alert,
  KeyboardAvoidingView,
} from 'react-native';
import { showMessage } from 'react-native-flash-message';
import { TextInputMask } from 'react-native-masked-text';
import { DefaultButton, DefaultInput, LoadingModal } from '../../components';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import { ImageBackground, Logo } from './styles';
import AsyncStorage from '@react-native-community/async-storage';
import loginBg from '../../assets/images/background.png';
import logo from '../../assets/images/logo.png';
import API, { AxiosWithHeaders } from '../../services/api';

class RegisterScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      email: '',
      password: '',
      phone: '',
      loading: false,
    };
  }

  onNameChanged = name => this.setState({ name });
  onEmailChanged = email => this.setState({ email });
  onPhoneChanged = phone => this.setState({ phone });
  onPasswordChanged = password => this.setState({ password });

  login = async () => {
    try {
      this.setState({ loading: true });
      const response = await API.post('users/authenticate', {
        email: this.state.email,
        password: this.state.password,
      });

      if (response.status === 200) {
        this.setState(
          {
            loading: false,
          },
          async () => {
            await AsyncStorage.setItem(
              'DATA_KEY',
              JSON.stringify(response.data.token),
            );

            await AsyncStorage.setItem(
              'REFRESH_TOKEN',
              JSON.stringify(response.data.refreshToken),
            );

            this.props.setToken(response.data.token);

            const AxiosInstance = AxiosWithHeaders({
              Authorization: `Bearer ${response.data.token}`,
            });

            let getResponse;

            if (response.data.role === 'student') {
              getResponse = await AxiosInstance.get(
                'students/' + response.data.student.id,
              );
            } else {
              getResponse = await AxiosInstance.get(
                'professionals/' + response.data.professional.id,
              );
            }

            console.log(response.data);

            console.log(getResponse);

            this.props.setUser(getResponse.data);
          },
        );
        console.log('Login successed.');

        response.data.role === 'student'
          ? this.props.navigation.navigate('StudentTutorialScreen')
          : this.props.navigation.navigate('ProfessionalTutorialScreen');
      }
    } catch (err) {
      console.log('We got an error', err);

      this.setState({ loading: false });
      showMessage({
        type: 'danger',
        message: 'Erro inesperado',
        description: 'Tente novamente',
        titleStyle: {
          textAlign: 'center',
        },
        textStyle: {
          textAlign: 'center',
        },
      });
    }
  };

  register = async () => {
    const { name, email, password, phone } = this.state;

    if (!name || !email || !password) {
      showMessage({
        type: 'danger',
        message: 'Erro ao realizar o cadastro',
        description: 'Preencha todas as informações',
        titleStyle: {
          textAlign: 'center',
        },
        textStyle: {
          textAlign: 'center',
        },
      });

      return;
    }

    this.setState({ loading: true });

    try {
      const response = await API.post('/users/sign-up', {
        user: {
          name,
          username: email,
          email,
          password,
          contact: phone,
          role: 'student',
        },
      });

      if (response.status === 200) {
        this.setState({ loading: false }, () => {
          this.login();
        });

        console.log('Register successed.');
      }
    } catch (err) {
      this.setState({ loading: false });
      console.log('Algo deu errado\n' + err);

      if (err.response.status === 409) {
        showMessage({
          type: 'danger',
          message: 'E-mail já cadastrado',
          description: 'Tente novamente',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      } else {
        showMessage({
          type: 'danger',
          message: 'Erro ao efetuar o cadastro',
          description: 'Tente novamente',
          titleStyle: {
            textAlign: 'center',
          },
          textStyle: {
            textAlign: 'center',
          },
        });
      }
    }
  };

  render() {
    return (
      <>
        <ImageBackground source={loginBg}>
          <KeyboardAvoidingView
            behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
            style={{ flex: 1 }}>
            <ScrollView
              contentContainerStyle={{ flexGrow: 1 }}
              showsVerticalScrollIndicator={false}>
              <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                <View
                  style={{
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Logo
                    source={logo}
                    style={{ alignItems: 'center', height: 170, width: 170 }}
                    resizeMode="contain"
                  />
                </View>

                <View
                  style={{
                    backgroundColor: '#F8F8F8',
                    marginTop: 40,
                    padding: 20,
                    paddingBottom: 60,
                    borderTopLeftRadius: 40,
                    borderTopRightRadius: 40,
                  }}>
                  <Text
                    style={{
                      fontSize: 23,
                      fontFamily: 'SofiaPro-Medium',
                      textAlign: 'center',
                      padding: 10,
                      color: '#F76E1E',
                    }}>{`Vamos Começar?`}</Text>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'SofiaPro-Regular',
                      textAlign: 'center',
                      color: '#707070',
                      paddingBottom: 30,
                    }}>{`Crie sua conta.`}</Text>
                  <View
                    style={{
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <DefaultInput
                      onChangeText={this.onNameChanged}
                      value={this.state.name}
                      placeholder={'Nome'}
                    />
                    <DefaultInput
                      onChangeText={this.onEmailChanged}
                      value={this.state.email}
                      placeholder={'E-mail'}
                    />

                    <TextInputMask
                      placeholderTextColor={'#999999'}
                      style={{
                        fontFamily: 'SofiaPro-Regular',
                        textAlign: 'center',
                        width: '100%',
                        height: 40,
                        backgroundColor: '#fff',
                        color: '#636363',
                        borderColor: '#707070',
                        fontSize: 14,
                        margin: 10,
                        borderRadius: 50,
                        borderWidth: 0.1,
                      }}
                      type={'cel-phone'}
                      keyboardType={'number-pad'}
                      placeholder={'Telefone'}
                      value={this.state.phone}
                      onChangeText={async text => {
                        await this.setState({ phone: text });
                      }}
                    />

                    <DefaultInput
                      onChangeText={this.onPasswordChanged}
                      value={this.state.password}
                      secureTextEntry={true}
                      placeholder={'Senha'}
                    />

                    <DefaultButton
                      onPress={() => {
                        this.register();
                      }}
                      title={'Criar'}
                      width={190}
                    />

                    <View
                      style={{
                        justifyContent: 'center',
                        flexDirection: 'row',
                        alignItems: 'center',
                        margin: 10,
                        padding: 5,
                      }}>
                      <Text
                        style={{
                          fontFamily: 'SofiaProLight',
                          fontSize: 12,
                          textAlign: 'center',
                          borderRadius: 20,
                          color: '#37153D',
                        }}>{`Já tem uma conta?`}</Text>
                      <Text
                        onPress={() =>
                          this.props.navigation.navigate('AccessScreen')
                        }
                        style={{
                          fontFamily: 'SofiaPro-Bold',
                          fontSize: 12,
                          textAlign: 'center',
                          borderRadius: 20,
                          color: '#37153D',
                        }}>{` Entre agora.`}</Text>
                    </View>
                  </View>
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </ImageBackground>

        <LoadingModal show={this.state.loading} />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(RegisterScreen);
