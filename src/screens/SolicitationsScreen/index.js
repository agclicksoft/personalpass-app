import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  RefreshControl,
  Image,
  ScrollView,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import { Creators as authActions } from '../../redux/reducers/auth';
import {
  DefaultButton,
  SolicitationItem,
  LoadingModal,
} from '../../components';
import { AxiosWithHeaders } from '../../services/api';
import { SafeAreaView } from 'react-native-safe-area-context';
import { TextInputMask } from 'react-native-masked-text';
import RNPickerSelect from 'react-native-picker-select';
import AwesomeAlert from 'react-native-awesome-alerts';
import moment from 'moment';
import 'moment-timezone';

class SolicitationsScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      lessons: [],
      loading: true,
      refreshing: false,
      tokenModal: false,
      code: '',
      choice: {},
      index: '',
      page: 1,
      showFilter: false,
      filtering: false,
      status: '',
      optionsStatus: [
        { description: 'Disponível', value: 'DISPONIVEL', checked: false },
        { description: 'Em andamento', value: 'EM ANDAMENTO', checked: false },
        { description: 'Confirmada', value: 'CONFIRMADO', checked: false },
        { description: 'Concluída', value: 'CONCLUIDO', checked: false },
        { description: 'Pendente', value: 'PENDENTE', checked: false },
        { description: 'Cancelada', value: 'SUSPENSO', checked: false },
      ],
    };
  }

  async componentDidMount() {
    await this.loadLessons();

    this.focusListener = this.props.navigation.addListener('focus', () =>
      this._onRefresh(),
    );
  }

  componentWillUnmount() {
    this.focusListener();
  }

  loadLessons = async () => {
    const { token } = this.props;
    const { page } = this.state;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(`/contracts?page=${page}`)
      .then(response => {
        this.setState({
          lessons:
            this.state.page === 1
              ? response.data.data
              : [...this.state.lessons, ...response.data.data],
          loading: false,
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  startClass = async () => {
    const { user, token } = this.props;

    const { code, choice } = this.state;

    const AxiosInstace = AxiosWithHeaders({ Authorization: `Bearer ${token}` });

    try {
      const response = await AxiosInstace.post(
        `/scheduled-services/${choice.scheduledServices[0].id}/give-present`,
        {
          tk_auth_service: code,
        },
      );

      if (response.status === 200) {
        this.setState({
          tokenModal: false,
        });
        await this.getStudent(choice.student.id);
      }
    } catch (err) {
      console.log('We got an error', err);

      if (err.response && err.response.status === 401) {
        Alert.alert(
          'Oops!',
          'Código incorreto, tente novamente.',
          [{ text: 'OK' }],
          {
            cancelable: false,
          },
        );
      }
    }
  };

  loadOnScroll = async () => {
    this.setState(
      {
        page: this.state.page + 1,
      },
      () => this.loadLessons(),
    );
  };

  _onRefresh = async () => {
    await this.setState({ lessons: [], loading: true, page: 1 });
    await this.loadLessons();
  };

  getStudent = async id => {
    const { token } = this.props;

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(`/students/${id}`)
      .then(response => {
        this.setState({
          student: response.data,
        });

        this.props.navigation.navigate('StudentProfileScreen', {
          student: this.state.student,
          img_profile: this.state.student.user.img_profile,
          choice: this.state.choice,
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  filter = async lessonStatus => {
    const { token } = this.props;

    await this.setState({ lessons: [], loading: true, page: 1 });

    const AxiosInstance = AxiosWithHeaders({
      Authorization: `Bearer ${token}`,
    });

    await AxiosInstance.get(`/contracts?page=${this.state.page}&perPage=40`)
      .then(response => {
        this.setState({
          lessons: response.data.data.filter(e => e.status === lessonStatus),
          loading: false,
        });
        console.log(response.data.data);
      })
      .catch(err => {
        console.log('Error:' + err);
      });
  };

  render() {
    const { lessons, showFilter, tokenModal } = this.state;

    const style = {
      inputAndroid: {
        width: 190,
        height: 40,
        color: '#707070',
        paddingHorizontal: 10,
        fontFamily: 'SofiaPro-Medium',
        fontSize: 16,
      },
      inputIOS: {
        width: 150,
        height: 35,
        color: '#707070',
        paddingHorizontal: 10,
        textAlign: 'center',
        fontFamily: 'SofiaPro-Medium',
        fontSize: 16,
      },
    };

    return (
      <>
        <SafeAreaView style={{ backgroundColor: '#F9F9F9' }} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F9F9F9',
            shadowColor: '#000',
            alignItems: 'center',
            paddingHorizontal: 30,
            paddingVertical: 20,
            elevation: 3,
          }}>
          <Text
            style={{
              fontFamily: 'SofiaPro-Medium',
              fontSize: 18,
              flexWrap: 'wrap',
              color: '#37153D',
            }}>
            Solicitações
          </Text>

          {/* <DefaultButton
            title={'Filtrar'}
            width={100}
            height={35}
            margin={'0px'}
            onPress={() => {
              this.setState({
                showFilter: true,
                status: '',
              });
            }}
          /> */}
        </View>

        {this.state.filtering == true ? (
          <TouchableWithoutFeedback
            onPress={() => {
              this.setState({
                filtering: false,
                loading: true,
                lessons: [],
              });
              this.loadLessons();
            }}>
            <View
              style={{
                alginItems: 'center',
                justifyContent: 'center',
                alignSelf: 'center',
                borderRadius: 50,
                paddingHorizontal: 15,
                paddingVertical: 5,
                backgroundColor: '#F76E1E',
                margin: 10,
                width: 135,
              }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  marginLeft: 5,
                  fontSize: 15,
                  color: '#fff',
                  textAlign: 'center',
                }}>
                Limpar filtro
              </Text>
            </View>
          </TouchableWithoutFeedback>
        ) : null}

        <View style={{ flex: 1 }}>
          {lessons != '' ? (
            <FlatList
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={async () => await this._onRefresh()}
                />
              }
              onEndReachedThreshold={0}
              onEndReached={this.loadOnScroll}
              showsVerticalScrollIndicator={false}
              data={lessons}
              keyExtractor={lesson => String(lesson.id)}
              renderItem={({ item, index }) =>
                item.amount == 1 ? (
                  <SolicitationItem
                    date={moment
                      .parseZone(item.scheduledServices[0].date)
                      .format('DD/MM/YYYY - HH:mm')}
                    avatar={{
                      uri: item.student.user?.img_profile
                        ? `${
                            item.student.user?.img_profile
                          }?random=${Math.random()
                            .toString(36)
                            .substring(7)}`
                        : 'https://static.scrum.org/web/images/profile-placeholder.png',
                    }}
                    gym={item.scheduledServices[0].establishment.name}
                    student={item.student.user.name}
                    plan={item.professionalPlan.plan.description}
                    methodPayment={item.method_payment}
                    status={item.scheduledServices[0].status.toLowerCase()}
                    onPress={async () => {
                      if (item.scheduledServices[0].status == 'CONCLUIDO') {
                        this.props.navigation.navigate('TrainingDetailScreen', {
                          class: item.scheduledServices[0],
                          classId: item.scheduledServices[0].id,
                        });
                      } else if (
                        item.scheduledServices[0].status == 'DISPONIVEL'
                      ) {
                        await this.setState({
                          choice: item,
                          tokenModal: !this.state.tokenModal,
                        });
                      } else {
                        await this.setState({
                          choice: item,
                        });
                        await this.getStudent(item.student.id);
                      }
                    }}
                  />
                ) : (
                  <SolicitationItemMultiple
                    avatar={{
                      uri: item.student.user.img_profile
                        ? `${
                            item.student.user?.img_profile
                          }?random=${Math.random()
                            .toString(36)
                            .substring(7)}`
                        : 'https://static.scrum.org/web/images/profile-placeholder.png',
                    }}
                    gym={item.scheduledServices[0].establishment.name}
                    student={item.student.user.name}
                    plan={item.professionalPlan.plan.description}
                    status={item.scheduledServices[0].status.toLowerCase()}
                    methodPayment={item.method_payment}
                    onPress={async () => {
                      await this.setState({
                        choice: item,
                      });
                      await this.getStudent(item.student.id);
                    }}
                  />
                )
              }
            />
          ) : (
            <View
              style={{
                marginTop: 20,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              {this.state.filtering === false &&
              this.state.loading === false ? (
                <>
                  <ItemModal item={'Você ainda não possui aulas agendadas.'} />

                  <TouchableWithoutFeedback
                    onPress={() => {
                      this.setState({
                        filtering: false,
                        loading: true,
                      });
                      this.loadLessons();
                    }}>
                    <View
                      style={{
                        alginItems: 'center',
                        justifyContent: 'center',
                        alignSelf: 'center',
                        borderRadius: 50,
                        paddingHorizontal: 15,
                        paddingVertical: 5,
                        backgroundColor: '#F76E1E',
                        margin: 10,
                        width: 135,
                      }}>
                      <Text
                        style={{
                          fontFamily: 'SofiaPro-Regular',
                          marginLeft: 5,
                          fontSize: 15,
                          color: '#fff',
                          textAlign: 'center',
                        }}>
                        Recarregar
                      </Text>
                    </View>
                  </TouchableWithoutFeedback>
                </>
              ) : null}

              {this.state.filtering === true && this.state.loading === false ? (
                <ItemModal item={'Nenhuma aula encontrada.'} />
              ) : null}
            </View>
          )}
        </View>

        <LoadingModal show={this.state.loading} />

        <AwesomeAlert
          show={showFilter}
          showProgress={false}
          closeOnHardwareBackPress={false}
          overlayStyle={{
            backgroundColor: 'rgba(0, 0, 0, 0.4)',
          }}
          contentContainerStyle={{
            maxWidth: '80%',
            minWidth: '70%',
            height: 260,
            margin: 10,
            borderRadius: 30,
            justifyContent: 'center',
          }}
          customView={
            <ScrollView showsVerticalScrollIndicator={false}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginVertical: 15,
                }}>
                <Text
                  style={{
                    fontFamily: 'SofiaPro-Medium',
                    fontSize: 22,
                    textAlign: 'center',
                    flexWrap: 'wrap',
                    color: '#37153D',
                  }}>
                  Status
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginVertical: 10,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#FAFAFA',
                    borderColor: '#707070',
                    borderWidth: 0.1,
                    borderRadius: 30,
                    width: 200,
                    height: 50,
                  }}>
                  <RNPickerSelect
                    style={style}
                    placeholder={{
                      label: 'Selecionar',
                    }}
                    doneText={'Selecionar'}
                    useNativeAndroidPickerStyle={false}
                    onValueChange={async (itemValue, itemIndex) => {
                      await this.setState({ status: itemValue });
                    }}
                    value={this.state.status}
                    items={this.state.optionsStatus.map(item => {
                      return {
                        label: item.description,
                        value: item.value,
                        key: item.value,
                      };
                    })}
                  />
                </View>
              </View>

              <View
                style={{
                  alignSelf: 'center',
                }}>
                <DefaultButton
                  title={'Filtrar'}
                  onPress={() => {
                    this.setState({
                      showFilter: !this.state.showFilter,
                      filtering: true,
                    });
                    this.filter(this.state.status);
                  }}
                  width={200}
                  height={40}
                  backgroundColor={'#F76E1E'}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 5,
                }}>
                <TouchableWithoutFeedback
                  onPress={() => {
                    this.setState({
                      showFilter: !this.state.showFilter,
                    });
                  }}>
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      color: '#707070',
                    }}>
                    Cancelar
                  </Text>
                </TouchableWithoutFeedback>
              </View>
            </ScrollView>
          }
        />

        <AwesomeAlert
          show={tokenModal}
          showProgress={false}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          contentContainerStyle={{
            backgroundColor: '#F8F8F8',
            borderRadius: 25,
          }}
          customView={
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: 300,
                backgroundColor: '#F8F8F8',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 20,
                }}>
                <View
                  style={{
                    flexDirection: 'column',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'SofiaPro-Medium',
                      fontSize: 22,
                      textAlign: 'center',
                      flexWrap: 'wrap',
                      color: '#37153D',
                    }}>
                    Digite aqui o código informado pelo aluno:
                  </Text>

                  <MaskedInput
                    question={null}
                    type={'custom'}
                    options={{
                      mask: '999999',
                    }}
                    keyboardType={'number-pad'}
                    answer={this.state.code}
                    onChangeText={text => this.setState({ code: text })}
                  />
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                  marginTop: 20,
                }}>
                <DefaultButton
                  onPress={() => {
                    this.setState({
                      changePassAlert: false,
                    });
                    this.startClass();
                  }}
                  backgroundColor={'#37153D'}
                  title={'Confirmar'}
                  width={200}
                  height={40}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  elevation: 1,
                }}>
                <TouchableWithoutFeedback
                  onPress={() =>
                    this.setState({
                      tokenModal: false,
                    })
                  }>
                  <Text style={{ marginTop: 10, color: '#999' }}>Voltar</Text>
                </TouchableWithoutFeedback>
              </View>
            </View>
          }
        />
      </>
    );
  }
}

const ItemModal = ({ item, answer }) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'center',
        margin: 2,
      }}>
      <Text
        style={{
          fontFamily: 'SofiaPro-Regular',
          flexWrap: 'wrap',
          color: '#F76E1E',
          fontSize: 13,
        }}>
        {item}
      </Text>
      <Text
        style={{
          fontFamily: 'SofiaProLight',
          flexWrap: 'wrap',
          color: '#2E2E2E',
          fontSize: 13,
        }}>
        {answer}
      </Text>
    </View>
  );
};

const SolicitationItemMultiple = ({
  onPress,
  status,
  avatar,
  student,
  gym,
  plan,
  methodPayment,
}) => {
  return (
    <>
      <View
        style={{
          flexDirection: 'column',
          alignItems: 'center',
          paddingTop: 20,
          paddingBottom: 10,
          margin: 10,
          borderRadius: 10,
          elevation: 1,
          backgroundColor: '#fff',
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            width: 320,
          }}>
          <Image
            style={{
              height: 120,
              width: 120,
              borderRadius: 135,
              marginRight: 13,
            }}
            source={avatar}
          />
          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'space-evenly',
              height: 100,
            }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Local:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {gym}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Aluno:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {student}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Pacote:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {plan}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontFamily: 'SofiaPro-Regular',
                  flexWrap: 'wrap',
                  color: '#F76E1E',
                  fontSize: 13,
                }}>
                Pagamento:{' '}
              </Text>
              <Text
                style={{
                  fontFamily: 'SofiaProLight',
                  flexWrap: 'wrap',
                  color: '#2E2E2E',
                  fontSize: 13,
                }}>
                {getStatusPayment(methodPayment ?? 'PENDENTE')}
              </Text>
            </View>
          </View>
        </View>

        <View
          style={{
            borderTopWidth: 1,
            borderTopColor: 'rgba(112, 112, 112, 0.1)',
            padding: 5,
            width: '100%',
            alignItems: 'center',
            marginTop: 15,
          }}>
          <DefaultButton
            title={getStatusTitle(status)}
            onPress={onPress}
            backgroundColor={getStatusBG(status)}
            fontColor={getStatusColor(status)}
            width={220}
            height={35}
          />
        </View>
      </View>
    </>
  );
};

let getStatusTitle = text => {
  switch (text) {
    case 'em andamento':
      return 'Em andamento';
    case 'concluido':
      return 'Concluída';
    case 'pendente':
      return 'Responder';
    case 'confirmado':
      return 'Confirmado';
    case 'cancelado':
      return 'Cancelado';
    default:
      return 'Responder';
  }
};

let getStatusPayment = text => {
  switch (text) {
    case 'CARTAO':
      return 'Cartão de crédito';
    case 'PENDENTE':
      return 'Pendente';
    case 'MAOS':
      return 'Em mãos';
    case 'BOLETO':
      return 'Em mãos';
    default:
      return 'Não identificado';
  }
};

let getStatusBG = text => {
  switch (text) {
    case 'pendente':
      return '#37153D';
    default:
      return '#E9E9E9';
  }
};

let getStatusColor = text => {
  switch (text) {
    case 'em andamento':
      return '#F76E1E';
    case 'pendente':
      return '#E9E9E9';
    case 'concluido':
      return '#37153D';
    case 'confirmado':
      return '#049526';
    case 'cancelado':
      return '#D22020';
    default:
      return '#37153D';
  }
};

const MaskedInput = ({
  question,
  answer,
  width,
  keyboardType,
  onChangeText,
  placeholder,
  type,
  options,
  maxLength,
}) => {
  return (
    <View style={{ flexDirection: 'column', marginLeft: 10, marginRight: 10 }}>
      <Text
        style={{
          color: '#2E2E2E',
          fontFamily: 'SofiaPro-Medium',
          margin: 10,
          fontSize: 13,
        }}>
        {question}
      </Text>

      <TextInputMask
        style={{
          color: '#2E2E2E',
          fontSize: 18,
          backgroundColor: '#fff',
          height: 40,
          borderRadius: 50,
          borderWidth: 0.1,
          paddingLeft: 30,
          paddingRight: 30,
          width: width ? width : 200,
          textAlign: 'center',
        }}
        maxLength={maxLength}
        type={type}
        value={answer}
        keyboardType={keyboardType}
        onChangeText={onChangeText}
        options={options}
        placeholder={placeholder}
      />
    </View>
  );
};

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    token: state.auth.token,
  };
};

export default connect(
  mapStateToProps,
  { ...authActions },
)(SolicitationsScreen);
