import moment from 'moment';

const ageLimitInYearsAllowed = {
  minimum: 100,
  maximum: 5,
};

const birthdayIsValid = (date = null) => {
  // formato de entrada: dd/mm/yyyy
  // resposta: true ou false
  // limite mínimo: data atual - 100 anos
  // limite máximo: data atual - 5 anos

  if (!date) {
    return false;
  }

  const { minimum, maximum } = ageLimitInYearsAllowed;
  const yearMinimum = moment().format('YYYY') - minimum;
  const yearMaximum = moment().format('YYYY') - maximum;
  const dateMinimum = `${yearMinimum}-01-01`;
  const dateMaximum = `${yearMaximum}-12-31`;

  const arrayDateOfBirth = date.split('/');
  const formattedDateOfBirth = `${arrayDateOfBirth[2]}-${arrayDateOfBirth[1]}-${
    arrayDateOfBirth[0]
  }`;
  if (!moment(formattedDateOfBirth).isBetween(dateMinimum, dateMaximum)) {
    //usuário com mais de 100 anos ou com menos de 5 anos
    return false;
  }

  return true;
};

export default birthdayIsValid;
