const characterLimitForInstagramUser = 3;
const isUserInstagramValid = (text = null) => {
  // Entrada: texto aleatório
  // Resposta: null ou "usuário do instagram"
  // Limite de caracteres: 3
  // Bloqueos:

  if (!text || text.length < characterLimitForInstagramUser) {
    return null;
  }

  try {
    var res = text.match(
      /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g
    );
    console.log(res);

    if (res) {
      return null;
    } else {
      const formatTextParameter =
        text[0] == '@' ? text.substring(1, text.length) : text;

      return formatTextParameter;
    }
  } catch (_) {
    const formatTextParameter =
      text[0] == '@' ? text.substring(1, text.length) : text;

    return formatTextParameter;
  }
};

export default isUserInstagramValid;
