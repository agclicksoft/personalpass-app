import axios from 'axios'
// import { store } from '../redux/store'
import { BASE_URL } from './baseUrl'

const API = {
  get(path, params = {}, customHeaders = {}) {
    return this.makeRequest(path, params, customHeaders, 'get')
  },

  post(path, params = {}, customHeaders = {}) {
    return this.makeRequest(path, params, customHeaders, 'post')
  },

  put(path, params = {}, customHeaders = {}) {
    return this.makeRequest(path, params, customHeaders, 'put')
  },

  delete(path, params = {}, customHeaders = {}) {
    return this.makeRequest(path, params, customHeaders, 'delete')
  },

  getAuthHeaders() {
    const token = ''
    return {
      Authorization: `Bearer ${token}`,
    }
  },

  makeRequest(path, params, customHeaders, method) {
    const externalRequest = path.includes('https') || path.includes('http')

    let axiosConfig = {
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        Accept: 'application/json',
        'Access-Control-Allow-Origin': '*',
        'X-Client': 'mobile',
        ...customHeaders,
      },
    }

    if (!externalRequest) {
      const authHeaders = this.getAuthHeaders()
      axiosConfig.baseURL = BASE_URL
      axiosConfig.headers = { ...axiosConfig.headers, ...authHeaders }
    }

    const AXIOS_REQUEST = axios.create(axiosConfig)

    switch (method) {
      case 'get':
        return AXIOS_REQUEST.get(path, params)

      case 'post':
        return AXIOS_REQUEST.post(path, params)

      case 'put':
        return AXIOS_REQUEST.put(path, params)

      case 'delete':
        return AXIOS_REQUEST.delete(path, params)

      default:
        return new Promise((resolve, _) => {
          resolve(null)
        })
    }
  },
}

export default API

export const AxiosWithHeaders = headers =>
  axios.create({
    baseURL: BASE_URL,
    headers,
  })
