import React, { useState, useEffect } from 'react';

import Routes from './src/routes';
import { store, persistor } from './src/redux/store';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { PermissionsAndroid, Platform } from 'react-native';
import { RootSiblingParent } from 'react-native-root-siblings';
import { StatusBar } from 'react-native';
import FlashMessage from 'react-native-flash-message';
import Geolocation from 'react-native-geolocation-service';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import localization from 'moment/locale/pt-br';
import 'moment-timezone';

const App: () => React$Node = () => {
  const [hasLocationPermission, setHasLocationPermission] = useState(false);
  const [userPosition, setUserPosition] = useState(false);

  verifyLocationPermission();

  useEffect(() => {
    if (hasLocationPermission) {
      Geolocation.getCurrentPosition(
        position => {
          setUserPosition({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          });
        },
        err => {
          console.log(err.code, err.message);
        },
      );
    }
  }, [hasLocationPermission]);

  async function verifyLocationPermission() {
    try {
      if (Platform.OS === 'android') {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );

        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          setHasLocationPermission(true);
        } else {
          setHasLocationPermission(false);
        }
      } else {
        setHasLocationPermission(true);
      }
    } catch (err) {
      console.log(err);
    }
  }

  Icon.loadFont();
  moment.updateLocale('pt-br', localization);
  moment.tz.setDefault('America/Sao_Paulo');

  return (
    <Provider store={store}>
      <RootSiblingParent>
        <PersistGate persistor={persistor}>
          <Routes />
          <StatusBar hidden={true} />
          <FlashMessage position="top" />
        </PersistGate>
      </RootSiblingParent>
    </Provider>
  );
};

export default App;
